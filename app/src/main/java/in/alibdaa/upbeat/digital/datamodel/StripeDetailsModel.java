package in.alibdaa.upbeat.digital.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hari on 13-12-2018.
 */

public class StripeDetailsModel extends BaseResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("publishable_key")
        @Expose
        private String publishable_key;
        @SerializedName("secret_key")
        @Expose
        private String secret_key;

        public String getPublishable_key() {
            return publishable_key;
        }

        public void setPublishable_key(String publishable_key) {
            this.publishable_key = publishable_key;
        }

        public String getSecret_key() {
            return secret_key;
        }

        public void setSecret_key(String secret_key) {
            this.secret_key = secret_key;
        }

    }
}
