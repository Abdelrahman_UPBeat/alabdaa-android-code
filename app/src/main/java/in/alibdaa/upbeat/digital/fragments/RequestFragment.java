package in.alibdaa.upbeat.digital.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.alibdaa.upbeat.digital.CreateRequestActivity;
import in.alibdaa.upbeat.digital.MainActivity;
import in.alibdaa.upbeat.digital.MyLoginActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.adapters.RequestAdapter;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

public class RequestFragment extends Fragment {

    MainActivity mainActivity;
    Context mContext;

    //View
    @BindView(R.id.tv_no_data)
    public TextView tvNoData;
    @BindView(R.id.rv_requests_list)
    public RecyclerView rvRequestsList;
    @BindView(R.id.fab_request)
    FloatingActionButton fabRequest;
    Unbinder unbinder;

    //Fragment maintain state
    LinearLayoutManager mLayoutManager;
    public boolean isInitiated = false;
    public Bundle savedState;
    private boolean createdStateInDestroyView;

    //Pagination
    public int totalOrders, totalPages;
    public boolean isLoading = false;
    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;

    //Adapter and data
    public RequestAdapter mAdapter;
    public ArrayList<RequestListData.RequestList> myData = new ArrayList<>();
    LanguageModel.Common_used_texts commonUsedTexts = new LanguageModel().new Common_used_texts();

    public void myRequestFragment(MainActivity mainActivity, Context mContext) {
        this.mainActivity = mainActivity;
        this.mContext = mContext;
        commonUsedTexts = mainActivity.commonData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_requests_list, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        rvRequestsList.setLayoutManager(mLayoutManager);
        try {
            fabRequest.setBackgroundTintList(ColorStateList.valueOf(mainActivity.appColor));
        } catch (Exception e) {
        }
        tvNoData.setText(AppUtils.cleanLangStr(getActivity(), commonUsedTexts.getLg7_no_data_were_fo(), R.string.txt_no_data));

        if (rvRequestsList.getAdapter() == null) {
            mAdapter = new RequestAdapter(mainActivity, mContext, new ArrayList<RequestListData.RequestList>(), 0);
            rvRequestsList.setAdapter(mAdapter);
        }
        if (savedState != null) {
            myData = savedState.getParcelableArrayList("REQUEST_LIST");
            if (myData != null && myData.size() > 0) {
                mAdapter.updateRecyclerView(mContext, myData);
                rvRequestsList.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
            } else {
                rvRequestsList.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
            }
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mainActivity.getRequestData(true);
                    }
                }, 1000);

            }
        });

        rvRequestsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManager.getItemCount();
                int displayedPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                if (displayedPosition == (mAdapter.itemsData.size() - 1)) {
                    if (mainActivity instanceof MainActivity) {
                        if (!isLoading && ((MainActivity) mainActivity).requestNextPage > 0) {
                            if (dy > 0) //check for scroll down
                            {
                                visibleItemCount = mLayoutManager.getChildCount();
                                firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                    isLoading = true;
                                    Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                    //((MainActivity) mainActivity).getRequestData(true);
                                    if (mAdapter.mOnLoadMoreListener != null) {
                                        mAdapter.mOnLoadMoreListener.onLoadMore();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        savedState = saveState();
        createdStateInDestroyView = true;
        myData = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (myData == null) {
            outState.putBundle("REQUEST_LIST", savedState);
        } else {
            outState.putBundle("REQUEST_LIST", createdStateInDestroyView ? savedState : saveState());
        }
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelableArrayList("REQUEST_LIST", mAdapter.itemsData);
        return state;
    }

    @OnClick(R.id.fab_request)
    public void onViewClicked() {
        String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

        if(userToken!=null) {
            Intent in = new Intent(getActivity(), CreateRequestActivity.class);
            AppUtils.appStartIntent(getActivity(), in);
        }else{
            showDialog();
        }

    }

    void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getResources().getString(R.string.log_in_first))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.txt_login), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //PreferenceStorage.clearPref();
                        Intent intent = new Intent(getContext(), MyLoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
