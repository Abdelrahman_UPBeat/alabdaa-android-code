package in.alibdaa.upbeat.digital.datamodel;

/**
 * Created by Hari on 15-05-2018.
 */

public class GeneralItem extends ListItem {

    private ChatDetailListData.ChatList chatList;


    public ChatDetailListData.ChatList getChatList() {
        return chatList;
    }

    public void setChatList(ChatDetailListData.ChatList chatList) {
        this.chatList = chatList;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }
}
