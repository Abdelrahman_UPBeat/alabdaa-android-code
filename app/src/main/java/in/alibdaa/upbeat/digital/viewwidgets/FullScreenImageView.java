package in.alibdaa.upbeat.digital.viewwidgets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.R;

public class FullScreenImageView extends AppCompatActivity {

    @BindView(R.id.iv_image_fullscreen)
    TouchImageView ivImageFullscreen;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_imageview);
        ButterKnife.bind(this);

        String imagePath = getIntent().getStringExtra("FilePath");
        Picasso.with(this)
                .load("file://" + new File(imagePath).getAbsolutePath())
                .placeholder(R.drawable.ic_pic_view)
                .error(R.drawable.ic_pic_view)
                .into(ivImageFullscreen);
    }

}
