package in.alibdaa.upbeat.digital;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.adapters.RequestAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class MyRequestListActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.rv_my_request_list)
    RecyclerView rvMyRequestList;

    LinearLayoutManager mLayoutManager;
    int requestNextPage = 1, requestPageNo;
    boolean isLoading = false;
    RequestListData requestListData;
    RequestAdapter mAdapter;
    @BindView(R.id.fab_request_filter)
    FloatingActionButton fabRequestFilter;
    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;

    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();

    String reqStatusType = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_my_request_list);
        ButterKnife.bind(this);
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_my_requests(), R.string.txt_my_requests));
        mLayoutManager = new LinearLayoutManager(this);
        rvMyRequestList.setLayoutManager(mLayoutManager);

        if (rvMyRequestList.getAdapter() == null) {
            mAdapter = new RequestAdapter(this, this, new ArrayList<RequestListData.RequestList>(), 1);
            rvMyRequestList.setAdapter(mAdapter);
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getRequestListData(true);
                    }
                }, 1000);

            }
        });

        rvMyRequestList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManager.getItemCount();
                int displayedPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                if (displayedPosition == (mAdapter.itemsData.size() - 1)) {
                    if (!isLoading && requestNextPage > 1) {
                        if (dy > 0) //check for scroll down
                        {
                            visibleItemCount = mLayoutManager.getChildCount();
                            firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                isLoading = true;
                                Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                if (mAdapter.mOnLoadMoreListener != null) {
                                    mAdapter.mOnLoadMoreListener.onLoadMore();
                                }
                            }
                        }
                    }
                }
            }
        });

        fabRequestFilter.setBackgroundTintList(ColorStateList.valueOf(appColor));


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (mAdapter != null) {
            mAdapter.saveStates(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (mAdapter != null) {
            mAdapter.restoreStates(savedInstanceState);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getRequestListData(false);
    }

    private void getRequestListData(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            if (!isLoadMore) {
                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
                requestNextPage = 1;
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<RequestListData> classificationCall = apiService.getMyRequestListData(requestNextPage, reqStatusType, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.REQUESTLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        switch (responseModel) {
            case AppConstants.REQUESTLIST_DATA:
                if (isLoadMore) {
                    //Remove loading item
                    mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);
                }
                isLoading = false;
                requestListData = (RequestListData) myRes;
                if (requestListData.getData() != null || requestListData.getData().getRequestList() != null && requestListData.getData().getRequestList().size() > 0) {
                    requestPageNo = Integer.parseInt(requestListData.getData().getCurrentPage());
                    requestNextPage = requestListData.getData().getNextPage();
                    if (!isLoadMore || requestPageNo == 1) {
                        mAdapter.itemsData = new ArrayList<>();
                        rvMyRequestList.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    mAdapter.updateRecyclerView(MyRequestListActivity.this, requestListData.getData().getRequestList());
                } else if (isLoadMore && mAdapter.itemsData != null && mAdapter.itemsData.size() > 0) {
                    rvMyRequestList.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    rvMyRequestList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
                break;
            case AppConstants.DELETE_REQUEST_DATA:
                EmptyData resData = (EmptyData) myRes;
                MyRequestListActivity.this.recreate();
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void noDataHandling(Object myRes, boolean isLoadMore, String responseModel) {
        isLoading = false;
        switch (responseModel) {
            case AppConstants.REQUESTLIST_DATA:
                if (isLoadMore && mAdapter != null && mAdapter.itemsData != null && mAdapter.itemsData.size() > 0) {
                    mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);
                } else {
                    rvMyRequestList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(AppUtils.cleanLangStr(this, commonData.getLg7_no_data_were_fo(), R.string.txt_no_data));
                }
                break;
        }
    }


    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            langReqProvData = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
        }
    }

    public void deleteRequest(final String requestId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_request_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(MyRequestListActivity.this)) {
                            ProgressDlg.clearDialog();
                            ProgressDlg.showProgressDialog(MyRequestListActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteRequestData(requestId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(MyRequestListActivity.this, classificationCall, AppConstants.DELETE_REQUEST_DATA, MyRequestListActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(MyRequestListActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @OnClick(R.id.fab_request_filter)
    public void onViewClicked() {
        final CharSequence[] items = {AppUtils.cleanLangStr(this, langReqProvData.getLg6_pending(), R.string.txt_pending),
                AppUtils.cleanLangStr(this, langReqProvData.getLg6_accepted(), R.string.txt_accepted),
                AppUtils.cleanLangStr(this, langReqProvData.getLg6_completed(), R.string.txt_completed),
                AppUtils.cleanLangStr(this, langReqProvData.getLg6_all(), R.string.txt_all)};
        AlertDialog.Builder builder = new AlertDialog.Builder(MyRequestListActivity.this);
        TextView title = new TextView(this);
        title.setText(AppUtils.cleanLangStr(this, "", R.string.txt_filter));//TODO: Lang
        title.setBackgroundColor(Color.WHITE);
        title.setPadding(15, 15, 15, 15);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(20);

        builder.setCustomTitle(title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        reqStatusType = AppConstants.REQ_STATUS_PENDING;
                        break;
                    case 1:
                        reqStatusType = AppConstants.REQ_STATUS_ACCEPTED;
                        break;
                    case 2:
                        reqStatusType = AppConstants.REQ_STATUS_COMPLETED;
                        break;
                    case 3:
                        reqStatusType = AppConstants.REQ_STATUS_ALL;
                        break;
                }
                dialog.dismiss();
                requestNextPage = 1;
                requestPageNo = 1;
                getRequestListData(false);
            }
        });
        builder.show();
    }
}
