package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.ShowMoreCommentsActivity;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.POSTQuestionsCommentsList;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Hari on 05-02-2019.
 */

public class AnswerCommentsAdapter extends RecyclerView.Adapter {


    LinearLayoutManager linearLayoutManager;
    Context mContext;
    private int SELF = 1, LOADING = 2;
    public OnLoadMoreListener mOnLoadMoreListener;
    String question_id = "";
    public List<POSTQuestionsCommentsList.CommentsList> mData = new ArrayList<POSTQuestionsCommentsList.CommentsList>();
    private int lastPosition = -1;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();


    public AnswerCommentsAdapter(Context mContext, List<POSTQuestionsCommentsList.CommentsList> questionCommentList, String question_id, LanguageModel.Common_used_texts commonData) {
        this.mContext = mContext;
        this.mData = questionCommentList;
        this.question_id = question_id;
        this.commonData = commonData;

    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;
        if (viewType == LOADING) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(itemView);
        } else if (viewType == SELF) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_video_comments, parent, false);
            return new AnswerCommentsViewHolder(itemView);
        }
        return null;


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof AnswerCommentsViewHolder) {
            final AnswerCommentsViewHolder holder = (AnswerCommentsViewHolder) viewHolder;
            holder.tvUsername.setText(mData.get(position).getName());
            holder.tvComments.setText(mData.get(position).getComment());
            holder.tvTime.setText(mData.get(position).getDaysAgo());
            holder.tvReply.setText(AppUtils.cleanLangStr(mContext, commonData.getLg7_reply(), R.string.txt_reply));
            holder.tvShowMoreReplies.setText(AppUtils.cleanLangStr(mContext, commonData.getLg7_show_more_repli(), R.string.show_more_replies));
            if (!mData.get(position).getRepliesCount().equalsIgnoreCase("0")) {
                holder.tvShowMoreReplies.setVisibility(View.VISIBLE);
                holder.tvReply.setVisibility(View.GONE);
            }

            Picasso.with(mContext)
                    .load(AppConstants.BASE_URL + mData.get(position).getProfileImage())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.ivUserImg);

            holder.tvReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.CommentList, mData.get(position));
                    Intent callCommentsAct = new Intent(mContext, ShowMoreCommentsActivity.class);
                    callCommentsAct.putExtra(AppConstants.QuestionID, question_id);
                    callCommentsAct.putExtras(bundle);
                    Activity thisActivity = (Activity) mContext;
                    thisActivity.startActivityForResult(callCommentsAct, 100);
//                    thisActivity.overridePendingTransition(R.anim.slide_up, 0);
                }
            });

            holder.tvShowMoreReplies.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.CommentList, mData.get(position));
                    Intent callCommentsAct = new Intent(mContext, ShowMoreCommentsActivity.class);
                    callCommentsAct.putExtra(AppConstants.QuestionID, question_id);
                    callCommentsAct.putExtra(AppConstants.isShowMore, "1");
                    callCommentsAct.putExtras(bundle);
                    mContext.startActivity(callCommentsAct);
                    Activity thisActivity = (Activity) mContext;
//                    thisActivity.overridePendingTransition(R.anim.slide_up, 0);
                }
            });

            setAnimation(viewHolder.itemView, position);

        }

    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition) {
//            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.up_to_down);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? android.R.anim.slide_in_left
                        : android.R.anim.slide_in_left);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position) == null ? LOADING : SELF;
    }

    public void updateRecyclerView(Context mContext, List<POSTQuestionsCommentsList.CommentsList> itemsData) {
        this.mContext = mContext;
        if (itemsData.size() > 0) {
            this.mData.addAll(itemsData);
            this.notifyDataSetChanged();
        } else {
            this.mData.clear();
            this.notifyDataSetChanged();
        }

    }


    public class AnswerCommentsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_user_img)
        CircleImageView ivUserImg;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_comments)
        TextView tvComments;
        @BindView(R.id.tv_reply)
        TextView tvReply;
        @BindView(R.id.tv_show_more_replies)
        TextView tvShowMoreReplies;

        public AnswerCommentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
