package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.CategoryList;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.viewwidgets.ViewBinderHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogEditCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mActivity;
    Context mContext;
    public ArrayList<ProviderListData.ProviderList> itemsData = new ArrayList<>();
    int viewType;
    LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    public OnLoadMoreListener mOnLoadMoreListener;
    List<CategoryList.Category_list> category_list;
    public String cat_id = "", subCatID = "";
    AlertDialog dialog;
    EditText tvCategory, etSubCategory;
    public boolean isAvaialble = true;
    Button btnDone;

    private int SELF = 1, LOADING = 2;


    public DialogEditCategoryListAdapter(Context mContext, List<CategoryList.Category_list> category_list, AlertDialog dialog, EditText etCategory, EditText etSubCategory, String cat_ID, String subCatID, Button btnDone) {
        this.mContext = mContext;
        this.dialog = dialog;
        this.tvCategory = etCategory;
        this.category_list = category_list;
        this.etSubCategory = etSubCategory;
        this.cat_id = cat_ID;
        this.btnDone = btnDone;
    }

    public DialogEditCategoryListAdapter(Context context) {
        this.mContext = mContext;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View itemView;
        itemView = LayoutInflater.from(mContext).inflate(R.layout.dlg_adapter_categories_list, parent, false);
        return new CategoryViewHolder(itemView);

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        CategoryViewHolder categoryViewHolder = (CategoryViewHolder) viewHolder;
        categoryViewHolder.tvCatName.setText(category_list.get(position).getCategory_name());

        categoryViewHolder.setIsRecyclable(false);


        if (category_list.get(position).isIs_checked()) {
            categoryViewHolder.tvCatName.setChecked(true);
        }


//        for (int i1 = 0; i1 < values.length; i1++) {
//            if (category_list.get(position).getCatrgory_id().equalsIgnoreCase(values[i1])) {
//                categoryViewHolder.tvCatName.setChecked(true);
//            }
//        }

        Picasso.with(mContext)
                .load(AppConstants.BASE_URL + category_list.get(position).getCategory_image())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(categoryViewHolder.ivCatImg);

        categoryViewHolder.tvCatName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                category_list.get(position).setIs_checked(isChecked);
            }
        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String catName = "";
                cat_id = "";
                for (int i = 0; i < category_list.size(); i++) {
                    if (category_list.get(i).isIs_checked()) {
                        catName = catName + category_list.get(i).getCategory_name() + ",";

                        cat_id = cat_id + category_list.get(i).getCatrgory_id() + ",";
                    }
                }
                if (!cat_id.isEmpty()) {
                    cat_id = cat_id.substring(0, cat_id.length() - 1);
                    tvCategory.setText(catName.substring(0, catName.length() - 1));
                    etSubCategory.setText("");
                    dialog.dismiss();
//                    if (category_list.get(position).getIs_subcategory().equalsIgnoreCase("1")) {
//                        isAvaialble = true;
//                    } else {
//                        isAvaialble = false;
//                    }
                } else {
                    Toast.makeText(mContext, "Select any one category to proceed", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    public void updateRecyclerView(Context mContext, ArrayList<ProviderListData.ProviderList> itemsData) {
        this.mContext = mContext;
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return category_list.size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_cat_img)
        ImageView ivCatImg;
        @BindView(R.id.tv_cat_name)
        CheckBox tvCatName;

        public CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }
    }
}
