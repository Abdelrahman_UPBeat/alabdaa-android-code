package in.alibdaa.upbeat.digital.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.google.gson.Gson;

import java.util.Calendar;

import in.alibdaa.upbeat.digital.datamodel.ColorSettingModel;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

public class AlarmIntervalReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String colorList = PreferenceStorage.getKey(AppConstants.COLOR_LIST);
            ColorSettingModel.Data colorData = new Gson().fromJson(colorList, ColorSettingModel.Data.class);
            int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            String appColor = colorData.getMorning();
            if (hour >= 20 || hour < 6) {
                appColor = colorData.getNight();
            } else if (hour >= 6 && hour < 12) {
                appColor = colorData.getMorning();
            } else if (hour >= 12 && hour < 17) {
                appColor = colorData.getAfternoon();
            } else if (hour >= 17 && hour < 20) {
                appColor = colorData.getEvening();
            }
            PreferenceStorage.setKey(AppConstants.APP_THEME, appColor);
        } catch (Exception e) {

        }
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

}
