package in.alibdaa.upbeat.digital;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import in.alibdaa.upbeat.digital.datamodel.BaseResponse;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class ProviderDetailActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.tv_provider_name)
    TextView tvProviderName;
    @BindView(R.id.tv_prov_email)
    TextView tvProvEmail;
    @BindView(R.id.tv_prov_contact)
    TextView tvProvContact;
    @BindView(R.id.tv_prov_title)
    TextView tvProvTitle;
    @BindView(R.id.ll_prov_desc_detail)
    LinearLayout llProvDescDetail;
    @BindView(R.id.tv_prov_location)
    TextView tvProvLocation;
    @BindView(R.id.ll_prov_avail)
    LinearLayout llProvAvail;

    ArrayList<String> descDataList = new ArrayList<>();
    ProviderListData.ProviderList providerDataDetail;
    @BindView(R.id.iv_user_img)
    ImageView ivUserImg;

    int viewType;

    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    @BindView(R.id.tv_chat)
    TextView tvChat;
    @BindView(R.id.ll_prov_chat)
    LinearLayout llProvChat;
    @BindView(R.id.tv_call)
    TextView tvCall;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    @BindView(R.id.ll_footer)
    LinearLayout llFooter;
    @BindView(R.id.tv_txt_date_range)
    TextView tvTxtDateRange;
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.tv_edit)
    TextView tvEdit;
    @BindView(R.id.ll_prov_edit)
    LinearLayout llProvEdit;
    @BindView(R.id.tv_delete)
    TextView tvDelete;
    @BindView(R.id.ll_delete)
    LinearLayout llDelete;
    @BindView(R.id.ll_user_footer)
    LinearLayout llUserFooter;
    @BindView(R.id.tv_show_direction)
    TextView tvShowDirection;
    @BindView(R.id.btn_book)
    Button btnBook;
    @BindView(R.id.tv_views)
    TextView tvViews;
    @BindView(R.id.tv_comments)
    TextView tvComments;
    @BindView(R.id.tv_reviews)
    TextView tvReviews;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_provider_details);
        ButterKnife.bind(this);

        getLocaleData();
        providerDataDetail = getIntent().getParcelableExtra("ProviderData");
        viewType = getIntent().getIntExtra("ViewType", 0);

        llCall.setBackgroundColor(appColor);
        tvComments.setBackgroundColor(appColor);
        tvShowDirection.setTextColor(appColor);
        btnBook.setBackgroundColor(appColor);
        if (providerDataDetail != null) {
            tvProviderName.setText(providerDataDetail.getUsername());
            tvProvEmail.setText(providerDataDetail.getEmail());
            tvProvContact.setText(providerDataDetail.getContactNumber());
            if (!providerDataDetail.getViews().isEmpty()) {
                tvViews.setText(providerDataDetail.getViews() + " views");
            } else {
                tvViews.setVisibility(View.INVISIBLE);
            }

            tvProvTitle.setText(providerDataDetail.getTitle());
            setToolBarTitle(providerDataDetail.getTitle());
            if (providerDataDetail.getDescriptionDetails() != null) {
                try {
                    JSONArray descList = new JSONArray(providerDataDetail.getDescriptionDetails());
                    for (int i = 0; i < descList.length(); i++) {
                        addDescView(i, descList.getString(i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                tvProvLocation.setText(providerDataDetail.getLocation());
            } catch (Exception e) {
                e.printStackTrace();
            }
            int requesterID = Integer.parseInt(providerDataDetail.getProviderId());
            int userId = PreferenceStorage.getIntKey(AppConstants.USER_ID);

//            if (viewType == 1) {
//                llUserFooter.setVisibility(View.VISIBLE);
//            } else {
//                llUserFooter.setVisibility(View.GONE);
//            }

            if (viewType == 1 || requesterID == userId) {
                llFooter.setVisibility(View.GONE);
                btnBook.setVisibility(View.GONE);
            } else {
                llFooter.setVisibility(View.VISIBLE);
                if ((PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE)) > 0) {
                    tvProvEmail.setVisibility(View.VISIBLE);
                    tvProvContact.setVisibility(View.VISIBLE);
                    llCall.setVisibility(View.VISIBLE);
                } else if (viewType != 1 && requesterID != userId) {
                    tvProvEmail.setVisibility(View.INVISIBLE);
                    tvProvContact.setVisibility(View.INVISIBLE);
                    llCall.setVisibility(View.GONE);
                }
            }
            String ivPic = AppConstants.BASE_URL + providerDataDetail.getProfileImg();
            Picasso.with(this)
                    .load(ivPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(ivUserImg);

            llCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", providerDataDetail.getProfileContactNo(), null));
                        startActivity(intent);
                    } catch (Exception e) {
                        if (e instanceof ActivityNotFoundException) {
                            AppUtils.showToast(ProviderDetailActivity.this, "Dialing not supported");//TODO:
                        }
                    }
                }
            });

            try {
                String fromDate = AppUtils.formatDateToApp(providerDataDetail.getStartDate());
                tvStartDate.setText(fromDate);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String endDate = AppUtils.formatDateToApp(providerDataDetail.getEndDate());
                tvEndDate.setText(endDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        postViews();

    }

    private synchronized void setDayText(TextView tvTxtDesc, String dayVal) {
        switch (dayVal) {
            case "1":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_sunday(), R.string.txt_sunday));
                break;
            case "2":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_monday(), R.string.txt_monday));
                break;
            case "3":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_tuesday(), R.string.txt_tuesday));
                break;
            case "4":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_wednesday(), R.string.txt_wednesday));
                break;
            case "5":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_thursday(), R.string.txt_thursday));
                break;
            case "6":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_friday(), R.string.txt_friday));
                break;
            case "7":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_saturday(), R.string.txt_saturday));
                break;
        }
    }

    private synchronized void addDescView(int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View inflatedLayout = inflater.inflate(R.layout.layout_bullet_textview, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setText(descVal);
        llProvDescDetail.addView(inflatedLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        tvTxtDateRange.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_date(), R.string.txt_date));
        tvShowDirection.setText(AppUtils.cleanLangStr(this, commonData.getLg7_show_directions(), R.string.show_directions));
        tvReviews.setText(AppUtils.cleanLangStr(this, commonData.getLg7_view_reviews(), R.string.txt_view_reviews));
        tvComments.setText(AppUtils.cleanLangStr(this, commonData.getLg7_view_comments(), R.string.txt_view_comments));
        btnBook.setText(AppUtils.cleanLangStr(this, commonData.getLg7_book(), R.string.txt_book));
        tvCall.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_call(), R.string.txt_call));
        tvChat.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_chat(), R.string.txt_chat));
        SparseArray<Integer> availDays = new SparseArray<>();
        for (int i = 1; i <= 7; i++) {
            availDays.put(i, 0);
        }
        llProvAvail.removeAllViews();
        if (providerDataDetail.getAvailability() != null && !providerDataDetail.getAvailability().isEmpty()) {
            try {
                JSONArray av1 = new JSONArray(providerDataDetail.getAvailability());
                for (int i = 0; i < av1.length(); i++) {
                    LayoutInflater inflater = LayoutInflater.from(this);
                    View inflatedLayout = inflater.inflate(R.layout.layout_provider_avail_item, null, false);
                    TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_prov_day_text);
                    TextView tvTxtAvailVal = (TextView) inflatedLayout.findViewById(R.id.tv_prov_day_val);
                    JSONObject av2 = av1.getJSONObject(i);
                    availDays.put(Integer.parseInt(av2.getString("day")), 1);
                    String dayVal = av2.getString("day");
                    setDayText(tvTxtDesc, dayVal);
                    tvTxtAvailVal.setText(av2.getString("from_time") + " - " + av2.getString("to_time"));
                    llProvAvail.addView(inflatedLayout);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (int i = 0; i < availDays.size(); i++) {
                if (availDays.valueAt(i) == 0) {
                    LayoutInflater inflater = LayoutInflater.from(this);
                    View inflatedLayout = inflater.inflate(R.layout.layout_provider_avail_item, null, false);
                    TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_prov_day_text);
                    TextView tvTxtAvailVal = (TextView) inflatedLayout.findViewById(R.id.tv_prov_day_val);
                    String dayVal = String.valueOf(availDays.keyAt(i));
                    setDayText(tvTxtDesc, dayVal);
                    tvTxtAvailVal.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_closed(), R.string.txt_closed));
                    llProvAvail.addView(inflatedLayout);
                }
            }
        }


    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            langReqProvData = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
        }
    }

    @OnClick({R.id.ll_prov_edit, R.id.ll_delete, R.id.tv_show_direction, R.id.ll_prov_chat, R.id.btn_book, R.id.tv_comments, R.id.tv_reviews})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_prov_edit:
                Intent detailPage = new Intent(this, EditProviderActivity.class);
                detailPage.putExtra("ViewType", viewType);
                detailPage.putExtra("ProviderData", providerDataDetail);
                AppUtils.appStartIntent(this, detailPage);
                ProviderDetailActivity.this.finish();
                break;
            case R.id.ll_delete:
                deleteProvider(providerDataDetail.getPId());
                break;
            case R.id.tv_show_direction:
                Intent callDirectionAct = new Intent(ProviderDetailActivity.this, MapsGetDirections.class);
                callDirectionAct.putExtra(AppConstants.LATITUDE, providerDataDetail.getLatitude());
                callDirectionAct.putExtra(AppConstants.LONGITUDE, providerDataDetail.getLongitude());
                startActivity(callDirectionAct);
                break;
            case R.id.ll_prov_chat:
                String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
                if(userToken!=null) {
                    if ((PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE)) > 0) {
                        Intent callChatDetailAct = new Intent(this, ChatDetailActivity.class);
                        callChatDetailAct.putExtra(AppConstants.chatFrom, providerDataDetail.getProviderId());
                        callChatDetailAct.putExtra(AppConstants.chatUsername, providerDataDetail.getUsername());
                        callChatDetailAct.putExtra(AppConstants.chatImg, providerDataDetail.getProfileImg());
                        AppUtils.appStartIntent(this, callChatDetailAct);
                    } else {
                        Intent subsIntent = new Intent(this, GoToSubscriptionActivity.class);
                        subsIntent.putExtra(AppConstants.chatFrom, providerDataDetail.getProviderId());
                        subsIntent.putExtra(AppConstants.chatUsername, providerDataDetail.getUsername());
                        subsIntent.putExtra(AppConstants.chatImg, providerDataDetail.getProfileImg());
                        subsIntent.putExtra("FromPage", AppConstants.PAGE_CHAT);
                    }
                }else{
                    showDialog();

                }
                break;
            case R.id.btn_book:

                 userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

                if(userToken!=null) {
                    Intent callBookingAct = new Intent(this, ProviderBookAppointment.class);
                    callBookingAct.putExtra(AppConstants.PID, providerDataDetail.getPId());
                    startActivity(callBookingAct);
                }else{

                    showDialog();
                }
                break;

            case R.id.tv_comments:
                 userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

                if(userToken!=null) {
                    Intent callCommentAct = new Intent(this, QuestionsCommentsActivity.class);
                    callCommentAct.putExtra(AppConstants.QuestionID, providerDataDetail.getPId());
                    startActivity(callCommentAct);
                }else{
                    showDialog();

                }
                break;
            case R.id.tv_reviews:
                userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

                if(userToken!=null) {
                    Intent callProviderReviewListAct = new Intent(this, ProviderReviewListActivity.class);
                    callProviderReviewListAct.putExtra(AppConstants.CatID, providerDataDetail.getPId());
                    startActivity(callProviderReviewListAct);
                }else{
                    showDialog();
                }
                break;
        }
    }

    public void deleteProvider(final String serviceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_provider_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(ProviderDetailActivity.this)) {
                            ProgressDlg.clearDialog();
                            ProgressDlg.showProgressDialog(ProviderDetailActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteProviderData(serviceId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(ProviderDetailActivity.this, classificationCall, AppConstants.DELETE_PROVIDER_DATA, ProviderDetailActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(ProviderDetailActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {
        switch (responseType) {
            case AppConstants.DELETE_PROVIDER_DATA:
                ProviderDetailActivity.this.finish();
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    public void postViews() {
        if (AppUtils.isNetworkAvailable(this)) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BaseResponse> classificationCall = apiService.postViews(PreferenceStorage.getKey(AppConstants.USER_TOKEN), providerDataDetail.getPId(), String.valueOf(PreferenceStorage.getIntKey(AppConstants.USER_ID)));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.VIEWS, ProviderDetailActivity.this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProviderDetailActivity.this);
        builder.setMessage(getResources().getString(R.string.log_in_first))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.txt_login), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //PreferenceStorage.clearPref();
                        Intent intent = new Intent(ProviderDetailActivity.this, MyLoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


}
