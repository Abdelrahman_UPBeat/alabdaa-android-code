package in.alibdaa.upbeat.digital;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.fragments.DatePickerFragment;
import in.alibdaa.upbeat.digital.interfaces.OnSetMyLocation;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.InputFilterMinMax;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class EditRequestActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler, OnSetMyLocation {

    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.ll_desc_parent)
    LinearLayout llDescParent;
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.et_date)
    EditText etDate;
    @BindView(R.id.et_time)
    EditText etTime;
    @BindView(R.id.et_proposed_fee)
    EditText etProposedFee;
    @BindView(R.id.et_contact_no)
    EditText etContactNo;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    int descCount = 0;

    ArrayList<String> descDataList = new ArrayList<>();
    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    @BindView(R.id.tv_txt_title)
    TextView tvTxtTitle;
    @BindView(R.id.tv_txt_location)
    TextView tvTxtLocation;
    @BindView(R.id.tv_txt_date)
    TextView tvTxtDate;
    @BindView(R.id.tv_txt_time)
    TextView tvTxtTime;
    @BindView(R.id.tv_txt_prop_fee)
    TextView tvTxtPropFee;
    @BindView(R.id.tv_txt_cont)
    TextView tvTxtCont;

    RequestListData.RequestList requestDataDetail;
    int viewType = 0;

    String latitude, longitude, address;
    public static Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_create_request);
        ButterKnife.bind(this);
        mContext = this;

        /*addDescView(false);
        removeLocationData();*/
        //etTitle.addTextChangedListener(new updateRequestTextWatcher(etTitle));
        etLocation.addTextChangedListener(new updateRequestTextWatcher(etLocation));
        etDate.addTextChangedListener(new updateRequestTextWatcher(etDate));
        etTime.addTextChangedListener(new updateRequestTextWatcher(etTime));
        etProposedFee.addTextChangedListener(new updateRequestTextWatcher(etProposedFee));
        etContactNo.addTextChangedListener(new updateRequestTextWatcher(etContactNo));


        requestDataDetail = getIntent().getParcelableExtra("RequestData");
        viewType = getIntent().getIntExtra("ViewType", 0);

        if (requestDataDetail != null) {

            etTitle.setText(requestDataDetail.getTitle());
            setToolBarTitle(requestDataDetail.getTitle());
            etTitle.setEnabled(false);
            etTitle.setFocusable(false);
            if (requestDataDetail.getDescription() != null) {
                try {
                    JSONArray descList = new JSONArray(requestDataDetail.getDescription());
                    for (int i = 0; i < (descList.length()); i++) {
                        addDescView(true, descList.getString(i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                /*AppUtils.getAddressFromLocation(Double.parseDouble(requestDataDetail.getLatitude()), Double.parseDouble(requestDataDetail.getLongitude()),
                        getApplicationContext(), new GeocoderHandler());*/
                etLocation.setText(requestDataDetail.getLocation());
                latitude = requestDataDetail.getLatitude();
                longitude = requestDataDetail.getLongitude();
                address = requestDataDetail.getLocation();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String date = AppUtils.formatDateToApp(requestDataDetail.getRequestDate());
            etDate.setText(date);
            etTime.setText(requestDataDetail.getRequestTime());
            etProposedFee.setText(requestDataDetail.getAmount());
            etContactNo.setText(requestDataDetail.getProfileContactNo());
        }

        btnSubmit.setText(AppUtils.cleanLangStr(this, "", R.string.txt_update));//TODO : lang
        etProposedFee.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100000000")});
    }

    private synchronized void addDescView(boolean isNewData, String... descVal) {
        descCount++;
        if (descCount > 1) {
            for (int i = 0; i < llDescParent.getChildCount(); i++) {
                ((LinearLayout) llDescParent.getChildAt(i)).findViewById(R.id.btn_desc_add).setVisibility(View.GONE);
                ((LinearLayout) llDescParent.getChildAt(i)).findViewById(R.id.btn_desc_remove).setVisibility(View.VISIBLE);
            }
        }
        LayoutInflater inflater = LayoutInflater.from(this);
        View inflatedLayout = inflater.inflate(R.layout.layout_request_desc, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_txt_desc);
        tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_desc(), R.string.txt_desc_pt) + " " + descCount);
        final EditText tvDesc = (EditText) inflatedLayout.findViewById(R.id.et_decs);
        if (isNewData && descVal != null && descVal.length > 0) {
            tvDesc.setText(descVal[0]);
        }
        Button btnDescAdd = (Button) inflatedLayout.findViewById(R.id.btn_desc_add);
        btnDescAdd.setTag("Add_" + descCount);
        btnDescAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvDesc != null && tvDesc.getText().toString() != null
                        && !tvDesc.getText().toString().isEmpty())
                    addDescView(false);
                else
                    AppUtils.showToast(EditRequestActivity.this, getResources().getString(R.string.err_txt_desc_empty));//TODO : lang
            }
        });

        Button btnDescRemove = (Button) inflatedLayout.findViewById(R.id.btn_desc_remove);
        btnDescRemove.setTag("Remove_" + descCount);
        llDescParent.addView(inflatedLayout);
        for (int i = 0; i < llDescParent.getChildCount(); i++) {
            Button btnAdd = (Button) (((LinearLayout) llDescParent.getChildAt(i)).findViewById(R.id.btn_desc_add));
            Button btnRemove = (Button) (((LinearLayout) llDescParent.getChildAt(i)).findViewById(R.id.btn_desc_remove));
            if (i == llDescParent.getChildCount() - 1) {
                EditText etDesc = (EditText) (((LinearLayout) llDescParent.getChildAt(i)).findViewById(R.id.et_decs));
                etDesc.requestFocus();
            }
            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeDescView(v.getTag().toString());
                }
            });
        }
    }

    private void removeDescView(String tag) {
        int index = Integer.parseInt(tag.split("_")[1]);
        llDescParent.removeViewAt(index - 1);
        getDescData();
        llDescParent.removeAllViews();
        descCount = 0;
        for (int i = 0; i < descDataList.size(); i++) {
            addDescView(true, descDataList.get(i));
        }
    }

    private void getDescData() {
        descDataList.clear();
        for (int i = 0; i < llDescParent.getChildCount(); i++) {
            EditText descVal = (EditText) (((LinearLayout) llDescParent.getChildAt(i)).findViewById(R.id.et_decs));
            if (!descVal.getText().toString().trim().isEmpty())
                descDataList.add(descVal.getText().toString().trim());
        }
    }

    @OnClick({R.id.et_location, R.id.et_date, R.id.et_time, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_location:
                Intent callMapActivity = new Intent(EditRequestActivity.this, MapActivity.class);
                callMapActivity.putExtra("From", AppConstants.PAGE_EDIT_REQUEST);
                callMapActivity.putExtra("Latitude", latitude);
                callMapActivity.putExtra("Longitude", longitude);
                callMapActivity.putExtra("Address", address);
                AppUtils.appStartIntent(EditRequestActivity.this, callMapActivity);
                break;
            case R.id.et_date:
                String datelimit = "0", setDate = "0";
                try {
                    setDate = etDate.getTag().toString();
                } catch (Exception e) {
                }
                DatePickerFragment newFragment = new DatePickerFragment(EditRequestActivity.this, etDate, datelimit, true, setDate, true);
                newFragment.show(getFragmentManager(), getString(R.string.txt_date));
                break;
            case R.id.et_time:
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE) + 20;
                new TimePickerDialog(EditRequestActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM;
                        if (selectedHour < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        selectedHour = selectedHour > 12 ? selectedHour - 12 : selectedHour;
                        String hr = selectedHour < 10 ? "0" + selectedHour : "" + selectedHour;
                        String min = selectedMinute < 10 ? "0" + selectedMinute : "" + selectedMinute;
                        String totTime = hr + ":" + min + " " + AM_PM;
                        etTime.setText(totTime);
                    }
                }, hour, minute, false).show();

                break;
            case R.id.btn_submit:
                postRequestData();
                break;
        }
    }

    @Override
    public void onLocationSet(String latitude, String longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        etLocation.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
        etLocation.setText(address);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getLocaleData();

            //setToolBarTitle(AppUtils.cleanLangStr(this, langReqProvData.getLg6_request(), R.string.txt_request));
            tvTxtTitle.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_title(), R.string.txt_title));
            tvTxtLocation.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_location(), R.string.txt_location));
            tvTxtDate.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_date(), R.string.txt_date));
            tvTxtTime.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_time(), R.string.txt_time));
            tvTxtPropFee.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_proposed_fee(), R.string.txt_props_fee));
            tvTxtCont.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_contact_number(), R.string.txt_cont_num));
            btnSubmit.setText(AppUtils.cleanLangStr(this, "", R.string.txt_update));//TODO : lang
            btnSubmit.setBackgroundColor(appColor);

        } catch (Exception e) {
        }
    }

    private void postRequestData() {
        if (AppUtils.isNetworkAvailable(this)) {
            String title = etTitle.getText().toString();
            String date = etDate.getText().toString();
            String time = etTime.getText().toString();
            String fee = etProposedFee.getText().toString();
            String contactNo = etContactNo.getText().toString();
            String locAddr = etLocation.getText().toString();
            String lat = latitude;
            String lng = longitude;
            getDescData();
            if (/*validateData(etTitle, title, AppUtils.cleanLangStr(this, langReqProvData.getLg6_title_cannot_be(), R.string.err_txt_title))
                    && */validateData(etLocation, etLocation.getText().toString(), AppUtils.cleanLangStr(this, langReqProvData.getLg6_location_addres(), R.string.err_txt_addr))
                    && validateData(etDate, etDate.getText().toString(), AppUtils.cleanLangStr(this, langReqProvData.getLg6_date_cannot_be_(), R.string.err_txt_date))
                    && validateData(etTime, etTime.getText().toString(), AppUtils.cleanLangStr(this, langReqProvData.getLg6_time_cannot_be_(), R.string.err_txt_time))
                    && validateData(etProposedFee, etProposedFee.getText().toString(), AppUtils.cleanLangStr(this, langReqProvData.getLg6_proposed_fee_ca(), R.string.err_txt_fee))
                    && validateData(null, lat, AppUtils.cleanLangStr(this, langReqProvData.getLg6_error_in_locati(), R.string.err_txt_latlng))
                    && validateData(null, lng, AppUtils.cleanLangStr(this, langReqProvData.getLg6_error_in_locati(), R.string.err_txt_latlng))
                    && validateData(etContactNo, etContactNo.getText().toString(), AppUtils.cleanLangStr(this, langReqProvData.getLg6_contact_number_(), R.string.err_txt_contact_no))) {

                if (descDataList == null || descDataList.size() == 0) {
                    AppUtils.showToast(EditRequestActivity.this, AppUtils.cleanLangStr(this, langReqProvData.getLg6_please_enter_at(), R.string.err_txt_desc_atleast_1));
                    return;
                }

                //Convert date to server format
                date = AppUtils.formatDateToServer(etDate.getText().toString());

                JSONArray descArr = new JSONArray(descDataList);
                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);
                Call<EmptyData> classificationCall = apiService.updateRequestData(requestDataDetail.getRId(), title, descArr.toString(), locAddr, date, time, fee, contactNo, lat, lng, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(EditRequestActivity.this, classificationCall, AppConstants.UPDATE_REQUEST_DATA, this, false);
            }
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    private boolean validateData(EditText etTxt, String etVal, String msg) {
        String msg2 = "";
        if (etTxt != null) {
            /*if (etTxt.getId() == R.id.et_title) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_title_cannot_be(), R.string.err_txt_title);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            } else*/ if (etTxt.getId() == R.id.et_location) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_location_addres(), R.string.err_txt_addr);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            } else if (etTxt.getId() == R.id.et_date) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_date_cannot_be_(), R.string.err_txt_date);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            } else if (etTxt.getId() == R.id.et_time) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_time_cannot_be_(), R.string.err_txt_time);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_proposed_fee) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_proposed_fee_ca(), R.string.err_txt_fee);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_contact_no) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_contact_number_(), R.string.err_txt_contact_no);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else if (etTxt.getText().toString().length() < 10 || etTxt.getText().toString().length() > 15) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_contact_number_(), R.string.err_txt_contact_no);//TODO:
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditRequestActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else {
                msg2 = msg;
                AppUtils.showToast(EditRequestActivity.this, msg2);
                return false;
            }
        }


        return true;
    }


    private class updateRequestTextWatcher implements TextWatcher {
        private View view;

        private updateRequestTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (view.getId() == R.id.et_title) {
                if (!etTitle.getText().toString().isEmpty()) {
                    etTitle.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                    validateData(etTitle, etTitle.getText().toString(), AppUtils.cleanLangStr(EditRequestActivity.this, langReqProvData.getLg6_title_cannot_be(), R.string.err_txt_title));
                }
            } else if (view.getId() == R.id.et_location) {
                if (etLocation.getText().toString().isEmpty()) {
                    etLocation.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                    validateData(etLocation, etLocation.getText().toString(), AppUtils.cleanLangStr(EditRequestActivity.this, langReqProvData.getLg6_location_addres(), R.string.err_txt_addr));
                }
            } else if (view.getId() == R.id.et_date) {
                if (!etDate.getText().toString().isEmpty()) {
                    etDate.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                    validateData(etDate, etDate.getText().toString(), AppUtils.cleanLangStr(EditRequestActivity.this, langReqProvData.getLg6_date_cannot_be_(), R.string.err_txt_date));
                }
            } else if (view.getId() == R.id.et_time) {
                if (!etTime.getText().toString().isEmpty()) {
                    etTime.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                    validateData(etTime, etTime.getText().toString(), AppUtils.cleanLangStr(EditRequestActivity.this, langReqProvData.getLg6_time_cannot_be_(), R.string.err_txt_time));
                }
            } else if (view.getId() == R.id.et_proposed_fee) {
                if (!etProposedFee.getText().toString().isEmpty()) {
                    etProposedFee.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                    validateData(etProposedFee, etProposedFee.getText().toString(), AppUtils.cleanLangStr(EditRequestActivity.this, langReqProvData.getLg6_proposed_fee_ca(), R.string.err_txt_fee));
                }
            } else if (view.getId() == R.id.et_contact_no) {
                if (!etContactNo.getText().toString().isEmpty()) {
                    etContactNo.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                    validateData(etContactNo, etContactNo.getText().toString(), AppUtils.cleanLangStr(EditRequestActivity.this, langReqProvData.getLg6_contact_number_(), R.string.err_txt_contact_no));
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        EmptyData emptyData = (EmptyData) myRes;
        AppUtils.showToast(EditRequestActivity.this, emptyData.getResponseHeader().getResponseMessage());
        Intent nextIntent = null;
        switch (viewType) {
            case 0:
                nextIntent = new Intent(EditRequestActivity.this, MainActivity.class);
                nextIntent.putExtra("MyFragment", 0);
                break;
            case 1:
                nextIntent = new Intent(EditRequestActivity.this, MyRequestListActivity.class);
                break;
        }
        AppUtils.appStartIntent(EditRequestActivity.this, nextIntent);
        EditRequestActivity.this.finish();
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            langReqProvData = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
        }
    }
}
