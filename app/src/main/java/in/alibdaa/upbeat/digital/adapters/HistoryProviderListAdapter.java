package in.alibdaa.upbeat.digital.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.alibdaa.upbeat.digital.HistoryProviderDetailActivity;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import in.alibdaa.upbeat.digital.MapsGetDirections;
import in.alibdaa.upbeat.digital.ProviderHistoryActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderHistoryModel;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import in.alibdaa.upbeat.digital.viewwidgets.SwipeRevealLayout;

public class HistoryProviderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ProviderHistoryActivity mActivity;
    Context mContext;
    public List<ProviderHistoryModel.Booking_list> itemsData = new ArrayList<>();
    public int viewType;
    LanguageModel.Request_and_provider_list request_and_provider_list = new LanguageModel().new Request_and_provider_list();

    public OnLoadMoreListener mOnLoadMoreListener;
    private int SELF = 1, LOADING = 2;
    LanguageModel.Common_used_texts commonUsedTexts;

    // viewType - 1 - Pending
    // viewType - 2 - Completed
    public HistoryProviderListAdapter(ProviderHistoryActivity mActivity, Context mContext, List<ProviderHistoryModel.Booking_list> itemsData, int viewType, LanguageModel.Common_used_texts commonUsedTexts) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        request_and_provider_list = mActivity.requestAndProviderList;
        this.commonUsedTexts = commonUsedTexts;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View itemView;
        if (viewType == LOADING) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_provider_history, parent, false);
            return new HistoryViewHolder(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (itemsData.get(position) == null) {
            return LOADING;
        } else {
            return SELF;
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (viewHolder instanceof HistoryViewHolder) {
            HistoryViewHolder historyViewHolder = (HistoryViewHolder) viewHolder;
            historyViewHolder.tvRequestTitle.setText(AppUtils.cleanString(mContext, itemsData.get(position).getTitle()));

            historyViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
            historyViewHolder.tvRequestDesc1.setText(itemsData.get(position).getNotes());

            if (itemsData.get(position).getService_status() != null) {
                if (itemsData.get(position).getService_status().equalsIgnoreCase("1")) {
                    historyViewHolder.tvRequestStatus.setText("Pending");
                } else {
                    historyViewHolder.tvRequestStatus.setText("Completed");
                }
            }
            String profPic = AppConstants.BASE_URL + itemsData.get(position).getProfile_img();

            Picasso.with(mContext)
                    .load(profPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(historyViewHolder.ivUserimg);
            historyViewHolder.tvAppntDate.setText(AppUtils.cleanString(mContext, itemsData.get(position).getService_date()));
            historyViewHolder.tvAppntTime.setText(AppUtils.cleanString(mContext, itemsData.get(position).getService_time()));
            historyViewHolder.tvAppntFee.setText(AppUtils.cleanLangStr(mContext, commonUsedTexts.getLg7_show_directions(), R.string.show_directions));
//            historyViewHolder.tvAppntFee.setText(AppUtils.cleanString(mContext, itemsData.get(position).getCurrencyCode() + " " + itemsData.get(position).getAmount()));

            historyViewHolder.swipeLayout.setLockDrag(true);

//            try {
//                historyViewHolder.tvTxtAppnt.setText(AppUtils.cleanLangStr(mContext, request_and_provider_list.getLg6_appointment(), R.string.txt_appnt));
//            } catch (Exception e) {
//            }
            historyViewHolder.tvAppntFee.setTextColor(mActivity.appColor);
            historyViewHolder.tvAppntFee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callDirectionAct = new Intent(mContext, MapsGetDirections.class);
                    callDirectionAct.putExtra(AppConstants.LATITUDE, itemsData.get(position).getLatitude());
                    callDirectionAct.putExtra(AppConstants.LONGITUDE, itemsData.get(position).getLongitude());
                    mContext.startActivity(callDirectionAct);
                }
            });

            historyViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProviderHistoryModel.Booking_list historyData = itemsData.get(position);
                    Intent detailPage = new Intent(mContext, HistoryProviderDetailActivity.class);
                    detailPage.putExtra("ViewType", viewType);
                    detailPage.putExtra("HistoryData", historyData);
                    AppUtils.appStartIntent(mContext, detailPage);
                }
            });

        }
    }

    public void updateRecyclerView(Context mContext, List<ProviderHistoryModel.Booking_list> itemsData) {
        this.mContext = mContext;
        this.itemsData.clear();
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    private synchronized void addDescView(LinearLayout llReqDescDetail, int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View inflatedLayout = inflater.inflate(R.layout.layout_desc_single, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setTag(i);
        tvTxtDesc.setText(descVal);
        llReqDescDetail.addView(inflatedLayout);
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }


    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_userimg)
        ImageView ivUserimg;
        @BindView(R.id.tv_request_title)
        TextView tvRequestTitle;
        @BindView(R.id.tv_request_desc1)
        TextView tvRequestDesc1;
        @BindView(R.id.tv_appnt_date)
        TextView tvAppntDate;
        @BindView(R.id.tv_appnt_time)
        TextView tvAppntTime;
        @BindView(R.id.tv_request_status)
        TextView tvRequestStatus;
        @BindView(R.id.tv_appnt_fee)
        TextView tvAppntFee;
        @BindView(R.id.card_view)
        CardView cardView;

        @BindView(R.id.ll_req_desc_detail)
        LinearLayout llReqDesc;

        @BindView(R.id.swipe_layout_1)
        SwipeRevealLayout swipeLayout;

        public HistoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
