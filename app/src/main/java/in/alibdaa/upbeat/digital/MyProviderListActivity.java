package in.alibdaa.upbeat.digital;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import in.alibdaa.upbeat.digital.adapters.ProviderAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import retrofit2.Call;

public class MyProviderListActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.rv_my_provider_list)
    RecyclerView rvMyProviderList;

    LinearLayoutManager mLayoutManager;
    ProviderAdapter mAdapter;
    int providerNextPage, providerPageNo;
    public boolean isLoading = false;
    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;
    ProviderListData providerListData;

    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_my_provider_list);
        ButterKnife.bind(this);
        getLocaleData();

        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_my_services(), R.string.txt_my_providers));
        mLayoutManager = new LinearLayoutManager(this);
        rvMyProviderList.setLayoutManager(mLayoutManager);

        if (rvMyProviderList.getAdapter() == null) {
            mAdapter = new ProviderAdapter(this, this, new ArrayList<ProviderListData.ProviderList>(), 1);
            rvMyProviderList.setAdapter(mAdapter);
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("ChatDetailActivity", "Load More");

                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("ChatDetailActivity", "Load More 2");
                        getProviderListData(true);
                    }
                }, 1000);

            }
        });

        rvMyProviderList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManager.getItemCount();
                int displayedPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                if (displayedPosition == (mAdapter.itemsData.size() - 1)) {
                    if (!isLoading && providerNextPage > 0) {
                        if (dy > 0) //check for scroll down
                        {
                            visibleItemCount = mLayoutManager.getChildCount();
                            firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                isLoading = true;
                                Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                if (mAdapter.mOnLoadMoreListener != null) {
                                    mAdapter.mOnLoadMoreListener.onLoadMore();
                                }
                            }
                        }
                    }
                }
            }
        });


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (mAdapter != null) {
            mAdapter.saveStates(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (mAdapter != null) {
            mAdapter.restoreStates(savedInstanceState);
        }
    }

    private void getProviderListData(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            if (!isLoadMore) {
                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
                providerNextPage = 1;
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<ProviderListData> classificationCall = apiService.getMyProviderListData(providerNextPage, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROVIDERLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        switch (responseModel) {
            case AppConstants.PROVIDERLIST_DATA:
                if (isLoadMore) {
                    //Remove loading item
                    mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);
                }
                providerListData = (ProviderListData) myRes;
                isLoading = false;
                if (providerListData.getData() != null || providerListData.getData().getProviderList() != null && providerListData.getData().getProviderList().size() > 0) {
                    providerPageNo = Integer.parseInt(providerListData.getData().getCurrentPage());
                    providerNextPage = providerListData.getData().getNextPage();
                    if (!isLoadMore || providerPageNo == 1) {
                        mAdapter.itemsData = new ArrayList<>();
                        rvMyProviderList.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    mAdapter.updateRecyclerView(MyProviderListActivity.this, providerListData.getData().getProviderList());
                } else if (isLoadMore && mAdapter.itemsData != null && mAdapter.itemsData.size() > 0) {
                    rvMyProviderList.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    rvMyProviderList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
                break;
            case AppConstants.DELETE_PROVIDER_DATA:
                EmptyData resData = (EmptyData) myRes;
                MyProviderListActivity.this.recreate();
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void noDataHandling(Object myRes, boolean isLoadMore, String responseModel) {
        isLoading = false;
        switch (responseModel) {
            case AppConstants.PROVIDERLIST_DATA:
                if (isLoadMore && mAdapter != null && mAdapter.itemsData != null && mAdapter.itemsData.size() > 0) {
                    mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);
                } else {
                    rvMyProviderList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(AppUtils.cleanLangStr(this, commonData.getLg7_no_data_were_fo(), R.string.txt_no_data));
                }
                break;
        }
    }


    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
        }
    }

    public void deleteProvider(final String serviceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_provider_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(MyProviderListActivity.this)) {
                            ProgressDlg.clearDialog();
                            ProgressDlg.showProgressDialog(MyProviderListActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteProviderData(serviceId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(MyProviderListActivity.this, classificationCall, AppConstants.DELETE_PROVIDER_DATA, MyProviderListActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(MyProviderListActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProviderListData(false);
    }
}
