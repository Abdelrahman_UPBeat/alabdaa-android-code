package in.alibdaa.upbeat.digital;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import in.alibdaa.upbeat.digital.adapters.ProviderAdapter;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import retrofit2.Call;

public class ProvideActivity extends BaseAppCompatActivity implements RetrofitHandler.RetrofitResHandler {

    ProvideActivity mainActivity;
    Context mContext;
    private Paint p = new Paint();

    @BindView(R.id.tv_no_data)
    public TextView tvNoData;
    @BindView(R.id.rv_providers_list)
    public RecyclerView rvProvidersList;
    @BindView(R.id.fab_provider)
    FloatingActionButton fabProvider;
    Unbinder unbinder;

    //Fragment maintain state
    LinearLayoutManager mLayoutManager;
    public boolean isInitiated = false;
    public Bundle savedState;
    private boolean createdStateInDestroyView;

    //Pagination
    public int totalOrders, totalPages;
    public boolean isLoading = false;
    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;
    public int requestPageNo = 0, requestNextPage = 1, providerPageNo, providerNextPage = 1;
    //Adapter and data
    public ProviderAdapter mAdapter;
    public ArrayList<ProviderListData.ProviderList> myData = new ArrayList<>();

    ProviderListData providerListData = new ProviderListData();
    ArrayList<ProviderListData.ProviderList> providerList = new ArrayList<>();
    boolean isFirstTime;
    //    LanguageModel.Dashboard dashboardData = new LanguageModel().new Dashboard();
    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();

    LanguageModel.Common_used_texts commonUsedTexts = new LanguageModel().new Common_used_texts();

    private String catID = "", subCatID = "";

    public void myProvideActivity(ProvideActivity mainActivity, Context mContext) {
        this.mainActivity = mainActivity;
        this.mContext = mContext;
        commonUsedTexts = mainActivity.commonData;
    }


    @Nullable
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_list);
        unbinder = ButterKnife.bind(this);
        mLayoutManager = new LinearLayoutManager(this);
        rvProvidersList.setLayoutManager(mLayoutManager);
        setToolBarTitle(getIntent().getStringExtra(AppConstants.CATNAME));
        tvNoData.setText(AppUtils.cleanLangStr(this, commonUsedTexts.getLg7_no_data_were_fo(), R.string.txt_no_data));
        try {
            fabProvider.setBackgroundTintList(ColorStateList.valueOf(mainActivity.appColor));
        } catch (Exception e) {
        }
        if (rvProvidersList.getAdapter() == null) {
            mAdapter = new ProviderAdapter(mainActivity, mContext, new ArrayList<ProviderListData.ProviderList>(), 0);
            rvProvidersList.setAdapter(mAdapter);
        }
        if (savedState != null) {
            myData = savedState.getParcelableArrayList("PROVIDER_LIST");
            if (myData != null && myData.size() > 0) {
                mAdapter.updateRecyclerView(mContext, myData);
                rvProvidersList.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
            } else {
                rvProvidersList.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
            }
        }

        catID = getIntent().getStringExtra(AppConstants.CatID);
        subCatID = getIntent().getStringExtra(AppConstants.SubCatID);

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getProviderData(true);
                    }
                }, 1000);

            }
        });

        rvProvidersList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                try {
                    totalItemCount = mLayoutManager.getItemCount();
                    int displayedPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                    if (displayedPosition == (mAdapter.itemsData.size() - 1)) {
                        if (!isLoading && mainActivity.providerNextPage > 0) {
                            if (dy > 0) //check for scroll down
                            {
                                visibleItemCount = mLayoutManager.getChildCount();
                                firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                    isLoading = true;
                                    Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                    if (mAdapter.mOnLoadMoreListener != null) {
                                        mAdapter.mOnLoadMoreListener.onLoadMore();
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getProviderData(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        savedState = saveState();
        createdStateInDestroyView = true;
        myData = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (myData == null) {
            outState.putBundle("PROVIDER_LIST", savedState);
        } else {
            outState.putBundle("PROVIDER_LIST", createdStateInDestroyView ? savedState : saveState());
        }
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelableArrayList("PROVIDER_LIST", mAdapter.itemsData);
        return state;
    }

    @OnClick(R.id.fab_provider)
    public void onViewClicked() {
        if (PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE) > 0) {
            Intent in = new Intent(this, CreateProviderActivity.class);
            AppUtils.appStartIntent(this, in);
        } else {
            Intent myIntent = new Intent(this, SubscriptionActivity.class);
            myIntent.putExtra("FromPage", AppConstants.PAGE_CREATE_PROVIDER);
            AppUtils.appStartIntent(this, myIntent);
        }
    }


    public void getProviderData(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            if (!isLoadMore) {
                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            String latitude = PreferenceStorage.getKey(AppConstants.MY_LATITUDE) == null ? getLatitude() : PreferenceStorage.getKey(AppConstants.MY_LATITUDE);
            String longitude = PreferenceStorage.getKey(AppConstants.MY_LONGITUDE) == null ? getLongitude() : PreferenceStorage.getKey(AppConstants.MY_LONGITUDE);
            Call<ProviderListData> classificationCall = apiService.getProviderListData(providerNextPage, latitude, longitude, catID, subCatID, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROVIDERLIST_DATA, ProvideActivity.this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        isFirstTime = true;
        switch (responseModel) {
            case AppConstants.PROVIDERLIST_DATA:
                providerListData = (ProviderListData) myRes;
                if (isLoadMore) {
                    //Remove loading item
                    mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);

                }
                isInitiated = true;
                isLoading = false;

                if (providerListData.getData() != null || providerListData.getData().getProviderList() != null && providerListData.getData().getProviderList().size() > 0) {
                    providerPageNo = Integer.parseInt(providerListData.getData().getCurrentPage());
                    providerNextPage = providerListData.getData().getNextPage();
                    if (!isLoadMore || providerPageNo == 1) {
                        mAdapter.itemsData = new ArrayList<>();
                        rvProvidersList.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    mAdapter.updateRecyclerView(ProvideActivity.this, providerListData.getData().getProviderList());
                } else if (isLoadMore && mAdapter.itemsData.size() > 0) {
                    rvProvidersList.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    rvProvidersList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
                }
                break;

            case AppConstants.DELETE_PROVIDER_DATA:
                try {
                    providerNextPage = 1;
                    mAdapter.itemsData = new ArrayList<>();
                    myData = null;
                    savedState = null;
                    isInitiated = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getProviderData(false);
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

        if (myRes instanceof ProviderListData) {
            providerListData = (ProviderListData) myRes;
            if (providerListData != null && providerListData.getResponseHeader() != null
                    && providerListData.getResponseHeader().getResponseCode() != null
                    && providerListData.getResponseHeader().getResponseCode().equalsIgnoreCase("0")) {
                isInitiated = true;
            }
        }

        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void noDataHandling(Object myRes, boolean isLoadMore, String responseModel) {

        isLoading = false;
        if (responseModel.equalsIgnoreCase(AppConstants.PROVIDERLIST_DATA)) {
            if (isLoadMore && mAdapter != null && mAdapter.itemsData != null && mAdapter.itemsData.size() > 0) {
                mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);
            } else {
                rvProvidersList.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
            }
        }

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    public void deleteProvider(final String serviceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_provider_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(ProvideActivity.this)) {
                            ProgressDlg.clearDialog();
                            ProgressDlg.showProgressDialog(ProvideActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteProviderData(serviceId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(ProvideActivity.this, classificationCall, AppConstants.DELETE_PROVIDER_DATA, ProvideActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(ProvideActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


}
