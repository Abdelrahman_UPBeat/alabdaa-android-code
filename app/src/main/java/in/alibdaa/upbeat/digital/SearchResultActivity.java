package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.adapters.ProviderAdapter;
import in.alibdaa.upbeat.digital.adapters.RequestAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class SearchResultActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.rv_search_request_list)
    RecyclerView rvMyRequestList;

    @BindView(R.id.rv_search_provider_list)
    RecyclerView rvMyProviderList;

    LinearLayoutManager mLayoutManagerReq, mLayoutManagerProv;
    int requestNextPage, requestPageNo;
    boolean isLoading = false;
    RequestListData requestListData;
    RequestAdapter requestAdapter;

    ProviderListData providerListData;
    ProviderAdapter providerAdapter;

    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;

    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();

    String searchType, latitude, longitude, location, reqDate, reqTime, title, maxPrice, minPrice,cat,subCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_search_result_list);
        ButterKnife.bind(this);
        getLocaleData();

        latitude = getIntent().getStringExtra("latitude");
        longitude = getIntent().getStringExtra("longitude");
        title = getIntent().getStringExtra("search_title");
        reqDate = getIntent().getStringExtra("request_date");
        minPrice = getIntent().getStringExtra("min_price");
        maxPrice = getIntent().getStringExtra("max_price");
        location = getIntent().getStringExtra("location");
        reqTime = getIntent().getStringExtra("request_time");
        searchType = getIntent().getStringExtra("searchType");
        cat = getIntent().getStringExtra("category");
        subCat = getIntent().getStringExtra("subcategory");


        if (rvMyRequestList.getAdapter() == null) {
            requestAdapter = new RequestAdapter(this, this, new ArrayList<RequestListData.RequestList>(), 2);
            rvMyRequestList.setAdapter(requestAdapter);
        }
        mLayoutManagerReq = new LinearLayoutManager(this);
        rvMyRequestList.setLayoutManager(mLayoutManagerReq);

        requestAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                requestAdapter.itemsData.add(null);
                //mUsers.add(null);
                requestAdapter.notifyItemInserted(requestAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getRequestListData(true, requestNextPage);
                    }
                }, 1000);

            }
        });

        rvMyRequestList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManagerReq.getItemCount();
                int displayedPosition = mLayoutManagerReq.findLastCompletelyVisibleItemPosition();
                if (displayedPosition == (requestAdapter.itemsData.size() - 1)) {
                    if (!isLoading && requestNextPage > 0) {
                        if (dy > 0) //check for scroll down
                        {
                            visibleItemCount = mLayoutManagerReq.getChildCount();
                            firstVisibleItemPosition = mLayoutManagerReq.findFirstVisibleItemPosition();
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                isLoading = true;
                                Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                if (requestAdapter.mOnLoadMoreListener != null) {
                                    requestAdapter.mOnLoadMoreListener.onLoadMore();
                                }
                            }
                        }
                    }
                }
            }
        });


        if (rvMyProviderList.getAdapter() == null) {
            providerAdapter = new ProviderAdapter(this, this, new ArrayList<ProviderListData.ProviderList>(), 2);
            rvMyProviderList.setAdapter(providerAdapter);
        }
        mLayoutManagerProv = new LinearLayoutManager(this);
        rvMyProviderList.setLayoutManager(mLayoutManagerProv);

        providerAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                providerAdapter.itemsData.add(null);
                //mUsers.add(null);
                providerAdapter.notifyItemInserted(providerAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getProviderListData(true, requestNextPage);
                    }
                }, 1000);

            }
        });

        rvMyProviderList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManagerProv.getItemCount();
                int displayedPosition = mLayoutManagerProv.findLastCompletelyVisibleItemPosition();
                if (displayedPosition == (providerAdapter.itemsData.size() - 1)) {
                    if (!isLoading && requestNextPage > 0) {
                        if (dy > 0) //check for scroll down
                        {
                            visibleItemCount = mLayoutManagerProv.getChildCount();
                            firstVisibleItemPosition = mLayoutManagerProv.findFirstVisibleItemPosition();
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                isLoading = true;
                                Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                if (providerAdapter.mOnLoadMoreListener != null) {
                                    providerAdapter.mOnLoadMoreListener.onLoadMore();
                                }
                            }
                        }
                    }
                }
            }
        });


    }

    private void getProviderListData(boolean isLoadMore, int requestNextPage) {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            if (!isLoadMore)
                ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<ProviderListData> classificationCall = apiService.getSearchProviderData(requestNextPage, latitude, longitude, title, reqDate, location, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE),cat,subCat);
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROVIDERLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        tvNoData.setText(AppUtils.cleanLangStr(this, commonData.getLg7_no_data_were_fo(), R.string.txt_no_data));
        switch (searchType) {
            case AppConstants.SEARCH_REQUEST:
                rvMyRequestList.setLayoutManager(mLayoutManagerReq);
                setToolBarTitle(AppUtils.cleanLangStr(this, langReqProvData.getLg6_request(), R.string.txt_request));
                rvMyRequestList.setVisibility(View.VISIBLE);
                rvMyProviderList.setVisibility(View.GONE);
                getRequestListData(false, 1);
                break;
            case AppConstants.SEARCH_PROVIDER:
                rvMyProviderList.setLayoutManager(mLayoutManagerProv);
                setToolBarTitle(AppUtils.cleanLangStr(this, langReqProvData.getLg6_provide(), R.string.txt_provider));
                rvMyProviderList.setVisibility(View.VISIBLE);
                rvMyRequestList.setVisibility(View.GONE);
                getProviderListData(false, 1);
                break;
        }
    }

    private void getRequestListData(boolean isLoadMore, int requestNextPage) {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            if (!isLoadMore)
                ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<RequestListData> classificationCall = apiService.getSearchRequestData(requestNextPage, latitude, longitude, title, reqDate, reqTime, minPrice, maxPrice, location, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.REQUESTLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        switch (responseModel) {
            case AppConstants.REQUESTLIST_DATA:
                if (isLoadMore) {
                    //Remove loading item
                    requestAdapter.itemsData.remove(requestAdapter.itemsData.size() - 1);
                    requestAdapter.notifyItemRemoved(requestAdapter.itemsData.size() - 1);
                }
                isLoading = false;
                requestListData = (RequestListData) myRes;
                if (requestListData.getData() != null || requestListData.getData().getRequestList() != null && requestListData.getData().getRequestList().size() > 0) {
                    requestPageNo = Integer.parseInt(requestListData.getData().getCurrentPage());
                    requestNextPage = requestListData.getData().getNextPage();
                    if (!isLoadMore || requestPageNo == 1) {
                        requestAdapter.itemsData = new ArrayList<>();
                        rvMyRequestList.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    requestAdapter.updateRecyclerView(SearchResultActivity.this, requestListData.getData().getRequestList());
                } else if (isLoadMore && requestAdapter.itemsData != null && requestAdapter.itemsData.size() > 0) {
                    rvMyRequestList.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    rvMyRequestList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
                break;
            case AppConstants.PROVIDERLIST_DATA:
                providerListData = (ProviderListData) myRes;
                isLoading = false;
                if (isLoadMore) {
                    //Remove loading item
                    providerAdapter.itemsData.remove(providerAdapter.itemsData.size() - 1);
                    providerAdapter.notifyItemRemoved(providerAdapter.itemsData.size() - 1);
                }
                if (providerListData.getData() != null || providerListData.getData().getProviderList() != null && providerListData.getData().getProviderList().size() > 0) {
                    requestPageNo = Integer.parseInt(providerListData.getData().getCurrentPage());
                    requestNextPage = providerListData.getData().getNextPage();
                    if (!isLoadMore || requestPageNo == 1) {
                        providerAdapter.itemsData = new ArrayList<>();
                        rvMyProviderList.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    providerAdapter.updateRecyclerView(SearchResultActivity.this, providerListData.getData().getProviderList());
                } else if (isLoadMore && providerAdapter.itemsData != null && providerAdapter.itemsData.size() > 0) {
                    rvMyProviderList.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    rvMyProviderList.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
                break;
        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    public void noDataHandling(Object myRes, boolean isLoadMore, String responseModel) {
        if (searchType.equalsIgnoreCase(AppConstants.SEARCH_REQUEST)) {
            if (requestAdapter != null && requestAdapter.itemsData != null && requestAdapter.itemsData.size() > 0) {
                requestAdapter.itemsData.remove(requestAdapter.itemsData.size() - 1);
                requestAdapter.notifyItemRemoved(requestAdapter.itemsData.size() - 1);
            } else {
                rvMyRequestList.setVisibility(View.GONE);
                rvMyProviderList.setVisibility(View.GONE);
                tvNoData.setText(AppUtils.cleanLangStr(this, commonData.getLg7_no_data_were_fo(), R.string.txt_no_data));
                tvNoData.setVisibility(View.VISIBLE);
            }
        } else {
            if (providerAdapter != null && providerAdapter.itemsData != null && providerAdapter.itemsData.size() > 0) {
                providerAdapter.itemsData.remove(providerAdapter.itemsData.size() - 1);
                providerAdapter.notifyItemRemoved(providerAdapter.itemsData.size() - 1);
            } else {
                rvMyRequestList.setVisibility(View.GONE);
                rvMyProviderList.setVisibility(View.GONE);
                tvNoData.setText(AppUtils.cleanLangStr(this, commonData.getLg7_no_data_were_fo(), R.string.txt_no_data));
                tvNoData.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            langReqProvData = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
        }
    }
}
