package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.EditRequestActivity;
import in.alibdaa.upbeat.digital.MainActivity;
import in.alibdaa.upbeat.digital.MyRequestListActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.RequestDetailActivity;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import in.alibdaa.upbeat.digital.viewwidgets.SwipeRevealLayout;
import in.alibdaa.upbeat.digital.viewwidgets.ViewBinderHelper;

public class RequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mActivity;
    Context mContext;
    public ArrayList<RequestListData.RequestList> itemsData = new ArrayList<>();
    public int viewType;
    LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    public OnLoadMoreListener mOnLoadMoreListener;

    private int SELF = 1, LOADING = 2;

    // viewType - 0 - Dashboard Request
    // viewType - 1 - My Request List
    // viewType - 2 - Search Request
    public RequestAdapter(Activity mActivity, Context mContext, ArrayList<RequestListData.RequestList> itemsData, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        if (mActivity instanceof MainActivity) {
            langReqProvData = ((MainActivity) mActivity).langReqProvData;
        } else if (mActivity instanceof MyRequestListActivity) {
            langReqProvData = ((MyRequestListActivity) mActivity).langReqProvData;
        }
        // uncomment if you want to open only one row at a time
        binderHelper.setOpenOnlyOne(true);

    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
       /* // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_request, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;*/

        View itemView;
        if (viewType == LOADING) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.list_item_request, parent, false);
            return new RequestViewHolder(itemView);
        }
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (viewHolder instanceof RequestViewHolder) {
            RequestViewHolder chatDetailViewHolder = (RequestViewHolder) viewHolder;

            if (viewType == 1) {
                chatDetailViewHolder.tvRequestTitle.setText(AppUtils.cleanString(mContext, itemsData.get(position).getTitle()));
                chatDetailViewHolder.tvUsername.setVisibility(View.GONE);
            } else {
                chatDetailViewHolder.tvRequestTitle.setText(AppUtils.cleanString(mContext, itemsData.get(position).getTitle()));
                chatDetailViewHolder.tvUsername.setText(itemsData.get(position).getUsername());
            }
        /*viewHolder.tvRequestDesc1.setVisibility(View.INVISIBLE);
        viewHolder.tvRequestDesc2.setVisibility(View.INVISIBLE);
        viewHolder.tvRequestDesc3.setVisibility(View.INVISIBLE);*/

            if (itemsData != null && 0 <= position && position < itemsData.size()) {
                final String data = itemsData.get(position).getTitle();
                // Use ViewBindHelper to restore and save the open/close state of the SwipeRevealView
                // put an unique string id as value, can be any string which uniquely define the data
                binderHelper.bind(chatDetailViewHolder.swipeLayout, data);
                // Bind your data here
                chatDetailViewHolder.bind(data);
            }

            try {
                JSONArray descList = new JSONArray(itemsData.get(position).getDescription());
                chatDetailViewHolder.tvRequestDesc1.setVisibility(View.INVISIBLE);
                chatDetailViewHolder.tvRequestDesc2.setVisibility(View.INVISIBLE);
                chatDetailViewHolder.tvRequestDesc3.setVisibility(View.INVISIBLE);
                if (descList.length() == 1) {
                    chatDetailViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        chatDetailViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                } else if (descList.length() == 2) {
                    chatDetailViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        chatDetailViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                    chatDetailViewHolder.tvRequestDesc2.setText(AppUtils.cleanString(mContext, descList.getString(1)));
                    if (!AppUtils.cleanString(mContext, descList.getString(1)).isEmpty())
                        chatDetailViewHolder.tvRequestDesc2.setVisibility(View.VISIBLE);
                } else if (descList.length() >= 3) {
                    chatDetailViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        chatDetailViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                    chatDetailViewHolder.tvRequestDesc2.setText(AppUtils.cleanString(mContext, descList.getString(1)));
                    if (!AppUtils.cleanString(mContext, descList.getString(1)).isEmpty())
                        chatDetailViewHolder.tvRequestDesc2.setVisibility(View.VISIBLE);
                    chatDetailViewHolder.tvRequestDesc3.setText(AppUtils.cleanString(mContext, descList.getString(2)));
                    if (!AppUtils.cleanString(mContext, descList.getString(2)).isEmpty())
                        chatDetailViewHolder.tvRequestDesc3.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Allow swipe drag(Edit/Delete) IFF, My request & status is 0 i.e. Pending
            if (viewType == 1 && itemsData.get(position).getStatus().equalsIgnoreCase("0")) {
                chatDetailViewHolder.swipeLayout.setLockDrag(false);
            } else {
                chatDetailViewHolder.swipeLayout.setLockDrag(true);
            }

            if (viewType == 1) {
                chatDetailViewHolder.tvRequestStatus.setVisibility(View.VISIBLE);
                switch (itemsData.get(position).getStatus()) {
                    case "-1":
                        chatDetailViewHolder.tvRequestStatus.setText("");
                        break;
                    case "0":
                        chatDetailViewHolder.tvRequestStatus.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_pending(), R.string.txt_pending));
                        chatDetailViewHolder.tvRequestStatus.setTextColor(Color.parseColor("#e60000"));
                        break;
                    case "1":
                        chatDetailViewHolder.tvRequestStatus.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_accepted(), R.string.txt_accepted));
                        chatDetailViewHolder.tvRequestStatus.setTextColor(Color.parseColor("#ffbf00"));
                        break;
                    case "2":
                        chatDetailViewHolder.tvRequestStatus.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_completed(), R.string.txt_completed));
                        chatDetailViewHolder.tvRequestStatus.setTextColor(Color.parseColor("#00cc00"));
                        break;
                }
            }
            //String profPic = PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG);
            String profPic = AppConstants.BASE_URL + itemsData.get(position).getProfileImg();
            Picasso.with(mContext)
                    .load(profPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(chatDetailViewHolder.ivUserimg);
            chatDetailViewHolder.tvAppntDate.setText(AppUtils.cleanString(mContext, AppUtils.formatDateToApp(itemsData.get(position).getRequestDate())));
            chatDetailViewHolder.tvAppntTime.setText(AppUtils.cleanString(mContext, itemsData.get(position).getRequestTime()));
            chatDetailViewHolder.tvAppntFee.setText(AppUtils.cleanString(mContext, itemsData.get(position).getCurrencyCode() + " " + itemsData.get(position).getAmount()));

            try {
                chatDetailViewHolder.tvTxtAppnt.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_appointment(), R.string.txt_appnt));
            } catch (Exception e) {
            }

            if (mActivity instanceof MainActivity) {
                chatDetailViewHolder.tvAppntFee.setTextColor(((MainActivity) mActivity).appColor);
            } else if (mActivity instanceof MyRequestListActivity) {
                chatDetailViewHolder.tvAppntFee.setTextColor(((MyRequestListActivity) mActivity).appColor);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (itemsData.get(position) == null) {
            return LOADING;
        } else {
            return SELF;
        }
    }

    public void updateRecyclerView(Context mContext, ArrayList<RequestListData.RequestList> itemsData) {
        this.mContext = mContext;
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    private synchronized void addDescView(LinearLayout llReqDescDetail, int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View inflatedLayout = inflater.inflate(R.layout.layout_desc_single, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setText(descVal);
        llReqDescDetail.addView(inflatedLayout);
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public class RequestViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_userimg)
        ImageView ivUserimg;
        @BindView(R.id.tv_request_title)
        TextView tvRequestTitle;
        @BindView(R.id.tv_request_status)
        TextView tvRequestStatus;
        @BindView(R.id.tv_request_desc1)
        TextView tvRequestDesc1;
        @BindView(R.id.tv_request_desc2)
        TextView tvRequestDesc2;
        @BindView(R.id.tv_request_desc3)
        TextView tvRequestDesc3;
        @BindView(R.id.tv_txt_appnt)
        TextView tvTxtAppnt;
        @BindView(R.id.tv_appnt_date)
        TextView tvAppntDate;
        @BindView(R.id.tv_appnt_time)
        TextView tvAppntTime;
        @BindView(R.id.tv_appnt_fee)
        TextView tvAppntFee;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.swipe_layout_1)
        SwipeRevealLayout swipeLayout;

        @BindView(R.id.tv_req_edit)
        TextView tvReqEdit;
        @BindView(R.id.ll_req_edit)
        LinearLayout llReqEdit;
        @BindView(R.id.tv_req_delete)
        TextView tvReqDelete;
        @BindView(R.id.ll_req_delete)
        LinearLayout llReqDelete;


        @BindView(R.id.tv_username)
        TextView tvUsername;

        @BindView(R.id.ll_req_desc_detail)
        LinearLayout llReqDesc;

        public RequestViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int position = getAdapterPosition();
                        RequestListData.RequestList requestData = itemsData.get(position);
                        Intent detailPage = new Intent(mContext, RequestDetailActivity.class);
                        detailPage.putExtra("ViewType", viewType);
                        detailPage.putExtra("RequestData", requestData);
                        AppUtils.appStartIntent(mContext, detailPage);
                    } catch (Exception e) {

                    }
                }
            });
        }

        public void bind(final String data) {
            llReqEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    RequestListData.RequestList requestData = itemsData.get(position);
                    Intent detailPage = new Intent(mContext, EditRequestActivity.class);
                    detailPage.putExtra("ViewType", viewType);
                    detailPage.putExtra("RequestData", requestData);
                    AppUtils.appStartIntent(mContext, detailPage);
                }
            });

            llReqDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mActivity instanceof MainActivity) {
                        ((MainActivity) mActivity).deleteRequest(itemsData.get(getAdapterPosition()).getRId());
                    } else if (mActivity instanceof MyRequestListActivity) {
                        ((MyRequestListActivity) mActivity).deleteRequest(itemsData.get(getAdapterPosition()).getRId());
                    }
                }
            });
        }
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed.
     * Call this method in {@link Activity#onSaveInstanceState(Bundle)}
     */
    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed.
     * Call this method in {@link Activity#onRestoreInstanceState(Bundle)}
     */
    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

}
