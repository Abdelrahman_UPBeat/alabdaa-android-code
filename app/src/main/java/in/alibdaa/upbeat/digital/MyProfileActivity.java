package in.alibdaa.upbeat.digital;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProfileData;
import in.alibdaa.upbeat.digital.datamodel.ProfileUpdateModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MyProfileActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.btn_edit)
    Button btnEdit;
    @BindView(R.id.signup_header)
    ConstraintLayout signupHeader;
    @BindView(R.id.iv_prof_pic)
    CircleImageView ivProfPic;
    @BindView(R.id.cl_header)
    ConstraintLayout clHeader;
    @BindView(R.id.tiet_subscription)
    TextInputEditText tietSubscription;
    @BindView(R.id.tiet_username)
    TextInputEditText tietUsername;
    @BindView(R.id.tiet_email)
    TextInputEditText tietEmail;
    @BindView(R.id.tiet_phone)
    TextInputEditText tietPhone;
    @BindView(R.id.tiet_ic_number)
    TextInputEditText tietIcNumber;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.ll_footer)
    LinearLayout llFooter;
    @BindView(R.id.iv_prof_pic_edit)
    ImageView ivProfPicEdit;

    ProfileData profileData;

    private static final int RC_LOAD_IMG_CAMERA = 101;
    private static final int RC_LOAD_IMG_BROWSER = 102;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    @BindView(R.id.iv_ic_card_img)
    ImageView ivIcCardImg;
    @BindView(R.id.btn_upload_ic)
    Button btnUploadIc;
    @BindView(R.id.tv_txt_subs)
    TextView tvTxtSubs;
    @BindView(R.id.tv_txt_username)
    TextView tvTxtUsername;
    @BindView(R.id.tv_txt_email)
    TextView tvTxtEmail;
    @BindView(R.id.tv_txt_phone)
    TextView tvTxtPhone;

    @BindView(R.id.tv_txt_ic_card)
    TextView tvTxtIcCard;
    @BindView(R.id.btn_change_pwd)
    Button btnChangePwd;
    @BindView(R.id.btn_edit_subscription)
    ImageView btnEditSubscription;

    private BottomSheetDialog attachChooser;

    MultipartBody.Part profileImg;
    MultipartBody.Part icCardImg;
    ArrayList<MultipartBody.Part> imagePartList = new ArrayList<>();
    LanguageModel.Profile profileTextData = new LanguageModel().new Profile();
    String type = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);

        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_my_profile(), R.string.txt_my_profile));
        getProfileData();
        tietUsername.addTextChangedListener(new ProfileTextWatcher(tietUsername));
        tietEmail.addTextChangedListener(new ProfileTextWatcher(tietEmail));
        tietPhone.addTextChangedListener(new ProfileTextWatcher(tietPhone));
        signupHeader.setBackgroundColor(appColor);
        btnChangePwd.setBackgroundColor(appColor);
        btnEditSubscription.setColorFilter(appColor, android.graphics.PorterDuff.Mode.MULTIPLY);

        permissions = new ArrayList();
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(CAMERA);
    }

    private void setViewText() {
        tvTxtSubs.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_subscription(), R.string.hint_subscription));
        tvTxtUsername.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_username(), R.string.hint_username));
        tvTxtEmail.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_email(), R.string.hint_email));
        tvTxtIcCard.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_ic_card(), R.string.hint_IC_number));
        tvTxtPhone.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_phone(), R.string.hint_phone));
        btnChangePwd.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_change_password(), R.string.txt_change_pwd));
        btnUploadIc.setText(AppUtils.cleanLangStr(this, profileTextData.getLg4_upload_ic_card(), R.string.txt_upload_ic_card));
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_my_profile(), R.string.txt_my_profile));

    }

    private void getProfileData() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<ProfileData> classificationCall = apiService.getProfileData(PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROFILE_DATA, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @OnClick({R.id.btn_edit, R.id.iv_prof_pic_edit, R.id.btn_upload_ic, R.id.btn_save, R.id.btn_change_pwd, R.id.btn_edit_subscription})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_edit:
                permissionsToRequest = findUnAskedPermissions(permissions);
                if (permissionsToRequest != null && permissionsToRequest.size() == 0) {
                    makeProfileEdit();
                } else {
                    checkLocationPermission();
                }
                break;
            case R.id.iv_prof_pic_edit:
                type = "0";
                selectImage();
                break;
            case R.id.btn_upload_ic:
                type = "1";
                selectImage();
                break;
            case R.id.btn_save:
                performProfileUpdate();
                break;
            case R.id.btn_change_pwd:
                Intent changePwdInt = new Intent(MyProfileActivity.this, ChangePasswordActivity.class);
                AppUtils.appStartIntent(MyProfileActivity.this, changePwdInt);
                break;
            case R.id.btn_edit_subscription:
                Intent callSubscriptionAct = new Intent(MyProfileActivity.this, GoToSubscriptionActivity.class);
                callSubscriptionAct.putExtra("FromPage", AppConstants.PAGE_MY_PROFILE);
                AppUtils.appStartIntent(MyProfileActivity.this, callSubscriptionAct);
                break;
        }
    }

    private void performProfileUpdate() {
        if (!validatePhoneNo()) {
            return;
        } else {

            if (profileImg != null) {
                imagePartList.add(profileImg);
            }

            if (icCardImg != null) {
                imagePartList.add(icCardImg);
            }

            RequestBody userName = RequestBody.create(MediaType.parse("text/plain"), tietUsername.getText().toString());
            RequestBody phoneNo = RequestBody.create(MediaType.parse("text/plain"), tietPhone.getText().toString());
            if (AppUtils.isNetworkAvailable(this)) {

                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
                ApiInterface apiService =
                        ApiClient.getClientNoHeader().create(ApiInterface.class);
                try {
                    String latitude = PreferenceStorage.getKey(AppConstants.MY_LATITUDE) == null ? getLatitude() : PreferenceStorage.getKey(AppConstants.MY_LATITUDE);
                    String longitude = PreferenceStorage.getKey(AppConstants.MY_LONGITUDE) == null ? getLongitude() : PreferenceStorage.getKey(AppConstants.MY_LONGITUDE);
                    Call<ProfileUpdateModel> classificationCall = apiService.postProfileUpdate(userName, phoneNo,
                            RequestBody.create(MediaType.parse("text/plain"), latitude),
                            RequestBody.create(MediaType.parse("text/plain"), longitude),
                            imagePartList, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                    RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROFILE_UPDATE_DATA, this, true);
                } catch (Exception e) {
                    ProgressDlg.dismissProgressDialog();
                    e.printStackTrace();
                }

            } else {
                AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
            }
        }
    }

    private class ProfileTextWatcher implements TextWatcher {
        private View view;

        private ProfileTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (view.getId() == R.id.tiet_username) {
                if (!tietUsername.getText().toString().isEmpty()) {
                    validateUsername();
                }
            } else if (view.getId() == R.id.tiet_email) {
                if (!tietEmail.getText().toString().isEmpty()) {
                    validateEmail();
                }
            } else if (view.getId() == R.id.tiet_phone) {
                if (!tietPhone.getText().toString().isEmpty()) {
                    validatePhoneNo();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    }

    private void makeProfileEdit() {
        tietSubscription.setEnabled(false);
        btnEditSubscription.setVisibility(View.VISIBLE);
        tietUsername.setEnabled(false);
        tietPhone.setEnabled(true);
        ivProfPicEdit.setVisibility(View.VISIBLE);
        btnEdit.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);
        btnUploadIc.setVisibility(View.VISIBLE);
        llFooter.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes instanceof ProfileData) {
            profileData = (ProfileData) myRes;
            if (profileData != null && profileData.getData() != null) {

                if (profileData.getData().getProfileDetails().getSubscriptionDetails() != null &&
                        profileData.getData().getProfileDetails().getSubscriptionDetails().getSubscriptionName() != null &&
                        !profileData.getData().getProfileDetails().getSubscriptionDetails().getSubscriptionName().isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("(");
                    sb.append(AppUtils.cleanLangStr(this, profileTextData.getLg4_valid_till(), R.string.txt_valid_upto));
                    sb.append(" ");
                    sb.append(profileData.getData().getProfileDetails().getSubscriptionDetails().getExpiryDateTime().split(" ")[0]);
                    sb.append(")");
                    sb.append(" ");
                    sb.append(profileData.getData().getProfileDetails().getSubscriptionDetails().getSubscriptionName());
                    tietSubscription.setText(sb.toString());
                }
                tietUsername.setText(profileData.getData().getProfileDetails().getUsername());
                tietEmail.setText(profileData.getData().getProfileDetails().getEmail());
                tietPhone.setText(profileData.getData().getProfileDetails().getMobileNo());
                Picasso.with(MyProfileActivity.this)
                        .load(AppConstants.BASE_URL + profileData.getData().getProfileDetails().getProfileImg())
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(ivProfPic);
                /*Picasso.with(MyProfileActivity.this)
                        .load(AppConstants.BASE_URL + profileData.getData().getProfileDetails().getProfileImg())
                        .placeholder(R.drawable.ic_pic_view)
                        .error(R.drawable.ic_pic_view)
                        .into((CircleImageView) ivProfPicEdit.findViewById(R.id.iv_picture));*/
                Picasso.with(MyProfileActivity.this)
                        .load(AppConstants.BASE_URL + profileData.getData().getProfileDetails().getIcCardImage())
                        .placeholder(R.drawable.ic_card_placeholder)
                        .error(R.drawable.ic_card_placeholder)
                        .into(ivIcCardImg);
            }
        } else if (myRes instanceof ProfileUpdateModel) {
            ProfileUpdateModel updateResponse = (ProfileUpdateModel) myRes;
            if (updateResponse != null && updateResponse.getData() != null) {
                PreferenceStorage.setKey(AppConstants.USER_PROFILE_IMG, updateResponse.getData().getProfileDetails().getProfileImg());
                PreferenceStorage.setKey(AppConstants.USER_PHONE, updateResponse.getData().getProfileDetails().getMobileNo());
                AppUtils.showToast(MyProfileActivity.this, updateResponse.getResponseHeader().getResponseMessage());
                MyProfileActivity.this.finish();
            }
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (imagePartList != null) {
            if (imagePartList.contains(profileImg)) {
                imagePartList.remove(profileImg);
            }
            if (imagePartList.contains(icCardImg)) {
                imagePartList.remove(icCardImg);
            }
        }

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (imagePartList != null) {
            if (imagePartList.contains(profileImg)) {
                imagePartList.remove(profileImg);
            }
            if (imagePartList.contains(icCardImg)) {
                imagePartList.remove(icCardImg);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getLocaleData();
            setViewText();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validateUsername() {
        if (tietUsername.getText().toString().isEmpty()) {
            tietUsername.setError(AppUtils.cleanLangStr(this, profileTextData.getLg4_enter_username(), R.string.err_username));
            tietUsername.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateEmail() {
        if (tietEmail.getText().toString().isEmpty()) {
            tietEmail.setError(AppUtils.cleanLangStr(this, profileTextData.getLg4_enter_email(), R.string.err_email));
            tietEmail.requestFocus();
            return false;
        } else if (!AppUtils.isValidEmail(tietEmail.getText().toString())) {
            tietEmail.setError(AppUtils.cleanLangStr(this, profileTextData.getLg4_enter_valid_ema(), R.string.err_valid_email));
            tietEmail.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validatePhoneNo() {
        if (tietPhone.getText().toString().isEmpty()) {
            tietPhone.setError(AppUtils.cleanLangStr(this, profileTextData.getLg4_enter_phone_num(), R.string.err_phone_no));
            tietPhone.requestFocus();
            return false;
        } else if (tietPhone.getText().toString().length() > 15) {
            tietPhone.setError(AppUtils.cleanLangStr(this, profileTextData.getLg4_maximum_15_numb(), R.string.err_max_no));
            tietPhone.requestFocus();
            return false;
        }
        return true;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_LOAD_IMG_BROWSER) {
            onSelectFromGalleryResult(data);
        } else if (requestCode == RC_LOAD_IMG_CAMERA) {
            onCaptureImageResult(data);
        }
    }

    private void selectImage() {
        attachChooser = new BottomSheetDialog(MyProfileActivity.this);
        attachChooser.setContentView((MyProfileActivity.this).getLayoutInflater().inflate(R.layout.popup_add_attach_options,
                new LinearLayout(MyProfileActivity.this)));
        attachChooser.show();
        //TODO: lang
        LinearLayout btnStartCamera = (LinearLayout) attachChooser.findViewById(R.id.btn_from_camera);
        LinearLayout btnStartFileBrowser = (LinearLayout) attachChooser.findViewById(R.id.btn_from_local);
        btnStartCamera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attachChooser.dismiss();
                if (AppUtils.checkCameraPermission(MyProfileActivity.this))
                    cameraIntent();
            }
        });
        btnStartFileBrowser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attachChooser.dismiss();
                if (AppUtils.checkPermission(MyProfileActivity.this))
                    galleryIntent();
            }
        });
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RC_LOAD_IMG_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(intent, RC_LOAD_IMG_BROWSER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case AppConstants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /*if (requestCode == RC_LOAD_IMG_CAMERA)
                        cameraIntent();
                    else if (requestCode == RC_LOAD_IMG_BROWSER)
                        galleryIntent();*/
                    galleryIntent();
                } else {
                }
                break;
            case AppConstants.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /*if (requestCode == RC_LOAD_IMG_CAMERA)
                        cameraIntent();
                    else if (requestCode == RC_LOAD_IMG_BROWSER)
                        galleryIntent();*/
                    cameraIntent();
                } else {
                }
                break;
            /*case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel(AppUtils.cleanLangStr(this, profileTextData.getLg4_these_permissio(), R.string.err_txt_permission),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;*/
        }
    }

    /*public boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }*/


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(AppUtils.cleanLangStr(this, profileTextData.getLg4_ok(), R.string.ok), okListener)
                .setNegativeButton(AppUtils.cleanLangStr(this, profileTextData.getLg4_cancel(), R.string.txt_cancel), null)
                .create()
                .show();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap thumbnail = null;
        Bitmap scaled = null;
        try {
            if (data != null) {
                thumbnail = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                int nh = (int) (160 * (512.0 / 160));
                thumbnail = Bitmap.createScaledBitmap(thumbnail, 512, nh, true);
            }
            if (thumbnail != null) {
                scaled = thumbnail;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                Uri selectedImage = data.getData();
                String encodeImage = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());
                if (type.equalsIgnoreCase("0")) {
                    profileImg = MultipartBody.Part.createFormData("profile_img", "profile_img.jpg", requestFile);
                    ivProfPic.setImageBitmap(scaled);
                } else if (type.equalsIgnoreCase("1")) {
                    icCardImg = MultipartBody.Part.createFormData("ic_card_image", "cardImage.jpg", requestFile);
                    ivIcCardImg.setImageBitmap(scaled);
                }
            }
        } catch (Exception e) {

        }

    }

    private void onCaptureImageResult(Intent data) {
        try {
            if (data != null) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                if (thumbnail != null) {
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    String encodeImage = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                }
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());

                if (type.equalsIgnoreCase("0")) {
                    profileImg = MultipartBody.Part.createFormData("profile_img", "profile_img.jpg", requestFile);
                    ivProfPic.setImageBitmap(thumbnail);
                } else if (type.equalsIgnoreCase("1")) {
                    icCardImg = MultipartBody.Part.createFormData("ic_card_image", "cardImage.jpg", requestFile);
                    ivIcCardImg.setImageBitmap(thumbnail);
                }
            }
        } catch (Exception e) {

        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.profile);
            profileTextData = new Gson().fromJson(commonDataStr, LanguageModel.Profile.class);
        } catch (Exception e) {
            profileTextData = new LanguageModel().new Profile();
        }
    }
}
