package in.alibdaa.upbeat.digital;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends BaseAppCompatActivity {

    @BindView(R.id.webview)
    WebView webview;
    String url = "", title = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);

        url = getIntent().getStringExtra(AppConstants.URL);
        title = getIntent().getStringExtra(AppConstants.TbTitle);
        setToolBarTitle(title);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        ProgressDlg.showProgressDialog(WebViewActivity.this, null, null);
//        cProgressDialog.showDialog(this);

        webview.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                Uri uri = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    uri = request.getUrl();
                }
                view.loadUrl(uri.toString());
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
//                cProgressDialog.dismissDialog();
                ProgressDlg.dismissProgressDialog();
            }

        });

        webview.loadUrl(url);

    }
}
