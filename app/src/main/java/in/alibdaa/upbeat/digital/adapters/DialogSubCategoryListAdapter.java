package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import in.alibdaa.upbeat.digital.MainActivity;
import in.alibdaa.upbeat.digital.MyProviderListActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.datamodel.SubCategoryList;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.viewwidgets.ViewBinderHelper;

public class DialogSubCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mActivity;
    Context mContext;
    public ArrayList<ProviderListData.ProviderList> itemsData = new ArrayList<>();
    int viewType;
    LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    public OnLoadMoreListener mOnLoadMoreListener;
    List<SubCategoryList.Category_list> category_list;
    public String cat_id = "", subCatID = "";

    AlertDialog dialog;
    EditText tvCategory;
    Button btnDone;

    private int SELF = 1, LOADING = 2;

    public DialogSubCategoryListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public DialogSubCategoryListAdapter(Context mContext, List<SubCategoryList.Category_list> category_list, AlertDialog dialog, EditText etCategory, String subCatID) {
        this.mContext = mContext;
        this.dialog = dialog;
        this.tvCategory = etCategory;
        this.category_list = category_list;
        this.subCatID = subCatID;
    }

    public DialogSubCategoryListAdapter(Activity mActivity, Context mContext, ArrayList<ProviderListData.ProviderList> itemsData, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        if (mActivity instanceof MainActivity) {
            langReqProvData = ((MainActivity) mActivity).langReqProvData;
        } else if (mActivity instanceof MyProviderListActivity) {
            langReqProvData = ((MyProviderListActivity) mActivity).requestAndProviderList;
        }
        // uncomment if you want to open only one row at a time
        binderHelper.setOpenOnlyOne(true);
    }

    public DialogSubCategoryListAdapter(Context mContext, List<SubCategoryList.Category_list> category_list, AlertDialog dialog, EditText etCategory, String subCatID, Button btnDone) {
        this.mContext = mContext;
        this.dialog = dialog;
        this.tvCategory = etCategory;
        this.category_list = category_list;
        this.subCatID = subCatID;
        this.btnDone = btnDone;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
       /* // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_provider, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;*/
        View itemView;
        itemView = LayoutInflater.from(mContext).inflate(R.layout.dlg_adapter_categories_list, parent, false);
        return new CategoryViewHolder(itemView);


//        if (viewType == LOADING) {
//            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
//            return new LoadingViewHolder(itemView);
//        } else {
//            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_provider, parent, false);
//            return new CategoryListAdapter.CategoryViewHolder(itemView);
//        }

    }

//    @Override
//    public int getItemViewType(int position) {
//        if (itemsData.get(position) == null) {
//            return LOADING;
//        } else {
//            return SELF;
//        }
//    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        CategoryViewHolder categoryViewHolder = (CategoryViewHolder) viewHolder;


        categoryViewHolder.tvCatName.setText(category_list.get(position).getSubcategory_name());

        Picasso.with(mContext)
                .load(AppConstants.BASE_URL + category_list.get(position).getSubcategory_image())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(categoryViewHolder.ivCatImg);

        categoryViewHolder.tvCatName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                category_list.get(position).setChecked(isChecked);
            }
        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String catName = "";
                for (int i = 0; i < category_list.size(); i++) {
                    if (category_list.get(i).isChecked()) {
                        catName = catName + category_list.get(i).getSubcategory_name() + ",";
                        cat_id = cat_id + category_list.get(i).getCategory_id() + ",";
                        subCatID = subCatID + category_list.get(i).getSubcategory_id() + ",";
                    }
                }
                tvCategory.setText(catName.substring(0, catName.length() - 1));
                cat_id = cat_id.substring(0, cat_id.length() - 1);
                subCatID = subCatID.substring(0, subCatID.length() - 1);
                dialog.dismiss();
            }
        });
    }

    public void updateRecyclerView(Context mContext, ArrayList<ProviderListData.ProviderList> itemsData) {
        this.mContext = mContext;
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return category_list.size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_cat_img)
        ImageView ivCatImg;
        @BindView(R.id.tv_cat_name)
        CheckBox tvCatName;

        public CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }
    }
}
