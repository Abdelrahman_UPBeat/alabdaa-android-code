package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.HistoryListData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class HistoryDetailActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    ArrayList<String> descDataList = new ArrayList<>();
    @BindView(R.id.tv_requester_name)
    TextView tvRequesterName;
    @BindView(R.id.tv_req_email)
    TextView tvReqEmail;
    @BindView(R.id.tv_req_contact)
    TextView tvReqContact;
    @BindView(R.id.cv_req_user_detail)
    CardView cvReqUserDetail;
    @BindView(R.id.tv_request_title)
    TextView tvRequestTitle;
    @BindView(R.id.ll_req_desc_detail)
    LinearLayout llReqDescDetail;
    @BindView(R.id.cv_req_detail)
    CardView cvReqDetail;
    @BindView(R.id.tv_req_location)
    TextView tvReqLocation;
    @BindView(R.id.tv_req_date)
    TextView tvReqDate;
    @BindView(R.id.tv_req_time)
    TextView tvReqTime;
    @BindView(R.id.cv_req_availability)
    CardView cvReqAvailability;
    @BindView(R.id.tv_req_fee)
    TextView tvReqFee;
    @BindView(R.id.iv_user_img)
    ImageView ivUserImg;
    @BindView(R.id.tv_txt_req_proc)
    TextView tvTxtReqProc;
    @BindView(R.id.iv_req_proc_img)
    ImageView ivReqProcImg;
    @BindView(R.id.tv_req_proc_name)
    TextView tvReqProcName;

    @BindView(R.id.btn_request_complete)
    Button btnRequestAccept;
    @BindView(R.id.tv_req_proc_email)
    TextView tvReqProcEmail;
    @BindView(R.id.tv_req_proc_contact)
    TextView tvReqProcContact;

    HistoryListData.RequestList requestDataDetail;

    int hisReqType;
    int requesterID, accepterID, userId;
    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();
    @BindView(R.id.btn_ratenow)
    Button btnRatenow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_history_details);
        ButterKnife.bind(this);

        requestDataDetail = getIntent().getParcelableExtra("RequestData");
        hisReqType = getIntent().getIntExtra("ViewType", 1);

        if (requestDataDetail != null) {
            tvRequesterName.setText(requestDataDetail.getRequesterName());
            tvReqEmail.setText(requestDataDetail.getRequesterEmail());
            tvReqContact.setText(requestDataDetail.getRequesterMobile());

            tvRequestTitle.setText(requestDataDetail.getTitle());
            setToolBarTitle(requestDataDetail.getTitle());
            if (requestDataDetail.getDescription() != null) {
                try {
                    JSONArray descList = new JSONArray(requestDataDetail.getDescription());
                    for (int i = 0; i < (descList.length()); i++) {
                        addDescView(i, descList.getString(i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                /*AppUtils.getAddressFromLocation(Double.parseDouble(requestDataDetail.getLatitude()), Double.parseDouble(requestDataDetail.getLongitude()),
                        getApplicationContext(), new GeocoderHandler());*/
                tvReqLocation.setText(AppUtils.cleanString(this, requestDataDetail.getLocation()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            String ivPic;
            ivPic = AppConstants.BASE_URL + PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG)/*requestDataDetail.getRequestImage()*/;
            tvRequesterName.setText(PreferenceStorage.getKey(AppConstants.USER_NAME));
            tvReqEmail.setText(PreferenceStorage.getKey(AppConstants.USER_EMAIL));
            tvReqContact.setText(PreferenceStorage.getKey(AppConstants.USER_PHONE));

            Picasso.with(this)
                    .load(ivPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .error(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .into(ivUserImg);

            tvReqDate.setText(requestDataDetail.getRequestDate());
            tvReqTime.setText(requestDataDetail.getRequestTime());
            tvReqFee.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_expecting_fee(), R.string.txt_expecting_fee) + requestDataDetail.getCurrencyCode() + " " + requestDataDetail.getAmount());

            requesterID = Integer.parseInt(requestDataDetail.getRequesterId());
            accepterID = Integer.parseInt(requestDataDetail.getAcceptorId());
            userId = PreferenceStorage.getIntKey(AppConstants.USER_ID);
            Log.d("TAG", "Status = " + requestDataDetail.getStatus());
            Log.d("TAG", "requesterID = " + requesterID + " && accepterID = " + accepterID + " && userId = " + userId);
            if (requestDataDetail.getStatus().equalsIgnoreCase("1") && requesterID == userId) {
                btnRequestAccept.setVisibility(View.VISIBLE);
                btnRequestAccept.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_complete_reques(), R.string.txt_complete_request));
            }
            if (requesterID == userId) {
                tvTxtReqProc.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_acceptor(), R.string.txt_accepter));
                tvReqProcName.setText(requestDataDetail.getAcceptorName());
                tvReqProcEmail.setText(requestDataDetail.getAcceptorEmail());
                tvReqProcContact.setText(requestDataDetail.getAcceptorMobile());
                Picasso.with(this)
                        .load(AppConstants.BASE_URL + requestDataDetail.getAcceptorImage())
                        .placeholder(R.drawable.ic_pic_view)
                        .transform(new CircleTransform())
                        .error(R.drawable.ic_pic_view)
                        .into(ivReqProcImg);
            } else if (accepterID == userId) {
                tvTxtReqProc.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_requester(), R.string.txt_requester));
                tvReqProcName.setText(requestDataDetail.getRequesterName());
                tvReqProcEmail.setText(requestDataDetail.getRequesterEmail());
                tvReqProcContact.setText(requestDataDetail.getRequesterMobile());
                Picasso.with(this)
                        .load(AppConstants.BASE_URL + requestDataDetail.getRequestImage())
                        .placeholder(R.drawable.ic_pic_view)
                        .transform(new CircleTransform())
                        .error(R.drawable.ic_pic_view)
                        .into(ivReqProcImg);
            }
        }
        btnRequestAccept.setBackgroundColor(appColor);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //userSubsType = PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE);
        getLocaleData();
        tvReqFee.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_expecting_fee(), R.string.txt_expecting_fee) + requestDataDetail.getCurrencyCode() + " " + requestDataDetail.getAmount());
        if (requestDataDetail.getStatus().equalsIgnoreCase("1") && requesterID == userId) {
            btnRequestAccept.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_complete_reques(), R.string.txt_complete_request));
        }
        if (requesterID == userId) {
            tvTxtReqProc.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_acceptor(), R.string.txt_accepter));
        } else if (accepterID == userId) {
            tvTxtReqProc.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_requester(), R.string.txt_requester));
        }

    }

    private synchronized void addDescView(int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View inflatedLayout = inflater.inflate(R.layout.layout_bullet_textview, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setText(descVal);
        llReqDescDetail.addView(inflatedLayout);
    }

    @OnClick(R.id.btn_request_complete)
    public void onViewClicked() {
        performReqAccept();
    }

    private void performReqAccept() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(HistoryDetailActivity.this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            try {
                Call<EmptyData> classificationCall = apiService.postRequestCompleteData(Integer.parseInt(requestDataDetail.getRId()), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(HistoryDetailActivity.this, classificationCall, AppConstants.REQUEST_COMPLETE_DATA, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }
        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        EmptyData requestAcceptData = (EmptyData) myRes;
        AppUtils.showToast(HistoryDetailActivity.this, requestAcceptData.getResponseHeader().getResponseMessage());
        HistoryDetailActivity.this.finish();
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
        }
    }



}
