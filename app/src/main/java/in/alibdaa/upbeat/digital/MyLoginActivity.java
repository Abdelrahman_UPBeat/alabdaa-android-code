package in.alibdaa.upbeat.digital;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.LoginData;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.NotificationUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import retrofit2.Call;

public class MyLoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.iv_app_logo)
    ImageView ivAppLogo;
    @BindView(R.id.tiet_username)
    TextInputEditText tietUsername;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.tiet_password)
    TextInputEditText tietPassword;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.btn_signin)
    Button btnSignin;
    @BindView(R.id.tv_forgot_pwd)
    TextView tvForgotPwd;
    @BindView(R.id.tv_account_msg)
    TextView tvAccountMsg;
    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;
    @BindView(R.id.iv_facebook)
    ImageView ivFacebook;
    @BindView(R.id.tv_facebook)
    TextView tvFacebook;
    @BindView(R.id.ll_login_fb)
    LinearLayout llLoginFb;
    @BindView(R.id.iv_google)
    ImageView ivGoogle;
    @BindView(R.id.tv_google)
    TextView tvGoogle;
    @BindView(R.id.ll_login_google)
    LinearLayout llLoginGoogle;

    private static final String TAG = MyLoginActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private int loginThrough = AppConstants.REG_NORMAL;
    private String socialId, socialEmail, socialName;


    private CallbackManager callbackManager;

    LanguageModel.Login loginStr;

    //Broadcast Receiver for push notification
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setTextForLan();
        String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
        if (userToken != null) {
            Intent myIntent = new Intent(MyLoginActivity.this, MainActivity.class);
            AppUtils.appStartIntent(MyLoginActivity.this, myIntent);
            MyLoginActivity.this.finish();
        }
        //ValidateTextWatcher
        tietUsername.addTextChangedListener(new LoginTextWatcher(tietUsername));
        tietPassword.addTextChangedListener(new LoginTextWatcher(tietPassword));

        //Hide Keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //G+ Integration
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //FB Integration
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    String email = "", facebookId = "";
                                    if (object.has("email")) {
                                        email = object.getString("email");
                                        Log.e(TAG, "email = " + email);
                                    }
                                    facebookId = object.getString("id");
                                    Log.e(TAG, "facebookId = " + facebookId);
                                    loginThrough = AppConstants.REG_FB;
                                    socialId = facebookId;
                                    socialEmail = email;
                                    socialName = object.getString("first_name");
                                    loginCheck(object.getString("first_name"), "");//Validate login for FB
                                } catch (Exception e) {
                                    Log.e(TAG, e.getLocalizedMessage());
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,cover,picture.type(large),email,birthday");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("error", String.valueOf(error));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        setAppTheme();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(AppConstants.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(AppConstants.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
        tietUsername.setText("");
        tietPassword.setText("");
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct != null) {
                Log.e(TAG, "display name: " + acct.getDisplayName());
                String personPhotoUrl = "";
                Log.e(TAG, "Token =  " + acct.getIdToken());
                Log.e(TAG, "ID =  " + acct.getId());
                String personName = acct.getDisplayName();
                if (acct.getPhotoUrl() != null) {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                }
                String email = acct.getEmail();
                Log.e(TAG, "Name: " + personName + ", email: " + email
                        + ", Image: " + personPhotoUrl);
                tietUsername.setText(personName);

                loginThrough = AppConstants.REG_GPLUS;
                socialId = acct.getId();
                socialEmail = email;
                socialName = acct.getDisplayName();
                loginCheck(acct.getDisplayName(), "");//Validate login for G+
            }
        }
    }

    @OnClick({R.id.btn_signin, R.id.tv_forgot_pwd, R.id.tv_sign_up, R.id.ll_login_fb, R.id.ll_login_google})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signin:
                validateLogin();
                break;
            case R.id.tv_forgot_pwd:
                Intent forgotPwdInt = new Intent(MyLoginActivity.this, ForgotPasswordActivity.class);
                startActivity(forgotPwdInt);
                break;
            case R.id.tv_sign_up:
                Log.d("Preference", PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                Log.d("Locale", Locale.getDefault().getLanguage());
                Locale.setDefault(new Locale(Locale.getDefault().getLanguage()));
                Intent myIntent = new Intent(MyLoginActivity.this, SignUpActivity.class);
                myIntent.putExtra("LoginType", AppConstants.REG_NORMAL);
                startActivity(myIntent);
                break;
            case R.id.ll_login_fb:
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(MyLoginActivity.this, Arrays.asList("email"));
                break;
            case R.id.ll_login_google:
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    private void validateLogin() {

        if (!validateUsername()) {
            return;
        } else if (!validatePassword()) {
            return;
        } else {
            String userName = tietUsername.getText().toString().trim();
            String password = tietPassword.getText().toString().trim();
            loginThrough = AppConstants.REG_NORMAL;
            loginCheck(userName, password);
        }
    }

    private void loginCheck(String userName, String password) {
        if (AppUtils.isNetworkAvailable(MyLoginActivity.this)) {

            ProgressDlg.showProgressDialog(MyLoginActivity.this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Call<LoginData> classificationCall = apiService.getLoginDetails(userName, password, AppConstants.deviceType, PreferenceStorage.getKey(AppConstants.refreshedToken), String.valueOf(loginThrough), socialId, PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(MyLoginActivity.this, classificationCall, AppConstants.LOGIN_DATA, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }

    private class LoginTextWatcher implements TextWatcher {
        private View view;

        private LoginTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (view.getId() == R.id.tiet_username) {
                if (!tietUsername.getText().toString().isEmpty()) {
                    validateUsername();
                }
            } else if (view.getId() == R.id.tiet_password) {
                if (!tietPassword.getText().toString().isEmpty()) {
                    validatePassword();
                }

            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    private boolean validateUsername() {
        if (tietUsername.getText().toString().isEmpty()) {
            tilUsername.setError(AppUtils.cleanLangStr(this, loginStr.getLg2_please_enter_us(), R.string.txt_msg_invalid_username));
            tilUsername.requestFocus();
            return false;
        } else {
            tilUsername.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        if (tietPassword.getText().toString().isEmpty()) {
            tilPassword.setError(AppUtils.cleanLangStr(this, loginStr.getLg2_please_enter_pa(), R.string.txt_msg_invalid_password));
            tilPassword.requestFocus();
            return false;
        } else {
            tilPassword.setErrorEnabled(false);
        }
        return true;
    }


    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        LoginData response = (LoginData) myRes;
        if (response.getData() != null && response.getData().getUserDetails() != null) {
            String token = response.getData().getUserDetails().getToken();
            String userName = response.getData().getUserDetails().getUsername();
            PreferenceStorage.setKey(AppConstants.USER_TOKEN, token);
            PreferenceStorage.setKey(AppConstants.USER_ID, Integer.parseInt(response.getData().getUserDetails().getUserId()));
            PreferenceStorage.setKey(AppConstants.USER_NAME, userName);
            PreferenceStorage.setKey(AppConstants.USER_SUBS_TYPE, response.getData().getUserDetails().getSubscribedUser());
            PreferenceStorage.setKey(AppConstants.USER_EMAIL, response.getData().getUserDetails().getEmail());
            PreferenceStorage.setKey(AppConstants.USER_PROFILE_IMG, response.getData().getUserDetails().getProfileImg());
            PreferenceStorage.setKey(AppConstants.USER_PHONE, response.getData().getUserDetails().getMobileNo());

            /*Intent myIntent = new Intent(MyLoginActivity.this, SubscriptionActivity.class);
            myIntent.putExtra("GotoPage", "MAIN");*/
            Intent myIntent = new Intent(MyLoginActivity.this, MainActivity.class);
            AppUtils.appStartIntent(MyLoginActivity.this, myIntent);
            MyLoginActivity.this.finish();
        } else if (response.getResponseHeader().getResponseCode().equalsIgnoreCase("-1")) {
            if (loginThrough == 1)
                AppUtils.showToast(getApplicationContext(), response.getResponseHeader().getResponseMessage());
            else if (loginThrough == 2 || loginThrough == 3) {
                Intent myIntent = new Intent(MyLoginActivity.this, SignUpActivity.class);
                myIntent.putExtra("Email", socialEmail);
                myIntent.putExtra("Name", socialName);
                myIntent.putExtra("Id", socialId);
                myIntent.putExtra("LoginType", loginThrough);
                loginThrough = AppConstants.REG_NORMAL;
                startActivity(myIntent);
            }
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes != null && myRes instanceof LoginData) {
            LoginData response = (LoginData) myRes;
//            if (response.getResponseHeader().getResponseCode().equalsIgnoreCase("0")) {
            if (loginThrough == 1)
                AppUtils.showToast(MyLoginActivity.this, response.getResponseHeader().getResponseMessage());
            else if (loginThrough == 2 || loginThrough == 3) {
                if (response.getResponseHeader().getResponseCode().equalsIgnoreCase("-2")) {
                    Intent myIntent = new Intent(MyLoginActivity.this, SignUpActivity.class);
                    Log.d("TAG_LOGIN", "socialEmail = " + socialEmail + " && socialName = "
                            + socialName + " && socialId = " + socialId + " && loginThrough = " + loginThrough);
                    myIntent.putExtra("Email", socialEmail);
                    myIntent.putExtra("Name", socialName);
                    myIntent.putExtra("Id", socialId);
                    myIntent.putExtra("LoginType", loginThrough);
                    loginThrough = AppConstants.REG_NORMAL;
                    startActivity(myIntent);
                }
            }
//            }
        }
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof TextInputEditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    public void setTextForLan() {
        try {
            try {
                String lgData = PreferenceStorage.getKey(CommonLangModel.login);
                loginStr = new Gson().fromJson(lgData, LanguageModel.Login.class);
            } catch (JsonSyntaxException e) {
                loginStr = new LanguageModel().new Login();
            }
            tilUsername.setHint(AppUtils.cleanLangStr(this, loginStr.getLg2_username(), R.string.hint_username_email)); //:TODO
            tilPassword.setHint(AppUtils.cleanLangStr(this, loginStr.getLg2_password(), R.string.hint_password));
            btnSignin.setText(AppUtils.cleanLangStr(this, loginStr.getLg2_sign_in(), R.string.txt_signin));
            tvForgotPwd.setText(AppUtils.cleanLangStr(this, loginStr.getLg2_forgot_password(), R.string.txt_forgot_pwd));
            tvSignUp.setText(AppUtils.cleanLangStr(this, loginStr.getLg2_signup_now(), R.string.txt_sign_up_now));
            tvAccountMsg.setText(AppUtils.cleanLangStr(this, loginStr.getLg2_don_t_have_an_a(), R.string.txt_req_sign_up));
            tvFacebook.setText(AppUtils.cleanLangStr(this, loginStr.getLg2_login_with_face(), R.string.login_facebook));
            tvGoogle.setText(AppUtils.cleanLangStr(this, loginStr.getLg2_login_with_goog(), R.string.login_google));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAppTheme() {
        int appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }
        btnSignin.setBackgroundColor(appColor);
        tvSignUp.setTextColor(appColor);
    }
}
