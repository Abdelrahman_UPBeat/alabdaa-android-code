package in.alibdaa.upbeat.digital.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import in.alibdaa.upbeat.digital.utils.AppConstants;

@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {


    Activity act;

    TextView showDate;

    long datelimit = 0;

    long setDate = 0;

    boolean checkToDate = false, isNoPast = false;

    String devicePattern;

    public DatePickerFragment(Activity act, TextView lblDate1, String setDate) {
        this.act = act;
        showDate = lblDate1;
        this.setDate = Long.valueOf(setDate);
        SimpleDateFormat deviceDateFormat = (SimpleDateFormat) android.text.format.DateFormat.getDateFormat(act.getApplicationContext());
        devicePattern = deviceDateFormat.toLocalizedPattern();
    }

    public DatePickerFragment(Activity act, TextView lblDate1, String startFrom, String setDate) {
        this.act = act;
        showDate = lblDate1;
        this.datelimit = Long.valueOf(startFrom);
        this.setDate = Long.valueOf(setDate);
        SimpleDateFormat deviceDateFormat = (SimpleDateFormat) android.text.format.DateFormat.getDateFormat(act.getApplicationContext());
        devicePattern = deviceDateFormat.toLocalizedPattern();
    }

    public DatePickerFragment() {
    }

    public DatePickerFragment(Activity act, EditText lblDate1, String startFrom, boolean checkToDate, String setDate) {
        this.act = act;
        showDate = lblDate1;
        this.datelimit = Long.valueOf(startFrom);
        this.checkToDate = checkToDate;
        this.setDate = Long.valueOf(setDate);
        SimpleDateFormat deviceDateFormat = new SimpleDateFormat(AppConstants.APP_DATE_FORMAT, Locale.ENGLISH);
        devicePattern = deviceDateFormat.toLocalizedPattern();
    }

    public DatePickerFragment(Activity act, EditText lblDate1, String startFrom, boolean checkToDate, String setDate, boolean isNoPast) {
        this.act = act;
        showDate = lblDate1;
        this.datelimit = Long.valueOf(startFrom);
        this.checkToDate = checkToDate;
        this.setDate = Long.valueOf(setDate);
        this.isNoPast = isNoPast;
        SimpleDateFormat deviceDateFormat = new SimpleDateFormat(AppConstants.APP_DATE_FORMAT, Locale.ENGLISH);
        devicePattern = deviceDateFormat.toLocalizedPattern();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        if (setDate > 0)
            c.setTimeInMillis(setDate);
        else if (!checkToDate && datelimit > 0)
            c.setTimeInMillis(datelimit);
        final int myear = c.get(Calendar.YEAR);
        final int mmonth = c.get(Calendar.MONTH);
        final int mday = c.get(Calendar.DAY_OF_MONTH);


        final Calendar calendar = Calendar.getInstance();
        if (datelimit != 0)
            calendar.setTimeInMillis(datelimit);
        final int minYear = calendar.get(Calendar.YEAR);
        final int minMonth = calendar.get(Calendar.MONTH);
        final int minDay = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, myear, mmonth, mday);
        if (isNoPast) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }

        if (!checkToDate && datelimit > 0) {
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        } else if (checkToDate && datelimit > 0) {
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        }

        datePickerDialog.getDatePicker().init(myear, mmonth, mday, new OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                if (checkToDate && datelimit != 0) {
                    if (newDate.after(calendar)) {
                        view.init(minYear, minMonth, minDay, this);
                    }
                } else if (!checkToDate && datelimit != 0) {
                    /*Calendar c1 = Calendar.getInstance();
                    c1.setTimeInMillis(datelimit);
                    Long date2 = c1.getTimeInMillis();
                    view.init(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH), this);*/
                    if (newDate.before(calendar)) {
                        view.init(minYear, minMonth, minDay, this);
                    }
                }
            }
        });
        /*if (checkToDate && datelimit != 0)
            datePickerDialog.getDatePicker().setMaxDate(datelimit);
        else if (!checkToDate && datelimit != 0)
            datePickerDialog.getDatePicker().setMinDate(datelimit);*/

        datePickerDialog.setCancelable(true);
        datePickerDialog.setCanceledOnTouchOutside(true);

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        Long date1 = c.getTimeInMillis();

        /*Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(datelimit);
        Long date2 = c1.getTimeInMillis();

        if (datelimit > 0) {

            if (!checkToDate) {
                if (date1 < date2) {
                    Toast.makeText(act, "To date should not be less than From date ", Toast.LENGTH_SHORT).show();
                *//*showDate.setTag(c1.getTimeInMillis());
                //int month = monthOfYear + 1;
                Date selectedDate = new Date(c1.getTimeInMillis());
                String finalDate = new SimpleDateFormat(devicePattern, Locale.getDefault()).format(selectedDate);
                showDate.setText(finalDate);*//*
                } else {
                    showDate.setTag(c.getTimeInMillis());
                    //int month = monthOfYear + 1;
                    Date selectedDate = new Date(c.getTimeInMillis());
                    String finalDate = new SimpleDateFormat(devicePattern, Locale.getDefault()).format(selectedDate);
                    showDate.setText(finalDate);
                }
            } else {
                showDate.setTag(c.getTimeInMillis());
                //int month = monthOfYear + 1;
                Date selectedDate = new Date(c.getTimeInMillis());
                String finalDate = new SimpleDateFormat(devicePattern, Locale.getDefault()).format(selectedDate);
                showDate.setText(finalDate);
            }

        } else {
            showDate.setTag(c.getTimeInMillis());
            //int month = monthOfYear + 1;
            Date selectedDate = new Date(c.getTimeInMillis());
            String finalDate = new SimpleDateFormat(devicePattern, Locale.getDefault()).format(selectedDate);
            showDate.setText(finalDate);
        }*/
        showDate.setTag(c.getTimeInMillis());
        //int month = monthOfYear + 1;
        Date selectedDate = new Date(c.getTimeInMillis());
        String finalDate = new SimpleDateFormat(devicePattern, Locale.ENGLISH).format(selectedDate);
        showDate.setText(finalDate);

        //showDate.setText(dayOfMonth + "/" + month + "/" + year);
        //showDate.setText(dayOfMonth + "/" + month + "/" + year+" "+CommonMembers.getDateTime(2));
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (setDate > 0) {
            showDate.setTag(setDate);
            Date selectedDate = new Date(setDate);
            String finalDate = new SimpleDateFormat(devicePattern, Locale.ENGLISH).format(selectedDate);
            showDate.setText(finalDate);
        } else {
            showDate.setTag(null);
            showDate.setText("");
        }
        super.onCancel(dialog);
    }

}
