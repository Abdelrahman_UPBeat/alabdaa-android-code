package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class ChangePasswordActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.tv_txt_cur_pwd)
    TextView tvTxtCurPwd;
    @BindView(R.id.tiet_cur_password)
    TextInputEditText tietCurPassword;
    @BindView(R.id.til_cur_password)
    TextInputLayout tilCurPassword;
    @BindView(R.id.tv_txt_new_pwd)
    TextView tvTxtNewPwd;
    @BindView(R.id.tiet_new_password)
    TextInputEditText tietNewPassword;
    @BindView(R.id.til_new_password)
    TextInputLayout tilNewPassword;
    @BindView(R.id.tv_txt_cnf_pwd)
    TextView tvTxtCnfPwd;
    @BindView(R.id.tiet_cnf_password)
    TextInputEditText tietCnfPassword;
    @BindView(R.id.til_cnf_password)
    TextInputLayout tilCnfPassword;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        btnSubmit.setBackgroundColor(appColor);

        tietCurPassword.addTextChangedListener(new RegisterTextWatcher(tietCurPassword));
        tietNewPassword.addTextChangedListener(new RegisterTextWatcher(tietNewPassword));
        tietCnfPassword.addTextChangedListener(new RegisterTextWatcher(tietCnfPassword));
    }

    private class RegisterTextWatcher implements TextWatcher {
        private View view;

        private RegisterTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (view.getId() == R.id.tiet_cur_password) {
                if (!tietCurPassword.getText().toString().isEmpty()) {
                    validateCurPassword();
                }
            } else if (view.getId() == R.id.tiet_new_password) {
                if (!tietNewPassword.getText().toString().isEmpty()) {
                    validatePassword();
                }
            } else if (view.getId() == R.id.tiet_cnf_password) {
                if (!tietCnfPassword.getText().toString().isEmpty()) {
                    validateRPassword();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private boolean validateCurPassword() {
        if (tietCurPassword.getText().toString().isEmpty()) {
            tietCurPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_password(), R.string.err_password));
            tietCurPassword.requestFocus();
            return false;
        } /*else if (!tietCurPassword.getText().toString().matches(AppConstants.passwordMatch)) {
            tietCurPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_password_must_b(), R.string.err_msg_password));
            tietCurPassword.requestFocus();
            return false;
        } */ else {
            tietCurPassword.setError(null);
        }
        return true;
    }

    private boolean validateRPassword() {
        if (tietCnfPassword.getText().toString().isEmpty()) {
            tietCnfPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_confirm_p(), R.string.err_confirm_pwd));
            tietCnfPassword.requestFocus();
            return false;
        } else if (!tietCnfPassword.getText().toString().equalsIgnoreCase(tietNewPassword.getText().toString())) {
            tietCnfPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_password_and_co(), R.string.err_match_pwd));
            tietCnfPassword.requestFocus();
            return false;
        } else {
            tietCnfPassword.setError(null);
        }
        return true;
    }

    private boolean validatePassword() {
        if (tietNewPassword.getText().toString().isEmpty()) {
            tietNewPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_password(), R.string.err_password));
            tietNewPassword.requestFocus();
            return false;
        }
//        else if (!tietNewPassword.getText().toString().matches(AppConstants.passwordMatch)) {
//            tietNewPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_password_must_b(), R.string.err_msg_password));
//            tietNewPassword.requestFocus();
//            return false;
//        }
        else {
            tietNewPassword.setError(null);
        }
        return true;
    }


    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {

        if (myRes instanceof EmptyData) {
            EmptyData emptyData = (EmptyData) myRes;
            Toast.makeText(this, emptyData.getResponseHeader().getResponseMessage(), Toast.LENGTH_SHORT).show();
            ChangePasswordActivity.this.finish();
        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        updatePassword();
    }

    private void updatePassword() {
        if (!validateCurPassword()) {
            return;
        } else if (!validatePassword()) {
            return;
        } else if (!validateRPassword()) {
            return;
        }

        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                //TODO : API call - Change password
                Call<EmptyData> classificationCall = apiService.postChangePassword(tietCurPassword.getText().toString(), tietCurPassword.getText().toString(), tietCurPassword.getText().toString(), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.CHANGEPASSWORD, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(ChangePasswordActivity.this, AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvTxtCurPwd.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_current_passwor(), R.string.txt_old_pwd));
        tvTxtNewPwd.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_new_password(), R.string.txt_new_pwd));
        tvTxtCnfPwd.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_confirm_passwor(), R.string.txt_cnf_pwd));
        setToolBarTitle(AppUtils.cleanLangStr(this, profileData.getLg4_change_password(), R.string.txt_change_pwd));
        btnSubmit.setText(AppUtils.cleanLangStr(this, profileData.getLg4_submit(), R.string.txt_submit));
    }
}
