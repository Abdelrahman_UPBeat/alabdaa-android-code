package in.alibdaa.upbeat.digital.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.POSTShowMoreReplies;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Hari on 05-02-2019.
 */

public class ShowMoreCommentsAdapter extends RecyclerView.Adapter {


    Context mContext;

    private int SELF = 1, LOADING = 2;
    public OnLoadMoreListener mOnLoadMoreListener;
    public List<POSTShowMoreReplies.Replies_list> mData = new ArrayList<POSTShowMoreReplies.Replies_list>();


    public ShowMoreCommentsAdapter(Context mContext, List<POSTShowMoreReplies.Replies_list> questionCommentList) {
        this.mContext = mContext;
        this.mData = questionCommentList;

    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_video_comments, parent, false);
        return new AnswerCommentsViewHolder(itemView);
//             if (viewType == LOADING) {
//            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
//            return new LoadingViewHolder(itemView);
//        } else if (viewType == SELF) {
//            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_video_comments, parent, false);
//            return new AnswerCommentsViewHolder(itemView);
//        }
//        return null;


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof AnswerCommentsViewHolder) {
            final AnswerCommentsViewHolder holder = (AnswerCommentsViewHolder) viewHolder;
            holder.tvUsername.setText(mData.get(position).getName());
            holder.tvComments.setText(mData.get(position).getReplies());
            holder.tvTime.setText(mData.get(position).getDays_ago());
            holder.tvReply.setVisibility(View.GONE);

            Picasso.with(mContext)
                    .load(AppConstants.BASE_URL + mData.get(position).getProfile_image())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.ivUserImg);
        }

    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

//    @Override
//    public int getItemViewType(int position) {
//        return mData.get(position) == null ? LOADING : SELF;
//    }

//    public void updateRecyclerView(Context mContext, List<POSTQuestionsCommentsList.Comment_detail> itemsData) {
//        this.mContext = mContext;
//        if (itemsData.size() > 0) {
//            this.mData.addAll(itemsData);
//            this.notifyDataSetChanged();
//        } else {
//            this.mData.clear();
//            this.notifyDataSetChanged();
//        }
//
//    }


    public class AnswerCommentsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_user_img)
        CircleImageView ivUserImg;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_comments)
        TextView tvComments;
        @BindView(R.id.tv_reply)
        TextView tvReply;

        public AnswerCommentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
