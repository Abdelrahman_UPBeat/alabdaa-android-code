package in.alibdaa.upbeat.digital;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.adapters.SubscriptionAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionData;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionSuccessModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class SubscriptionActivity extends AppCompatActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.rv_subs_list)
    RecyclerView rvSubsList;
    @BindView(R.id.tv_skipnow)
    TextView tvSkipnow;

    LinearLayoutManager mLayoutManager;
    SubscriptionAdapter subscriptionAdapter;
    ArrayList<SubscriptionData.SubscriptionList> subsList = new ArrayList<>();

    String fromPage = null;
    Window window;
    public int appColor = 0;
    public LanguageModel.Subscription subscription_used_texts = new LanguageModel().new Subscription();
    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        ButterKnife.bind(this);
        /*setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(AppUtils.cleanLangStr(this, subscriptionTexts.getLg9_subscription(), R.string.hint_subscription));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        tvSkipnow.setText(AppUtils.cleanLangStr(this, subscriptionTexts.getLg9_skip_now(), R.string.txt_skipnow));*/


       /* window = this.getWindow();
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.10);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.10);
        window.setLayout(width, height);*/
        fromPage = getIntent().getStringExtra("FromPage");
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvSubsList.setLayoutManager(mLayoutManager);
        subscriptionAdapter = new SubscriptionAdapter(this, new ArrayList<SubscriptionData.SubscriptionList>(), fromPage);
        rvSubsList.setAdapter(subscriptionAdapter);

    }

    private void getSubsDataList() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(SubscriptionActivity.this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            try {
                Call<SubscriptionData> classificationCall = apiService.getSubsDetails(PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(SubscriptionActivity.this, classificationCall, AppConstants.SUBSCRIPTION_DATA, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }

    @OnClick(R.id.tv_skipnow)
    public void onViewClicked() {
        moveToDashboard();
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {


        if (myRes instanceof SubscriptionSuccessModel) {
            ProgressDlg.dismissProgressDialog();
            SubscriptionSuccessModel subscriptionSuccessModel = (SubscriptionSuccessModel) myRes;
            Intent callStipeSuccessAct = new Intent(SubscriptionActivity.this, SubscriptionThankYouActivity.class);
            callStipeSuccessAct.putExtra("FromPage", fromPage);
            AppUtils.appStartIntent(SubscriptionActivity.this, callStipeSuccessAct);
            PreferenceStorage.setKey(AppConstants.USER_SUBS_TYPE, Integer.parseInt(subscriptionSuccessModel.getData().getSubscriberId()));
            finish();
        } else if (myRes instanceof SubscriptionData) {
            SubscriptionData myData = (SubscriptionData) myRes;
            if (myData.getData() != null && myData.getData().getSubscriptionList() != null && myData.getData().getSubscriptionList().size() > 0) {
                subsList = myData.getData().getSubscriptionList();
                subscriptionAdapter.updateRecyclerView(SubscriptionActivity.this, subsList);
            /*int width = (int) (getResources().getDisplayMetrics().widthPixels * 1.0);
            int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.80);
            window.setLayout(width, height);
            window.setGravity(Gravity.CENTER);*/
            } else {
                AppUtils.showToast(SubscriptionActivity.this, "Currently no subscriptions available. So you are entering into free mode!");
                moveToDashboard();
            }
        }


    }

    private void moveToDashboard() {
        Intent gotoDashboard = null;
        if (fromPage != null) {
            switch (fromPage) {
                case AppConstants.PAGE_LOGIN:
                    gotoDashboard = new Intent(SubscriptionActivity.this, MainActivity.class);
                    break;
            }
            if (gotoDashboard != null)
                AppUtils.appStartIntent(SubscriptionActivity.this, gotoDashboard);
        }

        finish();
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        AppUtils.showToast(SubscriptionActivity.this, "Currently no subscriptions available. So you are entering into free mode!");
        moveToDashboard();
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        setAppTheme();
        getLocaleData();
        setSupportActionBar(mToolbar);
        try {
            mToolbar.setTitle(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_subscription(), R.string.hint_subscription));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mToolbar.setBackgroundColor(appColor);
            tvSkipnow.setText(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_skip_now(), R.string.txt_skipnow));
            tvSkipnow.setTextColor(appColor);
            getSubsDataList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.subscription);
            subscription_used_texts = new Gson().fromJson(commonDataStr, LanguageModel.Subscription.class);
        } catch (Exception e) {
            subscription_used_texts = new LanguageModel().new Subscription();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setAppTheme() {
        appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }
    }

    public void finishActivity() {
        finish();
    }


    public void subscribeUser(String subID) {

        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(SubscriptionActivity.this, null, null);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

            Call<SubscriptionSuccessModel> subscriptionSuccessModelCall = apiInterface.postSuccessSubscription(subID, "free", "", PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(SubscriptionActivity.this, subscriptionSuccessModelCall, AppConstants.SUBSCRIPTIONSUCCESS, SubscriptionActivity.this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }


    }


}