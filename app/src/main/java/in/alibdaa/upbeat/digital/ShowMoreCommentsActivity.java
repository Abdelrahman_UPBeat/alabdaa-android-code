package in.alibdaa.upbeat.digital;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import in.alibdaa.upbeat.digital.adapters.ShowMoreCommentsAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.POSTQuestionsCommentsList;
import in.alibdaa.upbeat.digital.datamodel.POSTSendReplies;
import in.alibdaa.upbeat.digital.datamodel.POSTShowMoreReplies;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowMoreCommentsActivity extends AppCompatActivity {


    @BindView(R.id.tb_toolbar)
    Toolbar tbToolbar;
    @BindView(R.id.iv_user_img)
    CircleImageView ivUserImg;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_comments)
    TextView tvComments;
    @BindView(R.id.rv_reply_comments)
    RecyclerView rvReplyComments;
    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.btn_writecomment)
    ImageView btnWritecomment;
    @BindView(R.id.cv_send_comments)
    CardView cvSendComments;
    public POSTQuestionsCommentsList.CommentsList comment_details;
    ApiInterface apiInterface;
    LinearLayoutManager linearLayoutManager;
    ShowMoreCommentsAdapter showMoreCommentsAdapter;
    //    CustomProgressDialog customProgressDialog;
    String replies = "", question_id = "", isReplied = "";
    POSTShowMoreReplies.Replies_list comment_detail = new POSTShowMoreReplies.Replies_list();
    List<POSTShowMoreReplies.Replies_list> replies_details = new ArrayList<>();
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_comments);
        ButterKnife.bind(this);
        setSupportActionBar(tbToolbar);
        getLocaleData();
        tbToolbar.setTitle(AppUtils.cleanLangStr(this, commonData.getLg7_replies(), R.string.replies));
        etComment.setHint(AppUtils.cleanLangStr(this, commonData.getLg7_write_a_comment(), R.string.write_a_comment));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        customProgressDialog = new CustomProgressDialog(this);
        Bundle bundle = this.getIntent().getExtras();
        if (bundle.getSerializable(AppConstants.CommentList) != null) {
            comment_details = (POSTQuestionsCommentsList.CommentsList) bundle.getSerializable(AppConstants.CommentList);
        }

        if (getIntent().getStringExtra(AppConstants.QuestionID) != null) {
            question_id = getIntent().getStringExtra(AppConstants.QuestionID);
        }

        if (comment_details != null) {
            tvComments.setText(comment_details.getComment());
            tvTime.setText(comment_details.getDaysAgo());
            tvUsername.setText(comment_details.getName());
            Picasso.with(this)
                    .load(AppConstants.BASE_URL)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(ivUserImg);
        }

        linearLayoutManager = new LinearLayoutManager(ShowMoreCommentsActivity.this);
        rvReplyComments.setLayoutManager(linearLayoutManager);
        showMoreCommentsAdapter = new ShowMoreCommentsAdapter(ShowMoreCommentsActivity.this, replies_details);
        rvReplyComments.setAdapter(showMoreCommentsAdapter);

        if (getIntent().getStringExtra(AppConstants.isShowMore) != null) {
            postShowMoreReplies();
        }

    }


    public void postShowMoreReplies() {
        if (AppUtils.isNetworkAvailable(this)) {
//            customProgressDialog.showDialog();
            apiInterface.postCommentsReplyList(PreferenceStorage.getKey(AppConstants.USER_TOKEN), comment_details.getCommentId()).enqueue(new Callback<POSTShowMoreReplies>() {
                @Override
                public void onResponse(Call<POSTShowMoreReplies> call, Response<POSTShowMoreReplies> response) {
//                    customProgressDialog.dismiss();
                    if (response.body().getResponse().getResponse_code().equalsIgnoreCase("1")) {
                        showMoreCommentsAdapter.mData.addAll(response.body().getData().getReplies_list());
                        showMoreCommentsAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<POSTShowMoreReplies> call, Throwable t) {
//                    customProgressDialog.dismiss();
                }
            });
        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.err_no_internet));
        }

    }

    public void postSendComments() {
        if (AppUtils.isNetworkAvailable(this)) {
//            customProgressDialog.showDialog();
            apiInterface.postSendReplies(PreferenceStorage.getKey(AppConstants.USER_TOKEN), question_id, replies, comment_details.getCommentId()).enqueue(new Callback<POSTSendReplies>() {
                @Override
                public void onResponse(Call<POSTSendReplies> call, Response<POSTSendReplies> response) {
//                    customProgressDialog.dismiss();
                    if (response.body().getResponse().getResponse_code().equalsIgnoreCase("1")) {
                        replies = "";
                        comment_detail = new POSTShowMoreReplies.Replies_list();
                        comment_detail.setComment_id(response.body().getData().getComment_id());
                        comment_detail.setDays_ago(response.body().getData().getDays_ago());
                        comment_detail.setName(response.body().getData().getName());
                        comment_detail.setReplies(response.body().getData().getReplies());
                        comment_detail.setUser_id(response.body().getData().getComment_id());
                        comment_detail.setProfile_image(response.body().getData().getProfile_image());
//                    questionCommentList.add(0, comment_detail);
                        showMoreCommentsAdapter.mData.add(comment_detail);
                        showMoreCommentsAdapter.notifyDataSetChanged();
                        rvReplyComments.smoothScrollToPosition(0);
                        etComment.setText("");
                        isReplied = "1";
//                    tvTotalcomments.setText(String.valueOf(showMoreCommentsAdapter.mData.size()));
                    }
                }

                @Override
                public void onFailure(Call<POSTSendReplies> call, Throwable t) {
//                    customProgressDialog.dismiss();
                }
            });
        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.err_no_internet));
        }
    }

    @OnClick(R.id.btn_writecomment)
    public void onViewClicked(View view) {

        if (!etComment.getText().toString().isEmpty()) {
            replies = etComment.getText().toString();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            postSendComments();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra(AppConstants.CommentsID, comment_details.getCommentId());
            intent.putExtra(AppConstants.isReplied, isReplied);
            setResult(Activity.RESULT_OK, intent);
            finish();
//            overridePendingTransition(0, R.anim.slide_down);
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(AppConstants.CommentsID, comment_details.getCommentId());
        intent.putExtra(AppConstants.isReplied, isReplied);
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
//        overridePendingTransition(0, R.anim.slide_down);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();

        }
    }
}
