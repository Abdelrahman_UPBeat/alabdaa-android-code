package in.alibdaa.upbeat.digital;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.adapters.ViewPagerAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderHistoryModel;
import in.alibdaa.upbeat.digital.fragments.BookingMyProvidersFragment;
import in.alibdaa.upbeat.digital.fragments.BookingMyProvidersHistoryFragment;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

public class ProviderHistoryActivity extends BaseNavigationActivity implements ViewPager.OnPageChangeListener, RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.sliding_tabs)
    TabLayout slidingTabs;
    @BindView(R.id.vp_history)
    ViewPager historyViewPager;
    @BindView(R.id.fab_hist_list)
    FloatingActionButton fabHistList;

    public int hPendingNextPage = 1, hPendingPageNo, hCompletedNextPage = 1, hCompletedPageNo;
    ProviderHistoryModel hPendingData, hCompletedData;

    BookingMyProvidersFragment hMyBookings;
    BookingMyProvidersHistoryFragment hMyHistory;

    int hisStatus = 1, hisReqType = 1;

    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_history_request_list);
        ButterKnife.bind(this);
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_provider_histor(), R.string.txt_provider_history));
        getLocaleData();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragments to adapter
        hMyBookings = new BookingMyProvidersFragment();
        hMyBookings.bookingMyProvidersFragment(ProviderHistoryActivity.this, ProviderHistoryActivity.this);
        adapter.addFragment(hMyBookings, AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_my_bookings(), R.string.my_bookings));

        hMyHistory = new BookingMyProvidersHistoryFragment();
        hMyHistory.bookingMyProvidersHistoryFragment(ProviderHistoryActivity.this, ProviderHistoryActivity.this);
        adapter.addFragment(hMyHistory, AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_history(), R.string.history));

        historyViewPager.setAdapter(adapter);
        historyViewPager.setOnPageChangeListener(this);
        historyViewPager.setOffscreenPageLimit(0);

        //Tab
        slidingTabs.setupWithViewPager(historyViewPager);
        slidingTabs.setSelectedTabIndicatorColor(appColor);

        fabHistList.setBackgroundTintList(ColorStateList.valueOf(appColor));
        fabHistList.setVisibility(View.GONE);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0://My Bookings
                try {
                    hMyBookings.getMyBookingsList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 1://History
                try {
                    hMyHistory.getMyHistoryBookingsList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (historyViewPager != null && historyViewPager.getCurrentItem() == 1) {
                hMyHistory.getMyHistoryBookingsList();
            } else if (historyViewPager != null && historyViewPager.getCurrentItem() == 0) {
                hMyBookings.getMyBookingsList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getLocaleData() {

        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

//        switch (historyViewPager.getCurrentItem()) {
//            case 0:
//                hPendingData = (ProviderHistoryModel) myRes;
//                hMyBookings.mAdapter.itemsData = new ArrayList<>();
//                hMyBookings.mAdapter.updateRecyclerView(ProviderHistoryActivity.this, hPendingData.getData().getBooking_list());
//                break;
//
//            case 1:
//                hCompletedData = (ProviderHistoryModel) myRes;
//                hMyHistory.mAdapter.itemsData = new ArrayList<>();
//                hMyHistory.mAdapter.updateRecyclerView(ProviderHistoryActivity.this, hCompletedData.getData().getBooking_list());
//        }


    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }


}

