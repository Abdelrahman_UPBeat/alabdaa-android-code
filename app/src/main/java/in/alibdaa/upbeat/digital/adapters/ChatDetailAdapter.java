package in.alibdaa.upbeat.digital.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.ChatDetailListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

/**
 * Created by Hari on 14-05-2018.
 */

public class ChatDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    public ArrayList<ChatDetailListData.ChatList> itemsData = new ArrayList<>();
    public OnLoadMoreListener mOnLoadMoreListener;
    HashMap<String, ArrayList<ChatDetailListData.ChatList>> hashMap = new HashMap<String, ArrayList<ChatDetailListData.ChatList>>();
    ArrayList<ChatDetailListData.ChatList> tempData;

    String key = "";

    private int SELF = 1, OTHER = 2, LOADING = 3;
    private int lastPosition = -1;
    private int lastPosition1 = -1;


    public ChatDetailAdapter(Context mContext, ArrayList<ChatDetailListData.ChatList> itemsData) {
        this.mContext = mContext;
        this.itemsData = itemsData;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == LOADING) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(itemView);
        } else if (viewType == SELF) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_from_msg, parent, false);
            return new ChatDetailViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_to_msg, parent, false);
            return new ChatDetailViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof ChatDetailViewHolder) {
            ChatDetailViewHolder chatDetailViewHolder = (ChatDetailViewHolder) holder;

            if (key.isEmpty()) {
            } else if (hashMap.containsKey(itemsData.get(position).getDate())) {
                chatDetailViewHolder.setDate.setText(itemsData.get(position).getDate());
                chatDetailViewHolder.setDate.setVisibility(View.VISIBLE);
            }
            if (!itemsData.get(position).getChatUtcTime().isEmpty()) {
                chatDetailViewHolder.tvFromMsgTime.setVisibility(View.VISIBLE);
                String aDate = itemsData.get(position).getChatUtcTime();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm aa");
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date dates = simpleDateFormat.parse(aDate);
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    String formattedDate = simpleDateFormat2.format(dates);
                    chatDetailViewHolder.tvFromMsgTime.setText(formattedDate);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
                setAnimationRight(chatDetailViewHolder.itemView, position);
            } else {
                chatDetailViewHolder.tvFromMsgTime.setVisibility(View.GONE);
            }

            if (!itemsData.get(position).getContent().isEmpty()) {
                chatDetailViewHolder.tvFromMsg.setText(itemsData.get(position).getContent());
            } else {
                holder.itemView.setVisibility(View.GONE);
            }
            setAnimationLeft( chatDetailViewHolder.itemView, position);
        }
    }

    private void setAnimationLeft(View viewToAnimate, int position) {
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.slide_in_left
                        : R.anim.slide_in_left);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
    }

    private void setAnimationRight(View viewToAnimate, int position) {
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition1) ? R.anim.slide_in_right
                        : R.anim.slide_in_right);
        viewToAnimate.startAnimation(animation);
        lastPosition1 = position;
    }

    @Override
    public int getItemViewType(int position) {
        if (itemsData.get(position) == null) {
            return LOADING;
        } else if (itemsData.get(position).getChatFrom().equals(String.valueOf(PreferenceStorage.getIntKey(AppConstants.USER_ID)))) {
            return SELF;
        } else {
            return OTHER;
        }

    }


    @Override
    public int getItemCount() {
        return itemsData == null ? 0 : itemsData.size();
    }

    public void updateRecyclerView(Context mContext, ArrayList<ChatDetailListData.ChatList> itemsData) {
        this.mContext = mContext;

        for (int i = 0; i < itemsData.size(); i++) {
            if (hashMap.containsKey(itemsData.get(i).getDate())) {
                ArrayList<ChatDetailListData.ChatList> list = hashMap.get(itemsData.get(i).getDate());
                list.add(itemsData.get(i));
            } else {
                ArrayList<ChatDetailListData.ChatList> list = new ArrayList<ChatDetailListData.ChatList>();
                list.add(itemsData.get(i));
                hashMap.put(itemsData.get(i).getDate(), list);
            }
        }

        this.itemsData.addAll(0, itemsData);
        notifyDataSetChanged();
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar1);
        }
    }

    static class ChatDetailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_from_msg)
        TextView tvFromMsg;
        @BindView(R.id.tv_from_msg_time)
        TextView tvFromMsgTime;
        @BindView(R.id.tv_date)
        TextView setDate;

        public ChatDetailViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
