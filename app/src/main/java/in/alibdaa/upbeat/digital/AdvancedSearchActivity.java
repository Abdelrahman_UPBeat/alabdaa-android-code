package in.alibdaa.upbeat.digital;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import in.alibdaa.upbeat.digital.adapters.DialogEditCategoryListAdapter;
import in.alibdaa.upbeat.digital.adapters.DialogEditSubCategoryListAdapter;
import in.alibdaa.upbeat.digital.datamodel.CategoryList;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.SubCategoryList;
import in.alibdaa.upbeat.digital.fragments.DatePickerFragment;
import in.alibdaa.upbeat.digital.interfaces.OnSetMyLocation;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.InputFilterMinMax;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import retrofit2.Call;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class AdvancedSearchActivity extends AppCompatActivity implements RetrofitHandler.RetrofitResHandler, OnSetMyLocation {

    public static Context mContext;
    public String latitude, longitude, address;
    @BindView(R.id.tv_txt_appnt_date)
    TextView tvTxtAppntDate;
    @BindView(R.id.et_appnt_date)
    EditText etAppntDate;
    @BindView(R.id.btn_clr_appnt_date)
    Button btnClrAppntDate;
    @BindView(R.id.tv_txt_time)
    TextView tvTxtTime;
    @BindView(R.id.et_time)
    EditText etTime;
    @BindView(R.id.btn_clr_time)
    Button btnClrTime;
    @BindView(R.id.tv_txt_location)
    TextView tvTxtLocation;
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.btn_clr_location)
    Button btnClrLocation;
    @BindView(R.id.tv_txt_price)
    TextView tvTxtPrice;
    @BindView(R.id.et_price_min)
    EditText etPriceMin;
    @BindView(R.id.tv_txt_to)
    TextView tvTxtTo;
    @BindView(R.id.et_price_max)
    EditText etPriceMax;
    @BindView(R.id.btn_clr_price)
    Button btnClrPrice;
    @BindView(R.id.btn_search)
    Button btnSearch;
    String searchType;
    @BindView(R.id.ll_date)
    LinearLayout llDate;
    @BindView(R.id.ll_time)
    LinearLayout llTime;
    @BindView(R.id.ll_location)
    LinearLayout llLocation;
    @BindView(R.id.ll_price)
    LinearLayout llPrice;
    @BindView(R.id.tb_toolbar)
    Toolbar tbToolbar;

    Window window;
    public int appColor = 0;
    public LanguageModel.Common_used_texts commonTexts = new LanguageModel().new Common_used_texts();

    public ArrayList permissionsToRequest;
    public ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    @BindView(R.id.rl_adv_search)
    RelativeLayout rlAdvSearch;
    @BindView(R.id.iv_loc)
    ImageView ivLoc;
    @BindView(R.id.tv_loc_text)
    TextView tvLocText;
    @BindView(R.id.tv_loc_sub_text)
    TextView tvLocSubText;
    @BindView(R.id.bt_turn_loc)
    Button btTurnLoc;
    @BindView(R.id.layout_permission_check)
    LinearLayout layoutPermissionCheck;
    @BindView(R.id.tv_txt_category)
    TextView tvTxtCategory;
    @BindView(R.id.et_category)
    EditText etCategory;
    @BindView(R.id.btn_clr_category)
    Button btnClrCategory;
    @BindView(R.id.ll_category)
    LinearLayout llCategory;
    @BindView(R.id.tv_txt_subcategory)
    TextView tvTxtSubcategory;
    @BindView(R.id.et_subcategory)
    EditText etSubcategory;
    @BindView(R.id.btn_clr_subcategory)
    Button btnClrSubcategory;
    @BindView(R.id.ll_subcategory)
    LinearLayout llSubcategory;
    DialogEditCategoryListAdapter categoryListAdapter;
    DialogEditSubCategoryListAdapter subCategoryListAdapter;
    public String cat_id = "", subCatID = "";
    public Button btnDone;
    public Button btnCatDone, btnsubCatDone;
    String[] category;
    String[] subcategory;
    AlertDialog dialog, sucCatDialog;
    List<CategoryList.Category_list> category_list = new ArrayList<>();
    List<SubCategoryList.Category_list> subcategory_list = new ArrayList<>();
    LayoutInflater CatInflater, subCatInflater;
    View catCustomView, subCatCustomView;
    RecyclerView rvCategoryList, rvsubCategoryList;
    TextView tvTitle, tvsubCatTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_advanced);
        ButterKnife.bind(this);
        mContext = this;

        initCategories();
        initSubCategories();

        searchType = getIntent().getStringExtra("SearchType");

        etPriceMax.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100000000")});
        etPriceMin.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100000000")});

        if (searchType.equalsIgnoreCase(AppConstants.SEARCH_REQUEST)) {

            tvTxtCategory.setVisibility(View.GONE);
            tvTxtSubcategory.setVisibility(View.GONE);
            llCategory.setVisibility(View.GONE);
            llSubcategory.setVisibility(View.GONE);

        } else if (searchType.equalsIgnoreCase(AppConstants.SEARCH_PROVIDER)) {
            tvTxtPrice.setVisibility(View.GONE);
            tvTxtTime.setVisibility(View.GONE);
            llTime.setVisibility(View.GONE);
            llPrice.setVisibility(View.GONE);
        }

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        latitude = PreferenceStorage.getKey(AppConstants.MY_LATITUDE);
        longitude = PreferenceStorage.getKey(AppConstants.MY_LONGITUDE);
        categoryListAdapter = new DialogEditCategoryListAdapter(this, category_list, dialog, etCategory, etSubcategory, cat_id, subCatID, btnCatDone);
        subCategoryListAdapter = new DialogEditSubCategoryListAdapter(this, subcategory_list, sucCatDialog, etSubcategory, subCatID, btnsubCatDone);
    }

    @Override
    public void onLocationSet(String latitude, String longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        etLocation.setText(address);
    }

    @OnClick({R.id.et_appnt_date, R.id.btn_clr_appnt_date, R.id.et_time, R.id.btn_clr_time, R.id.et_location, R.id.btn_clr_location, R.id.btn_clr_price, R.id.btn_search, R.id.btn_clr_category, R.id.btn_clr_subcategory, R.id.et_category, R.id.et_subcategory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_appnt_date:
                String setDate = String.valueOf(System.currentTimeMillis());
                try {
                    setDate = etAppntDate.getTag().toString();
                } catch (Exception e) {
                }
                DatePickerFragment newFragment = new DatePickerFragment(this, etAppntDate, "0", false, setDate);
                newFragment.show(getFragmentManager(), getString(R.string.txt_as_appnt_date));//TODO: lang
                break;
            case R.id.btn_clr_appnt_date:
                etAppntDate.setText("");
                etAppntDate.setTag(null);
                break;
            case R.id.et_time:
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM;
                        if (selectedHour < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        selectedHour = selectedHour > 12 ? selectedHour - 12 : selectedHour;
                        String hr = selectedHour < 10 ? "0" + selectedHour : "" + selectedHour;
                        String min = selectedMinute < 10 ? "0" + selectedMinute : "" + selectedMinute;
                        String totTime = hr + ":" + min + " " + AM_PM;
                        etTime.setText(totTime);
                    }
                }, hour, minute, false).show();
                break;
            case R.id.btn_clr_time:
                etTime.setText("");
                break;
            case R.id.et_location:
                Intent callMapActivity = new Intent(this, MapActivity.class);
                callMapActivity.putExtra("From", AppConstants.PAGE_ADVANCED_SEARCH);
                callMapActivity.putExtra("Latitude", latitude);
                callMapActivity.putExtra("Longitude", longitude);
                callMapActivity.putExtra("Address", address);
                AppUtils.appStartIntent(this, callMapActivity);
                break;
            case R.id.btn_clr_location:
                etLocation.setText("");
                break;
            case R.id.btn_clr_price:
                etPriceMin.setText("");
                etPriceMax.setText("");
                break;
            case R.id.btn_search:
                buildSearchParams();
                break;
            case R.id.btn_clr_category:
                etCategory.setText("");
                etSubcategory.setText("");
                break;
            case R.id.btn_clr_subcategory:
                etSubcategory.setText("");
                break;
            case R.id.et_category:
                if (AppUtils.isNetworkAvailable(this)) {

                    ProgressDlg.showProgressDialog(this, null, null);
                    ApiInterface apiService =
                            ApiClient.getClientNoHeader().create(ApiInterface.class);
                    try {
                        Call<CategoryList> classificationCall = apiService.getCategories(PreferenceStorage.getKey(AppConstants.USER_TOKEN));
                        RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.CATEGORIES, this, false);
                    } catch (Exception e) {
                        ProgressDlg.dismissProgressDialog();
                        e.printStackTrace();
                    }

                } else {
                    AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
                }
                break;
            case R.id.et_subcategory:
                if (categoryListAdapter.isAvaialble) {
                    if (!etCategory.getText().toString().isEmpty()) {
                        if (AppUtils.isNetworkAvailable(this)) {

                            ProgressDlg.showProgressDialog(this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClientNoHeader().create(ApiInterface.class);
                            try {
                                Call<SubCategoryList> classificationCall = apiService.getSubCategories(PreferenceStorage.getKey(AppConstants.USER_TOKEN), categoryListAdapter.cat_id);
                                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.SUBCATEGORIES, this, false);
                            } catch (Exception e) {
                                ProgressDlg.dismissProgressDialog();
                                e.printStackTrace();
                            }

                        } else {
                            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
                        }
                    } else {
                        Toast.makeText(this, "Select Category first", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "No Sub Category Available", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void buildSearchParams() {
        String location = etLocation.getText().toString();
        String reqDate = "";
        if (!etAppntDate.getText().toString().isEmpty()) {
            reqDate = AppUtils.formatDateToServer(etAppntDate.getText().toString());
        }

        String reqTime = etTime.getText().toString();
        String minPrice = etPriceMin.getText().toString();
        String maxPrice = etPriceMax.getText().toString();
        String category = categoryListAdapter.cat_id; //TODO: Category
        String subCategory = subCategoryListAdapter.subCatID; //TODO: SUBCategory


        try {
            if (location.isEmpty()
                    && reqDate.isEmpty()
                    && reqTime.isEmpty()
                    && minPrice.isEmpty()
                    && maxPrice.isEmpty()
                    && etCategory.getText().toString().isEmpty()) {
            } else {
                Intent searchResult = new Intent(this, SearchResultActivity.class);
                searchResult.putExtra("latitude", latitude);
                searchResult.putExtra("longitude", longitude);
                searchResult.putExtra("search_title", "");
                searchResult.putExtra("request_date", reqDate);
                searchResult.putExtra("min_price", minPrice);
                searchResult.putExtra("max_price", maxPrice);
                searchResult.putExtra("location", location);
                searchResult.putExtra("request_time", reqTime);
                searchResult.putExtra("searchType", searchType);
                searchResult.putExtra("category", category);
                searchResult.putExtra("subcategory", subCategory);
                AppUtils.appStartIntent(this, searchResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        setAppTheme();
        setSupportActionBar(tbToolbar);
        tbToolbar.setTitle(AppUtils.cleanLangStr(this, commonTexts.getLg7_advanced_search(), R.string.txt_advanced_search));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tbToolbar.setBackgroundColor(appColor);
        tvLocText.setText(AppUtils.cleanLangStr(this, commonTexts.getLg7_location_access(), R.string.loc_text));
        tvLocSubText.setText(AppUtils.cleanLangStr(this, commonTexts.getLg7_let_us_know_whe(), R.string.loc_sub_text));
        btTurnLoc.setText(AppUtils.cleanLangStr(this, commonTexts.getLg7_turn_on_locatio(), R.string.turn_on_loc));
        ivLoc.setColorFilter(appColor, PorterDuff.Mode.SRC_ATOP);
        btTurnLoc.setBackgroundColor(appColor);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest == null && permissionsToRequest.size() != 0) {
            rlAdvSearch.setVisibility(View.GONE);
            layoutPermissionCheck.setVisibility(View.VISIBLE);
        } else {
            rlAdvSearch.setVisibility(View.VISIBLE);
            layoutPermissionCheck.setVisibility(View.GONE);
        }
        tvTxtTime.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_time(), R.string.txt_time));
        tvTxtPrice.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_price(), R.string.txt_price));
        tvTxtAppntDate.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_appointment_dat(), R.string.txt_as_appnt_date));
        tvTxtLocation.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_location(), R.string.txt_location));
        tvTxtTo.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_to(), R.string.txt_to));
        tvTxtCategory.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_category(), R.string.txt_category));
        tvTxtSubcategory.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_sub_category(), R.string.txt_subcategory));
        btnSearch.setText(AppUtils.cleanLangStr(AdvancedSearchActivity.this, commonTexts.getLg7_search(), R.string.txt_search));
        btnSearch.setBackgroundColor(appColor);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonTexts = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonTexts = new LanguageModel().new Common_used_texts();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setAppTheme() {
        appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }

        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }

    }

    public ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void checkLocationPermission() {
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }

    @OnClick(R.id.bt_turn_loc)
    public void onViewClicked() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    public void showCategoryPopupWindow(List<CategoryList.Category_list> categoryList) {


//        categoryListAdapter = new CategoryListAdapter(getActivity())

        tvTitle.setText(AppUtils.cleanLangStr(this, commonTexts.getLg7_category(), R.string.txt_category));

//        String[] values = providerDataDetail.getCategory().split(",");
        category_list.clear();
        category_list.addAll(categoryList);
        category = categoryListAdapter.cat_id.split(",");

        if (category.length > 0)
            for (int i = 0; i < category.length; i++) {
                for (int i1 = 0; i1 < category_list.size(); i1++) {
                    if (category_list.get(i1).getCatrgory_id().equalsIgnoreCase(category[i])) {
                        category_list.get(i1).setIs_checked(true);
                    }
                }
            }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCategoryList.setLayoutManager(linearLayoutManager);
        rvCategoryList.setAdapter(categoryListAdapter);
        categoryListAdapter.notifyDataSetChanged();

        dialog.show();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


    }

    public void showSubCategoryPopupWindow(List<SubCategoryList.Category_list> subCategory_list) {
        subcategory_list.clear();
        subcategory_list.addAll(subCategory_list);
        subcategory = subCategoryListAdapter.subCatID.split(",");

        if (subcategory.length > 0) {
            for (int i = 0; i < subcategory.length; i++) {
                for (int i1 = 0; i1 < subCategory_list.size(); i1++) {
                    if (subCategory_list.get(i1).getSubcategory_id().equalsIgnoreCase(subcategory[i])) {
                        subCategory_list.get(i1).setChecked(true);
                    }
                }
            }
        }


        tvsubCatTitle.setText(AppUtils.cleanLangStr(this, commonTexts.getLg7_sub_category(), R.string.txt_subcategory));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvsubCategoryList.setLayoutManager(linearLayoutManager);
        rvsubCategoryList.setAdapter(subCategoryListAdapter);
        subCategoryListAdapter.notifyDataSetChanged();
        sucCatDialog.show();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

        if (myRes instanceof CategoryList) {
            CategoryList categoryList = (CategoryList) myRes;
            if (categoryList.getData().getCategory_list().size() > 0) {
                showCategoryPopupWindow(categoryList.getData().getCategory_list());
            } else {
                Toast.makeText(mContext, "No Categories Available", Toast.LENGTH_SHORT).show();
            }

        } else if (myRes instanceof SubCategoryList) {
            SubCategoryList subCategoryList = (SubCategoryList) myRes;
            if (subCategoryList.getData().getCategory_list().size() > 0) {
                showSubCategoryPopupWindow(subCategoryList.getData().getCategory_list());
            } else {
                Toast.makeText(mContext, "No SubCategories Available", Toast.LENGTH_SHORT).show();

            }

        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    private void initSubCategories() {
        subCatInflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        subCatCustomView = subCatInflater.inflate(R.layout.dialog_category, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setView(subCatCustomView);
        alertDialogBuilder.setCancelable(false);
        sucCatDialog = alertDialogBuilder.create();
        rvsubCategoryList = subCatCustomView.findViewById(R.id.rv_categorylist);
        tvsubCatTitle = subCatCustomView.findViewById(R.id.tv_title);
        btnsubCatDone = subCatCustomView.findViewById(R.id.btn_done);
        sucCatDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    private void initCategories() {
        CatInflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        catCustomView = CatInflater.inflate(R.layout.dialog_category, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setView(catCustomView);
        alertDialogBuilder.setCancelable(false);
        dialog = alertDialogBuilder.create();
        rvCategoryList = catCustomView.findViewById(R.id.rv_categorylist);
        tvTitle = catCustomView.findViewById(R.id.tv_title);
        btnCatDone = catCustomView.findViewById(R.id.btn_done);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

}
