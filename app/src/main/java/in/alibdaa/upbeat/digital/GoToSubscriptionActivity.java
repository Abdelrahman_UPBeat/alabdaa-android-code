package in.alibdaa.upbeat.digital;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

public class GoToSubscriptionActivity extends AppCompatActivity {

    @BindView(R.id.tv_buy_subs)
    TextView tvBuySubs;
    @BindView(R.id.tv_thankyou)
    TextView tvThankyou;
    @BindView(R.id.bt_goto_subs)
    Button btGotoSubs;
    String fromPage;
    Window window;
    public int appColor = 0;
    public LanguageModel.Subscription subscription_used_texts = new LanguageModel().new Subscription();
    @BindView(R.id.iv_subs)
    ImageView ivSubs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goto_subscription);
        ButterKnife.bind(this);
        fromPage = getIntent().getStringExtra("FromPage");
    }

    @OnClick(R.id.bt_goto_subs)
    public void onViewClicked() {
        Intent callSubscriptionAct = new Intent(GoToSubscriptionActivity.this, SubscriptionActivity.class);
        callSubscriptionAct.putExtra("FromPage", fromPage);
        AppUtils.appStartIntent(GoToSubscriptionActivity.this, callSubscriptionAct);
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getLocaleData();
            setAppTheme();
            tvThankyou.setText(AppUtils.cleanLangStr(GoToSubscriptionActivity.this, subscription_used_texts.getLg9_thank_you_for_u(), R.string.thank_you_upgrade));
            tvBuySubs.setText(AppUtils.cleanLangStr(GoToSubscriptionActivity.this, subscription_used_texts.getLg9_buy_subscriptio(), R.string.buy_subscription));
            btGotoSubs.setText(AppUtils.cleanLangStr(GoToSubscriptionActivity.this, subscription_used_texts.getLg9_go_to_subscript(), R.string.gotoSubscription));
            btGotoSubs.setBackgroundColor(appColor);
            tvBuySubs.setTextColor(appColor);
            ivSubs.setColorFilter(appColor, PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.subscription);
            subscription_used_texts = new Gson().fromJson(commonDataStr, LanguageModel.Subscription.class);
        } catch (Exception e) {
            subscription_used_texts = new LanguageModel().new Subscription();
        }
    }


    public void setAppTheme() {
        appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            ivSubs.setColorFilter(appColor, android.graphics.PorterDuff.Mode.MULTIPLY);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }

    }

}
