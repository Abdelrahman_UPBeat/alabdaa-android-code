package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.ProvAvailData;
import in.alibdaa.upbeat.digital.utils.AppUtils;

public class ProviderAvailAdapter extends RecyclerView.Adapter<ProviderAvailAdapter.ViewHolder> {

    ArrayList<ProvAvailData> availList;
    Context mContext;
    Activity mActivity;

    public ProviderAvailAdapter(Activity mActivity, ArrayList<ProvAvailData> availList, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mActivity.getBaseContext();
        this.availList = availList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_provider_avail, parent, false);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        viewHolder.tvAvailDay.setText(AppUtils.cleanString(mContext, availList.get(position).getDayText()));

        viewHolder.switchSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (availList.get(position).isEnabled()) {
                    try {
                        availList.get(position).setChecked(isChecked);
                        if (position == 0) {
                            for (int i = 1; i < availList.size(); i++) {
                                availList.get(i).setEnabled(!isChecked);
                                if (isChecked)
                                    availList.get(i).setChecked(!isChecked);
                                notifyItemChanged(i);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    viewHolder.switchSelect.setTag(R.string.tag_check);
                    viewHolder.switchSelect.setChecked(!isChecked);
                    viewHolder.switchSelect.setTag(null);
                }
            }
        });

        if (availList.get(position).isEnabled()) {
            viewHolder.switchSelect.setEnabled(true);
            viewHolder.tvFromTime.setEnabled(true);
            viewHolder.tvToTime.setEnabled(true);
        } else {
            viewHolder.switchSelect.setEnabled(false);
            viewHolder.tvFromTime.setEnabled(false);
            viewHolder.tvToTime.setEnabled(false);
        }

        viewHolder.switchSelect.setTag(R.string.tag_check);
        viewHolder.switchSelect.setChecked(availList.get(position).isChecked());
        viewHolder.switchSelect.setTag(null);
        viewHolder.tvFromTime.setText(availList.get(position).getFromTime());
        viewHolder.tvToTime.setText(availList.get(position).getToTime());

        viewHolder.tvFromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (availList.get(position).isEnabled() && availList.get(position).isChecked()) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String AM_PM;
                            if (selectedHour < 12) {
                                AM_PM = "AM";
                            } else {
                                AM_PM = "PM";
                            }
                            selectedHour = selectedHour > 12 ? selectedHour - 12 : selectedHour;
                            String hr = selectedHour < 10 ? "0" + selectedHour : "" + selectedHour;
                            String min = selectedMinute < 10 ? "0" + selectedMinute : "" + selectedMinute;
                            String totTime = hr + ":" + min + " " + AM_PM;
                            viewHolder.tvFromTime.setText(totTime);
                            availList.get(position).setFromTime(totTime);
                        }
                    }, hour, minute, false).show();//Yes 24 hour time
                }
            }
        });

        viewHolder.tvToTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (availList.get(position).isEnabled() && availList.get(position).isChecked()) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String AM_PM;
                            if (selectedHour < 12) {
                                AM_PM = "AM";
                            } else {
                                AM_PM = "PM";
                            }
                            selectedHour = selectedHour > 12 ? selectedHour - 12 : selectedHour;
                            String hr = selectedHour < 10 ? "0" + selectedHour : "" + selectedHour;
                            String min = selectedMinute < 10 ? "0" + selectedMinute : "" + selectedMinute;
                            String totTime = hr + ":" + min + " " + AM_PM;
                            viewHolder.tvToTime.setText(totTime);
                            availList.get(position).setToTime(totTime);
                        }
                    }, hour, minute, false).show();//Yes 24 hour time
                }
            }
        });
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return availList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.switch_select)
        Switch switchSelect;
        @BindView(R.id.tv_avail_day)
        TextView tvAvailDay;
        @BindView(R.id.tv_from_time)
        TextView tvFromTime;
        @BindView(R.id.tv_to_time)
        TextView tvToTime;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
