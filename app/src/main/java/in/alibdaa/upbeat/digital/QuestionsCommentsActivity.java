package in.alibdaa.upbeat.digital;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;

import in.alibdaa.upbeat.digital.adapters.AnswerCommentsAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.POSTQuestionsCommentsList;
import in.alibdaa.upbeat.digital.datamodel.POSTSendComments;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionsCommentsActivity extends AppCompatActivity {


    @BindView(R.id.tv_comments)
    TextView tvComments;
    @BindView(R.id.lv_comments)
    RecyclerView rvComments;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.post_comments)
    TextView postComments;
    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.btn_writecomment)
    ImageView btnWritecomment;
    @BindView(R.id.ll_comments)
    LinearLayout llComments;
    @BindView(R.id.cv_send_comments)
    CardView cvSendComments;
    @BindView(R.id.tb_toolbar)
    Toolbar tbToolbar;
    @BindView(R.id.view_line)
    View viewLine;
    private int lastVisibleItem, totalItemCount, totalItems, totalPages;
    private int visibleThreshold = 1;
    List<POSTQuestionsCommentsList.CommentsList> questionCommentList = new ArrayList<POSTQuestionsCommentsList.CommentsList>();
    int currentPageNo = 1, nextPageNo = -1;
    private boolean isLoading;
    AnswerCommentsAdapter answerCommentsAdapter;
    //    CustomProgressDialog customProgressDialog;
    POSTQuestionsCommentsList.CommentsList comment_detail = new POSTQuestionsCommentsList.CommentsList();
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    String comments = "";
    ApiInterface mApiInterface;
    String question_id = "";
    String page_no = "";
    LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_comments);
        ButterKnife.bind(this);
        setSupportActionBar(tbToolbar);
        getLocaleData();
        tbToolbar.setTitle(AppUtils.cleanLangStr(this, commonData.getLg7_comments(), R.string.comments));
        etComment.setHint(AppUtils.cleanLangStr(this,commonData.getLg7_write_a_comment(),R.string.write_a_comment));
//        customProgressDialog = new CustomProgressDialog(this);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        if (getIntent().getStringExtra(AppConstants.QuestionID) != null) {
            question_id = getIntent().getStringExtra(AppConstants.QuestionID);
        }
        linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setLayoutManager(linearLayoutManager);
        if (rvComments.getAdapter() == null) {
            answerCommentsAdapter = new AnswerCommentsAdapter(this, questionCommentList, question_id,commonData);
            rvComments.setAdapter(answerCommentsAdapter);
        }

        postCommentsList(false);

        adapterLoadMoreData();

    }


    private void adapterLoadMoreData() {
        answerCommentsAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");

                isLoading = true;
                answerCommentsAdapter.mData.add(null);
                //mUsers.add(null);
                answerCommentsAdapter.notifyItemInserted(answerCommentsAdapter.mData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("haint", "Load More 2");
                        postCommentsList(true);
                    }
                }, 1000);

            }
        });

        rvComments.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && lastVisibleItem >= totalItemCount - visibleThreshold && !page_no.equalsIgnoreCase("-1")) {
                    isLoading = true;
                    if (answerCommentsAdapter.mOnLoadMoreListener != null) {
                        answerCommentsAdapter.mOnLoadMoreListener.onLoadMore();
                    }
                }
            }
        });
    }

    public void postCommentsList(final boolean isLoadMore) {

        if (AppUtils.isNetworkAvailable(this)) {
            if (!isLoadMore) {
//                customProgressDialog.showDialog();
                page_no = "1";
            }

            mApiInterface.postCommentsList(PreferenceStorage.getKey(AppConstants.MY_LANGUAGE),PreferenceStorage.getKey(AppConstants.USER_TOKEN), question_id, page_no, AppConstants.pageLimit).enqueue(new Callback<POSTQuestionsCommentsList>() {
                @Override
                public void onResponse(Call<POSTQuestionsCommentsList> call, Response<POSTQuestionsCommentsList> response) {
//                    customProgressDialog.dismiss();
                    if (response.body().getResponse().getResponseCode().equalsIgnoreCase("1")) {
                        if (isLoadMore) {
                            //Remove loading item
                            answerCommentsAdapter.mData.remove(answerCommentsAdapter.mData.size() - 1);
                            answerCommentsAdapter.notifyItemRemoved(answerCommentsAdapter.mData.size());
                        }
                        isLoading = false;

                        if (response.body().getData().getCommentsList() != null && response.body().getData().getCommentsList().size() > 0) {
                            questionCommentList.clear();
                            questionCommentList.addAll(response.body().getData().getCommentsList());
                            currentPageNo = Integer.parseInt(response.body().getData().getCurrentPage());
                            if (!isLoadMore || page_no.equalsIgnoreCase("1")) {
                                answerCommentsAdapter.mData = new ArrayList<>();
                                rvComments.setVisibility(View.VISIBLE);
//                            tvNomentors.setVisibility(View.GONE);
                            }
                            //Load data
                            answerCommentsAdapter.updateRecyclerView(QuestionsCommentsActivity.this, questionCommentList);
                        } else {
//                        tvNomentors.setVisibility(View.VISIBLE);
//                        tvNomentors.setText(response.body().getMessage());
//                        aglGuruLists.setVisibility(View.GONE);
                            questionCommentList.clear();
                            answerCommentsAdapter.updateRecyclerView(QuestionsCommentsActivity.this, questionCommentList);
                            Toast.makeText(QuestionsCommentsActivity.this, response.body().getResponse().getResponseMessage(), Toast.LENGTH_SHORT).show();
                        }


                        page_no = String.valueOf(response.body().getData().getNextPage());
                    } else {
                        AppUtils.showToast(getApplicationContext(), response.body().getResponse().getResponseMessage());
                    }
                }

                @Override
                public void onFailure(Call<POSTQuestionsCommentsList> call, Throwable t) {
//                    customProgressDialog.dismiss();
                }
            });

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.err_no_internet));
        }
    }

    public void postSendComments() {

        if (AppUtils.isNetworkAvailable(this)) {
//            customProgressDialog.showDialog();
            mApiInterface.postSendComments(PreferenceStorage.getKey(AppConstants.USER_TOKEN), question_id, comments).enqueue(new Callback<POSTSendComments>() {
                @Override
                public void onResponse(Call<POSTSendComments> call, Response<POSTSendComments> response) {
//                    customProgressDialog.dismiss();
                    if (response.body().getResponse().getResponseCode().equalsIgnoreCase("1")) {
                        comments = "";
                        comment_detail = new POSTQuestionsCommentsList.CommentsList();
                        comment_detail.setCommentId(response.body().getData().getComment_id());
                        comment_detail.setDaysAgo(response.body().getData().getDays_ago());
                        comment_detail.setName(response.body().getData().getName());
                        comment_detail.setComment(response.body().getData().getComment());
                        comment_detail.setUserId(response.body().getData().getUser_id());
                        comment_detail.setProfileImage(response.body().getData().getProfile_image());
                        comment_detail.setRepliesCount(response.body().getData().getReplies_count());
//                    questionCommentList.add(0, comment_detail);
                        answerCommentsAdapter.mData.add(0, comment_detail);
                        answerCommentsAdapter.notifyDataSetChanged();
                        rvComments.smoothScrollToPosition(0);
                        etComment.setText("");
                    }
                }

                @Override
                public void onFailure(Call<POSTSendComments> call, Throwable t) {
//                    customProgressDialog.dismiss();
                }
            });

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }


    @OnClick({R.id.btn_writecomment})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_writecomment:
                if (!etComment.getText().toString().isEmpty()) {
                    comments = etComment.getText().toString();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    postSendComments();

                }

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(0, R.anim.slide_down);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
//            overridePendingTransition(0, R.anim.slide_down);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            if (!data.getStringExtra(AppConstants.isReplied).isEmpty()) {
                for (int i = 0; i < questionCommentList.size(); i++) {
                    if (questionCommentList.get(i).getCommentId().equalsIgnoreCase(data.getStringExtra(AppConstants.CommentsID))) {
                        questionCommentList.get(i).setRepliesCount("1");
                        answerCommentsAdapter.updateRecyclerView(this, questionCommentList);
                    }
                }
            }
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();

        }
    }
}
