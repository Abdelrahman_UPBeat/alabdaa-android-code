package in.alibdaa.upbeat.digital;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Locale;

import in.alibdaa.upbeat.digital.datamodel.ColorSettingModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.service.AlarmIntervalReceiver;
import in.alibdaa.upbeat.digital.service.GPSTracker;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.LocaleUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import retrofit2.Call;

import static in.alibdaa.upbeat.digital.utils.AppConstants.REQUEST_PERMISSIONS;

public class SplashScreenActivity extends AppCompatActivity implements RetrofitHandler.RetrofitResHandler {

    String language = "en";
    boolean gps_enabled = false;
    boolean network_enabled = false;
    GPSTracker gpsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        language = PreferenceStorage.getKey(AppConstants.MY_LANGUAGE);
        gpsTracker = new GPSTracker(this);


        marshallMallowPermission();
        printHashKey(this);
    }

    private void marshallMallowPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS,
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.SET_ALARM,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.INTERNET};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);
            } else {
                if (gpsTracker.canGetLocation()) {
                    GPSTracker.isFromSetting = false;
                    moveToNext();
                } else {
                    gpsTracker.showSettingsAlert();
                }

            }
        }
    }

    private void moveToNext() {
//        checkLocationSettings();
        checkForColorSettings();
    }

    private synchronized void checkForColorSettings() {
        if (AppUtils.isNetworkAvailable(this)) {
//            ProgressDlg.clearDialog();
//            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<ColorSettingModel> classificationCall = apiService.getColorData();
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.COLORSETTING_DATA, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), getResources().getString(R.string.txt_enable_internet));
            checkForLocale();
        }
    }

    private synchronized void checkForLocale() {
        if (!PreferenceStorage.getBoolKey(AppConstants.LANGUAGE_SET)) {
            final String[] items = {getResources().getString(R.string.txt_english), getResources().getString(R.string.txt_malay),
                    getResources().getString(R.string.txt_arabic)};
//            final String[] items = {getResources().getString(R.string.txt_english)};
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
            builder.setCancelable(false);
            LayoutInflater inflater = this.getLayoutInflater();
            View titleView = inflater.inflate(R.layout.list_custom_alert_dialog_tiltle, null);
            builder.setCustomTitle(titleView);
            builder.setAdapter(new LanguageAdapter(SplashScreenActivity.this, R.layout.list_item_language, items), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    String lang = "en";
                    switch (item) {
                        case 0:
                            lang = "en";
                            break;
                        case 1:
                            lang = "ma";
                            break;
                        case 2:
                            lang = "ar";
                            break;
                    }
                    dialog.dismiss();
                    language = lang;
                    getLocaleData(lang);
                }
            });
            builder.show();
//            language = "en";
//            getLocaleData(language);

        } else {
//            language = "en";
            setLocale(language);
            String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
            if (userToken != null) {
                Intent myIntent = new Intent(SplashScreenActivity.this, MyLoginActivity.class);
                startActivity(myIntent);
            }else{
                Intent myIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
            isGPSEnable();
            SplashScreenActivity.this.finish();

        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    GPSTracker.isFromSetting = false;
                    moveToNext();
//                    if (gpsTracker.canGetLocation()) {
//                        GPSTracker.isFromSetting = false;
//                        moveToNext();
//                    } else {
//                        gpsTracker.showSettingsAlert();
//                    }
                } else {
                    moveToNext();
                }
                break;
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public synchronized void getLocaleData(String langCode) {
        if (AppUtils.isNetworkAvailable(this)) {
//            ProgressDlg.clearDialog();
//            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<LanguageModel> classificationCall = apiService.getLanguageData(langCode);
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.LANGUAGE_DATA, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), getResources().getString(R.string.txt_enable_internet));
            String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
            if (userToken != null) {
                Intent myIntent = new Intent(SplashScreenActivity.this, MyLoginActivity.class);
                startActivity(myIntent);
            }else{
                Intent myIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
            SplashScreenActivity.this.finish();
        }
    }

    @Override
    public synchronized void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes != null) {
            if (myRes instanceof LanguageModel) {
                setLocale(language);
                PreferenceStorage.setKey(AppConstants.LANGUAGE_SET, true);
                PreferenceStorage.setKey(AppConstants.MY_LANGUAGE, language);
                AppUtils.setLangInPref((LanguageModel) myRes);
                String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
                if (userToken != null) {
                    Intent myIntent = new Intent(SplashScreenActivity.this, MyLoginActivity.class);
                    startActivity(myIntent);
                }else{
                    Intent myIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(myIntent);
                }
                isGPSEnable();
                SplashScreenActivity.this.finish();


            } else if (myRes instanceof ColorSettingModel) {
                ColorSettingModel colorData = (ColorSettingModel) myRes;
                if (colorData != null && colorData.getData() != null) {
                    String colorList = new Gson().toJson(colorData.getData());
                 //   PreferenceStorage.setKey(AppConstants.COLOR_LIST, colorList);
                    int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    String appColor = colorData.getData().getMorning();
//                    String appColor = "#000080";
                    if (hour >= 20 || hour < 6) {
                        appColor = colorData.getData().getNight();
                    } else if (hour >= 6 && hour < 12) {
                        appColor = colorData.getData().getMorning();
                    } else if (hour >= 12 && hour < 17) {
                        appColor = colorData.getData().getAfternoon();
                    } else if (hour >= 17 && hour < 20) {
                        appColor = colorData.getData().getEvening();
                    }
                    PreferenceStorage.setKey(AppConstants.APP_THEME, appColor);
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.HOUR_OF_DAY, hour);
                    AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    Intent serviceIntent = new Intent(SplashScreenActivity.this, AlarmIntervalReceiver.class);
                    PendingIntent pi = PendingIntent.getBroadcast(SplashScreenActivity.this, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    am.setRepeating(AlarmManager.RTC, cal.getTimeInMillis(), 60 * 60 * 1000, pi);
                }
                checkForLocale();
            }
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes instanceof ColorSettingModel)
            checkForLocale();
        else if (myRes instanceof LanguageModel) {
            if (PreferenceStorage.getKey(AppConstants.USER_ID) != null) {

                String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
                if (userToken != null) {
                    Intent myIntent = new Intent(SplashScreenActivity.this, MyLoginActivity.class);
                    startActivity(myIntent);
                }else{
                    Intent myIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(myIntent);
                }
                isGPSEnable();
                SplashScreenActivity.this.finish();
            } else {
                AppUtils.showToast(SplashScreenActivity.this, getResources().getString(R.string.txt_server_error));
                SplashScreenActivity.this.finish();
            }
        }

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes instanceof ColorSettingModel)
            checkForLocale();
        else if (myRes instanceof LanguageModel) {
            if (PreferenceStorage.getKey(AppConstants.USER_ID) != null) {

                String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
                if (userToken != null) {
                    Intent myIntent = new Intent(SplashScreenActivity.this, MyLoginActivity.class);
                    startActivity(myIntent);
                }else{
                    Intent myIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(myIntent);
                }
                isGPSEnable();
                SplashScreenActivity.this.finish();
            } else {
                AppUtils.showToast(SplashScreenActivity.this, getResources().getString(R.string.txt_server_error));
                SplashScreenActivity.this.finish();
            }
        }
    }

    private class LanguageAdapter extends ArrayAdapter<String> {
        String[] items = new String[3];

        public LanguageAdapter(@NonNull Context context, int resource, String[] items) {
            super(context, resource, items);
            this.items = items;
//            items[0] = AppUtils.cleanLangStr(context, commonData.getLg7_english(), R.string.txt_english);
//            items[1] = AppUtils.cleanLangStr(context, commonData.getLg7_malay(), R.string.txt_malay);
//            items[1] = AppUtils.cleanLangStr(context, commonData.getLg7_malay(), R.string.txt_malay);
        }

        ViewHolder holder;

        class ViewHolder {
            ImageView icon;
            TextView title;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_language, null);
                holder = new ViewHolder();
                holder.icon = (ImageView) convertView.findViewById(R.id.iv_lang_icon);
                holder.title = (TextView) convertView.findViewById(R.id.tv_lang_txt);
                convertView.setTag(holder);
            } else {
                // view already defined, retrieve view holder
                holder = (ViewHolder) convertView.getTag();
            }
            holder.title.setText(items[position]);
            if (position == 0)
                holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_lang_english));
            else if (position == 1) {
                holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_lang_malay));
            } else
                holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_arabic_48));

            return convertView;
        }
    }

    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("KeyHash", "printHashKey() Hash Key: " + hashKey);

                Log.i("SHA_KEY", "printHashKey() Hash Key: " + md.toString());
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("KeyHash", "printHashKey()", e);
        }
    }

    public void checkLocationSettings() {
        if (!gps_enabled && !network_enabled) {
            // notify user
            notifyUser();
        } else {
            checkForColorSettings();
        }
    }

    private void notifyUser() {
        new AlertDialog.Builder(this)
                .setTitle("Location Permission Required!")
                .setMessage("Enable Location in Settings")
                .setPositiveButton("Open Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        paramDialogInterface.dismiss();
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    public void isGPSEnable() {
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        final boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Toast.makeText(SplashScreenActivity.this, "Enable GPS to See your nearbuy service and providers", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    }

    public void setLocale(String localeName) {
//        myLocale = new Locale(localeName);
        PreferenceStorage.setKey("locale", localeName);
//        SessionHandler.getInstance().save(Splashscreen.this, "locale", localeName);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = new Configuration();
//        Locale.setDefault(myLocale);
//        conf.locale = myLocale;
//        conf.setLayoutDirection(new Locale(localeName));
//        getBaseContext().getResources().updateConfiguration(conf, null);
//        onConfigurationChanged(conf);
        PreferenceStorage.setKey("localechanged", "true");
//        SessionHandler.getInstance().save(Splashscreen.this, "localechanged", "true");
        AppConstants.localeName = localeName;
        LocaleUtils.setLocale(new Locale(localeName));
        LocaleUtils.updateConfigActivity(this, getBaseContext().getResources().getConfiguration());
//        aviLoadingView.setVisibility(View.VISIBLE);
//        loadData();
    }


}
