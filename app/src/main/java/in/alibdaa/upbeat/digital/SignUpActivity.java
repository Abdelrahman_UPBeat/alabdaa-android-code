package in.alibdaa.upbeat.digital;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppLocationService;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, RetrofitHandler.RetrofitResHandler
        , GoogleApiClient.ConnectionCallbacks, LocationListener {


    @BindView(R.id.tiet_username)
    TextInputEditText tietUsername;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.tiet_email)
    TextInputEditText tietEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.tiet_password)
    TextInputEditText tietPassword;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.tiet_phone)
    TextInputEditText tietPhone;
    @BindView(R.id.til_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.iv_prof_pic)
    CircleImageView ivProfPic;
    @BindView(R.id.signup_header)
    ConstraintLayout signupHeader;
    @BindView(R.id.cl_header)
    ConstraintLayout clHeader;
    @BindView(R.id.btn_upload_ic)
    Button btnUploadIc;
    @BindView(R.id.iv_ic_card)
    ImageView ivIcCard;
    @BindView(R.id.iv_ic_card_ok)
    ImageView ivIcCardOk;
    @BindView(R.id.iv_ic_card_cancel)
    ImageView ivIcCardCancel;
    @BindView(R.id.tiet_confirm_pwd)
    TextInputEditText tietConfirmPwd;
    @BindView(R.id.til_confirm_pwd)
    TextInputLayout tilConfirmPwd;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.tv_account_msg)
    TextView tvAccountMsg;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.iv_facebook)
    ImageView ivFacebook;
    @BindView(R.id.tv_facebook)
    TextView tvFacebook;
    @BindView(R.id.ll_signup_fb)
    LinearLayout llSignupFb;
    @BindView(R.id.iv_google)
    ImageView ivGoogle;
    @BindView(R.id.tv_google)
    TextView tvGoogle;
    @BindView(R.id.ll_signup_google)
    LinearLayout llSignupGoogle;
    @BindView(R.id.ll_signup_footer)
    LinearLayout llSignupFooter;
    @BindView(R.id.iv_prof_pic_edit)
    ImageView ivProfPicEdit;
    @BindView(R.id.cb_acccept_tc)
    CheckBox cbAccceptTc;
    @BindView(R.id.tv_view_tc)
    TextView tvViewTc;

    private BottomSheetDialog attachChooser;
    private int registerType = AppConstants.REG_NORMAL;

    private static final int RC_SIGN_IN = 007;
    private static final int RC_LOAD_IMG_CAMERA = 101;
    private static final int RC_LOAD_IMG_BROWSER = 102;

    RequestBody requestFile;
    RequestBody userName, password, email_id, phoneNo;
    MultipartBody.Part profileImg;
    MultipartBody.Part icCardImg;

    private CallbackManager callbackManager;
    private AppLocationService appLocationService;

    private Location mLocation;


    String email, name, socialId = "", Latitude, Longitude;

    private static final String TAG = SignUpActivity.class.getSimpleName();

    protected GoogleApiClient mGoogleApiLocClient;
    protected GoogleApiClient mGoogleApiAuthClient;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;


    List<MultipartBody.Part> imagePartList = new ArrayList<>();

    LanguageModel.Sign_up signUpData = new LanguageModel().new Sign_up();
    LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        changeLanguage(PreferenceStorage.getKey(AppConstants.MY_LANGUAGE), this);
        super.onCreate(savedInstanceState);
//        Locale.setDefault(new Locale(PreferenceStorage.getKey(AppConstants.MY_LANGUAGE)));

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        Intent myIntent = getIntent();

        Log.d("OnCPreference", PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
        Log.d("OnCLocale", Locale.getDefault().getLanguage());

        setLocaleData();


        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(READ_CONTACTS);
        permissions.add(CAMERA);

        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiAuthClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleApiLocClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        registerType = myIntent.getIntExtra("LoginType", AppConstants.REG_NORMAL);
        if (registerType > AppConstants.REG_NORMAL) {
            email = myIntent.getStringExtra("Email");
            name = myIntent.getStringExtra("Name");
            socialId = myIntent.getStringExtra("Id");
            tietEmail.setText(email);
            tietUsername.setText(name);
            llSignupFooter.setVisibility(View.GONE);
        }

        appLocationService = new AppLocationService(this);

        //AddTextwatcherListner
        tietUsername.addTextChangedListener(new RegisterTextWatcher(tietUsername));
        tietEmail.addTextChangedListener(new RegisterTextWatcher(tietEmail));
        tietPhone.addTextChangedListener(new RegisterTextWatcher(tietPhone));
        tietPassword.addTextChangedListener(new RegisterTextWatcher(tietPassword));
        tietConfirmPwd.addTextChangedListener(new RegisterTextWatcher(tietConfirmPwd));

        //Hide Keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        //FB Integration
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    Log.d("faaaace",object.toString());
                                    String email = "", userName = "", facebookId = "";
                                    if (object.has("email")) {
                                        email = object.getString("email");
                                        Log.e(TAG, "email = " + email);
                                    }
                                    if (object.has("first_name")) {
                                        userName = object.getString("first_name");
                                        Log.e(TAG, "first_name = " + email);
                                    }
                                    if(object.has("picture")){
                                        try {


                                            JSONObject picture = object.getJSONObject("picture");
                                            JSONObject data = picture.getJSONObject("data");
                                            String ImageUrl = data.getString("url");
                                            Picasso.with(SignUpActivity.this).load(ImageUrl).into(ivProfPic);

                                            BitmapDrawable drawable = (BitmapDrawable) ivProfPic.getDrawable();
                                            Bitmap bitmap = drawable.getBitmap();
                                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                            byte[] byteArray = stream.toByteArray();
                                            bitmap.recycle();
                                            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), byteArray);
                                            profileImg = MultipartBody.Part.createFormData("profile_img", "profile_img.jpg", requestFile);
                                        }catch (Exception e){


                                        }



                                                }
                                    facebookId = object.getString("id");
                                    Log.e(TAG, "facebookId = " + facebookId);
                                    tietEmail.setText(email);
                                    tietUsername.setText(userName);
                                    socialId = facebookId;
                                } catch (Exception e) {
                                    Log.e(TAG, e.getLocalizedMessage());
                                }

                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,cover,picture.type(large),email,birthday");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("error", String.valueOf(error));
            }
        });


        getFusedLocation();


    }


    private void getFusedLocation() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    Latitude = String.valueOf(location.getLatitude());
                    Longitude = String.valueOf(location.getLongitude());
                }
                Log.d("TAG_Location", "userName = " + userName
                        + " && Latitude = " + Latitude + " && Longitude = " + Longitude
                        + " && socialId = " + socialId + " && Longitude = " + Longitude
                        + " && password = " + password + " && registerType = " + registerType);
            }


        });
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiLocClient != null) {
            mGoogleApiLocClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("OnRPreference", PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
        Log.d("OnRLocale", Locale.getDefault().getLanguage());

        getFusedLocation();
        setAppTheme();
        if (!checkPlayServices()) {
            Toast.makeText(this, "Please install Google Play services.", Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void setContentView(int layoutResID) {
//        super.setContentView(layoutResID);
//    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == RC_LOAD_IMG_BROWSER) {
            onSelectFromGalleryResult(data);
        } else if (requestCode == RC_LOAD_IMG_CAMERA) {
            onCaptureImageResult(data);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct != null) {
                Log.e(TAG, "display name: " + acct.getDisplayName());
                String personPhotoUrl = "";
                Log.e(TAG, "Token =  " + acct.getIdToken());
                Log.e(TAG, "ID =  " + acct.getId());
                String personName = acct.getDisplayName();
                if (acct.getPhotoUrl() != null) {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                }
                String email = acct.getEmail();
                Log.e(TAG, "Name: " + personName + ", email: " + email
                        + ", Image: " + personPhotoUrl);
                tietUsername.setText(personName);
                tietEmail.setText(email);
                socialId = acct.getId();
            }
        }
    }

    @OnClick({R.id.iv_prof_pic_edit, R.id.btn_upload_ic, R.id.iv_ic_card_ok, R.id.iv_ic_card_cancel, R.id.btn_signup, R.id.tv_login, R.id.ll_signup_fb, R.id.ll_signup_google, R.id.tv_view_tc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_prof_pic_edit:
                selectImage();
                break;
            case R.id.btn_upload_ic:
                selectImage();
                break;
            case R.id.iv_ic_card_ok:
                break;
            case R.id.iv_ic_card_cancel:
                break;
            case R.id.tv_view_tc:
                Intent i = new Intent(this, WebViewActivity.class);
                i.putExtra(AppConstants.TbTitle, "Terms and Conditions");
                i.putExtra(AppConstants.URL, AppConstants.TermsConditionsURL);
                startActivity(i);
                break;
            case R.id.btn_signup:
                registerUser();
                break;
            case R.id.tv_login:
                Intent loginIntent = new Intent(SignUpActivity.this, MyLoginActivity.class);
                AppUtils.appStartIntent(this, loginIntent);
                break;
            case R.id.ll_signup_fb:
                registerType = AppConstants.REG_FB;
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
                break;
            case R.id.ll_signup_google:
                registerType = AppConstants.REG_GPLUS;
                Auth.GoogleSignInApi.signOut(mGoogleApiAuthClient);
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiAuthClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    @SuppressLint("ResourceType")
    private void registerUser() {
        if (!validateUsername()) {
            return;
        } else if (!validateEmail()) {
            return;
        } else if (!validatePhoneNo()) {
            return;
        } else if (!validatePassword()) {
            /*tilPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_password(), R.string.err_password));
            tilPassword.requestFocus();*/
            return;
        } else if (!validateRPassword()) {
            /*tilConfirmPwd.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_confirm_p(), R.string.err_confirm_pwd));
            tilConfirmPwd.requestFocus();*/
            return;
        } else if (!validateTermsConditions()) {
            return;
        }

        /*else if (ivProfPic.getTag() == null) {
            AppUtils.showToast(this, AppUtils.cleanLangStr(this, signUpData.getLg1_choose_picture(), R.string.err_pro_img));
            return;
        } else if (icCardImg == null) {
            AppUtils.showToast(this, getString(R.string.err_card_img));
            return;
        } */
        else {
            userName = RequestBody.create(MediaType.parse("text/plain"), tietUsername.getText().toString().trim());
            password = RequestBody.create(MediaType.parse("text/plain"), tietPassword.getText().toString().trim());
            email_id = RequestBody.create(MediaType.parse("text/plain"), tietEmail.getText().toString().trim());
            phoneNo = RequestBody.create(MediaType.parse("text/plain"), tietPhone.getText().toString().trim());
        }

        /*Bitmap icThumbnail = null;
        try {
            File filePath = new File(ivProfPic.getTag().toString());
            CircleImageView cvProfile = ((CircleImageView) ivProfPic.findViewById(R.id.iv_picture));
            icThumbnail = ((BitmapDrawable) cvProfile.getDrawable()).getBitmap();
            //icThumbnail = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), Uri.parse("file://" + filePath.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error e) {
            new Exception(e).printStackTrace();
        }
        if (icThumbnail != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            icThumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());
            profileImg = MultipartBody.Part.createFormData("profile_img", "profile_img.jpg", requestFile);
            imagePartList.add(profileImg);
        }*/ /*else {
            return;
        }*/

        if (profileImg != null)
            imagePartList.add(profileImg);

        if (AppUtils.isNetworkAvailable(this)) {

            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Log.d("TAG_SIGNUP", "userName = " + userName
                        + " && email_id = " + email_id + " && phoneNo = " + phoneNo
                        + " && Latitude = " + Latitude + " && Longitude = " + Longitude
                        + " && socialId = " + socialId + " && Longitude = " + Longitude
                        + " && password = " + password + " && registerType = " + registerType);
                Call<EmptyData> classificationCall = apiService.postSignUp(userName, email_id, phoneNo, password,
                        RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerType)),
                        RequestBody.create(MediaType.parse("text/plain"), Latitude),
                        RequestBody.create(MediaType.parse("text/plain"), Longitude),
                        RequestBody.create(MediaType.parse("text/plain"), socialId),
                        RequestBody.create(MediaType.parse("text/plain"), AppConstants.deviceType),
                        RequestBody.create(MediaType.parse("text/plain"), AppUtils.getDeviceId(SignUpActivity.this)),
                        imagePartList, PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.SIGNUP_DATA, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(SignUpActivity.this, AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        EmptyData response = (EmptyData) myRes;
        AppUtils.showToast(SignUpActivity.this, response.getResponseHeader().getResponseMessage());
        Intent myIntent = new Intent(SignUpActivity.this, MyLoginActivity.class);
        AppUtils.appStartIntent(SignUpActivity.this, myIntent);
        SignUpActivity.this.finish();
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    private void selectImage() {
        attachChooser = new BottomSheetDialog(SignUpActivity.this);
        attachChooser.setContentView((SignUpActivity.this).getLayoutInflater().inflate(R.layout.popup_add_attach_options,
                new LinearLayout(SignUpActivity.this)));
        attachChooser.show();

        LinearLayout btnStartCamera = (LinearLayout) attachChooser.findViewById(R.id.btn_from_camera);
        LinearLayout btnStartFileBrowser = (LinearLayout) attachChooser.findViewById(R.id.btn_from_local);
        btnStartCamera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attachChooser.dismiss();
                if (AppUtils.checkPermission(SignUpActivity.this))
                    cameraIntent();
            }
        });
        btnStartFileBrowser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attachChooser.dismiss();
                if (AppUtils.checkPermission(SignUpActivity.this))
                    galleryIntent();
            }
        });
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RC_LOAD_IMG_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, RC_LOAD_IMG_BROWSER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case AppConstants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (requestCode == RC_LOAD_IMG_CAMERA)
                        cameraIntent();
                    else if (requestCode == RC_LOAD_IMG_BROWSER)
                        galleryIntent();
                } else {
                }
                break;

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel(AppUtils.cleanLangStr(this, commonData.getLg7_these_permissio(), R.string.err_txt_permission),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }

                break;
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap thumbnail = null;
        Bitmap scaled = null;
        try {
            if (data != null) {
                thumbnail = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                int nh = (int) (160 * (512.0 / 160));
                thumbnail = Bitmap.createScaledBitmap(thumbnail, 512, nh, true);
                if (thumbnail != null) {
                    scaled = thumbnail;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    String encodeImage = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());
                    profileImg = MultipartBody.Part.createFormData("profile_img", "profile_img.jpg", requestFile);
                }
                ivProfPic.setImageBitmap(scaled);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            if (data != null) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                int nh = (int) (160 * (512.0 / 160));
                thumbnail = Bitmap.createScaledBitmap(thumbnail, 512, nh, true);
                Bitmap scaled = thumbnail;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                if (thumbnail != null) {
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    String encodeImage = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                }
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());
                profileImg = MultipartBody.Part.createFormData("profile_img", "profile_img.jpg", requestFile);
                //imagePartList.add(icCardImg);
                //ivIcCard.setImageBitmap(thumbnail);
                ivProfPic.setImageBitmap(scaled);
            }
        } catch (Exception e) {
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLocation != null) {
            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());
            Log.d("TAG_Location", "userName = " + userName
                    + " && Latitude = " + Latitude + " && Longitude = " + Longitude
                    + " && socialId = " + socialId + " && Longitude = " + Longitude
                    + " && password = " + password + " && registerType = " + registerType);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_enable_permissi(), R.string.err_txt_enable_permission), Toast.LENGTH_LONG).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiLocClient, mLocationRequest, this);


    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle("Required App Permission")
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_ok(), R.string.ok), okListener)
                //.setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_cancel(), R.string.txt_cancel), null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }


    public void stopLocationUpdates() {
        if (mGoogleApiLocClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiLocClient, (LocationListener) this);
            mGoogleApiLocClient.disconnect();
        }
    }


    private class RegisterTextWatcher implements TextWatcher {
        private View view;

        private RegisterTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (view.getId() == R.id.tiet_username) {
                if (!tietUsername.getText().toString().isEmpty()) {
                    validateUsername();
                }
            } else if (view.getId() == R.id.tiet_email) {

                validateEmail();

            } else if (view.getId() == R.id.tiet_phone) {
                if (!tietPhone.getText().toString().isEmpty()) {
                    validatePhoneNo();
                }
            } else if (view.getId() == R.id.tiet_password) {
                if (!tietPassword.getText().toString().isEmpty()) {
                    validatePassword();
                }
            } else if (view.getId() == R.id.tiet_confirm_pwd) {
                if (!tietConfirmPwd.getText().toString().isEmpty()) {
                    validateRPassword();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private boolean validateUsername() {
        if (tietUsername.getText().toString().isEmpty()) {
            tilUsername.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_username(), R.string.err_username));
            tilUsername.requestFocus();
            return false;
        } else if (tietUsername.getText().toString().length() > 20) {
            tilUsername.setError(AppUtils.cleanLangStr(this, "", R.string.err_max_characters));//Todo:
            tilUsername.requestFocus();
            return false;
        } else {
            tilUsername.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateRPassword() {
        if (tietConfirmPwd.getText().toString().isEmpty()) {
            tilConfirmPwd.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_confirm_p(), R.string.err_confirm_pwd));
            tilConfirmPwd.requestFocus();
            return false;
        } else if (!tietConfirmPwd.getText().toString().equalsIgnoreCase(tietPassword.getText().toString())) {
            tilConfirmPwd.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_password_and_co(), R.string.err_match_pwd));
            tilConfirmPwd.requestFocus();
            return false;
        } else {
            tilConfirmPwd.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateTermsConditions() {
        if (!cbAccceptTc.isChecked()) {
            Toast.makeText(this, AppUtils.cleanLangStr(this, "", R.string.err_terms_conditions), Toast.LENGTH_SHORT).show(); //:TODO
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        if (tietPassword.getText().toString().isEmpty()) {
            tilPassword.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_password(), R.string.err_password));
            tilPassword.requestFocus();
            return false;
        }
//        else if (!tietPassword.getText().toString().matches(AppConstants.passwordMatch)) {
//            tilPassword.setError(AppUtils.cleanLangStr(this, commonData.getLg7_password_must_b(), R.string.err_msg_password));
//            tilPassword.requestFocus();
//            return false;
//        }
        else {
            tilPassword.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateEmail() {
        if (tietEmail.getText().toString().isEmpty()) {
            tilEmail.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_email(), R.string.err_email));
            tilEmail.requestFocus();
            return false;
        } else if (!AppUtils.isValidEmail(tietEmail.getText().toString())) {
            tilEmail.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_valid_ema(), R.string.err_valid_email));
            tilEmail.requestFocus();
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePhoneNo() {
        if (tietPhone.getText().toString().isEmpty()) {
            tilPhone.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_phone_num(), R.string.err_phone_no));
            tilPhone.requestFocus();
            return false;
        } else if (tietPhone.getText().toString().length() < 10) {
            tilPhone.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_maximum_15_numb(), R.string.err_min_no));
            tilPhone.requestFocus();
            return false;
        } else if (tietPhone.getText().toString().length() > 15) {
            tilPhone.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_maximum_15_numb(), R.string.err_max_no));
            tilPhone.requestFocus();
            return false;
        } else {
            tilPhone.setErrorEnabled(false);
        }
        return true;

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof TextInputEditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    private void setLocaleData() {
        try {
            try {
                String lgData = PreferenceStorage.getKey(CommonLangModel.sign_up);
                signUpData = new Gson().fromJson(lgData, LanguageModel.Sign_up.class);
            } catch (Exception e) {
                signUpData = new LanguageModel().new Sign_up();
            }

            try {
                String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
                commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
            } catch (Exception e) {
                commonData = new LanguageModel().new Common_used_texts();
            }

            tilUsername.setHint(AppUtils.cleanLangStr(this, signUpData.getLg1_username(), R.string.hint_username_email));
            tilEmail.setHint(AppUtils.cleanLangStr(this, signUpData.getLg1_email(), R.string.hint_email));
            tilPassword.setHint(AppUtils.cleanLangStr(this, signUpData.getLg1_password(), R.string.hint_password));
            tilConfirmPwd.setHint(AppUtils.cleanLangStr(this, signUpData.getLg1_confirm_passwor(), R.string.hint_cn_password));
            tilPhone.setHint(AppUtils.cleanLangStr(this, signUpData.getLg1_phone(), R.string.hint_phone));

            tvAccountMsg.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_already_have_an(), R.string.txt_already_acc));
            tvLogin.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_login(), R.string.txt_login));
            tvFacebook.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_signup_with_fac(), R.string.signup_facebook));
            tvGoogle.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_signup_with_goo(), R.string.signup_google));
            btnSignup.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_sign_up(), R.string.txt_signup));
            btnUploadIc.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_upload_ic_card(), R.string.txt_upload_ic_card));
            cbAccceptTc.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_accept_terms_an(), R.string.accept_terms_and_conditions));
            tvViewTc.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_read_tc(), R.string.txt_read_tc));
        } catch (Exception e) {

        }
    }

    public void setAppTheme() {
        int appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }
        btnSignup.setBackgroundColor(appColor);
        signupHeader.setBackgroundColor(appColor);
        tvLogin.setTextColor(appColor);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public static void changeLanguage(String language, Context context) {
        try {
            if (null != language) {
                Locale locale = new Locale(language);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
            Log.i(TAG, "Language changed to " + language);
        } catch (Exception e) {
            Log.e(TAG, "[changeLanguage]" + e.getMessage());
        }
    }
}
