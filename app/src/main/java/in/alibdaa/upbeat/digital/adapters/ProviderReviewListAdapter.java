package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import in.alibdaa.upbeat.digital.MainActivity;
import in.alibdaa.upbeat.digital.MyProviderListActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.POSTReviewList;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;
import in.alibdaa.upbeat.digital.viewwidgets.ViewBinderHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProviderReviewListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mActivity;
    Context mContext;
    public ArrayList<POSTReviewList.Review_list> itemsData = new ArrayList<>();
    int viewType;
    LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    public OnLoadMoreListener mOnLoadMoreListener;
    List<POSTReviewList.Review_list> category_list;


    private int SELF = 1, LOADING = 2;

    public ProviderReviewListAdapter(Context mContext, List<POSTReviewList.Review_list> category_list) {
        this.mContext = mContext;
        this.category_list = category_list;
    }

    public ProviderReviewListAdapter(Activity mActivity, Context mContext, ArrayList<POSTReviewList.Review_list> itemsData, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        if (mActivity instanceof MainActivity) {
            langReqProvData = ((MainActivity) mActivity).langReqProvData;
        } else if (mActivity instanceof MyProviderListActivity) {
            langReqProvData = ((MyProviderListActivity) mActivity).requestAndProviderList;
        }
        // uncomment if you want to open only one row at a time
        binderHelper.setOpenOnlyOne(true);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View itemView;
        itemView = LayoutInflater.from(mContext).inflate(R.layout.adapter_review_list, parent, false);
        return new ProviderViewHolder(itemView);

    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        ProviderViewHolder providerViewHolder = (ProviderViewHolder) viewHolder;
        Picasso.with(mContext)
                .load(AppConstants.BASE_URL+category_list.get(position).getProfile_img())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(providerViewHolder.ivUserImg);

        providerViewHolder.rbReviews.setRating(Float.parseFloat(category_list.get(position).getRating()));
        providerViewHolder.tvComments.setText(category_list.get(position).getReview());
        providerViewHolder.tvUsername.setText(category_list.get(position).getReviewer());

    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return category_list.size();
    }


    public class ProviderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_user_img)
        CircleImageView ivUserImg;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.rb_reviews)
        RatingBar rbReviews;
        @BindView(R.id.tv_comments)
        TextView tvComments;

        public ProviderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }
    }
}
