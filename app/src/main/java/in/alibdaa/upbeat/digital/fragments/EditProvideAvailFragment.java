package in.alibdaa.upbeat.digital.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.alibdaa.upbeat.digital.EditProviderActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.adapters.ProviderAvailAdapter;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProvAvailData;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;

public class EditProvideAvailFragment extends Fragment {

    @BindView(R.id.tv_txt_availability)
    TextView tvTxtAvailability;
    @BindView(R.id.tv_txt_from)
    TextView tvTxtFrom;
    @BindView(R.id.tv_txt_to)
    TextView tvTxtTo;
    @BindView(R.id.rv_avail_parent)
    RecyclerView rvAvailParent;
    @BindView(R.id.btn_provider_submit)
    Button btnProviderSubmit;

    @BindView(R.id.tv_txt_from_date)
    TextView tvTxtFromDate;
    @BindView(R.id.et_from_date)
    EditText etFromDate;
    @BindView(R.id.tv_txt_to_date)
    TextView tvTxtToDate;
    @BindView(R.id.et_to_date)
    EditText etToDate;

    Unbinder unbinder;

    LinearLayoutManager mLayoutManager;
    ProviderAvailAdapter mAdapter;
    EditProviderActivity mActivity;
    Context mContext;
    Calendar myCalendar = Calendar.getInstance();

    LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();
    @BindView(R.id.ll_date_range)
    LinearLayout llDateRange;

    List<Integer> dayIndex = new ArrayList<>();

    ProviderListData.ProviderList providerDataDetail;

    public void myCreateProvideAvailFragment(EditProviderActivity createProviderActivity, ProviderListData.ProviderList providerDataDetail) {
        this.mActivity = createProviderActivity;
        this.mContext = createProviderActivity.getBaseContext();
        requestAndProviderList = createProviderActivity.langReqProvData;
        this.providerDataDetail = providerDataDetail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_create_provider_avail, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvAvailParent.setLayoutManager(mLayoutManager);

        tvTxtAvailability.setText(AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_availability(), R.string.txt_availability));
        tvTxtFrom.setText(AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_from(), R.string.txt_from));
        tvTxtTo.setText(AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_to(), R.string.txt_to));
        //TODO: Lang tvTxtFromDate, tvTxtToDate


        btnProviderSubmit.setText(AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_submit(), R.string.txt_submit));
        btnProviderSubmit.setBackgroundColor(mActivity.appColor);
        try {
            String fromDate = AppUtils.formatDateToApp(providerDataDetail.getStartDate());
            SimpleDateFormat sdf = new SimpleDateFormat(AppConstants.APP_DATE_FORMAT);
            Date parsedDate = sdf.parse(fromDate);
            etFromDate.setTag(parsedDate.getTime());
            etFromDate.setText(fromDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String endDate = AppUtils.formatDateToApp(providerDataDetail.getEndDate());
            SimpleDateFormat sdf = new SimpleDateFormat(AppConstants.APP_DATE_FORMAT);
            Date parsedDate = sdf.parse(endDate);
            etToDate.setTag(parsedDate.getTime());
            etToDate.setText(endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String[] availList = {AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_all_days(), R.string.txt_all_days),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_sunday(), R.string.txt_sunday),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_monday(), R.string.txt_monday),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_tuesday(), R.string.txt_tuesday),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_wednesday(), R.string.txt_wednesday),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_thursday(), R.string.txt_thursday),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_friday(), R.string.txt_friday),
                AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_saturday(), R.string.txt_saturday)};
        ArrayList<ProvAvailData> provAvailData = new ArrayList<>();
        for (int i = 0; i < availList.length; i++) {
            ProvAvailData pvd = new ProvAvailData();
            pvd.setDayText(availList[i]);
            provAvailData.add(pvd);
        }
        if (providerDataDetail.getAvailability() != null && !providerDataDetail.getAvailability().isEmpty()) {
            try {
                JSONArray av1 = new JSONArray(providerDataDetail.getAvailability());
                for (int j = 0; j < provAvailData.size(); j++) {
                    for (int i = 0; i < av1.length(); i++) {
                        JSONObject av2 = av1.getJSONObject(i);
                        int jsonDayIndex = Integer.parseInt(av2.getString("day"));
                        provAvailData.get(jsonDayIndex).setChecked(true);
                        provAvailData.get(jsonDayIndex).setEnabled(true);
                        provAvailData.get(jsonDayIndex).setFromTime(av2.getString("from_time"));
                        provAvailData.get(jsonDayIndex).setToTime(av2.getString("to_time"));
                        //tvTxtAvailVal.setText(av2.getString("from_time") + " - " + av2.getString("to_time"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (rvAvailParent.getAdapter() == null) {
            mAdapter = new ProviderAvailAdapter(mActivity, provAvailData, 0);
            rvAvailParent.setAdapter(mAdapter);
        }
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_provider_submit)
    public void onViewClicked() {

        if (!validateData(etFromDate)) {
            return;
        }
        if (!validateData(etToDate)) {
            return;
        }

        mActivity.providerData.setFromDate(etFromDate.getText().toString());
        mActivity.providerData.setToDate(etToDate.getText().toString());

        int count = rvAvailParent.getAdapter().getItemCount();
        ArrayList<JSONObject> provAvail = new ArrayList<>();
        dayIndex = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            RecyclerView.ViewHolder vh = rvAvailParent.findViewHolderForAdapterPosition(i);
            Switch switchAvail = (Switch) vh.itemView.findViewById(R.id.switch_select);
            TextView tvFrom = (TextView) vh.itemView.findViewById(R.id.tv_from_time);
            TextView tvTo = (TextView) vh.itemView.findViewById(R.id.tv_to_time);
            if (switchAvail.isEnabled() && switchAvail.isChecked()) {
                switch (i) {
                    case 0:
                        for (int j = 1; j <= 7; j++) {
                            try {
                                JSONObject availData = new JSONObject();
                                availData.put("day", String.valueOf(j));
                                availData.put("from_time", tvFrom.getText().toString());
                                availData.put("to_time", tvTo.getText().toString());
                                provAvail.add(availData);
                                dayIndex.add(j);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        try {
                            JSONObject availData = new JSONObject();
                            availData.put("day", String.valueOf(i));
                            availData.put("from_time", tvFrom.getText().toString());
                            availData.put("to_time", tvTo.getText().toString());
                            provAvail.add(availData);
                            dayIndex.add(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }
        }
        if (provAvail.size() <= 0) {
            AppUtils.showToast(mContext, AppUtils.cleanLangStr(mContext, requestAndProviderList.getLg6_please_select_a(), R.string.err_txt_avail_time));
            return;
        }
        JSONArray availFinalData = new JSONArray(provAvail);

        mActivity.providerData.setAvailListData(availFinalData.toString());

        if (!validateDateRange()) {
            AppUtils.showToast(mContext, AppUtils.cleanLangStr(mContext, "Selected days not applicable in date range", R.string.err_txt_avail_time));
            return;
        }

        mActivity.postDataToServer();
    }

    @OnClick({R.id.et_from_date, R.id.et_to_date})
    public void onViewClicked(View view) {
        DialogFragment newFragment;
        String datelimit = "0";
        String setDate = "0";
        switch (view.getId()) {
            case R.id.et_from_date:
                datelimit = String.valueOf(System.currentTimeMillis() - 1000);
                /*DatePickerDialog fromDPG = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                fromDPG.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                fromDPG.show();*/
                if (etToDate.getTag() != null) {
                    datelimit = etToDate.getTag().toString();
                }

                try {
                    setDate = etFromDate.getTag().toString();
                } catch (Exception e) {
                }
                newFragment = new DatePickerFragment(getActivity(), etFromDate, datelimit, true, setDate, true);
                newFragment.show(getActivity().getFragmentManager(), getString(R.string.txt_from_date));
                break;
            case R.id.et_to_date:
                /*DatePickerDialog toDPG = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                toDPG.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                toDPG.show();*/
                datelimit = "0";
                if (etFromDate.getTag() != null) {
                    datelimit = etFromDate.getTag().toString();
                }
                try {
                    setDate = etToDate.getTag().toString();
                } catch (Exception e) {
                }
                newFragment = new DatePickerFragment(getActivity(), etToDate, datelimit, false, setDate, true);
                newFragment.show(getActivity().getFragmentManager(), getString(R.string.txt_to_date));
                break;
        }
    }

    private boolean validateData(EditText etTxt) {
        String msg2 = "";
        if (etTxt != null) {
            if (etTxt.getId() == R.id.et_from_date) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(getActivity(), requestAndProviderList.getLg6_date_cannot_be_(), R.string.err_txt_date);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(getActivity(), msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            } else if (etTxt.getId() == R.id.et_to_date) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(getActivity(), requestAndProviderList.getLg6_date_cannot_be_(), R.string.err_txt_date);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(getActivity(), msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            }
        }
        return true;
    }

    public boolean validateDateRange() {
        if (getDaysBetweenDates(etFromDate.getText().toString(), etToDate.getText().toString()) < 7) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(AppConstants.APP_DATE_FORMAT, Locale.getDefault());
            Date startDate = null, endDate = null;
            try {
                startDate = dateFormat.parse(etFromDate.getText().toString());
                endDate = dateFormat.parse(etToDate.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            List<Integer> dayOfWeek = getWeekDayNames(startDate, endDate);
            boolean validDay = true;
            for (int j = 0; j < dayIndex.size(); j++) {
                if (dayOfWeek.contains(dayIndex.get(j))) {
                    validDay = true && validDay;
                } else
                    validDay = false && validDay;
            }
            return validDay;
        }
        return true;
    }

    public long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(AppConstants.APP_DATE_FORMAT, Locale.getDefault());
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    private static long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    protected List<Integer> getWeekDayNames(Date startDate, Date endDate) {
        List<Integer> days = new ArrayList<Integer>();
        try {
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(startDate);
            Calendar endCal = Calendar.getInstance();
            endCal.setTime(endDate);
            if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
                days.add(startCal.get(Calendar.DAY_OF_WEEK));
                return Collections.unmodifiableList(days);
            }

            do {
                Calendar c = Calendar.getInstance();
                c.setTime(startCal.getTime());
                days.add(c.get(Calendar.DAY_OF_WEEK));
                startCal.add(Calendar.DAY_OF_MONTH, 1);
            } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.unmodifiableList(days);
    }



    /*DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {
        String myFormat = AppConstants.DATE_FORMAT; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        etFromDate.setText(sdf.format(myCalendar.getTime()));
    }*/
}
