package in.alibdaa.upbeat.digital.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.alibdaa.upbeat.digital.CreateProviderActivity;
import in.alibdaa.upbeat.digital.ProvideActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.SubscriptionActivity;
import in.alibdaa.upbeat.digital.adapters.ProviderAdapter;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

public class ProvideFragment extends Fragment {

    ProvideActivity mainActivity;
    Context mContext;
    private Paint p = new Paint();

    @BindView(R.id.tv_no_data)
    public TextView tvNoData;
    @BindView(R.id.rv_providers_list)
    public RecyclerView rvProvidersList;
    @BindView(R.id.fab_provider)
    FloatingActionButton fabProvider;
    Unbinder unbinder;

    //Fragment maintain state
    LinearLayoutManager mLayoutManager;
    public boolean isInitiated = false;
    public Bundle savedState;
    private boolean createdStateInDestroyView;

    //Pagination
    public int totalOrders, totalPages;
    public boolean isLoading = false;
    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;

    //Adapter and data
    public ProviderAdapter mAdapter;
    public ArrayList<ProviderListData.ProviderList> myData = new ArrayList<>();

    LanguageModel.Common_used_texts commonUsedTexts = new LanguageModel().new Common_used_texts();

    public void myProvideFragment(ProvideActivity mainActivity, Context mContext) {
        this.mainActivity = mainActivity;
        this.mContext = mContext;
        commonUsedTexts = mainActivity.commonData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_provider_list, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        rvProvidersList.setLayoutManager(mLayoutManager);
        tvNoData.setText(AppUtils.cleanLangStr(getActivity(), commonUsedTexts.getLg7_no_data_were_fo(), R.string.txt_no_data));
        try {
            fabProvider.setBackgroundTintList(ColorStateList.valueOf(mainActivity.appColor));
        } catch (Exception e) {
        }
        if (rvProvidersList.getAdapter() == null) {
            mAdapter = new ProviderAdapter(mainActivity, mContext, new ArrayList<ProviderListData.ProviderList>(), 0);
            rvProvidersList.setAdapter(mAdapter);
        }
        if (savedState != null) {
            myData = savedState.getParcelableArrayList("PROVIDER_LIST");
            if (myData != null && myData.size() > 0) {
                mAdapter.updateRecyclerView(mContext, myData);
                rvProvidersList.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
            } else {
                rvProvidersList.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
            }
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((ProvideActivity) mainActivity).getProviderData(true);
                    }
                }, 1000);

            }
        });

        rvProvidersList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                try {
                    totalItemCount = mLayoutManager.getItemCount();
                    int displayedPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                    if (displayedPosition == (mAdapter.itemsData.size() - 1)) {
                        if (!isLoading && mainActivity.providerNextPage > 0) {
                            if (dy > 0) //check for scroll down
                            {
                                visibleItemCount = mLayoutManager.getChildCount();
                                firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                    isLoading = true;
                                    Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                    if (mAdapter.mOnLoadMoreListener != null) {
                                        mAdapter.mOnLoadMoreListener.onLoadMore();
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        savedState = saveState();
        createdStateInDestroyView = true;
        myData = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (myData == null) {
            outState.putBundle("PROVIDER_LIST", savedState);
        } else {
            outState.putBundle("PROVIDER_LIST", createdStateInDestroyView ? savedState : saveState());
        }
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelableArrayList("PROVIDER_LIST", mAdapter.itemsData);
        return state;
    }

    @OnClick(R.id.fab_provider)
    public void onViewClicked() {
        if (PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE) > 0) {
            Intent in = new Intent(getActivity(), CreateProviderActivity.class);
            AppUtils.appStartIntent(getActivity(), in);
        } else {
            Intent myIntent = new Intent(getActivity(), SubscriptionActivity.class);
            myIntent.putExtra("FromPage", AppConstants.PAGE_CREATE_PROVIDER);
            AppUtils.appStartIntent(getActivity(), myIntent);
        }
    }
}
