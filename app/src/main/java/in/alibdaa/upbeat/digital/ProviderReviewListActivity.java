package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;

import in.alibdaa.upbeat.digital.adapters.ProviderReviewListAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.POSTReviewList;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class ProviderReviewListActivity extends BaseAppCompatActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.rv_reviewlist)
    RecyclerView rvSubCategorylist;
    LinearLayoutManager linearLayoutManager;
    ProviderReviewListAdapter providerReviewListAdapter;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    String CatID = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
//        setSupportActionBar(tbToolbar);
        ButterKnife.bind(this);
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, commonData.getLg7_reviews(), R.string.reviews));

        CatID = getIntent().getStringExtra(AppConstants.CatID);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        if (AppUtils.isNetworkAvailable(this)) {

            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Call<POSTReviewList> classificationCall = apiService.getReviewList(PreferenceStorage.getKey(AppConstants.MY_LANGUAGE), PreferenceStorage.getKey(AppConstants.USER_TOKEN), CatID, "1");
                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.REVIEWLIST, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }
        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }


    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

        POSTReviewList response = (POSTReviewList) myRes;
        if (response.getData() != null && response.getData().getReview_list() != null) {

            if (response.getData().getReview_list().size() > 0) {
                rvSubCategorylist.setLayoutManager(linearLayoutManager);
                providerReviewListAdapter = new ProviderReviewListAdapter(this, response.getData().getReview_list());
                rvSubCategorylist.setAdapter(providerReviewListAdapter);
            } else {
                Toast.makeText(this, "No Reviews Available", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(this, response.getResponseHeader().getResponseMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();

        }
    }
}
