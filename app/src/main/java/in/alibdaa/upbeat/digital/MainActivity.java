package in.alibdaa.upbeat.digital;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import in.alibdaa.upbeat.digital.adapters.ViewPagerAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.fragments.CategoryFragment;
import in.alibdaa.upbeat.digital.fragments.ProvideFragment;
import in.alibdaa.upbeat.digital.fragments.RequestFragment;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CustomViewPager;

import retrofit2.Call;

public class MainActivity extends BaseNavigationActivity implements TabLayout.OnTabSelectedListener, RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;
    @BindView(R.id.main_viewpager)
    CustomViewPager mainViewpager;

    RequestFragment requestFragment;
    ProvideFragment provideFragment;
    CategoryFragment categoryFragment;

    public int requestPageNo = 0, requestNextPage = 1, providerPageNo, providerNextPage = 1;

    RequestListData requestListData = new RequestListData();
    ArrayList<RequestListData.RequestList> requestList = new ArrayList<>();

    ProviderListData providerListData = new ProviderListData();
    ArrayList<ProviderListData.ProviderList> providerList = new ArrayList<>();

    boolean isFirstTime;
    LanguageModel.Dashboard dashboardData = new LanguageModel().new Dashboard();
    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    //public int appColor;
    AlertDialog alertDialog;
    @BindView(R.id.rl_main)
    RelativeLayout rlMain;
    @BindView(R.id.iv_loc)
    ImageView ivLoc;
    @BindView(R.id.tv_loc_text)
    TextView tvLocText;
    @BindView(R.id.tv_loc_sub_text)
    TextView tvLocSubText;
    @BindView(R.id.bt_turn_loc)
    Button btTurnLoc;
    @BindView(R.id.layout_permission_check)
    LinearLayout layoutPermissionCheck;
    String userToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

        //appColor = getAppTheme();
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, "", R.string.app_name));
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        categoryFragment = new CategoryFragment();
        categoryFragment.CategoryFragment(MainActivity.this, MainActivity.this);
        adapter.addFragment(categoryFragment, AppUtils.cleanLangStr(this, dashboardData.getLg8_providers(), R.string.txt_provider));


        // Add Fragments to adapter
        requestFragment = new RequestFragment();
        requestFragment.myRequestFragment(MainActivity.this, MainActivity.this);
        adapter.addFragment(requestFragment, AppUtils.cleanLangStr(this, dashboardData.getLg8_requests(), R.string.txt_request));

//        provideFragment = new ProvideFragment();
//        provideFragment.myProvideFragment(MainActivity.this, MainActivity.this);
//        adapter.addFragment(provideFragment, AppUtils.cleanLangStr(this, dashboardData.getLg8_providers(), R.string.txt_provider));



        mainViewpager.setAdapter(adapter);
        //mainViewpager.setOnPageChangeListener(this);
        mainViewpager.setOffscreenPageLimit(2);
        mainViewpager.disableScroll(false);

        //Tab
        tabLayout.setupWithViewPager(mainViewpager);
        tabLayout.setSelectedTabIndicatorColor(appColor);
        tabLayout.setOnTabSelectedListener(this);

        int currentFrag = getIntent().getIntExtra("MyFragment", 0);
        mainViewpager.setCurrentItem(currentFrag);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0://Requests
                try {
                    if (!requestFragment.isInitiated) {
                        if (!isFirstTime) {
                        //    ProgressDlg.clearDialog();
                       //     ProgressDlg.showProgressDialog(this, null, null);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                           //         ProgressDlg.dismissProgressDialog();
                                    getRequestData(false);
                                }
                            }, 500);
//                            getRequestData(false);
                        } else {
                            getRequestData(false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
//            case 1://Providers
//                try {
//                    if (!provideFragment.isInitiated)
//                        getProviderData(false);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.menu_action_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_search:
                loadSearchDialog();
                break;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false /*super.onOptionsItemSelected(item)*/;
    }

    private void loadSearchDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_search_view, null);
        dialogBuilder.setView(dialogView);

        SearchView srcView = (SearchView) dialogView.findViewById(R.id.sv_title);
        srcView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                alertDialog.dismiss();
                Intent searchResult = new Intent(MainActivity.this, SearchResultActivity.class);
                String searchType = mainViewpager.getCurrentItem() == 0 ? AppConstants.SEARCH_REQUEST : AppConstants.SEARCH_PROVIDER;
                searchResult.putExtra("latitude", PreferenceStorage.getKey(AppConstants.MY_LATITUDE));
                searchResult.putExtra("longitude", PreferenceStorage.getKey(AppConstants.MY_LONGITUDE));
                searchResult.putExtra("search_title", query);
                searchResult.putExtra("request_date", "");
                searchResult.putExtra("min_price", "");
                searchResult.putExtra("max_price", "");
                searchResult.putExtra("location", ""/*PreferenceStorage.getKey(AppConstants.MY_ADDRESS)*/);
                searchResult.putExtra("request_time", "");
                searchResult.putExtra("searchType", searchType);
                AppUtils.appStartIntent(MainActivity.this, searchResult);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        TextView editText = (TextView) dialogView.findViewById(R.id.tv_advanced_search);
        editText.setTextColor(appColor);
        editText.setText(AppUtils.cleanLangStr(this, commonData.getLg7_advanced_search(), R.string.txt_advanced_search));
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent gotTo = new Intent(MainActivity.this, AdvancedSearchActivity.class);
                String searchType = mainViewpager.getCurrentItem() == 0 ? AppConstants.SEARCH_REQUEST : AppConstants.SEARCH_PROVIDER;
                gotTo.putExtra("SearchType", searchType);
                AppUtils.appStartIntent(MainActivity.this, gotTo);
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.show();

        Window window = alertDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);

    }

    public void getRequestData(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
//            ProgressDlg.clearDialog();
//            if (!isLoadMore)
//                ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            String latitude = PreferenceStorage.getKey(AppConstants.MY_LATITUDE) == null ? getLatitude() : PreferenceStorage.getKey(AppConstants.MY_LATITUDE);
            String longitude = PreferenceStorage.getKey(AppConstants.MY_LONGITUDE) == null ? getLongitude() : PreferenceStorage.getKey(AppConstants.MY_LONGITUDE);
            String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);
            if(userToken!=null) {
                Call<RequestListData> classificationCall = apiService.getRequestListData(requestNextPage, latitude, longitude, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.REQUESTLIST_DATA, this, isLoadMore);
            }
            }

    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        isFirstTime = true;
        switch (responseModel) {
            case AppConstants.REQUESTLIST_DATA:
                if (isLoadMore) {
                    //Remove loading item
                    requestFragment.mAdapter.itemsData.remove(requestFragment.mAdapter.itemsData.size() - 1);
                    requestFragment.mAdapter.notifyItemRemoved(requestFragment.mAdapter.itemsData.size() - 1);
                }
                requestFragment.isInitiated = true;
                requestFragment.isLoading = false;
                requestListData = (RequestListData) myRes;
                try {
                    if (requestListData.getData() != null || requestListData.getData().getRequestList() != null && requestListData.getData().getRequestList().size() > 0) {
                        requestPageNo = Integer.parseInt(requestListData.getData().getCurrentPage());
                        requestNextPage = requestListData.getData().getNextPage();
                        if (!isLoadMore || requestPageNo == 1) {
                            requestFragment.mAdapter.itemsData = new ArrayList<>();
                            requestFragment.rvRequestsList.setVisibility(View.VISIBLE);
                            requestFragment.tvNoData.setVisibility(View.GONE);
                        }
                        requestFragment.mAdapter.updateRecyclerView(MainActivity.this, requestListData.getData().getRequestList());
                    } else if (isLoadMore && requestFragment.mAdapter.itemsData.size() > 0) {
                        requestFragment.rvRequestsList.setVisibility(View.VISIBLE);
                        requestFragment.tvNoData.setVisibility(View.GONE);
                    } else {
                        requestFragment.rvRequestsList.setVisibility(View.GONE);
                        requestFragment.tvNoData.setVisibility(View.VISIBLE);
                        requestFragment.tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
//            case AppConstants.PROVIDERLIST_DATA:
//                if (isLoadMore) {
//                    //Remove loading item
//                    provideFragment.mAdapter.itemsData.remove(provideFragment.mAdapter.itemsData.size() - 1);
//                    provideFragment.mAdapter.notifyItemRemoved(provideFragment.mAdapter.itemsData.size() - 1);
//                }
//                provideFragment.isInitiated = true;
//                provideFragment.isLoading = false;
//                providerListData = (ProviderListData) myRes;
//                if (providerListData.getData() != null || providerListData.getData().getProviderList() != null && providerListData.getData().getProviderList().size() > 0) {
//                    providerPageNo = Integer.parseInt(providerListData.getData().getCurrentPage());
//                    providerNextPage = providerListData.getData().getNextPage();
//                    if (!isLoadMore || providerPageNo == 1) {
//                        provideFragment.mAdapter.itemsData = new ArrayList<>();
//                        provideFragment.rvProvidersList.setVisibility(View.VISIBLE);
//                        provideFragment.tvNoData.setVisibility(View.GONE);
//                    }
//                    provideFragment.mAdapter.updateRecyclerView(MainActivity.this, providerListData.getData().getProviderList());
//                } else if (isLoadMore && provideFragment.mAdapter.itemsData.size() > 0) {
//                    provideFragment.rvProvidersList.setVisibility(View.VISIBLE);
//                    provideFragment.tvNoData.setVisibility(View.GONE);
//                } else {
//                    provideFragment.rvProvidersList.setVisibility(View.GONE);
//                    provideFragment.tvNoData.setVisibility(View.VISIBLE);
//                    provideFragment.tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
//                }
//                break;
            case AppConstants.DELETE_REQUEST_DATA:
                try {
                    requestNextPage = 1;
                    requestFragment.mAdapter.itemsData = new ArrayList<>();
                    requestFragment.myData = null;
                    requestFragment.savedState = null;
                    requestFragment.isInitiated = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getRequestData(false);
                break;
//            case AppConstants.DELETE_PROVIDER_DATA:
//                try {
//                    providerNextPage = 1;
//                    provideFragment.mAdapter.itemsData = new ArrayList<>();
//                    provideFragment.myData = null;
//                    provideFragment.savedState = null;
//                    provideFragment.isInitiated = false;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                getProviderData(false);
//                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        isFirstTime = true;
        switch (mainViewpager.getCurrentItem()) {
            case 0:
                if (myRes instanceof RequestListData) {
                    requestListData = (RequestListData) myRes;
                    if (requestListData != null && requestListData.getResponseHeader() != null
                            && requestListData.getResponseHeader().getResponseCode() != null
                            && requestListData.getResponseHeader().getResponseCode().equalsIgnoreCase("0")) {
                        requestFragment.isInitiated = true;
                        requestFragment.tvNoData.setVisibility(View.VISIBLE);
                        requestFragment.rvRequestsList.setVisibility(View.GONE);
                    }
                }
                break;
//            case 1:
//                if (myRes instanceof ProviderListData) {
//                    providerListData = (ProviderListData) myRes;
//                    if (providerListData != null && providerListData.getResponseHeader() != null
//                            && providerListData.getResponseHeader().getResponseCode() != null
//                            && providerListData.getResponseHeader().getResponseCode().equalsIgnoreCase("0")) {
//                        provideFragment.isInitiated = true;
//                    }
//                }
//                break;
        }
//        noDataHandling(myRes, isLoadMore, responseModel);
    }

    private void noDataHandling(Object myRes, boolean isLoadMore, String responseModel) {
        switch (mainViewpager.getCurrentItem()) {
            case 0:
                requestFragment.isLoading = false;
                if (responseModel.equalsIgnoreCase(AppConstants.REQUESTLIST_DATA)) {
                    if (isLoadMore && requestFragment.mAdapter != null && requestFragment.mAdapter.itemsData != null && requestFragment.mAdapter.itemsData.size() > 0) {
                        requestFragment.mAdapter.itemsData.remove(requestFragment.mAdapter.itemsData.size() - 1);
                        requestFragment.mAdapter.notifyItemRemoved(requestFragment.mAdapter.itemsData.size() - 1);
                    } else {
                        requestFragment.rvRequestsList.setVisibility(View.GONE);
                        requestFragment.tvNoData.setVisibility(View.VISIBLE);
                        requestFragment.tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
                    }
                }
                break;
//            case 1:
//                provideFragment.isLoading = false;
//                if (responseModel.equalsIgnoreCase(AppConstants.PROVIDERLIST_DATA)) {
//                    if (isLoadMore && provideFragment.mAdapter != null && provideFragment.mAdapter.itemsData != null && provideFragment.mAdapter.itemsData.size() > 0) {
//                        provideFragment.mAdapter.itemsData.remove(provideFragment.mAdapter.itemsData.size() - 1);
//                        provideFragment.mAdapter.notifyItemRemoved(provideFragment.mAdapter.itemsData.size() - 1);
//                    } else {
//                        provideFragment.rvProvidersList.setVisibility(View.GONE);
//                        provideFragment.tvNoData.setVisibility(View.VISIBLE);
//                        provideFragment.tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
//                    }
//                }
//                break;
        }
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
//        noDataHandling(myRes, isLoadMore, responseModel);
    }

//    public void getProviderData(boolean isLoadMore) {
//        if (AppUtils.isNetworkAvailable(this)) {
//            if (!isLoadMore) {
//                ProgressDlg.clearDialog();
//                ProgressDlg.showProgressDialog(this, null, null);
//            }
//            ApiInterface apiService =
//                    ApiClient.getClient().create(ApiInterface.class);
//            String latitude = PreferenceStorage.getKey(AppConstants.MY_LATITUDE) == null ? getLatitude() : PreferenceStorage.getKey(AppConstants.MY_LATITUDE);
//            String longitude = PreferenceStorage.getKey(AppConstants.MY_LONGITUDE) == null ? getLongitude() : PreferenceStorage.getKey(AppConstants.MY_LONGITUDE);
//            Call<ProviderListData> classificationCall = apiService.getProviderListData(providerNextPage, latitude, longitude, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
//            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROVIDERLIST_DATA, this, isLoadMore);
//        } else {
//            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG_MAIN", "Main resumed");
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_dashboard(), R.string.txt_dashboard));
        try {
            tabLayout.getTabAt(1).setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_requests(), R.string.txt_request));
            requestFragment.tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
        } catch (Exception e) {
        }
//        try {
//            tabLayout.getTabAt(1).setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_providers(), R.string.txt_provider));
//            provideFragment.tvNoData.setText(AppUtils.cleanLangStr(this, dashboardData.getLg8_no_data_were_fo(), R.string.txt_no_data));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        tvLocText.setText(AppUtils.cleanLangStr(this, commonData.getLg7_location_access(), R.string.loc_text));
        tvLocSubText.setText(AppUtils.cleanLangStr(this, commonData.getLg7_let_us_know_whe(), R.string.loc_sub_text));
        btTurnLoc.setText(AppUtils.cleanLangStr(this, commonData.getLg7_turn_on_locatio(), R.string.turn_on_loc));

        tvLocText.setTextColor(appColor);
        ivLoc.setColorFilter(appColor, PorterDuff.Mode.SRC_ATOP);
        btTurnLoc.setBackgroundColor(appColor);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest != null && permissionsToRequest.size() == 0) {
            rlMain.setVisibility(View.VISIBLE);
            layoutPermissionCheck.setVisibility(View.GONE);
            switch (mainViewpager.getCurrentItem()) {
                case 0:
                    try {
                        requestNextPage = 1;
                        requestFragment.mAdapter.itemsData = new ArrayList<>();
                        requestFragment.myData = null;
                        requestFragment.savedState = null;
                        requestFragment.isInitiated = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!isFirstTime) {
//                        ProgressDlg.clearDialog();
//                        ProgressDlg.showProgressDialog(this, null, null);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                ProgressDlg.dismissProgressDialog();
                                getRequestData(false);
                            }
                        }, 500);
//                        getRequestData(false);
                    } else {
                        getRequestData(false);
                    }
                    break;
//                case 1:
//                    try {
//                        providerNextPage = 1;
//                        provideFragment.mAdapter.itemsData = new ArrayList<>();
//                        provideFragment.myData = null;
//                        provideFragment.savedState = null;
//                        provideFragment.isInitiated = false;
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    getProviderData(false);
//                    break;
            }
        } else {
            rlMain.setVisibility(View.GONE);
            layoutPermissionCheck.setVisibility(View.VISIBLE);
            //Toast.makeText(this, "Enable permission...", Toast.LENGTH_SHORT).show();
        }
        //checkLocationPermission();

    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.dashboard);
            dashboardData = new Gson().fromJson(commonDataStr, LanguageModel.Dashboard.class);
        } catch (Exception e) {
            dashboardData = new LanguageModel().new Dashboard();
        }

        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            langReqProvData = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
        }
    }

    public void deleteProvider(final String serviceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_provider_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(MainActivity.this)) {
                        //    ProgressDlg.clearDialog();
                         //   ProgressDlg.showProgressDialog(MainActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteProviderData(serviceId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(MainActivity.this, classificationCall, AppConstants.DELETE_PROVIDER_DATA, MainActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(MainActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void deleteRequest(final String requestId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_request_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(MainActivity.this)) {
                     //       ProgressDlg.clearDialog();
                     //       ProgressDlg.showProgressDialog(MainActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteRequestData(requestId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(MainActivity.this, classificationCall, AppConstants.DELETE_REQUEST_DATA, MainActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(MainActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @OnClick(R.id.bt_turn_loc)
    public void onViewClicked() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }


}
