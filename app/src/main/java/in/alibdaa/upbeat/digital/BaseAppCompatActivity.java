package in.alibdaa.upbeat.digital;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class BaseAppCompatActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener {

    private LinearLayout view_stub; //This is the framelayout to keep your content view
    Toolbar mToolbar;
    private Activity currentActivity;
    private Location mLocation;
    String Latitude, Longitude;
    protected GoogleApiClient mGoogleApiClient;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    public ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    public ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    LanguageModel.Navigation navData = new LanguageModel().new Navigation();
    LanguageModel.Logout logoutData = new LanguageModel().new Logout();
    LanguageModel.Sign_up signUpData = new LanguageModel().new Sign_up();
    LanguageModel.Dashboard dashboardData = new LanguageModel().new Dashboard();
    LanguageModel.Profile profileData = new LanguageModel().new Profile();
    public int appColor = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base_layout);

        view_stub = (LinearLayout) findViewById(R.id.view_stub);

        getLocaleData();
        getAppTheme();

        mToolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        checkLocationPermission();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    public void checkLocationPermission() {
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }

    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
     * so that, we can make other activity implementations looks like normal activity subclasses.
     */
    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    public void setCurrentActivity(Activity activity) {
        currentActivity = activity;
    }

    public void setToolBarTitle(String title) {
        mToolbar.setTitle(title);
        try {
            getSupportActionBar().setTitle(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        if (mLocation != null) {
            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            ((Activity) this).getWindow().getDecorView().setLayoutDirection(PreferenceStorage.getKey(AppConstants.MY_LANGUAGE).equalsIgnoreCase("ar")
                    ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("TAG_BASE", "Base Resumed");
        getAppTheme();
        applyThemeColor();
        getLocaleData();
        if (!checkPlayServices()) {
            Toast.makeText(this, AppUtils.cleanLangStr(this, commonData.getLg7_please_install_(), R.string.err_txt_install_play_ser), Toast.LENGTH_SHORT).show();
        }
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
        }

    }

    private void applyThemeColor() {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(appColor));
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    public String getLatitude() {
        String latitude = Latitude;
        return latitude;
    }

    public String getLongitude() {
        String longitude = Longitude;
        return longitude;
    }

    public String getLocation() {
        String location = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try {
            if (mLocation != null) {
                addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1);
                if (addresses != null && addresses.size() > 0) {
                    location = addresses.get(0).getAddressLine(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mLocation != null) {
            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());
            if (PreferenceStorage.getKey(AppConstants.MY_LATITUDE) == null
                    && PreferenceStorage.getKey(AppConstants.MY_LONGITUDE) == null) {
                PreferenceStorage.setKey(AppConstants.MY_LATITUDE, Latitude);
                PreferenceStorage.setKey(AppConstants.MY_LONGITUDE, Longitude);
                PreferenceStorage.setKey(AppConstants.MY_ADDRESS, getLocation());
            }
        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_enable_permissi(), R.string.err_txt_enable_permission), Toast.LENGTH_LONG).show();

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);


    }


    public ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            mGoogleApiClient.disconnect();
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();
        }
        try {
            String navDataStr = PreferenceStorage.getKey(CommonLangModel.navigation);
            navData = new Gson().fromJson(navDataStr, LanguageModel.Navigation.class);
        } catch (Exception e) {
            navData = new LanguageModel().new Navigation();
        }

        try {
            String logoutDataStr = PreferenceStorage.getKey(CommonLangModel.logout);
            logoutData = new Gson().fromJson(logoutDataStr, LanguageModel.Logout.class);
        } catch (Exception e) {
            logoutData = new LanguageModel().new Logout();
        }
        try {
            String signUpDataStr = PreferenceStorage.getKey(CommonLangModel.sign_up);
            signUpData = new Gson().fromJson(signUpDataStr, LanguageModel.Sign_up.class);
        } catch (Exception e) {
            signUpData = new LanguageModel().new Sign_up();
        }
        try {
            String profileDataStr = PreferenceStorage.getKey(CommonLangModel.profile);
            profileData = new Gson().fromJson(profileDataStr, LanguageModel.Profile.class);
        } catch (Exception e) {
            profileData = new LanguageModel().new Profile();
        }

        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.dashboard);
            dashboardData = new Gson().fromJson(commonDataStr, LanguageModel.Dashboard.class);
        } catch (Exception e) {
            dashboardData = new LanguageModel().new Dashboard();
        }
    }

    public int getAppTheme() {
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }
        return appColor;
    }


}
