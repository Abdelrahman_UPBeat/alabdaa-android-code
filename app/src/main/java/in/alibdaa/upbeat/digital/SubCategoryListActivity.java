package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.adapters.SubCategoryListAdapter;
import in.alibdaa.upbeat.digital.datamodel.SubCategoryList;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class SubCategoryListActivity extends BaseAppCompatActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.rv_categorylist)
    RecyclerView rvSubCategorylist;
    LinearLayoutManager linearLayoutManager;
    SubCategoryListAdapter subCategoryListAdapter;
    String CatID = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory_list);
//        setSupportActionBar(tbToolbar);
        ButterKnife.bind(this);
        setToolBarTitle(getIntent().getStringExtra(AppConstants.SUBCATNAME));

        CatID = getIntent().getStringExtra(AppConstants.CatID);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        if (AppUtils.isNetworkAvailable(this)) {

            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Call<SubCategoryList> classificationCall = apiService.getSubCategories(PreferenceStorage.getKey(AppConstants.USER_TOKEN), CatID);
                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.SUBCATEGORIES, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }


    }


    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

        SubCategoryList response = (SubCategoryList) myRes;
        if (response.getData() != null && response.getData().getCategory_list() != null) {
            rvSubCategorylist.setLayoutManager(linearLayoutManager);
            subCategoryListAdapter = new SubCategoryListAdapter(this, response.getData().getCategory_list());
            rvSubCategorylist.setAdapter(subCategoryListAdapter);
        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }
}
