package in.alibdaa.upbeat.digital.datamodel;

public class CommonLangModel {
    public static String login = "login";
    public static String common_used_texts = "commonTexts";
    public static String dashboard = "dashboard";
    public static String logout = "logout";
    public static String navigation = "navigation";
    public static String profile = "profile";
    public static String request_and_provider_list = "request_and_provider_list";
    public static String sign_up = "sign_up";
    public static String subscription = "subscription";
}
