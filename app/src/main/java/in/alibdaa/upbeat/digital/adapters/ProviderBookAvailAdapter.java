package in.alibdaa.upbeat.digital.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.ProviderAvailableTimings;

public class ProviderBookAvailAdapter extends RecyclerView.Adapter<ProviderBookAvailAdapter.ViewHolder> {

    public List<ProviderAvailableTimings.Availability_detail> availList;
    Context mContext;


    public ProviderBookAvailAdapter(Context mActivity, List<ProviderAvailableTimings.Availability_detail> availList) {
        this.mContext = mActivity;
        this.availList = availList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_provider_choosedate, parent, false);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {


        viewHolder.setIsRecyclable(false);
        if (availList.get(position).getIs_selected().equalsIgnoreCase("1")) {
            viewHolder.rbTime.setChecked(true);
        } else {
            viewHolder.rbTime.setChecked(false);
        }
        viewHolder.rbTime.setText(availList.get(position).getService_date() + ", " + availList.get(position).getService_day() + ", " + availList.get(position).getService_time());

        viewHolder.rbTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for (int i = 0; i < availList.size(); i++) {
                    availList.get(i).setIs_selected("0");
                }
                availList.get(position).setIs_selected("1");
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });


            }
        });


    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return availList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rb_time)
        RadioButton rbTime;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
