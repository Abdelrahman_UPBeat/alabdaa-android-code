package in.alibdaa.upbeat.digital;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class RequestDetailActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    ArrayList<String> descDataList = new ArrayList<>();
    @BindView(R.id.tv_requester_name)
    TextView tvRequesterName;
    @BindView(R.id.tv_req_email)
    TextView tvReqEmail;
    @BindView(R.id.tv_req_contact)
    TextView tvReqContact;
    @BindView(R.id.cv_req_user_detail)
    CardView cvReqUserDetail;
    @BindView(R.id.tv_request_title)
    TextView tvRequestTitle;
    @BindView(R.id.ll_req_desc_detail)
    LinearLayout llReqDescDetail;
    @BindView(R.id.cv_req_detail)
    CardView cvReqDetail;
    @BindView(R.id.tv_req_location)
    TextView tvReqLocation;
    @BindView(R.id.tv_req_date)
    TextView tvReqDate;
    @BindView(R.id.tv_req_time)
    TextView tvReqTime;
    @BindView(R.id.cv_req_availability)
    CardView cvReqAvailability;
    @BindView(R.id.tv_req_fee)
    TextView tvReqFee;


    @BindView(R.id.btn_request_accept)
    Button btnRequestAccept;

    RequestListData.RequestList requestDataDetail;

    int viewType;
    @BindView(R.id.iv_user_img)
    ImageView ivUserImg;

    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();
    @BindView(R.id.tv_edit)
    TextView tvEdit;
    @BindView(R.id.ll_prov_edit)
    LinearLayout llProvEdit;
    @BindView(R.id.tv_delete)
    TextView tvDelete;
    @BindView(R.id.ll_delete)
    LinearLayout llDelete;
    @BindView(R.id.ll_user_footer)
    LinearLayout llUserFooter;
    @BindView(R.id.btn_request_complete)
    Button btnRequestComplete;
    @BindView(R.id.ll_footer)
    LinearLayout llFooter;
    @BindView(R.id.ll_prov_chat)
    LinearLayout llProvChat;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    @BindView(R.id.ll_footer1)
    LinearLayout llFooter1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_request_details);
        ButterKnife.bind(this);
        getLocaleData();

        requestDataDetail = getIntent().getParcelableExtra("RequestData");
        viewType = getIntent().getIntExtra("ViewType", 0);

        if (requestDataDetail != null) {
            tvRequesterName.setText(requestDataDetail.getUsername());
            tvReqEmail.setText(requestDataDetail.getEmail());
            tvReqContact.setText(requestDataDetail.getContactNumber());

            tvRequestTitle.setText(requestDataDetail.getTitle());
            setToolBarTitle(requestDataDetail.getTitle());
            if (requestDataDetail.getDescription() != null) {
                try {
                    JSONArray descList = new JSONArray(requestDataDetail.getDescription());
                    for (int i = 0; i < (descList.length()); i++) {
                        addDescView(i, descList.getString(i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                /*AppUtils.getAddressFromLocation(Double.parseDouble(requestDataDetail.getLatitude()), Double.parseDouble(requestDataDetail.getLongitude()),
                        getApplicationContext(), new GeocoderHandler());*/
                tvReqLocation.setText(requestDataDetail.getLocation());
            } catch (Exception e) {
                e.printStackTrace();
            }

            String ivPic;
            if (viewType == 1 || viewType == 2) {
                ivPic = AppConstants.BASE_URL + PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG);
                tvRequesterName.setText(PreferenceStorage.getKey(AppConstants.USER_NAME));
                tvReqEmail.setText(PreferenceStorage.getKey(AppConstants.USER_EMAIL));
//                tvReqContact.setText(PreferenceStorage.getKey(AppConstants.USER_PHONE));

            } else
                ivPic = AppConstants.BASE_URL + requestDataDetail.getProfileImg();

            Picasso.with(this)
                    .load(ivPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(ivUserImg);

            tvReqDate.setText(requestDataDetail.getRequestDate());
            tvReqTime.setText(requestDataDetail.getRequestTime());
            tvReqFee.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_expecting_fee(), R.string.txt_expecting_fee) + requestDataDetail.getCurrencyCode() + " " + requestDataDetail.getAmount());
            btnRequestAccept.setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_accept_request(), R.string.txt_accept_request));
        }
        btnRequestAccept.setBackgroundColor(appColor);
        llCall.setBackgroundColor(appColor);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int requesterID = Integer.parseInt(requestDataDetail.getRequesterId());
        int userId = PreferenceStorage.getIntKey(AppConstants.USER_ID);
        if (viewType != 1 && requesterID != userId && requestDataDetail.getStatus().equalsIgnoreCase("0")) {
            btnRequestAccept.setVisibility(View.VISIBLE);
            llFooter.setVisibility(View.VISIBLE);
        } else if (viewType == 1 && requestDataDetail.getStatus().equalsIgnoreCase("1")) {
            btnRequestComplete.setVisibility(View.VISIBLE);
        } else if (viewType == 1 && requestDataDetail.getStatus().equalsIgnoreCase("0")) {
            llUserFooter.setVisibility(View.VISIBLE);
            llFooter.setVisibility(View.GONE);

        }
        if (PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE) > 0) {
            tvReqContact.setVisibility(View.VISIBLE);
            tvReqEmail.setVisibility(View.VISIBLE);
        } else if (viewType != 1 && requesterID != userId) {
            tvReqContact.setVisibility(View.INVISIBLE);
            tvReqEmail.setVisibility(View.INVISIBLE);
        }


    }

    private synchronized void addDescView(int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View inflatedLayout = inflater.inflate(R.layout.layout_bullet_textview, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setText(descVal);
        llReqDescDetail.addView(inflatedLayout);
    }

    @OnClick(R.id.btn_request_accept)
    public void onViewClicked() {
        if ((PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE)) > 0) {
            performReqAccept();
        } else {
            Intent myIntent = new Intent(this, GoToSubscriptionActivity.class);
            myIntent.putExtra("FromPage", AppConstants.PAGE_REQUEST_DETAIL);
            AppUtils.appStartIntent(this, myIntent);
        }
    }

    private void performReqAccept() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(RequestDetailActivity.this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            try {
                Call<EmptyData> classificationCall = apiService.postRequestAcceptData(Integer.parseInt(requestDataDetail.getRId()), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(RequestDetailActivity.this, classificationCall, AppConstants.REQUEST_ACCEPT_DATA, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        switch (responseModel) {
            case AppConstants.REQUEST_ACCEPT_DATA:
                EmptyData requestAcceptData = (EmptyData) myRes;
                AppUtils.showToast(RequestDetailActivity.this, requestAcceptData.getResponseHeader().getResponseMessage());
                RequestDetailActivity.this.finish();
                break;
            case AppConstants.REQUEST_COMPLETE_DATA:
                EmptyData requestCompleteData = (EmptyData) myRes;
                AppUtils.showToast(RequestDetailActivity.this, requestCompleteData.getResponseHeader().getResponseMessage());
                RequestDetailActivity.this.finish();
                break;
            case AppConstants.DELETE_REQUEST_DATA:
                EmptyData requestDeleteData = (EmptyData) myRes;
                AppUtils.showToast(RequestDetailActivity.this, requestDeleteData.getResponseHeader().getResponseMessage());
                RequestDetailActivity.this.finish();
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
        }
    }

    @OnClick({R.id.ll_prov_edit, R.id.ll_delete, R.id.btn_request_complete, R.id.ll_prov_chat, R.id.ll_call})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_prov_edit:
                Intent detailPage = new Intent(RequestDetailActivity.this, EditRequestActivity.class);
                detailPage.putExtra("ViewType", viewType);
                detailPage.putExtra("RequestData", requestDataDetail);
                AppUtils.appStartIntent(RequestDetailActivity.this, detailPage);
                RequestDetailActivity.this.finish();
                break;
            case R.id.ll_delete:
                deleteRequest(requestDataDetail.getRId());
                break;
            case R.id.btn_request_complete:
                performReqComplete();
                break;
            case R.id.ll_prov_chat:
                Intent callChatDetailAct = new Intent(this, ChatRequestorDetailActivity.class);
                callChatDetailAct.putExtra(AppConstants.chatFrom, requestDataDetail.getRequesterId());
                callChatDetailAct.putExtra(AppConstants.chatUsername, requestDataDetail.getUsername());
                callChatDetailAct.putExtra(AppConstants.chatImg, requestDataDetail.getProfileImg());
                AppUtils.appStartIntent(this, callChatDetailAct);
                break;
            case R.id.ll_call:
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", requestDataDetail.getProfileContactNo(), null));
                    startActivity(intent);
                } catch (Exception e) {
                    if (e instanceof ActivityNotFoundException) {
                        AppUtils.showToast(RequestDetailActivity.this, "Dialing not supported");//TODO:
                    }
                }
                break;
        }
    }

    public void deleteRequest(final String requestId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AppUtils.cleanLangStr(this, "", R.string.txt_request_delete))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (AppUtils.isNetworkAvailable(RequestDetailActivity.this)) {
                            ProgressDlg.clearDialog();
                            ProgressDlg.showProgressDialog(RequestDetailActivity.this, null, null);
                            ApiInterface apiService =
                                    ApiClient.getClient().create(ApiInterface.class);
                            Call<EmptyData> classificationCall = apiService.deleteRequestData(requestId, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                            RetrofitHandler.executeRetrofit(RequestDetailActivity.this, classificationCall, AppConstants.DELETE_REQUEST_DATA, RequestDetailActivity.this, false);
                        } else {
                            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(RequestDetailActivity.this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
                        }
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void performReqComplete() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(RequestDetailActivity.this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            try {
                Call<EmptyData> classificationCall = apiService.postRequestCompleteData(Integer.parseInt(requestDataDetail.getRId()), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(RequestDetailActivity.this, classificationCall, AppConstants.REQUEST_COMPLETE_DATA, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }
        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }

}
