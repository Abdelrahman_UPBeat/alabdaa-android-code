package in.alibdaa.upbeat.digital;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.adapters.ViewPagerAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.HistoryListData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.fragments.HistoryCompletedFragment;
import in.alibdaa.upbeat.digital.fragments.HistoryPendingFragment;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class HistoryActivity extends BaseNavigationActivity implements ViewPager.OnPageChangeListener, RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.sliding_tabs)
    TabLayout slidingTabs;
    @BindView(R.id.vp_history)
    ViewPager historyViewPager;
    @BindView(R.id.fab_hist_list)
    FloatingActionButton fabHistList;

    public int hPendingNextPage = 1, hPendingPageNo, hCompletedNextPage = 1, hCompletedPageNo;
    HistoryListData hPendingData, hCompletedData;

    HistoryPendingFragment hPendingFragment;
    HistoryCompletedFragment hCompletedFragment;

    int hisStatus = 1, hisReqType = 1;

    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_history_request_list);
        ButterKnife.bind(this);
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_history(), R.string.txt_request_history));
        getLocaleData();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragments to adapter
        hPendingFragment = new HistoryPendingFragment();
        hPendingFragment.myHistoryFragment(HistoryActivity.this, HistoryActivity.this);
        adapter.addFragment(hPendingFragment, AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_pending(), R.string.txt_pending));

        hCompletedFragment = new HistoryCompletedFragment();
        hCompletedFragment.myHistoryFragment(HistoryActivity.this, HistoryActivity.this);
        adapter.addFragment(hCompletedFragment, AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_completed(), R.string.txt_completed));

        historyViewPager.setAdapter(adapter);
        historyViewPager.setOnPageChangeListener(this);
        historyViewPager.setOffscreenPageLimit(2);

        //Tab
        slidingTabs.setupWithViewPager(historyViewPager);
        slidingTabs.setSelectedTabIndicatorColor(appColor);

        fabHistList.setBackgroundTintList(ColorStateList.valueOf(appColor));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0://Pending
                try {
                    hisStatus = AppConstants.HIS_PENDING;
                    if (!hPendingFragment.isInitiated) {
                        getHistoryData(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 1://Completed
                try {
                    hisStatus = AppConstants.HIS_COMPLETED;
                    if (!hCompletedFragment.isInitiated) {
                        getHistoryData(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void getHistoryData(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            if (!isLoadMore)
                ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            int nextPage = 1;
            if (hisStatus == 1)
                nextPage = hPendingNextPage;
            else
                nextPage = hCompletedNextPage;
            Call<HistoryListData> classificationCall = apiService.getHistoryListData(hisStatus, hisReqType, nextPage, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.HISTORY_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        switch (historyViewPager.getCurrentItem()) {
            case 0:
                if (isLoadMore) {
                    //Remove loading item
                    hPendingFragment.mAdapter.itemsData.remove(hPendingFragment.mAdapter.itemsData.size() - 1);
                    hPendingFragment.mAdapter.notifyItemRemoved(hPendingFragment.mAdapter.itemsData.size() - 1);
                }
                hPendingFragment.isInitiated = true;
                hPendingFragment.isLoading = false;
                hPendingData = (HistoryListData) myRes;
                if (hPendingData.getData() != null || hPendingData.getData().getRequestList() != null && hPendingData.getData().getRequestList().size() > 0) {
                    try {
                        hPendingPageNo = Integer.parseInt(hPendingData.getData().getCurrentPage());
                        hPendingNextPage = hPendingData.getData().getNextPage();
                        if (!isLoadMore || hPendingPageNo == 1) {
                            hPendingFragment.mAdapter.itemsData = new ArrayList<>();
                            hPendingFragment.rvRequestsList.setVisibility(View.VISIBLE);
                            hPendingFragment.tvNoData.setVisibility(View.GONE);
                        }
                        hPendingFragment.mAdapter.updateRecyclerView(HistoryActivity.this, hPendingData.getData().getRequestList());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    hPendingFragment.rvRequestsList.setVisibility(View.GONE);
                    hPendingFragment.tvNoData.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                if (isLoadMore) {
                    //Remove loading item
                    hCompletedFragment.mAdapter.itemsData.remove(hCompletedFragment.mAdapter.itemsData.size() - 1);
                    hCompletedFragment.mAdapter.notifyItemRemoved(hCompletedFragment.mAdapter.itemsData.size() - 1);
                }
                hCompletedFragment.isInitiated = true;
                hCompletedFragment.isLoading = false;
                hCompletedData = (HistoryListData) myRes;
                if (hCompletedData.getData() != null || hCompletedData.getData().getRequestList() != null && hCompletedData.getData().getRequestList().size() > 0) {
                    try {
                        hCompletedPageNo = Integer.parseInt(hCompletedData.getData().getCurrentPage());
                        hCompletedNextPage = hCompletedData.getData().getNextPage();
                        if (!isLoadMore || hCompletedPageNo == 1) {
                            hCompletedFragment.mAdapter.itemsData = new ArrayList<>();
                            hCompletedFragment.rvRequestsList.setVisibility(View.VISIBLE);
                            hCompletedFragment.tvNoData.setVisibility(View.GONE);
                        }
                        hCompletedFragment.mAdapter.updateRecyclerView(HistoryActivity.this, hCompletedData.getData().getRequestList());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    hCompletedFragment.rvRequestsList.setVisibility(View.GONE);
                    hCompletedFragment.tvNoData.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        failureHandling(isLoadMore, responseModel);
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        failureHandling(isLoadMore, responseModel);
    }

    public void failureHandling(boolean isLoadMore, String responseModel) {
        try {
            switch (historyViewPager.getCurrentItem()) {
                case 0:
                    if (isLoadMore) {
                        //Remove loading item
                        hPendingFragment.mAdapter.itemsData.remove(hPendingFragment.mAdapter.itemsData.size() - 1);
                        hPendingFragment.mAdapter.notifyItemRemoved(hPendingFragment.mAdapter.itemsData.size() - 1);
                    }
                    hPendingFragment.isLoading = false;
                    if (!isLoadMore) {
                        hPendingFragment.rvRequestsList.setVisibility(View.GONE);
                        hPendingFragment.tvNoData.setVisibility(View.VISIBLE);
                    }
                    break;
                case 1:
                    if (isLoadMore) {
                        //Remove loading item
                        hCompletedFragment.mAdapter.itemsData.remove(hCompletedFragment.mAdapter.itemsData.size() - 1);
                        hCompletedFragment.mAdapter.notifyItemRemoved(hCompletedFragment.mAdapter.itemsData.size() - 1);
                    }
                    hCompletedFragment.isLoading = false;
                    if (!isLoadMore) {
                        hCompletedFragment.rvRequestsList.setVisibility(View.GONE);
                        hCompletedFragment.tvNoData.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        slidingTabs.getTabAt(0).setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_pending(), R.string.txt_pending));
        slidingTabs.getTabAt(1).setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_completed(), R.string.txt_completed));
        String txtNoData = AppUtils.cleanLangStr(this, commonData.getLg7_no_data_were_fo(), R.string.txt_no_data);
        switch (historyViewPager.getCurrentItem()) {
            case 0:
                try {
                    hPendingNextPage = 1;
                    hPendingPageNo = 1;
                    hPendingFragment.tvNoData.setText(txtNoData);
                    hPendingFragment.mAdapter.itemsData = new ArrayList<>();
                    hPendingFragment.myData = null;
                    hPendingFragment.savedState = null;
                    hPendingFragment.isInitiated = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                try {
                    hCompletedNextPage = 1;
                    hCompletedPageNo = 1;
                    hCompletedFragment.tvNoData.setText(txtNoData);
                    hCompletedFragment.mAdapter.itemsData = new ArrayList<>();
                    hCompletedFragment.myData = null;
                    hCompletedFragment.savedState = null;
                    hCompletedFragment.isInitiated = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        getHistoryData(false);
    }

    @OnClick(R.id.fab_hist_list)
    public void onViewClicked() {
        final CharSequence[] items = {AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_requested(), R.string.txt_requested),
                AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_accepted(), R.string.txt_accepted),
                AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_all(), R.string.txt_all)};
        AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
        builder.setTitle(AppUtils.cleanLangStr(this, navData.getLg3_history(), R.string.txt_services_history));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        hisReqType = AppConstants.HIS_USER_CREATED;
                        break;
                    case 1:
                        hisReqType = AppConstants.HIS_USER_ACCEPTED;
                        break;
                    case 2:
                        hisReqType = AppConstants.HIS_USER_ALL;
                        break;
                }
                dialog.dismiss();
                if (hisStatus == 1)
                    hPendingNextPage = 1;
                else
                    hCompletedNextPage = 1;
                getHistoryData(false);
            }
        });
        builder.show();
    }

    private void getLocaleData() {

        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
        }
    }
}

