package in.alibdaa.upbeat.digital;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Locale;

import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.FontsOverride;
import in.alibdaa.upbeat.digital.utils.LocaleUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import io.fabric.sdk.android.Fabric;

public class ServProApplication extends Application implements  GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener{

    PreferenceStorage preferenceStorage;
    ServProApplication servProApplication;

    //Location
    private Location mLocation;
    String Latitude, Longitude;
    protected GoogleApiClient mGoogleApiClient;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    public int appColor = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        servProApplication = this;
        if (!BuildConfig.DEBUG) { // only enable bug tracking in release version
            Fabric.with(this, new Crashlytics());
        }
//        Fabric.with(this, new Crashlytics());
        preferenceStorage = new PreferenceStorage(getApplicationContext());
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/POPPINS-REGULAR.TTF");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/POPPINS-REGULAR.TTF");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/POPPINS-REGULAR.TTF");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/POPPINS-REGULAR.TTF");
        /*FontsOverride.setDefaultFont(this, "POPPINS_MEDIUM", "fonts/POPPINS-MEDIUM.TTF");
        FontsOverride.setDefaultFont(this, "POPPINS_REGULAR", "fonts/POPPINS-REGULAR.TTF");*/

        if (PreferenceStorage.getKey("locale") != null) {
            LocaleUtils.setLocale(new Locale(PreferenceStorage.getKey("locale")));
            LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
        }

//        else {
//            LocaleUtils.setLocale(new Locale("en"));
//            LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
//        }

        getLocaleData();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        try {
            if (!checkPlayServices()) {
                Toast.makeText(this, AppUtils.cleanLangStr(this, commonData.getLg7_please_install_(), R.string.err_txt_install_play_ser), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        stopLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLocation != null) {
            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public String getLatitude() {
        String latitude = Latitude;
        return latitude;
    }

    public String getLongitude() {
        String longitude = Longitude;
        return longitude;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mLocation != null) {

            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());

        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            /*if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getApplicationContext(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else{}*/
               //TODO:

            return false;
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_enable_permissi(), R.string.err_txt_enable_permission), Toast.LENGTH_LONG).show();

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);


    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            mGoogleApiClient.disconnect();
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();
        }
    }
}
