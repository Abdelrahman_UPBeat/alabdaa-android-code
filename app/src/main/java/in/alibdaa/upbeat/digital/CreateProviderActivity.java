package in.alibdaa.upbeat.digital;


import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

import in.alibdaa.upbeat.digital.adapters.ViewPagerAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.fragments.CreateProvideAvailFragment;
import in.alibdaa.upbeat.digital.fragments.CreateProvideInfoFragment;
import in.alibdaa.upbeat.digital.interfaces.OnSetMyLocation;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CustomViewPager;

import retrofit2.Call;

public class CreateProviderActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler, OnSetMyLocation {

    @BindView(R.id.tab_create_provider)
    TabLayout tabCreateProvider;
    @BindView(R.id.pager_create_provider)
    CustomViewPager pagerCreateProvider;

    ViewPagerAdapter adapter;

    CreateProvideInfoFragment createProvideInfoFragment;
    CreateProvideAvailFragment createProvideAvailFragment;

    public ProviderData providerData;

    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();

    public static Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_create_provider);
        ButterKnife.bind(this);
        mContext = this;
        getLocaleData();


        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        providerData = new ProviderData();

        // Add Fragments to adapter
        createProvideInfoFragment = new CreateProvideInfoFragment();
        createProvideInfoFragment.myCreateProvideInfoFragment(this);
        adapter.addFragment(createProvideInfoFragment, AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_info(), R.string.txt_info));

        createProvideAvailFragment = new CreateProvideAvailFragment();
        createProvideAvailFragment.myCreateProvideAvailFragment(this);
        adapter.addFragment(createProvideAvailFragment, AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_availability(), R.string.txt_availability));

        pagerCreateProvider.setAdapter(adapter);
        pagerCreateProvider.setOffscreenPageLimit(2);
        pagerCreateProvider.disableScroll(true);

        //Tab
        tabCreateProvider.setupWithViewPager(pagerCreateProvider);
        tabCreateProvider.setSelectedTabIndicatorColor(appColor);
        LinearLayout tabStrip = ((LinearLayout) tabCreateProvider.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        pagerCreateProvider.setOnPageChangeListener(new CustomViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    pagerCreateProvider.disableScroll(true);
                } else if (position == 1) {
                    pagerCreateProvider.disableScroll(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_provide(), R.string.txt_provider));
        tabCreateProvider.getTabAt(0).setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_info(), R.string.txt_info));
        tabCreateProvider.getTabAt(1).setText(AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_availability(), R.string.txt_availability));
        tabCreateProvider.setSelectedTabIndicatorColor(appColor);
    }

    public boolean validateData(EditText etTxt, String etVal, String msg) {
        String msg2 = "";
        if (etTxt != null) {
            if (etTxt.getId() == R.id.et_title) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_title_cannot_be(), R.string.err_txt_title);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(CreateProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            } else if (etTxt.getId() == R.id.et_location) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_location_addres(), R.string.err_txt_addr);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(CreateProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_contact_no) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_contact_number_(), R.string.err_txt_contact_no);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(CreateProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_category) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, commonData.getLg7_please_choose_c(), R.string.err_category);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(CreateProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_subcategory) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, commonData.getLg7_please_choose_s(), R.string.err_subcategory);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(CreateProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else {
                msg2 = msg;
                AppUtils.showToast(CreateProviderActivity.this, msg2);
                return false;
            }

        }
        return true;
    }

    public boolean validatePhoneNum(EditText etContactNo, String contactNo, String s) {
        if (etContactNo.getText().toString().length() < 10 || etContactNo.getText().toString().length() > 15) {
            s = AppUtils.cleanLangStr(this, requestAndProviderList.getLg6_contact_number_(), R.string.err_txt_contact_no);
            etContactNo.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
            AppUtils.showToast(CreateProviderActivity.this, s);
            return false;
        }
        return true;
    }

    public void postDataToServer() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            if (providerData != null) {
                try {
                    String fromDate = AppUtils.formatDateToServer(providerData.getFromDate());
                    String toDate = AppUtils.formatDateToServer(providerData.getToDate());

                    Call<EmptyData> classificationCall = apiService.postProviderData(providerData.getTitle(),
                            providerData.getDescListData(), providerData.getLocation(), providerData.getAvailListData(),
                            providerData.getContactNo(), providerData.getProvLat(), providerData.getProvLng(), fromDate,
                            toDate, providerData.getCatID(), providerData.getSubCatID(), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));

//                          providerDa Call<EmptyData> classificationCall = apiService.postProviderData(providerData.getTitle(),
////                            providerData.getDescListData(), providerData.getLocation(),
////                     ta.getContactNo(), providerData.getProvLat(), providerData.getProvLng(), providerData.getCatID(), providerData.getSubCatID(), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));

                    RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.CREATE_PROVIDER_DATA, this, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onLocationSet(String latitude, String longitude, String address) {
        createProvideInfoFragment.setLocationData(latitude, longitude, address);
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        EmptyData emptyData = (EmptyData) myRes;
        AppUtils.showToast(CreateProviderActivity.this, emptyData.getResponseHeader().getResponseMessage());
        CreateProviderActivity.this.finish();
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void gotoNext() {
        pagerCreateProvider.setCurrentItem(1);
    }


    public class ProviderData {
        private String title;
        private String contactNo;
        private String location;
        private String provLat;
        private String provLng;
        private String fromDate;
        private String toDate;
        private String descListData;
        private String availListData;
        private String catID;
        private String subCatID;


        public String getCatID() {
            return catID;
        }

        public void setCatID(String catID) {
            this.catID = catID;
        }

        public String getSubCatID() {
            return subCatID;
        }

        public void setSubCatID(String subCatID) {
            this.subCatID = subCatID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getProvLat() {
            return provLat;
        }

        public void setProvLat(String provLat) {
            this.provLat = provLat;
        }

        public String getProvLng() {
            return provLng;
        }

        public void setProvLng(String provLng) {
            this.provLng = provLng;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getDescListData() {
            return descListData;
        }

        public void setDescListData(String descListData) {
            this.descListData = descListData;
        }

        public String getAvailListData() {
            return availListData;
        }

        public void setAvailListData(String availListData) {
            this.availListData = availListData;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof TextInputEditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
            commonData = new Gson().fromJson(PreferenceStorage.getKey(CommonLangModel.common_used_texts), LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
            commonData = new LanguageModel().new Common_used_texts();

        }
    }
}
