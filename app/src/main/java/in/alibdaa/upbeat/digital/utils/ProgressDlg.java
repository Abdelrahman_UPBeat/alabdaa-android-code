package in.alibdaa.upbeat.digital.utils;

import android.app.ProgressDialog;
import android.content.Context;

import com.google.gson.Gson;

import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;

public class ProgressDlg {

    private static ProgressDialog progressDialog = null;

    public static void showProgressDialog(Context context, String title, String message) {
        try {
            LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
            try {
                String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
                commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
            } catch (Exception e) {
                commonData = new LanguageModel().new Common_used_texts();
            }
            progressDialog = new ProgressDialog(context);
            //title = title == null || title.isEmpty() ? AppUtils.cleanLangStr(context, commonData.getLg7_loading(), R.string.txt_loading) : title;
            try {
                message = message == null || title.isEmpty() ? AppUtils.cleanLangStr(context, commonData.getLg7_please_wait(), R.string.txt_load_msg) : message;
            } catch (Exception e) {
                //e.printStackTrace();
                message = context.getResources().getString(R.string.txt_load_msg);
            }
            /* progressDialog.setTitle(title);*/
            progressDialog.setMessage(message);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissProgressDialog() {
        try {
            if (progressDialog != null || progressDialog.isShowing())
                progressDialog.dismiss();
        } catch (Exception e) {
        }
    }

    public static void clearDialog() {
        try {
            if (progressDialog != null)
                progressDialog.dismiss();
        } catch (Exception e) {
        }
        progressDialog = null;
    }

}