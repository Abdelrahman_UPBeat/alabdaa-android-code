package in.alibdaa.upbeat.digital;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import in.alibdaa.upbeat.digital.adapters.ChatDetailAdapterNew;
import in.alibdaa.upbeat.digital.datamodel.ChatDetailListData;
import in.alibdaa.upbeat.digital.datamodel.ChatSendMessageModel;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.DateItem;
import in.alibdaa.upbeat.digital.datamodel.GeneralItem;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ListItem;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.service.MyFirebaseMessagingService;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by Hari on 14-05-2018.
 */

public class ChatRequestorDetailActivity extends AppCompatActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.rv_chat_room)
    RecyclerView rvChatRoom;
    @BindView(R.id.et_message_content)
    EditText etMessageContent;
    @BindView(R.id.iv_send_message)
    ImageView ivSendMessage;
    ChatDetailAdapterNew mAdapter;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    int items = 10;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.iv_req_proc_img)
    CircleImageView ivReqProcImg;
    @BindView(R.id.ll_send_msg)
    LinearLayout llSendMsg;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private String chatId, chatUsername, chatImg;
    int currentPageNo = 1, nextPageNo = -1;
    LinearLayoutManager linearLayoutManager;
    ChatDetailListData chatDetailListData;
    ChatDetailListData tempChatDetailListData;
    ChatSendMessageModel chatSendMessageModel;
    boolean isFirstTime;
    HashMap<String, ArrayList<ChatDetailListData.ChatList>> groupedHashMap;
    ArrayList<ChatDetailListData.ChatList> chatDetailListDataAll = new ArrayList<>();
    public LanguageModel.Common_used_texts common_used_texts = new LanguageModel().new Common_used_texts();
    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;
    public int appColor = 0;
    private BroadcastReceiver myBroadcastReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_details);
        ButterKnife.bind(this);

        if (getIntent().getStringExtra(AppConstants.chatFrom) != null && !getIntent().getStringExtra(AppConstants.chatFrom).isEmpty()) {
            chatId = getIntent().getStringExtra(AppConstants.chatFrom);
        }

        getLocaleData();
        etMessageContent.setHint(AppUtils.cleanLangStr(this, common_used_texts.getLg7_type_something(), R.string.typeSomething));
        if (rvChatRoom.getAdapter() == null) {
            linearLayoutManager
                    = new LinearLayoutManager(ChatRequestorDetailActivity.this, LinearLayoutManager.VERTICAL, false);
            rvChatRoom.setLayoutManager(linearLayoutManager);
            mAdapter = new ChatDetailAdapterNew(this, new ArrayList<ListItem>());
            rvChatRoom.setAdapter(mAdapter);
        }
        UpdateMessage();
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("ChatDetailActivity", "Load More");
                isLoading = true;
                mAdapter.itemsData.add(0, null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(0);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("ChatDetailActivity", "Load More 2");
                        getChatDetailList(true);
                    }
                }, 1000);

            }
        });

        getChatDetailList(false);
        rvChatRoom.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                int displayedPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if (displayedPosition == 0) {
                    totalItemCount = linearLayoutManager.getItemCount();

                    if (!isLoading && (nextPageNo > currentPageNo) /*&& displayedPosition == 0*/) {
                        isLoading = true;
                        if (mAdapter.mOnLoadMoreListener != null) {
                            mAdapter.mOnLoadMoreListener.onLoadMore();
                        }
                    }
                }
            }
        });

    }

    public void sendChatMessage(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatSendMessageModel> chatDetailListDataCall = apiService.postRequestorSendChatMessage(etMessageContent.getText().toString(), Integer.parseInt(chatId), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, chatDetailListDataCall, AppConstants.CHAT_SENDDETAILLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, common_used_texts.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }


    public void getChatDetailList(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            if (!isLoadMore) {
                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
                nextPageNo = 1;
            }
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatDetailListData> chatDetailListDataCall = apiService.postChatRequestorDetailList(nextPageNo, Integer.parseInt(chatId), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, chatDetailListDataCall, AppConstants.CHAT_DETAILLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, common_used_texts.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (myBroadcastReceiver != null) {
            unregisterReceiver(myBroadcastReceiver);
        }
    }

    public void UpdateMessage() {
        myBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String result = intent.getStringExtra(MyFirebaseMessagingService.EXTRA_KEY_UPDATE);
                String body = intent.getStringExtra(MyFirebaseMessagingService.MESSAGE);
                if (!result.isEmpty()) {
                    try {
                        JSONObject mJSONObject = new JSONObject(result);
                        if (mJSONObject.get("from_userid").toString().equalsIgnoreCase(chatId)) {
                            ChatDetailListData.ChatList chatList1 = new ChatDetailListData.ChatList();
                            chatList1.setChatFrom(mJSONObject.get("from_userid").toString());
                            chatList1.setChatTo(mJSONObject.get("to_userid").toString());
                            chatList1.setChatUtcTime(mJSONObject.get("utctime").toString());
                            chatList1.setDate(mJSONObject.get("date").toString());
                            chatList1.setTime(mJSONObject.get("time").toString());
                            chatList1.setFromname(mJSONObject.get("from_username").toString());
                            chatList1.setToname(mJSONObject.get("to_username").toString());
                            chatList1.setContent(mJSONObject.get("message").toString());
                            chatDetailListDataAll.add(chatList1);
                            groupDataIntoHashMap(chatDetailListDataAll);

                            ArrayList<ListItem> consolidatedList = new ArrayList<>();
                            for (String date : groupedHashMap.keySet()) {
                                DateItem dateItem = new DateItem();
                                dateItem.setDate(date);
                                consolidatedList.add(dateItem);
                                for (ChatDetailListData.ChatList pojoOfJsonArray : groupedHashMap.get(date)) {
                                    GeneralItem generalItem = new GeneralItem();
                                    generalItem.setChatList(pojoOfJsonArray);
                                    consolidatedList.add(generalItem);
                                }
                            }
                            //Load data
                            mAdapter.updateRecyclerView(ChatRequestorDetailActivity.this, consolidatedList);
                            rvChatRoom.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
                        }

                        Log.i("TAG JSONRESULT ---->", mJSONObject.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {

        switch (responseModel) {
            case AppConstants.CHAT_DETAILLIST_DATA: {
                chatDetailListData = (ChatDetailListData) myRes;
                isLoading = false;
                if (isLoadMore) {
                    //Remove loading item
                    mAdapter.itemsData.remove(0);
                    mAdapter.notifyItemRemoved(0);
                }
                if (chatDetailListData != null && chatDetailListData.getData() != null && chatDetailListData.getData().getChatList().size() > 0) {
                    currentPageNo = chatDetailListData.getData().getCurrentPage();
                    nextPageNo = chatDetailListData.getData().getNextPage();

                    if (!isLoadMore || currentPageNo == 1) {
                        groupedHashMap = new LinkedHashMap<>();
                        chatDetailListDataAll = new ArrayList<>();
                        mAdapter.itemsData = new ArrayList<>();
                        rvChatRoom.setVisibility(View.VISIBLE);
                    }

                    chatDetailListDataAll.addAll(0, chatDetailListData.getData().getChatList());

                    groupDataIntoHashMap(chatDetailListDataAll);

                    ArrayList<ListItem> consolidatedList = new ArrayList<>();

                    for (String date : groupedHashMap.keySet()) {
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(date);
                        consolidatedList.add(dateItem);

                        for (ChatDetailListData.ChatList pojoOfJsonArray : groupedHashMap.get(date)) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setChatList(pojoOfJsonArray);
                            consolidatedList.add(generalItem);
                        }
                    }
                    //Load data
                    mAdapter.updateRecyclerView(this, consolidatedList);

                    if (!isFirstTime) {
                        scrollToBottom();
                        isFirstTime = true;
                    } else {
                        rvChatRoom.getLayoutManager().scrollToPosition(mAdapter.getItemCount() + 10);
                    }

                    //
                } else if (isLoadMore && mAdapter.itemsData.size() > 0) {
                    rvChatRoom.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    rvChatRoom.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }
            break;
            case AppConstants.CHAT_SENDDETAILLIST_DATA: {
                chatSendMessageModel = (ChatSendMessageModel) myRes;
                if (chatSendMessageModel.getResponseHeader().getResponseCode().equalsIgnoreCase("1")) {
                    ChatDetailListData.ChatList chatList = new ChatDetailListData.ChatList();
                    chatList.setId(chatSendMessageModel.getData().getId());
                    chatList.setChatFrom(chatSendMessageModel.getData().getChatFrom());
                    chatList.setChatTo(chatSendMessageModel.getData().getChatTo());
                    chatList.setChatUtcTime(chatSendMessageModel.getData().getChatUtcTime());
                    chatList.setContent(chatSendMessageModel.getData().getContent());
                    chatList.setFromname(chatSendMessageModel.getData().getFromname());
                    chatList.setToname(chatSendMessageModel.getData().getToname());
                    chatList.setDate(chatSendMessageModel.getData().getDate());
                    chatList.setTime(chatSendMessageModel.getData().getTime());
                    chatList.setStatus(chatSendMessageModel.getData().getStatus());
                    chatDetailListDataAll.add(chatList);

                    groupDataIntoHashMap(chatDetailListDataAll);

                    /*//Added data to adapter list
                    GeneralItem generalItem = new GeneralItem();
                    generalItem.setChatList(chatList);
                    mAdapter.itemsData.add(generalItem);
                    mAdapter.notifyDataSetChanged();*/
                    ArrayList<ListItem> consolidatedList = new ArrayList<>();
                    for (String date : groupedHashMap.keySet()) {
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(date);
                        consolidatedList.add(dateItem);
                        for (ChatDetailListData.ChatList pojoOfJsonArray : groupedHashMap.get(date)) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setChatList(pojoOfJsonArray);
                            consolidatedList.add(generalItem);
                        }
                    }
                    //Load data
                    mAdapter.updateRecyclerView(this, consolidatedList);
                    rvChatRoom.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
                    if (mAdapter.itemsData.size() > 0) {
                        rvChatRoom.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                    }

                    etMessageContent.setText("");
                }

            }
            break;
        }


    }

    public void scrollToBottom() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            if (mAdapter.getItemCount() > 1) {
                rvChatRoom.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
            }
        }
    }

    public void failureHandling(Object myRes, boolean isLoadMore, String responseModel) {
        switch (responseModel) {
            case AppConstants.CHAT_DETAILLIST_DATA: {
                if (isLoadMore) {
                    //Remove loading item
                    mAdapter.itemsData.remove(0);
                    mAdapter.notifyItemRemoved(0);
                    mAdapter.notifyDataSetChanged();
                }
            }
            break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        failureHandling(myRes, isLoadMore, responseModel);
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        failureHandling(myRes, isLoadMore, responseModel);
    }

    private synchronized HashMap<String, ArrayList<ChatDetailListData.ChatList>> groupDataIntoHashMap(ArrayList<ChatDetailListData.ChatList> listOfPojosOfJsonArray) {

        groupedHashMap = new LinkedHashMap<>();

        for (ChatDetailListData.ChatList pojoOfJsonArray : listOfPojosOfJsonArray) {

            String hashMapKey = pojoOfJsonArray.getDate();

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(pojoOfJsonArray);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                ArrayList<ChatDetailListData.ChatList> list = new ArrayList<ChatDetailListData.ChatList>();
                list.add(pojoOfJsonArray);
                groupedHashMap.put(hashMapKey, list);
            }
        }

        return groupedHashMap;
    }


    @OnClick(R.id.iv_send_message)
    public void onViewClicked() {
        if (!etMessageContent.getText().toString().isEmpty()) {
            sendChatMessage(false);
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            common_used_texts = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            common_used_texts = new LanguageModel().new Common_used_texts();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void onBackClick(View view) {
        finish();
    }

    public void setAppTheme() {
        appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        PreferenceStorage.setKey(AppConstants.currentActivity, "yes");
        setSupportActionBar(mToolbar);
        setAppTheme();
        IntentFilter intentFilter = new IntentFilter(MyFirebaseMessagingService.ACTION_MyUpdate);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(myBroadcastReceiver, intentFilter);
        mToolbar.setBackgroundColor(appColor);
        if (getIntent().getStringExtra(AppConstants.chatImg) != null && !getIntent().getStringExtra(AppConstants.chatImg).isEmpty()) {
            //chatImg = getIntent().getStringExtra(AppConstants.chatImg);
            Picasso.with(this)
                    .load(AppConstants.BASE_URL + getIntent().getStringExtra(AppConstants.chatImg))
                    .placeholder(R.drawable.ic_pic_view)
                    .error(R.drawable.ic_pic_view)
                    .into(ivReqProcImg);
        }
        if (getIntent().getStringExtra(AppConstants.chatUsername) != null && !getIntent().getStringExtra(AppConstants.chatUsername).isEmpty()) {
            //chatUsername = getIntent().getStringExtra(AppConstants.chatUsername);
            tvUsername.setText(getIntent().getStringExtra(AppConstants.chatUsername));
        }
        ivSendMessage.setColorFilter(appColor, PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceStorage.setKey(AppConstants.currentActivity, "");
    }
}
