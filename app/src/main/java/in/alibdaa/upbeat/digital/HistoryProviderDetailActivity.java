package in.alibdaa.upbeat.digital;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.POSTCompleteProvider;
import in.alibdaa.upbeat.digital.datamodel.POSTRatingsProvider;
import in.alibdaa.upbeat.digital.datamodel.ProviderHistoryModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import retrofit2.Call;

public class HistoryProviderDetailActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {

    @BindView(R.id.tv_provider_name)
    TextView tvProviderName;
    @BindView(R.id.tv_prov_email)
    TextView tvProvEmail;
    @BindView(R.id.tv_prov_contact)
    TextView tvProvContact;
    @BindView(R.id.tv_prov_title)
    TextView tvProvTitle;
    @BindView(R.id.ll_prov_desc_detail)
    LinearLayout llProvDescDetail;
    @BindView(R.id.tv_prov_location)
    TextView tvProvLocation;
    @BindView(R.id.ll_prov_avail)
    LinearLayout llProvAvail;

    ArrayList<String> descDataList = new ArrayList<>();
    ProviderHistoryModel.Booking_list providerDataDetail;
    @BindView(R.id.iv_user_img)
    ImageView ivUserImg;

    int viewType;

    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();

    @BindView(R.id.tv_chat)
    TextView tvChat;
    @BindView(R.id.ll_prov_chat)
    LinearLayout llProvChat;
    @BindView(R.id.tv_call)
    TextView tvCall;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    @BindView(R.id.ll_footer)
    LinearLayout llFooter;
    @BindView(R.id.tv_txt_date_range)
    TextView tvTxtDateRange;
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.tv_edit)
    TextView tvEdit;
    @BindView(R.id.ll_prov_edit)
    LinearLayout llProvEdit;
    @BindView(R.id.tv_delete)
    TextView tvDelete;
    @BindView(R.id.ll_delete)
    LinearLayout llDelete;
    @BindView(R.id.ll_user_footer)
    LinearLayout llUserFooter;
    @BindView(R.id.tv_show_direction)
    TextView tvShowDirection;
    @BindView(R.id.btn_book)
    Button btnBook;
    @BindView(R.id.tv_views)
    TextView tvViews;
    @BindView(R.id.btn_request_complete)
    Button btnRequestComplete;
    @BindView(R.id.btn_ratenow)
    Button btnRatenow;
    @BindView(R.id.tv_pro_date)
    TextView tvProDate;
    @BindView(R.id.tv_pro_time)
    TextView tvProTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_history_provider_details);
        ButterKnife.bind(this);

        getLocaleData();
        providerDataDetail = getIntent().getParcelableExtra("HistoryData");
        viewType = getIntent().getIntExtra("ViewType", 0);

        llCall.setBackgroundColor(appColor);
        tvShowDirection.setTextColor(appColor);
        btnBook.setBackgroundColor(appColor);
        tvShowDirection.setText(AppUtils.cleanLangStr(this, commonData.getLg7_show_directions(), R.string.show_directions));

        if (providerDataDetail != null) {
            tvProviderName.setText(providerDataDetail.getFull_name());
            tvProvEmail.setText(providerDataDetail.getEmail());
            tvProvContact.setText(providerDataDetail.getContact_number());
            tvProDate.setText(providerDataDetail.getService_date());
            tvProTime.setText(providerDataDetail.getService_time());
//            tvViews.setText(providerDataDetail.getViews() + " views");
            tvProvTitle.setText(providerDataDetail.getTitle());
            setToolBarTitle(providerDataDetail.getTitle());

            if (viewType == 1 && providerDataDetail.getService_status().equalsIgnoreCase("1")) {
                btnRequestComplete.setVisibility(View.VISIBLE);
                btnRequestComplete.setText(AppUtils.cleanLangStr(this, commonData.getLg7_complete(), R.string.complete));
            } else if (viewType == 0 && providerDataDetail.getService_status().equalsIgnoreCase("2") && providerDataDetail.getIs_rate().equalsIgnoreCase("0")) {
                btnRequestComplete.setVisibility(View.GONE);
                btnRatenow.setVisibility(View.VISIBLE);
                btnRatenow.setText(AppUtils.cleanLangStr(this, commonData.getLg7_rate_now(), R.string.txt_ratenow));
            }

            String ivPic = AppConstants.BASE_URL + providerDataDetail.getProfile_img();
            Picasso.with(this)
                    .load(ivPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(ivUserImg);

            llCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", providerDataDetail.getContact_number(), null));
                        startActivity(intent);
                    } catch (Exception e) {
                        if (e instanceof ActivityNotFoundException) {
                            AppUtils.showToast(HistoryProviderDetailActivity.this, "Dialing not supported");//TODO:
                        }
                    }
                }
            });

        }


    }

    private synchronized void setDayText(TextView tvTxtDesc, String dayVal) {
        switch (dayVal) {
            case "1":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_sunday(), R.string.txt_sunday));
                break;
            case "2":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_monday(), R.string.txt_monday));
                break;
            case "3":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_tuesday(), R.string.txt_tuesday));
                break;
            case "4":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_wednesday(), R.string.txt_wednesday));
                break;
            case "5":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_thursday(), R.string.txt_thursday));
                break;
            case "6":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_friday(), R.string.txt_friday));
                break;
            case "7":
                tvTxtDesc.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_saturday(), R.string.txt_saturday));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        tvTxtDateRange.setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_date(), R.string.txt_date));
    }

    private void getLocaleData() {
        try {
            String requestList = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            langReqProvData = new Gson().fromJson(requestList, LanguageModel.Request_and_provider_list.class);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
            commonData = new LanguageModel().new Common_used_texts();

        }
    }

    @OnClick({R.id.ll_prov_edit, R.id.ll_delete, R.id.tv_show_direction, R.id.ll_prov_chat, R.id.btn_book, R.id.btn_ratenow, R.id.btn_request_complete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_prov_edit:
                Intent detailPage = new Intent(this, EditProviderActivity.class);
                detailPage.putExtra("ViewType", viewType);
                detailPage.putExtra("ProviderData", providerDataDetail);
                AppUtils.appStartIntent(this, detailPage);
                HistoryProviderDetailActivity.this.finish();
                break;
            case R.id.ll_delete:
//                deleteProvider(providerDataDetail.getPId());
                break;

            case R.id.tv_show_direction:
                Intent callDirectionAct = new Intent(HistoryProviderDetailActivity.this, MapsGetDirections.class);
                callDirectionAct.putExtra(AppConstants.LATITUDE, providerDataDetail.getLatitude());
                callDirectionAct.putExtra(AppConstants.LONGITUDE, providerDataDetail.getLongitude());
                startActivity(callDirectionAct);
                break;
            case R.id.btn_ratenow:
                Intent callRatingAct = new Intent(this, RateProviderActivity.class);
                callRatingAct.putExtra(AppConstants.PID, providerDataDetail.getProvider_id());
                startActivity(callRatingAct);
                finish();
//                showRateusDialog();
                break;
            case R.id.btn_request_complete:
                postCompleteProvider();
                break;
        }

    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {
        switch (responseType) {
            case AppConstants.DELETE_PROVIDER_DATA:
                finish();
                break;
            case AppConstants.COMPLETEPROVIDER:
                POSTCompleteProvider postCompleteProvider = (POSTCompleteProvider) myRes;
                Toast.makeText(this, "Service Completed Successfully", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case AppConstants.RATINGS:
                POSTRatingsProvider postRatingsProvider = (POSTRatingsProvider) myRes;
                Toast.makeText(this, postRatingsProvider.getResponse().getResponse_message(), Toast.LENGTH_SHORT).show();
                finish();

                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    public void showRateusDialog() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_rate_provider, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setView(customView);
        alertDialogBuilder.setCancelable(true);
        final RatingBar ratingBar = customView.findViewById(R.id.rb_rating);
        final EditText etReviews = customView.findViewById(R.id.et_comments);
        Button btnSubmit = customView.findViewById(R.id.btn_send_feeedback);
        TextView tvType = customView.findViewById(R.id.tv_type);

        tvType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingBar.getRating() == 0) {
                    Toast.makeText(HistoryProviderDetailActivity.this, "Please fill rating", Toast.LENGTH_SHORT).show();
                } else if (etReviews.getText().toString().isEmpty()) {
                    Toast.makeText(HistoryProviderDetailActivity.this, "Please type reviews", Toast.LENGTH_SHORT).show();
                } else {
                    postRatings(ratingBar.getRating(), etReviews.getText().toString());
                }
            }
        });


        final AlertDialog dialog = alertDialogBuilder.create();
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void postCompleteProvider() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<POSTCompleteProvider> classificationCall = apiService.postCompleteProvider(PreferenceStorage.getKey(AppConstants.USER_TOKEN), providerDataDetail.getId(), providerDataDetail.getProvider_id());
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.COMPLETEPROVIDER, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, "", R.string.txt_enable_internet));
        }
    }

    public void postRatings(float rating, String review) {
//        if (AppUtils.isNetworkAvailable(this)) {
//            ProgressDlg.showProgressDialog(this, null, null);
//            ApiInterface apiService =
//                    ApiClient.getClient().create(ApiInterface.class);
//
//            Call<POSTRatingsProvider> classificationCall = apiService.postRatingsProvider(PreferenceStorage.getKey(AppConstants.USER_TOKEN), String.valueOf(rating), review, providerDataDetail.getProvider_id());
//            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.RATINGS, this, false);
//        } else {
//            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, "", R.string.txt_enable_internet));
//        }
    }
}
