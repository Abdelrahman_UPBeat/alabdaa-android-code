package in.alibdaa.upbeat.digital.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import in.alibdaa.upbeat.digital.ProviderHistoryActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.adapters.HistoryProviderListAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderHistoryModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import retrofit2.Call;

import static com.facebook.FacebookSdk.getApplicationContext;

public class BookingMyProvidersFragment extends Fragment implements RetrofitHandler.RetrofitResHandler {

    ProviderHistoryActivity mActivity;
    Context mContext;

    //View
    @BindView(R.id.tv_no_data_available)
    public TextView tvNoData;
    @BindView(R.id.rv_hist_pending_list)
    public RecyclerView rvRequestsList;
    Unbinder unbinder;
    LinearLayoutManager mLayoutManager;
    private boolean shouldRefreshOnResume = false;

    //Adapter and data
    public HistoryProviderListAdapter mAdapter;
    public List<ProviderHistoryModel.Booking_list> myData = new ArrayList<>();
    LanguageModel.Common_used_texts commonUsedTexts = new LanguageModel().new Common_used_texts();
    public LanguageModel.Request_and_provider_list requestAndProviderList = new LanguageModel().new Request_and_provider_list();

    public void bookingMyProvidersFragment(ProviderHistoryActivity mainActivity, Context mContext) {
        this.mActivity = mainActivity;
        this.mContext = mContext;
        commonUsedTexts = mainActivity.commonData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_history_pending_list, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        rvRequestsList.setLayoutManager(mLayoutManager);
        getLocaleData();
        LinearLayoutManager linearLayoutManager;
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mAdapter = new HistoryProviderListAdapter(mActivity, getActivity(), myData, 0, commonUsedTexts);
        rvRequestsList.setLayoutManager(linearLayoutManager);
        rvRequestsList.setAdapter(mAdapter);
        getMyBookingsList();
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    public void getMyBookingsList() {
        if (AppUtils.isNetworkAvailable(getActivity())) {
            ProgressDlg.showProgressDialog(getActivity(), null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ProviderHistoryModel> classificationCall = apiService.getMyBookings(PreferenceStorage.getKey(AppConstants.USER_TOKEN));
            RetrofitHandler.executeRetrofit(getActivity(), classificationCall, AppConstants.MYBOOKINGS, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(getActivity(), "", R.string.txt_enable_internet));
        }
    }

    private void getLocaleData() {

        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            requestAndProviderList = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            requestAndProviderList = new LanguageModel().new Request_and_provider_list();
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

        ProviderHistoryModel providerHistoryModel = (ProviderHistoryModel) myRes;
        ProgressDlg.clearDialog();
        myData = new ArrayList<>();
        if (providerHistoryModel.getData().getBooking_list().size() > 0) {
            myData.addAll(providerHistoryModel.getData().getBooking_list());
            mAdapter.updateRecyclerView(getActivity(), myData);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            rvRequestsList.setVisibility(View.GONE);
        }


    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {
        ProgressDlg.clearDialog();
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {
        ProgressDlg.clearDialog();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        // Check should we need to refresh the fragment
//        if (shouldRefreshOnResume) {
//            // refresh fragment
//            getMyBookingsList();
//        }
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        shouldRefreshOnResume = true;
//    }
}
