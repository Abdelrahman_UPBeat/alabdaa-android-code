package in.alibdaa.upbeat.digital.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.alibdaa.upbeat.digital.HistoryActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.adapters.HistoryListAdapter;
import in.alibdaa.upbeat.digital.datamodel.HistoryListData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppUtils;

public class HistoryPendingFragment extends Fragment {

    HistoryActivity mActivity;
    Context mContext;

    //View
    @BindView(R.id.tv_no_data_available)
    public TextView tvNoData;
    @BindView(R.id.rv_hist_pending_list)
    public RecyclerView rvRequestsList;

    Unbinder unbinder;

    //Fragment maintain state
    LinearLayoutManager mLayoutManager;
    public boolean isInitiated = false;
    public Bundle savedState;
    private boolean createdStateInDestroyView;

    //Pagination
    public boolean isLoading = false;
    private int visibleItemCount, firstVisibleItemPosition, totalItemCount;

    //Adapter and data
    public HistoryListAdapter mAdapter;
    public ArrayList<HistoryListData.RequestList> myData = new ArrayList<>();
    LanguageModel.Common_used_texts commonUsedTexts = new LanguageModel().new Common_used_texts();

    public void myHistoryFragment(HistoryActivity mainActivity, Context mContext) {
        this.mActivity = mainActivity;
        this.mContext = mContext;
        commonUsedTexts = mainActivity.commonData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_history_pending_list, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        rvRequestsList.setLayoutManager(mLayoutManager);

        tvNoData.setText(AppUtils.cleanLangStr(getContext(), commonUsedTexts.getLg7_no_data_were_fo(), R.string.txt_no_data));
        if (rvRequestsList.getAdapter() == null) {
            mAdapter = new HistoryListAdapter(mActivity, mContext, new ArrayList<HistoryListData.RequestList>(), 0);
            rvRequestsList.setAdapter(mAdapter);
        }
        if (savedState != null) {
            myData = savedState.getParcelableArrayList("HIST_PENDING_LIST");
            if (myData != null && myData.size() > 0) {
                mAdapter.updateRecyclerView(mContext, myData);
                rvRequestsList.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
            } else {
                rvRequestsList.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
            }
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.getHistoryData(true);
                    }
                }, 1000);

            }
        });

        rvRequestsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManager.getItemCount();
                int displayedPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                if (displayedPosition == (mAdapter.itemsData.size() - 1)) {
                    if (!isLoading && mActivity.hPendingNextPage > 0) {
                        if (dy > 0) //check for scroll down
                        {
                            visibleItemCount = mLayoutManager.getChildCount();
                            firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                                isLoading = true;
                                Log.v("TAG", " Reached Last Item visibleItemCount == " + visibleItemCount + " && firstVisibleItemPosition == " + firstVisibleItemPosition + " && totalItemCount == " + totalItemCount);
                                if (mAdapter.mOnLoadMoreListener != null) {
                                    mAdapter.mOnLoadMoreListener.onLoadMore();
                                }
                            }
                        }
                    }
                }
            }
        });
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        savedState = saveState();
        createdStateInDestroyView = true;
        myData = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (myData == null) {
            outState.putBundle("HIST_PENDING_LIST", savedState);
        } else {
            outState.putBundle("HIST_PENDING_LIST", createdStateInDestroyView ? savedState : saveState());
        }
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelableArrayList("HIST_PENDING_LIST", myData);
        return state;
    }
}
