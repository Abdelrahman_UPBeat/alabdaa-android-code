package in.alibdaa.upbeat.digital;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;

/**
 * Created by Hari on 16-05-2018.
 */

public class SubscriptionThankYouActivity extends AppCompatActivity {


    @BindView(R.id.bt_getStarted)
    Button btGetStarted;

    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;

    String fromPage;
    public int appColor = 0;
    LanguageModel.Subscription subscriptionTexts = new LanguageModel().new Subscription();
    @BindView(R.id.iv_subs)
    ImageView ivSubs;
    @BindView(R.id.tv_subs_thank_you)
    TextView tvSubsThankYou;
    @BindView(R.id.tv_subs_msg)
    TextView tvSubsMsg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe_thankyou);
        ButterKnife.bind(this);

        fromPage = getIntent().getStringExtra("FromPage");
    }

    @OnClick(R.id.bt_getStarted)
    public void onViewClicked() {
        Intent callSubDetailAct = new Intent(this, SubscriptionDetailsActivity.class);
        callSubDetailAct.putExtra("FromPage", fromPage);
        AppUtils.appStartIntent(this, callSubDetailAct);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAppTheme();
        getLocaleData();
        setSupportActionBar(mToolbar);
        try {
            mToolbar.setTitle(AppUtils.cleanLangStr(this, subscriptionTexts.getLg9_subscription(), R.string.hint_subscription));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mToolbar.setBackgroundColor(appColor);
            btGetStarted.setBackgroundColor(appColor);
            tvSubsThankYou.setTextColor(appColor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.subscription);
            subscriptionTexts = new Gson().fromJson(commonDataStr, LanguageModel.Subscription.class);
        } catch (Exception e) {
            subscriptionTexts = new LanguageModel().new Subscription();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setAppTheme() {
        appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            ivSubs.setColorFilter(appColor, android.graphics.PorterDuff.Mode.MULTIPLY);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }

    }
}
