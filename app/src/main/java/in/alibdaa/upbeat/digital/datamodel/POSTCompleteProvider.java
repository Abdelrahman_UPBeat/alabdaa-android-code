package in.alibdaa.upbeat.digital.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class POSTCompleteProvider extends BaseResponse {

    @SerializedName("data")
    @Expose
    private Boolean data;

    public Boolean getData() {
        return data;
    }

    public void setData(Boolean data) {
        this.data = data;
    }

}
