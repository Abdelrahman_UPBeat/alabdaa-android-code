package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.alibdaa.upbeat.digital.adapters.ReviewTypeListAdapter;
import in.alibdaa.upbeat.digital.datamodel.GETReviewTypes;
import in.alibdaa.upbeat.digital.datamodel.POSTRatingsProvider;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class RateProviderActivity extends BaseAppCompatActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.rb_rating)
    AppCompatRatingBar rbRating;
    @BindView(R.id.et_comments)
    EditText etComments;
    @BindView(R.id.btn_send_feeedback)
    Button btnSendFeeedback;
    String providerID = "";
    List<GETReviewTypes.Ratings_type_list> daoReviewtypes = new ArrayList<>();
    ReviewTypeListAdapter reviewTypeListAdapter;
    @BindView(R.id.rv_types)
    RecyclerView rvTypes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_rate_provider);
        ButterKnife.bind(this);
        setToolBarTitle("Post your Review");
        providerID = getIntent().getStringExtra(AppConstants.PID);
        getRatingTypes();


    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {
        switch (responseType) {
            case AppConstants.ReviewTypes:
                GETReviewTypes getReviewTypes = (GETReviewTypes) myRes;
                if (getReviewTypes.getData().getRatings_type_list() != null && getReviewTypes.getData().getRatings_type_list().size() > 0) {
                    daoReviewtypes.addAll(getReviewTypes.getData().getRatings_type_list());
                    GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2);
                    rvTypes.setLayoutManager(linearLayoutManager);
                    reviewTypeListAdapter = new ReviewTypeListAdapter(this, daoReviewtypes, tvType);
                    rvTypes.setAdapter(reviewTypeListAdapter);
                }
                break;
            case AppConstants.RATINGS:
                POSTRatingsProvider postRatingsProvider = (POSTRatingsProvider) myRes;
                PreferenceStorage.setKey("isRated", true);
                Toast.makeText(this, postRatingsProvider.getResponse().getResponse_message(), Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @OnClick({R.id.tv_type, R.id.btn_send_feeedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_type:
//                showRatingTypeList();
                break;
            case R.id.btn_send_feeedback:
                if (rbRating.getRating() == 0) {
                    Toast.makeText(RateProviderActivity.this, "Please fill rating", Toast.LENGTH_SHORT).show();
                } else if (etComments.getText().toString().isEmpty()) {
                    Toast.makeText(RateProviderActivity.this, "Please type reviews", Toast.LENGTH_SHORT).show();
                } else {
                    postRatings(rbRating.getRating(), etComments.getText().toString());
                }
                break;
        }
    }

    public void postRatings(float rating, String review) {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<POSTRatingsProvider> classificationCall = apiService.postRatingsProvider(PreferenceStorage.getKey(AppConstants.USER_TOKEN), String.valueOf(rating), review, providerID, reviewTypeListAdapter.typeID);
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.RATINGS, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, "", R.string.txt_enable_internet));
        }
    }

    public void getRatingTypes() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<GETReviewTypes> classificationCall = apiService.getReviewTypes(PreferenceStorage.getKey(AppConstants.USER_TOKEN));
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.ReviewTypes, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, "", R.string.txt_enable_internet));
        }
    }


    public void showRatingTypeList() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_ratingtype, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setView(customView);
        alertDialogBuilder.setCancelable(true);
        RecyclerView recyclerView = customView.findViewById(R.id.rv_reviewtype);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        final AlertDialog dialog = alertDialogBuilder.create();
        reviewTypeListAdapter = new ReviewTypeListAdapter(this, daoReviewtypes, dialog, tvType);
        recyclerView.setAdapter(reviewTypeListAdapter);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

}
