package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import in.alibdaa.upbeat.digital.ChatDetailActivity;
import in.alibdaa.upbeat.digital.EditProviderActivity;
import in.alibdaa.upbeat.digital.GoToSubscriptionActivity;
import in.alibdaa.upbeat.digital.MyLoginActivity;
import in.alibdaa.upbeat.digital.MyProviderListActivity;
import in.alibdaa.upbeat.digital.ProvideActivity;
import in.alibdaa.upbeat.digital.ProviderDetailActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import in.alibdaa.upbeat.digital.viewwidgets.SwipeRevealLayout;
import in.alibdaa.upbeat.digital.viewwidgets.ViewBinderHelper;

public class ProviderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mActivity;
    Context mContext;
    public ArrayList<ProviderListData.ProviderList> itemsData = new ArrayList<>();
    int viewType;
    LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    public OnLoadMoreListener mOnLoadMoreListener;

    private int SELF = 1, LOADING = 2;
    private int lastPosition = -1;

    // viewType - 0 - Dashboard
    // viewType - 1 - My Provider List
    public ProviderAdapter(Activity mActivity, Context mContext, ArrayList<ProviderListData.ProviderList> itemsData, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        if (mActivity instanceof ProvideActivity) {
            langReqProvData = ((ProvideActivity) mActivity).langReqProvData;
        } else if (mActivity instanceof MyProviderListActivity) {
            langReqProvData = ((MyProviderListActivity) mActivity).requestAndProviderList;
        }
        // uncomment if you want to open only one row at a time
        binderHelper.setOpenOnlyOne(true);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
       /* // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_provider, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;*/
        View itemView;
        if (viewType == LOADING) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_provider, parent, false);
            return new ProviderViewHolder(itemView);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (itemsData.get(position) == null) {
            return LOADING;
        } else {
            return SELF;
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        if (viewHolder instanceof ProviderViewHolder) {
            ProviderViewHolder providerViewHolder = (ProviderViewHolder) viewHolder;

            if (viewType == 1) {
                providerViewHolder.tvRequestTitle.setText(AppUtils.cleanString(mContext, itemsData.get(position).getTitle()));
                providerViewHolder.tvUsername.setVisibility(View.GONE);
            } else {
                providerViewHolder.tvRequestTitle.setText(AppUtils.cleanString(mContext, itemsData.get(position).getTitle()));
                providerViewHolder.tvUsername.setText(itemsData.get(position).getUsername());
            }


            if (!itemsData.get(position).getViews().isEmpty()) {
                providerViewHolder.tv_views.setText(itemsData.get(position).getViews() + " views");
            } else {
                providerViewHolder.tv_views.setText("0 views");
            }
            providerViewHolder.rbReviews.setRating(Float.parseFloat(itemsData.get(position).getRating()));

            if (itemsData != null && 0 <= position && position < itemsData.size()) {
                final String data = itemsData.get(position).getTitle();

                // Use ViewBindHelper to restore and save the open/close state of the SwipeRevealView
                // put an unique string id as value, can be any string which uniquely define the data
                try {
                    providerViewHolder.tvProvEdit.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_edit(), R.string.txt_edit));
                    providerViewHolder.tvProvDelete.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_delete(), R.string.txt_delete));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                binderHelper.bind(providerViewHolder.swipeLayout, data);
                // Bind your data here
                providerViewHolder.bind(data);
            }

            try {
                JSONArray descList = new JSONArray(itemsData.get(position).getDescriptionDetails());

                providerViewHolder.tvRequestDesc1.setVisibility(View.INVISIBLE);
                providerViewHolder.tvRequestDesc2.setVisibility(View.INVISIBLE);
                providerViewHolder.tvRequestDesc3.setVisibility(View.INVISIBLE);

                if (descList.length() == 1) {
                    providerViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        providerViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                } else if (descList.length() == 2) {
                    providerViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        providerViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                    providerViewHolder.tvRequestDesc2.setText(AppUtils.cleanString(mContext, descList.getString(1)));
                    if (!AppUtils.cleanString(mContext, descList.getString(1)).isEmpty())
                        providerViewHolder.tvRequestDesc2.setVisibility(View.VISIBLE);
                } else if (descList.length() == 3) {
                    providerViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        providerViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                    providerViewHolder.tvRequestDesc2.setText(AppUtils.cleanString(mContext, descList.getString(1)));
                    if (!AppUtils.cleanString(mContext, descList.getString(1)).isEmpty())
                        providerViewHolder.tvRequestDesc2.setVisibility(View.VISIBLE);
                    providerViewHolder.tvRequestDesc3.setText(AppUtils.cleanString(mContext, descList.getString(2)));
                    if (!AppUtils.cleanString(mContext, descList.getString(2)).isEmpty())
                        providerViewHolder.tvRequestDesc3.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            providerViewHolder.tvContNum.setText(AppUtils.cleanString(mContext, itemsData.get(position).getContactNumber()));

            String profPic = null;
            if (viewType == 1) {
                if (PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG) != null || !PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG).isEmpty()) {
                    profPic = PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG);
                }
                providerViewHolder.tvTxtChat.setVisibility(View.GONE);
            } else {

                if (!itemsData.get(position).getProviderId().equalsIgnoreCase(String.valueOf(PreferenceStorage.getIntKey(AppConstants.USER_ID)))) {
                    providerViewHolder.tvTxtChat.setVisibility(View.VISIBLE);
                }


                profPic = itemsData.get(position).getProfileImg();
                providerViewHolder.tvContNum.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (viewType != 1 && !itemsData.get(position).getProviderId().equalsIgnoreCase(String.valueOf(PreferenceStorage.getIntKey(AppConstants.USER_ID)))) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", itemsData.get(position).getProfileContactNo(), null));
                                mContext.startActivity(intent);
                            } catch (Exception e) {
                                if (e instanceof ActivityNotFoundException) {
                                    AppUtils.showToast(mContext, "Dialing not supported");//TODO: lang
                                }
                            }
                        }
                    }
                });

                if ((PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE)) > 0) {
                    providerViewHolder.tvTxtContNo.setVisibility(View.VISIBLE);
                    providerViewHolder.tvContNum.setVisibility(View.VISIBLE);
                } else {
                    providerViewHolder.tvTxtContNo.setVisibility(View.INVISIBLE);
                    providerViewHolder.tvContNum.setVisibility(View.INVISIBLE);
                }
            }

            if (viewType == 1) {
                providerViewHolder.swipeLayout.setLockDrag(false);
            } else {
                providerViewHolder.swipeLayout.setLockDrag(true);
            }

            /*if (AppConstants.BASE_URL.contains(profPic)) {
                profPic = profPic;
            } else {
                profPic = AppConstants.BASE_URL + profPic;
            }*/
            Picasso.with(mContext)
                    .load(AppConstants.BASE_URL + profPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(providerViewHolder.ivUserimg);

            try {
                providerViewHolder.tvTxtContNo.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_contact_number(), R.string.txt_cont_num));
                providerViewHolder.tvTxtChat.setText(AppUtils.cleanLangStr(mContext, langReqProvData.getLg6_chat(), R.string.txt_chat));
            } catch (Exception e) {
            }

            providerViewHolder.tvTxtChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

                    if (userToken != null) {

                        if (viewType != 1 && !itemsData.get(position).getProviderId().equalsIgnoreCase(String.valueOf(PreferenceStorage.getIntKey(AppConstants.USER_ID)))) {
                            if ((PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE)) > 0) {
                                Intent callChatDetailAct = new Intent(mContext, ChatDetailActivity.class);
                                callChatDetailAct.putExtra(AppConstants.chatFrom, itemsData.get(position).getProviderId());
                                callChatDetailAct.putExtra(AppConstants.chatUsername, itemsData.get(position).getUsername());
                                callChatDetailAct.putExtra(AppConstants.chatImg, itemsData.get(position).getProfileImg());
                                AppUtils.appStartIntent(mContext, callChatDetailAct);
                            } else {
                                Intent subsIntent = new Intent(mContext, GoToSubscriptionActivity.class);
                                subsIntent.putExtra(AppConstants.chatFrom, itemsData.get(position).getProviderId());
                                subsIntent.putExtra(AppConstants.chatUsername, itemsData.get(position).getUsername());
                                subsIntent.putExtra(AppConstants.chatImg, itemsData.get(position).getProfileImg());
                                subsIntent.putExtra("FromPage", AppConstants.PAGE_CHAT);
                                AppUtils.appStartIntent(mContext, subsIntent);
                            }
                        }
                    }else{

                        showDialog();
                    }


                }
            });

            if (mActivity instanceof ProvideActivity) {
                providerViewHolder.tvTxtChat.setTextColor(((ProvideActivity) mActivity).appColor);
            } else if (mActivity instanceof MyProviderListActivity) {
                providerViewHolder.tvTxtChat.setTextColor(((MyProviderListActivity) mActivity).appColor);
            }
            setAnimation(viewHolder.itemView, position);
        }

    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition) {
//            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.up_to_down);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? android.R.anim.slide_in_left
                        : android.R.anim.slide_in_left);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
    }

    private synchronized void addDescView(LinearLayout llReqDescDetail, int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View inflatedLayout = inflater.inflate(R.layout.layout_desc_single, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setText(descVal);
        llReqDescDetail.addView(inflatedLayout);
    }

    public void updateRecyclerView(Context mContext, ArrayList<ProviderListData.ProviderList> itemsData) {
        this.mContext = mContext;
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }


    public class ProviderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_userimg)
        ImageView ivUserimg;
        @BindView(R.id.tv_request_title)
        TextView tvRequestTitle;
        @BindView(R.id.tv_request_desc1)
        TextView tvRequestDesc1;
        @BindView(R.id.tv_request_desc2)
        TextView tvRequestDesc2;
        @BindView(R.id.tv_request_desc3)
        TextView tvRequestDesc3;
        @BindView(R.id.tv_txt_cont_no)
        TextView tvTxtContNo;
        @BindView(R.id.tv_cont_num)
        TextView tvContNum;
        @BindView(R.id.tv_txt_chat)
        TextView tvTxtChat;
        @BindView(R.id.card_view)
        CardView cardView;

        @BindView(R.id.swipe_layout_1)
        SwipeRevealLayout swipeLayout;

        @BindView(R.id.tv_prov_edit)
        TextView tvProvEdit;
        @BindView(R.id.ll_prov_edit)
        LinearLayout llProvEdit;
        @BindView(R.id.tv_prov_delete)
        TextView tvProvDelete;
        @BindView(R.id.ll_prov_delete)
        LinearLayout llProvDelete;

        @BindView(R.id.ll_prov_desc_detail)
        LinearLayout llProvDesc;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_views)
        TextView tv_views;
        @BindView(R.id.rb_reviews)
        RatingBar rbReviews;


        public ProviderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ProviderListData.ProviderList providerData = itemsData.get(position);
                    Intent detailPage = new Intent(mContext, ProviderDetailActivity.class);
                    detailPage.putExtra("ProviderData", providerData);
                    detailPage.putExtra("ViewType", viewType);
                    AppUtils.appStartIntent(mContext, detailPage);
                }
            });
        }

        public void bind(final String data) {
            llProvEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ProviderListData.ProviderList providerData = itemsData.get(position);
                    Intent detailPage = new Intent(mContext, EditProviderActivity.class);
                    detailPage.putExtra("ViewType", viewType);
                    detailPage.putExtra("ProviderData", providerData);
                    AppUtils.appStartIntent(mContext, detailPage);
                }
            });

            llProvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mActivity instanceof ProvideActivity) {
                        ((ProvideActivity) mActivity).deleteProvider(itemsData.get(getAdapterPosition()).getPId());
                    } else if (mActivity instanceof MyProviderListActivity) {
                        ((MyProviderListActivity) mActivity).deleteProvider(itemsData.get(getAdapterPosition()).getPId());
                    }
                }
            });
        }
    }


    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }


    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(mContext.getString(R.string.log_in_first))
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.txt_login), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //PreferenceStorage.clearPref();
                        Intent intent = new Intent(mContext, MyLoginActivity.class);
                        mContext.startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(mContext.getString(R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
