package in.alibdaa.upbeat.digital.interfaces;

public interface OnSetMyLocation {
    void onLocationSet(String latitude, String longitude, String address);
}
