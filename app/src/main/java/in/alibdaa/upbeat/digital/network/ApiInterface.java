package in.alibdaa.upbeat.digital.network;

import java.util.List;

import in.alibdaa.upbeat.digital.datamodel.BaseResponse;
import in.alibdaa.upbeat.digital.datamodel.CategoryList;
import in.alibdaa.upbeat.digital.datamodel.ChatDetailListData;
import in.alibdaa.upbeat.digital.datamodel.ChatHistoryListData;
import in.alibdaa.upbeat.digital.datamodel.ChatSendMessageModel;
import in.alibdaa.upbeat.digital.datamodel.ColorSettingModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.GETChatCount;
import in.alibdaa.upbeat.digital.datamodel.GETReviewTypes;
import in.alibdaa.upbeat.digital.datamodel.HistoryListData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.LoginData;
import in.alibdaa.upbeat.digital.datamodel.LogoutData;
import in.alibdaa.upbeat.digital.datamodel.POSTCompleteProvider;
import in.alibdaa.upbeat.digital.datamodel.POSTQuestionsCommentsList;
import in.alibdaa.upbeat.digital.datamodel.POSTRatingsProvider;
import in.alibdaa.upbeat.digital.datamodel.POSTReviewList;
import in.alibdaa.upbeat.digital.datamodel.POSTSendComments;
import in.alibdaa.upbeat.digital.datamodel.POSTSendReplies;
import in.alibdaa.upbeat.digital.datamodel.POSTShowMoreReplies;
import in.alibdaa.upbeat.digital.datamodel.ProfileData;
import in.alibdaa.upbeat.digital.datamodel.ProfileUpdateModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderAvailableTimings;
import in.alibdaa.upbeat.digital.datamodel.ProviderHistoryModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.datamodel.StripeDetailsModel;
import in.alibdaa.upbeat.digital.datamodel.SubCategoryList;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionData;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionImageUploadModel;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionSuccessModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    //Login
    @FormUrlEncoded
    @POST("api/login")
    Call<LoginData> getLoginDetails(@Field("username") String username,
                                    @Field("password") String password,
                                    @Field("device_type") String device_type,
                                    @Field("device_id") String device_id,
                                    @Field("login_through") String login_via,
                                    @Field("tokenid") String token_id,
                                    @Header("language") String language);

    @FormUrlEncoded
    @POST("api/forgot_password")
    Call<EmptyData> postForgotPassword(@Field("email_address") String emailAddress, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/logout")
    Call<LogoutData> postUserLogout(@Field("device_type") String device_type, @Field("deviceid") String device_id, @Header("token") String token, @Header("language") String language);

    @Multipart
    @POST("api/signup")
    Call<EmptyData> postSignUp(@Part("username") RequestBody RequestBody,
                               @Part("email") RequestBody email,
                               @Part("mobile_no") RequestBody mobile_no,
                               @Part("password") RequestBody password,
                               @Part("register_through") RequestBody register_through,
                               @Part("latitude") RequestBody latitude,
                               @Part("longitude") RequestBody longitude,
                               @Part("tokenid") RequestBody tokenid,
                               @Part("device_type") RequestBody device_type,
                               @Part("device_id") RequestBody device_id,
                               @Part List<MultipartBody.Part> files,
                               @Header("language") String language);

    @Multipart
    @POST("api/update_profile")
    Call<ProfileUpdateModel> postProfileUpdate(@Part("full_name") RequestBody RequestBody,
                                               @Part("mobile_no") RequestBody mobile_no,
                                               @Part("latitude") RequestBody latitude,
                                               @Part("longitude") RequestBody longitude,
                                               @Part List<MultipartBody.Part> files,
                                               @Header("token") String token,
                                               @Header("language") String language);


    @GET("api/subscription")
    Call<SubscriptionData> getSubsDetails(@Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/request_list")
    Call<RequestListData> getRequestListData(@Field("page") int page, @Field("latitude") String latitude,
                                             @Field("longitude") String longitude, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/provider_list")
    Call<ProviderListData> getProviderListData(@Field("page") int page, @Field("latitude") String latitude,
                                               @Field("longitude") String longitude, @Field("category") String category,
                                               @Field("subcategory") String subcategory, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/my_request_list")
    Call<RequestListData> getMyRequestListData(@Field("page") int page, @Field("filter_by") String filterBy, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/my_provider_list")
    Call<ProviderListData> getMyProviderListData(@Field("page") int page, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/request_accept")
    Call<EmptyData> postRequestAcceptData(@Field("request_id") int requestId, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/request_complete")
    Call<EmptyData> postRequestCompleteData(@Field("request_id") int requestId, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/request")
    Call<EmptyData> postRequestData(@Field("title") String title,
                                    @Field("description") String description,
                                    @Field("location") String location,
                                    @Field("request_date") String request_date,
                                    @Field("request_time") String request_time,
                                    @Field("proposed_fee") String proposed_fee,
                                    @Field("contact_number") String contact_number,
                                    @Field("latitude") String latitude,
                                    @Field("longitude") String longitude,
                                    @Header("token") String token,
                                    @Header("language") String language);

    @FormUrlEncoded
    @POST("api/request_update")
    Call<EmptyData> updateRequestData(@Field("request_id") String requestId,
                                      @Field("title") String title,
                                      @Field("description") String description,
                                      @Field("location") String location,
                                      @Field("request_date") String request_date,
                                      @Field("request_time") String request_time,
                                      @Field("proposed_fee") String proposed_fee,
                                      @Field("contact_number") String contact_number,
                                      @Field("latitude") String latitude,
                                      @Field("longitude") String longitude,
                                      @Header("token") String token,
                                      @Header("language") String language);

    @FormUrlEncoded
    @POST("api/request_remove")
    Call<EmptyData> deleteRequestData(@Field("request_id") String serviceId, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/provide")
    Call<EmptyData> postProviderData(@Field("title") String title,
                                     @Field("description_details") String description,
                                     @Field("location") String location,
                                     @Field("availability") String availability,
                                     @Field("contact_number") String contact_number,
                                     @Field("latitude") String latitude,
                                     @Field("longitude") String longitude,
                                     @Field("start_date") String start_date,
                                     @Field("end_date") String end_date,
                                     @Field("category") String category,
                                     @Field("subcategory") String subcategory,
                                     @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/provide_update")
    Call<EmptyData> updateProviderData(@Field("service_id") String serviceId,
                                       @Field("title") String title,
                                       @Field("description_details") String description,
                                       @Field("location") String location,
                                       @Field("availability") String availability,
                                       @Field("contact_number") String contact_number,
                                       @Field("latitude") String latitude,
                                       @Field("longitude") String longitude,
                                       @Field("start_date") String start_date,
                                       @Field("end_date") String end_date,
                                       @Header("token") String token,
                                       @Header("language") String language,
                                       @Field("category") String category,
                                       @Field("subcategory") String subcategory);

    @FormUrlEncoded
    @POST("api/service_remove")
    Call<EmptyData> deleteProviderData(@Field("service_id") String serviceId, @Header("token") String token, @Header("language") String language);

    @GET("api/profile")
    Call<ProfileData> getProfileData(@Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/history_list")
    Call<HistoryListData> getHistoryListData(@Field("status") int status, @Field("request") int request, @Field("page") int page, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/chat_history")
    Call<ChatHistoryListData> postChatHistoryList(@Field("page") int page, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/chat_details")
    Call<ChatDetailListData> postChatDetailList(@Field("page") int page, @Field("chat_id") int to, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/chat")
    Call<ChatSendMessageModel> postSendChatMessage(@Field("content") String content, @Field("to") int to, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/language")
    Call<LanguageModel> getLanguageData(@Field("language") String langCode);

    @GET("api/colour_settings")
    Call<ColorSettingModel> getColorData();

    @FormUrlEncoded
    @POST("api/search_request_list")
    Call<RequestListData> getSearchRequestData(@Field("page") int page,
                                               @Field("latitude") String latitude,
                                               @Field("longitude") String longitude,
                                               @Field("search_title") String search_title,
                                               @Field("request_date") String request_date,
                                               @Field("request_time") String request_time,
                                               @Field("min_price") String min_price,
                                               @Field("max_price") String max_price,
                                               @Field("location") String location,
                                               @Header("token") String token,
                                               @Header("language") String language);

    @FormUrlEncoded
    @POST("api/provider_search_list")
    Call<ProviderListData> getSearchProviderData(@Field("page") int page,
                                                 @Field("latitude") String latitude,
                                                 @Field("longitude") String longitude,
                                                 @Field("search_title") String search_title,
                                                 @Field("search_date") String search_date,
                                                 @Field("location") String location,
                                                 @Header("token") String token,
                                                 @Header("language") String language,
                                                 @Field("category") String category,
                                                 @Field("subcategory") String subcategory);

    @Multipart
    @POST("api/profile_image_upload")
    Call<SubscriptionImageUploadModel> postSubscriptionDetails(@Header("token") String token,
                                                               @Part List<MultipartBody.Part> files,
                                                               @Header("language") String language);

    @FormUrlEncoded
    @POST("api/subscription_success")
    Call<SubscriptionSuccessModel> postSuccessSubscription(@Field("subscription_id") String subscription_id,
                                                           @Field("transaction_id") String transaction_id,
                                                           @Field("payment_details") String payment_details,
                                                           @Header("token") String token,
                                                           @Header("language") String language);

    @FormUrlEncoded
    @POST("api/api/change_password")
    Call<EmptyData> postChangePassword(@Field("current_password") String current_password,
                                       @Field("new_password") String new_password,
                                       @Field("confirm_password") String confirm_password,
                                       @Header("token") String token,
                                       @Header("language") String language);

    @GET("api/stripe_details")
    Call<StripeDetailsModel> getStripeDetails(@Header("token") String token);

    @GET("api/api/category")
    Call<CategoryList> getCategories(@Header("token") String token);


    @FormUrlEncoded
    @POST("api/api/sub_category")
    Call<SubCategoryList> getSubCategories(@Header("token") String token, @Field("category") String category);

    @FormUrlEncoded
    @POST("api/api/views")
    Call<BaseResponse> postViews(@Header("token") String token, @Field("p_id") String p_id, @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("api/api/provider_details")
    Call<ProviderAvailableTimings> providerDetails(@Header("token") String token, @Field("p_id") String p_id);

    @FormUrlEncoded
    @POST("api/api/book_service")
    Call<BaseResponse> providerBookApnt(@Header("token") String token,
                                        @Field("provider_id") String p_id,
                                        @Field("service_time") String service_time,
                                        @Field("service_date") String service_date,
                                        @Field("latitude") String latitude,
                                        @Field("longitude") String longitude,
                                        @Field("notes") String notes);


    @GET("api/api/my_booking_list")
    Call<ProviderHistoryModel> getMyBookings(@Header("token") String token);

    @GET("api/api/booking_request_list")
    Call<ProviderHistoryModel> getHistoryBookings(@Header("token") String token);

    @FormUrlEncoded
    @POST("api/comments_list")
    Call<POSTQuestionsCommentsList> postCommentsList(@Header("language") String language,@Header("token") String token, @Field("p_id") String question_id, @Field("page") String page_no, @Field("limit") String limit);

    @FormUrlEncoded
    @POST("api/comments")
    Call<POSTSendComments> postSendComments(@Header("token") String token, @Field("p_id") String p_id, @Field("comment") String comment);

    @FormUrlEncoded
    @POST("api/replies")
    Call<POSTSendReplies> postSendReplies(@Header("token") String token, @Field("p_id") String p_id, @Field("replies") String comments, @Field("comment_id") String comment_id);

    @FormUrlEncoded
    @POST("api/replies_list")
    Call<POSTShowMoreReplies> postCommentsReplyList(@Header("token") String token, @Field("comment_id") String comment_id);

    @FormUrlEncoded
    @POST("api/api/complete_provider")
    Call<POSTCompleteProvider> postCompleteProvider(@Header("token") String token, @Field("book_service_id") String book_service_id, @Field("provider_id") String provider_id);

    @FormUrlEncoded
    @POST("api/api/rate_review")
    Call<POSTRatingsProvider> postRatingsProvider(@Header("token") String token, @Field("rating") String rating, @Field("review") String review, @Field("p_id") String p_id, @Field("type") String type);

    @FormUrlEncoded
    @POST("api/rate_review_list")
    Call<POSTReviewList> getReviewList(@Header("language") String language,@Header("token") String token, @Field("p_id") String p_id, @Field("page") String page);

    @FormUrlEncoded
    @POST("api/chat_history_requester")
    Call<ChatHistoryListData> postChatRequestorHistoryList(@Field("page") int page, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/chat_details_requester")
    Call<ChatDetailListData> postChatRequestorDetailList(@Field("page") int page, @Field("chat_id") int to, @Header("token") String token, @Header("language") String language);

    @FormUrlEncoded
    @POST("api/chat_requester")
    Call<ChatSendMessageModel> postRequestorSendChatMessage(@Field("content") String content, @Field("to") int to, @Header("token") String token, @Header("language") String language);

    @GET("api/ratings_type")
    Call<GETReviewTypes> getReviewTypes(@Header("token") String token);

    @GET("api/chat_history_count")
    Call<GETChatCount> getChatCount(@Header("token") String token);

}