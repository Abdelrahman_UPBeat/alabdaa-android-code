package in.alibdaa.upbeat.digital.utils;

public class AppConstants {

   //     public static final String BASE_URL = "https://dreamguys.co.in/servpro/";
//    public static final String BASE_URL = "https://dreamguys.co.in/servpro_dev/";
//public static final String BASE_URL = "https://upbeatdigitaleg.website/alibdaa/";

    public static final String BASE_URL = "https://alibdaapcc.com/app/";




    public static final int REG_NORMAL = 1;
    public static final int REG_FB = 2;
    public static final int REG_GPLUS = 3;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final int REQUEST_PERMISSIONS = 0;

    public static final String deviceType = "Android";

    //User details
    public static final String USER_TOKEN = "user_token";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_SUBS_TYPE = "user_subs_type";
    public static final String USER_PROFILE_IMG = "USER_PROFILE_IMG";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_PHONE = "USER_PHONE";


    //Retrofit API response model data
    public static final String LOGIN_DATA = "LoginData";
    public static final String SIGNUP_DATA = "SignUpData";
    public static final String SUBSCRIPTION_DATA = "SubscriptionData";
    public static final String REQUESTLIST_DATA = "RequestListData";
    public static final String PROVIDERLIST_DATA = "ProviderListData";
    public static final String REQUEST_ACCEPT_DATA = "RequestAcceptData";
    public static final String REQUEST_COMPLETE_DATA = "RequestCompleteData";
    public static final String CREATE_REQUEST_DATA = "CreateRequestData";
    public static final String UPDATE_REQUEST_DATA = "UpdateRequestData";
    public static final String DELETE_REQUEST_DATA = "DeleteRequestData";
    public static final String CREATE_PROVIDER_DATA = "CreateProviderData";
    public static final String UPDATE_PROVIDER_DATA = "UpdateProviderData";
    public static final String DELETE_PROVIDER_DATA = "DeleteProviderData";
    public static final String HISTORY_DATA = "HistoryData";
    public static final String LANGUAGE_DATA = "LanguageData";
    public static final String COLORSETTING_DATA = "ColorSettingData";
    public static final String PROFILE_DATA = "ProfileData";
    public static final String PROFILE_UPDATE_DATA = "ProfileUpdateData";
    public static final String CHAT_HISTORYLIST_DATA = "ChatHistoryListData";
    public static final String CHAT_DETAILLIST_DATA = "ChatDetailListData";
    public static final String CHAT_SENDDETAILLIST_DATA = "ChatSendDetailListData";
    public static final String SUBS_SUCCESS_DATA = "SUBS_SUCCESS_DATA";
    public static final String CHANGEPASSWORD = "ChangePasswordListData";
    public static final String FORGOT_PASSWORD = "ForgotPassword";


    public static final String APP_DATE_FORMAT = "dd-MMM-yyyy";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd";
    public static final String APP_THEME = "AppTheme";
    public static final String LANGUAGE_SET = "LanguageSet";
    public static final String COLOR_LIST = "ColorList";
    public static final String SEARCH_REQUEST = "1001";
    public static final String SEARCH_PROVIDER = "1002";

    public static final String MY_LONGITUDE = "MY_LONGITUDE";
    public static final String MY_LATITUDE = "MY_LATITUDE";
    public static final String MY_ADDRESS = "MY_ADDRESS";
    public static final String MY_LANGUAGE = "MY_LANGUAGE";

    public static final int HIS_PENDING = 1;
    public static final int HIS_COMPLETED = 2;
    public static final int HIS_USER_CREATED = 1;
    public static final int HIS_USER_ACCEPTED = 2;
    public static final int HIS_USER_ALL = 3;

    public static final String REQ_STATUS_ALL = null;
    public static final String REQ_STATUS_PENDING = "0";
    public static final String REQ_STATUS_ACCEPTED = "1";
    public static final String REQ_STATUS_COMPLETED = "2";

    public static final String PAGE_LOGIN = "LOGIN";
    public static final String PAGE_SIGN_UP = "SIGN_UP";
    public static final String PAGE_DASHBOARD = "DASHBOARD";
    public static final String PAGE_DASHBOARD_REQUEST = "DASHBOARD_REQUEST";
    public static final String PAGE_DASHBOARD_PROVIDER = "DASHBOARD_PROVIDER";
    public static final String PAGE_PROVIDER_DETAIL = "PROVIDER_DETAIL";
    public static final String PAGE_REQUEST_DETAIL = "REQUEST_DETAIL";
    public static final String PAGE_CREATE_PROVIDER = "CREATE_PROVIDER";
    public static final String PAGE_CREATE_REQUEST = "CREATE_REQUEST";
    public static final String PAGE_EDIT_REQUEST = "EDIT_REQUEST";
    public static final String PAGE_EDIT_PROVIDER = "EDIT_PROVIDER";
    public static final String PAGE_HISTORY = "HISTORY";
    public static final String PAGE_HISTORY_DETAIL = "HISTORY_DETAIL";
    public static final String PAGE_SETTINGS = "SETTINGS";
    public static final String PAGE_ADVANCED_SEARCH = "ADVANCED_SEARCH";
    public static final String PAGE_MY_PROFILE = "MY_PROFILE";
    public static final String PAGE_CHAT = "CHAT";
    public static final String SUBSCRIPTIONSUCCESS = "SUBSCRIPTIONSUCCESS";
    public static final String STRIPEDETAILS = "STRIPEDETAILS";
    public static final String CATEGORIES = "CATEGORIES";
    public static final String SubCatID = "SubCatID";
    public static final String SUBCATEGORIES = "SUBCATEGORIES";
    public static final String CatID = "CatID";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String VIEWS = "VIEWS";
    public static final String PROVIDERAVAIL = "PROVIDERAVAIL";
    public static final String PID = "PID";
    public static final String BOOK_PROVIDER = "BOOK_PROVIDER";
    public static final String PROVIDERBOOK = "PROVIDERBOOK";
    public static final String MYPROVIDERHISTORY = "MYPROVIDERHISTORY";
    public static final String MYBOOKINGS = "MYBOOKINGS";
    public static final String TermsConditionsURL = "https://alibdaapcc.com/ar/%D8%B3%D9%8A%D8%A7%D8%B3%D8%A9-%D8%A7%D9%84%D8%AE%D8%B5%D9%88%D8%B5%D9%8A%D8%A9/";
    public static final String ContactUsURL = "https://dreamguys.co.in/contactus/";
    public static final String CATNAME = "CATNAME";
    public static final String COMPLETEPROVIDER = "COMPLETEPROVIDER";
    public static final String RATINGS = "RATINGS";
    public static final String REVIEWLIST = "REVIEWLIST";
    public static final String ReviewTypes = "ReviewTypes";
    public static final String URL = "URL";
    public static final String TbTitle = "TbTitle";
    public static final String CHATCOUNT = "CHATCOUNT";
    public static final String SUBCATNAME = "SUBCATNAME";


    //Password Match
    public static String passwordMatch = "^(?=.*\\d)(?=.*[a-z]|[A-Z])[^\\s]{8,15}$";

    //refreshed Token
    public static String refreshedToken = "refreshedToken";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
    public static final String GROUP_KEY_WORK_EMAIL = "in.co.dreamguys.servpro";


    public static final String SUBSCRIPTIONIMAGELIST = "SubscriptionChatList";

    public static String chatFrom = "chatFrom";

    public static String chatUsername = "chatUsername";
    public static String chatImg = "chatImg";
    public static String StripePrice = "StripePrice";
    public static String StripeSubId = "StripeSubId";
    public static String StripeSubName = "StripeSubName";
    public static String DefaultCurrency = "MYR";
    public static String currentActivity = "";

    public static final String CommentList = "CommentList";
    public static final String QuestionID = "QuestionID";
    public static String isShowMore = "isShowMore";
    public static final String CommentsID = "CommentsID";
    public static String pageLimit = "30";
    public static String localeName = "ar";
    public static String isReplied = "isReplied";
}
