package in.alibdaa.upbeat.digital.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.HistoryActivity;
import in.alibdaa.upbeat.digital.HistoryDetailActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.HistoryListData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.viewwidgets.CircleTransform;
import in.alibdaa.upbeat.digital.viewwidgets.SwipeRevealLayout;

public class HistoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    HistoryActivity mActivity;
    Context mContext;
    public ArrayList<HistoryListData.RequestList> itemsData = new ArrayList<>();
    public int viewType;
    LanguageModel.Request_and_provider_list request_and_provider_list = new LanguageModel().new Request_and_provider_list();

    public OnLoadMoreListener mOnLoadMoreListener;
    private int SELF = 1, LOADING = 2;

    // viewType - 1 - Pending
    // viewType - 2 - Completed
    public HistoryListAdapter(HistoryActivity mActivity, Context mContext, ArrayList<HistoryListData.RequestList> itemsData, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        request_and_provider_list = mActivity.requestAndProviderList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemView;
        if (viewType == LOADING) {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_request, parent, false);
            return new HistoryViewHolder(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (itemsData.get(position) == null) {
            return LOADING;
        } else {
            return SELF;
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (viewHolder instanceof HistoryViewHolder) {
            HistoryViewHolder historyViewHolder = (HistoryViewHolder) viewHolder;
            historyViewHolder.tvRequestTitle.setText(AppUtils.cleanString(mContext, itemsData.get(position).getTitle()));

            historyViewHolder.tvRequestDesc1.setVisibility(View.INVISIBLE);
            historyViewHolder.tvRequestDesc2.setVisibility(View.INVISIBLE);
            historyViewHolder.tvRequestDesc3.setVisibility(View.INVISIBLE);

            try {
                JSONArray descList = new JSONArray(itemsData.get(position).getDescription());
                if (descList.length() == 1) {
                    historyViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        historyViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                } else if (descList.length() == 2) {
                    historyViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        historyViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                    historyViewHolder.tvRequestDesc2.setText(AppUtils.cleanString(mContext, descList.getString(1)));
                    if (!AppUtils.cleanString(mContext, descList.getString(1)).isEmpty())
                        historyViewHolder.tvRequestDesc2.setVisibility(View.VISIBLE);
                } else if (descList.length() >= 3) {
                    historyViewHolder.tvRequestDesc1.setText(AppUtils.cleanString(mContext, descList.getString(0)));
                    if (!AppUtils.cleanString(mContext, descList.getString(0)).isEmpty())
                        historyViewHolder.tvRequestDesc1.setVisibility(View.VISIBLE);
                    historyViewHolder.tvRequestDesc2.setText(AppUtils.cleanString(mContext, descList.getString(1)));
                    if (!AppUtils.cleanString(mContext, descList.getString(1)).isEmpty())
                        historyViewHolder.tvRequestDesc2.setVisibility(View.VISIBLE);
                    historyViewHolder.tvRequestDesc3.setText(AppUtils.cleanString(mContext, descList.getString(2)));
                    if (!AppUtils.cleanString(mContext, descList.getString(2)).isEmpty())
                        historyViewHolder.tvRequestDesc3.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String profPic = AppConstants.BASE_URL + itemsData.get(position).getRequestImage();

            Picasso.with(mContext)
                    .load(profPic)
                    .placeholder(R.drawable.ic_pic_view)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_pic_view)
                    .into(historyViewHolder.ivUserimg);
            historyViewHolder.tvAppntDate.setText(AppUtils.cleanString(mContext, itemsData.get(position).getRequestDate()));
            historyViewHolder.tvAppntTime.setText(AppUtils.cleanString(mContext, itemsData.get(position).getRequestTime()));
            historyViewHolder.tvAppntFee.setText(AppUtils.cleanString(mContext, itemsData.get(position).getCurrencyCode() + " " + itemsData.get(position).getAmount()));

            historyViewHolder.swipeLayout.setLockDrag(true);

            try {
                historyViewHolder.tvTxtAppnt.setText(AppUtils.cleanLangStr(mContext, request_and_provider_list.getLg6_appointment(), R.string.txt_appnt));
            } catch (Exception e) {
            }
            historyViewHolder.tvAppntFee.setTextColor(mActivity.appColor);
        }
    }

    public void updateRecyclerView(Context mContext, ArrayList<HistoryListData.RequestList> itemsData) {
        this.mContext = mContext;
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    private synchronized void addDescView(LinearLayout llReqDescDetail, int i, String descVal) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View inflatedLayout = inflater.inflate(R.layout.layout_desc_single, null, false);
        TextView tvTxtDesc = (TextView) inflatedLayout.findViewById(R.id.tv_bullet);
        tvTxtDesc.setTag(i);
        tvTxtDesc.setText(descVal);
        llReqDescDetail.addView(inflatedLayout);
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }


    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_userimg)
        ImageView ivUserimg;
        @BindView(R.id.tv_request_title)
        TextView tvRequestTitle;
        @BindView(R.id.tv_request_desc1)
        TextView tvRequestDesc1;
        @BindView(R.id.tv_request_desc2)
        TextView tvRequestDesc2;
        @BindView(R.id.tv_request_desc3)
        TextView tvRequestDesc3;
        @BindView(R.id.tv_txt_appnt)
        TextView tvTxtAppnt;
        @BindView(R.id.tv_appnt_date)
        TextView tvAppntDate;
        @BindView(R.id.tv_appnt_time)
        TextView tvAppntTime;
        @BindView(R.id.tv_appnt_fee)
        TextView tvAppntFee;
        @BindView(R.id.card_view)
        CardView cardView;

        @BindView(R.id.ll_req_desc_detail)
        LinearLayout llReqDesc;

        @BindView(R.id.swipe_layout_1)
        SwipeRevealLayout swipeLayout;

        public HistoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    HistoryListData.RequestList requestData = itemsData.get(position);
                    Intent detailPage = new Intent(mContext, HistoryDetailActivity.class);
                    detailPage.putExtra("ViewType", viewType);
                    detailPage.putExtra("RequestData", requestData);
                    AppUtils.appStartIntent(mContext, detailPage);
                }
            });
        }
    }
}
