package in.alibdaa.upbeat.digital;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.adapters.ProviderBookAvailAdapter;
import in.alibdaa.upbeat.digital.datamodel.BaseResponse;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderAvailableTimings;
import in.alibdaa.upbeat.digital.interfaces.OnSetMyLocation;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class ProviderBookAppointment extends BaseAppCompatActivity implements RetrofitHandler.RetrofitResHandler, OnSetMyLocation {


    public static Context mContext;

    @BindView(R.id.tv_choose_location)
    TextView tvChooseLocation;
    @BindView(R.id.rv_availabletime)
    RecyclerView rvAvailabletime;
    @BindView(R.id.et_notes)
    EditText etNotes;
    ProviderAvailableTimings providerAvailableTimings;
    LinearLayoutManager linearLayoutManager;
    ProviderBookAvailAdapter providerBookAvailAdapter;
    String pID = "", service_date = "", service_time = "";
    public String latitude, longitude, address;
    @BindView(R.id.btn_book)
    Button btnBook;
    @BindView(R.id.et_location)
    EditText etLocation;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    @BindView(R.id.tv_txt_notes)
    TextView tvTxtNotes;
    @BindView(R.id.tv_txt_location)
    TextView tvTxtLocation;
    @BindView(R.id.tv_available_time)
    TextView tvAvailableTime;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_provider);
        ButterKnife.bind(this);
        mContext = this;
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, commonData.getLg7_book_provider(), R.string.txt_book_provider));
        pID = getIntent().getStringExtra(AppConstants.PID);
        linearLayoutManager = new LinearLayoutManager(ProviderBookAppointment.this, LinearLayoutManager.VERTICAL, false);
        rvAvailabletime.setLayoutManager(linearLayoutManager);
        btnBook.setBackgroundColor(appColor);
        btnBook.setText(AppUtils.cleanLangStr(this, commonData.getLg7_book(), R.string.txt_book));
        tvChooseLocation.setText(AppUtils.cleanLangStr(this, commonData.getLg7_choose_location(), R.string.chooseLocation));
        tvTxtNotes.setText(AppUtils.cleanLangStr(this, commonData.getLg7_notes(), R.string.txt_note));
        tvTxtLocation.setText(AppUtils.cleanLangStr(this, commonData.getLg7_choose_location(), R.string.chooseLocation));
        tvAvailableTime.setText(AppUtils.cleanLangStr(this, commonData.getLg7_available_times(), R.string.txt_available_times));
        getAvailableTime();
    }

    public void getAvailableTime() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ProviderAvailableTimings> classificationCall = apiService.providerDetails(PreferenceStorage.getKey(AppConstants.USER_TOKEN), pID);
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROVIDERAVAIL, ProviderBookAppointment.this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

        if (myRes instanceof ProviderAvailableTimings) {
            providerAvailableTimings = (ProviderAvailableTimings) myRes;
            providerBookAvailAdapter = new ProviderBookAvailAdapter(this, providerAvailableTimings.getData().getAvailability_details());
            rvAvailabletime.setAdapter(providerBookAvailAdapter);
        } else if (myRes instanceof BaseResponse) {

            BaseResponse baseResponse = (BaseResponse) myRes;
            if (baseResponse.getResponseHeader().getResponseCode().equalsIgnoreCase("1")) {
                Toast.makeText(mContext, AppUtils.cleanLangStr(this, commonData.getLg7_booked_successf(), R.string.bookedsuccessfully), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(mContext, baseResponse.getResponseHeader().getResponseMessage(), Toast.LENGTH_SHORT).show();
            }

        }


    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onLocationSet(String latitude, String longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        etLocation.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
        etLocation.setText(address);
    }

    @OnClick({R.id.et_location, R.id.btn_book})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_location:
                Intent callMapActivity = new Intent(this, MapActivity.class);
                callMapActivity.putExtra("From", AppConstants.BOOK_PROVIDER);
                AppUtils.appStartIntent(this, callMapActivity);
                break;
            case R.id.btn_book:

                for (int i = 0; i < providerBookAvailAdapter.availList.size(); i++) {
                    if (providerBookAvailAdapter.availList.get(i).getIs_selected().equalsIgnoreCase("1")) {
                        Log.d("TAG", String.valueOf(providerBookAvailAdapter.availList.get(i).getService_date()));
                        service_date = providerBookAvailAdapter.availList.get(i).getService_date();
                        service_time = providerBookAvailAdapter.availList.get(i).getService_time();
                    }
                }


                if (latitude.isEmpty() && longitude.isEmpty()) {
                    AppUtils.showToast(this, AppUtils.cleanLangStr(this, commonData.getLg7_choose_location(), R.string.chooseLocation));
                    return;
                } else if (service_date.isEmpty() && service_time.isEmpty()) {
                    AppUtils.showToast(this, AppUtils.cleanLangStr(this, commonData.getLg7_choose_availabl(), R.string.choosedatetime));
                    return;
                } else if (etNotes.getText().toString().isEmpty()) {
                    AppUtils.showToast(this, AppUtils.cleanLangStr(this, commonData.getLg7_enter_notes(), R.string.enternotes));
                    return;
                } else {
                    bookProviderAppointment();
                }


                break;
        }
    }


    public void bookProviderAppointment() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<BaseResponse> classificationCall = apiService.providerBookApnt(PreferenceStorage.getKey(AppConstants.USER_TOKEN), pID, service_time, service_date, latitude, longitude, etNotes.getText().toString());
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.PROVIDERBOOK, ProviderBookAppointment.this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
