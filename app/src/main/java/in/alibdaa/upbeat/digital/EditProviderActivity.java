package in.alibdaa.upbeat.digital;


import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.adapters.ViewPagerAdapter;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.fragments.EditProvideAvailFragment;
import in.alibdaa.upbeat.digital.fragments.EditProvideInfoFragment;
import in.alibdaa.upbeat.digital.interfaces.OnSetMyLocation;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CustomViewPager;
import retrofit2.Call;

public class EditProviderActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler, OnSetMyLocation {

    public static Context mContext;

    @BindView(R.id.tab_create_provider)
    TabLayout tabCreateProvider;
    @BindView(R.id.pager_create_provider)
    CustomViewPager pagerCreateProvider;

    ViewPagerAdapter adapter;

    EditProvideInfoFragment createProvideInfoFragment;
    EditProvideAvailFragment createProvideAvailFragment;

    public ProviderData providerData;

    public LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();

    ProviderListData.ProviderList providerDataDetail;

    int viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_create_provider);
        ButterKnife.bind(this);
        mContext = this;
        getLocaleData();
        setToolBarTitle(AppUtils.cleanLangStr(this, langReqProvData.getLg6_provide(), R.string.txt_provider));

        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        providerData = new ProviderData();

        providerDataDetail = getIntent().getParcelableExtra("ProviderData");
        viewType = getIntent().getIntExtra("ViewType", 0);

        // Add Fragments to adapter
        createProvideInfoFragment = new EditProvideInfoFragment();
        createProvideInfoFragment.myCreateProvideInfoFragment(this, providerDataDetail);
        adapter.addFragment(createProvideInfoFragment, AppUtils.cleanLangStr(this, langReqProvData.getLg6_info(), R.string.txt_info));

        createProvideAvailFragment = new EditProvideAvailFragment();
        createProvideAvailFragment.myCreateProvideAvailFragment(this, providerDataDetail);
        adapter.addFragment(createProvideAvailFragment, AppUtils.cleanLangStr(this, langReqProvData.getLg6_availability(), R.string.txt_availability));

        pagerCreateProvider.setAdapter(adapter);
        pagerCreateProvider.setOffscreenPageLimit(2);
        pagerCreateProvider.disableScroll(true);

        //Tab
        tabCreateProvider.setupWithViewPager(pagerCreateProvider);
        tabCreateProvider.setSelectedTabIndicatorColor(appColor);
        LinearLayout tabStrip = ((LinearLayout) tabCreateProvider.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        pagerCreateProvider.setOnPageChangeListener(new CustomViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    pagerCreateProvider.disableScroll(true);
                } else if (position == 1) {
                    pagerCreateProvider.disableScroll(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        setToolBarTitle(providerDataDetail.getTitle());
        tabCreateProvider.getTabAt(0).setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_info(), R.string.txt_info));
        tabCreateProvider.getTabAt(1).setText(AppUtils.cleanLangStr(this, langReqProvData.getLg6_availability(), R.string.txt_availability));
        tabCreateProvider.setSelectedTabIndicatorColor(appColor);
    }

    public boolean validateData(EditText etTxt, String etVal, String msg) {
        String msg2 = "";
        if (etTxt != null) {
            if (etTxt.getId() == R.id.et_title) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_title_cannot_be(), R.string.err_txt_title);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else if (etTxt.getText().toString().length() > 30) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_title_cannot_be(), R.string.err_max_characters);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }

            } else if (etTxt.getId() == R.id.et_location) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_location_addres(), R.string.err_txt_addr);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_contact_no) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_contact_number_(), R.string.err_txt_contact_no);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else if (etTxt.getText().toString().length() < 10 || etTxt.getText().toString().length() > 15) {
                    msg2 = AppUtils.cleanLangStr(this, langReqProvData.getLg6_contact_number_(), R.string.err_txt_contact_no);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));//TODO:
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_category) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, commonData.getLg7_please_choose_c(), R.string.err_category);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else if (etTxt.getId() == R.id.et_subcategory) {
                if (etTxt.getText().toString().isEmpty()) {
                    msg2 = AppUtils.cleanLangStr(this, commonData.getLg7_please_choose_s(), R.string.err_subcategory);
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner_red));
                    AppUtils.showToast(EditProviderActivity.this, msg2);
                    return false;
                } else {
                    etTxt.setBackground(getResources().getDrawable(R.drawable.shape_rect_round_corner));
                }
            } else {
                msg2 = msg;
                AppUtils.showToast(EditProviderActivity.this, msg2);
                return false;
            }

        }
        return true;
    }

    public void postDataToServer() {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            if (providerData != null) {
                try {
                    String fromDate = AppUtils.formatDateToServer(providerData.getFromDate());
                    String toDate = AppUtils.formatDateToServer(providerData.getToDate());

                    Call<EmptyData> classificationCall = apiService.updateProviderData(providerDataDetail.getPId(), providerData.getTitle(),
                            providerData.getDescListData(), providerData.getLocation(), providerData.getAvailListData(),
                            providerData.getContactNo(), providerData.getProvLat(), providerData.getProvLng(), fromDate,
                            toDate, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE), providerData.getCatID(), providerData.getSubCatID());
                    RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.UPDATE_PROVIDER_DATA, this, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    public void onLocationSet(String latitude, String longitude, String address) {
        createProvideInfoFragment.setLocationData(latitude, longitude, address);
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        EmptyData emptyData = (EmptyData) myRes;
        AppUtils.showToast(EditProviderActivity.this, emptyData.getResponseHeader().getResponseMessage());
        Intent nextIntent = null;
        switch (viewType) {
            case 0:
                nextIntent = new Intent(EditProviderActivity.this, MainActivity.class);
                nextIntent.putExtra("MyFragment", 1);
                break;
            case 1:
                nextIntent = new Intent(EditProviderActivity.this, MyProviderListActivity.class);
                break;
        }
        AppUtils.appStartIntent(EditProviderActivity.this, nextIntent);
        EditProviderActivity.this.finish();
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void gotoNext() {
        pagerCreateProvider.setCurrentItem(1);
    }

    public class ProviderData {
        private String title;
        private String contactNo;
        private String location;
        private String provLat;
        private String provLng;
        private String fromDate;
        private String toDate;
        private String descListData;
        private String availListData;
        private String catID;
        private String subCatID;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getProvLat() {
            return provLat;
        }

        public void setProvLat(String provLat) {
            this.provLat = provLat;
        }

        public String getProvLng() {
            return provLng;
        }

        public void setProvLng(String provLng) {
            this.provLng = provLng;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getDescListData() {
            return descListData;
        }

        public void setDescListData(String descListData) {
            this.descListData = descListData;
        }

        public String getAvailListData() {
            return availListData;
        }

        public void setAvailListData(String availListData) {
            this.availListData = availListData;
        }

        public String getCatID() {
            return catID;
        }

        public void setCatID(String catID) {
            this.catID = catID;
        }

        public String getSubCatID() {
            return subCatID;
        }

        public void setSubCatID(String subCatID) {
            this.subCatID = subCatID;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof TextInputEditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.request_and_provider_list);
            langReqProvData = new Gson().fromJson(commonDataStr, LanguageModel.Request_and_provider_list.class);
        } catch (Exception e) {
            langReqProvData = new LanguageModel().new Request_and_provider_list();
        }
    }
}
