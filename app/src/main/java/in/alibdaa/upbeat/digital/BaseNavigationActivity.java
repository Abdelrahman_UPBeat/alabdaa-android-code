package in.alibdaa.upbeat.digital;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import in.alibdaa.upbeat.digital.datamodel.GETChatCount;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.LogoutData;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class BaseNavigationActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener {

    private NavigationView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;
    private LinearLayout view_stub; //This is the framelayout to keep your content view
    Toolbar mToolbar;
    private Activity currentActivity;
    private Location mLocation;
    String Latitude, Longitude;
    private ImageView iv_profile_img;
    private TextView tv_username, tv_email;

    protected GoogleApiClient mGoogleApiClient;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    public ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    public ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    LanguageModel.Navigation navData = new LanguageModel().new Navigation();
    LanguageModel.Logout logoutData = new LanguageModel().new Logout();
    LanguageModel.Sign_up signUpData = new LanguageModel().new Sign_up();
    LanguageModel.Profile profileData = new LanguageModel().new Profile();
    public int appColor = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base_navigation_layout);
        getLocaleData();
        getAppTheme();
        view_stub = (LinearLayout) findViewById(R.id.view_stub);
        navigation_view = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        View header = navigation_view.getHeaderView(0);

        iv_profile_img = (ImageView) header.findViewById(R.id.iv_userimage);
        tv_username = (TextView) header.findViewById(R.id.input_username);
        tv_email = (TextView) header.findViewById(R.id.input_email);

        updateUserInfoDetails();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mToolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        mToolbar.setTitle(AppUtils.cleanLangStr(this, "", R.string.app_name));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#d42129")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(AppUtils.cleanLangStr(this, "", R.string.app_name));
        getLocaleData();

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        checkLocationPermission();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        setNavigationText("", "", "", "");

    }

    public void checkLocationPermission() {
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }

    private void setNavigationText(String provider_chat_count, String requester_chat_count, String requestHistoryCount, String providerHistoryCount) {
        drawerMenu = navigation_view.getMenu();
        for (int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
            String title = "";
            switch (i) {
                case 0:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_dashboard(), R.string.txt_dashboard);
                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 1:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_my_profile(), R.string.txt_my_profile);
                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 2:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_my_requests(), R.string.txt_my_requests);
                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 3:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_my_services(), R.string.txt_my_providers);
                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 4:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_history(), R.string.txt_request_history);
                    if (!requestHistoryCount.isEmpty() && !requestHistoryCount.equalsIgnoreCase("0")) {
                        title = AppUtils.cleanLangStr(this, navData.getLg3_history(), R.string.txt_request_history) + "   " + requestHistoryCount + " ";
                        SpannableString sColored = new SpannableString(title);
                        sColored.setSpan(new BackgroundColorSpan(Color.GRAY), title.length() - (requestHistoryCount.length() + 2), title.length(), 0);
                        sColored.setSpan(new ForegroundColorSpan(Color.WHITE), title.length() - (requestHistoryCount.length() + 2), title.length(), 0);
                        drawerMenu.getItem(i).setTitle(sColored);
                    } else {
                        drawerMenu.getItem(i).setTitle(title);
                    }
//                    title = AppUtils.cleanLangStr(this, navData.getLg3_history(), R.string.txt_request_history);
//                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 5:

                    title = AppUtils.cleanLangStr(this, navData.getLg3_provider_histor(), R.string.txt_provider_history);
                    if (!providerHistoryCount.isEmpty() && !providerHistoryCount.equalsIgnoreCase("0")) {
                        title = AppUtils.cleanLangStr(this, navData.getLg3_provider_histor(), R.string.txt_provider_history) + "   " + providerHistoryCount + " ";
                        SpannableString sColored = new SpannableString(title);
                        sColored.setSpan(new BackgroundColorSpan(Color.GRAY), title.length() - (providerHistoryCount.length() + 2), title.length(), 0);
                        sColored.setSpan(new ForegroundColorSpan(Color.WHITE), title.length() - (providerHistoryCount.length() + 2), title.length(), 0);
                        drawerMenu.getItem(i).setTitle(sColored);
                    } else {
                        drawerMenu.getItem(i).setTitle(title);
                    }

//                    title = AppUtils.cleanLangStr(this, navData.getLg3_provider_histor(), R.string.txt_provider_history);
//                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 6:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_chat_history(), R.string.txt_req_chat_history);
                    if (!requester_chat_count.isEmpty() && !requester_chat_count.equalsIgnoreCase("0")) {
                        title = AppUtils.cleanLangStr(this, navData.getLg3_chat_history(), R.string.txt_req_chat_history) + "   " + requester_chat_count + " ";
                        SpannableString sColored = new SpannableString(title);
                        sColored.setSpan(new BackgroundColorSpan(Color.GRAY), title.length() - (requester_chat_count.length() + 2), title.length(), 0);
                        sColored.setSpan(new ForegroundColorSpan(Color.WHITE), title.length() - (requester_chat_count.length() + 2), title.length(), 0);
                        drawerMenu.getItem(i).setTitle(sColored);
                    } else {
                        drawerMenu.getItem(i).setTitle(title);
                    }
                    break;
                case 7:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_provider_chat_h(), R.string.txt_chat_reqhistory);
                    if (!provider_chat_count.isEmpty() && !provider_chat_count.equalsIgnoreCase("0")) {
                        title = AppUtils.cleanLangStr(this, navData.getLg3_provider_chat_h(), R.string.txt_chat_reqhistory) + "   " + provider_chat_count + " ";
                        SpannableString sColored1 = new SpannableString(title);
                        sColored1.setSpan(new BackgroundColorSpan(Color.GRAY), title.length() - (requester_chat_count.length() + 2), title.length(), 0);
                        sColored1.setSpan(new ForegroundColorSpan(Color.WHITE), title.length() - (requester_chat_count.length() + 2), title.length(), 0);
                        drawerMenu.getItem(i).setTitle(sColored1);
                    } else {
                        drawerMenu.getItem(i).setTitle(title);
                    }
                    break;
                case 8:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_settings(), R.string.txt_settings);
                    drawerMenu.getItem(i).setTitle(title);
                    break;
                case 9:
                    title = AppUtils.cleanLangStr(this, navData.getLg3_logout(), R.string.txt_logout);
                    String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);



                    if (userToken != null) {
                        drawerMenu.getItem(i).setTitle(title);
                    }
                    else {
                        drawerMenu.getItem(i).setTitle(getResources().getString(R.string.txt_login));

                    }
                    break;
            }

//            if (!requester_chat_count.equalsIgnoreCase("0") || !provider_chat_count.equalsIgnoreCase("0")) {
////                String s = title + "   " + requester_chat_count + " ";
//                SpannableString sColored = new SpannableString(title);
//                sColored.setSpan(new BackgroundColorSpan(Color.GRAY), title.length() - (requester_chat_count.length() + 2), title.length(), 0);
//                sColored.setSpan(new ForegroundColorSpan(Color.WHITE), title.length() - (requester_chat_count.length() + 2), title.length(), 0);
//                drawerMenu.getItem(i).setTitle(sColored);
//            } else {
//                drawerMenu.getItem(i).setTitle(title);
//            }


        }
    }

    private void updateUserInfoDetails() {
        try {
            tv_username.setText(PreferenceStorage.getKey(AppConstants.USER_NAME));
            tv_email.setText(PreferenceStorage.getKey(AppConstants.USER_EMAIL));
            if (PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG) != null || !PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG).isEmpty()) {
                Picasso.with(this)
                        .load(AppConstants.BASE_URL + PreferenceStorage.getKey(AppConstants.USER_PROFILE_IMG))
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(iv_profile_img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
     * so that, we can make other activity implementations looks like normal activity subclasses.
     */
    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    public void setCurrentActivity(Activity activity) {
        currentActivity = activity;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setToolBarTitle(String title) {
        mToolbar.setTitle(title);
        try {
            getSupportActionBar().setTitle(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Intent newIntent = null;
        String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);


        switch (item.getItemId()) {
            case R.id.nav_dashboard:
                newIntent = new Intent(getApplicationContext(), MainActivity.class);
                break;
            case R.id.nav_my_profile:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), MyProfileActivity.class);
                } else {
                    showDialog();
                }
                break;
            case R.id.nav_my_requests:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), MyRequestListActivity.class);
                } else {
                    showDialog();
                }
                break;
            case R.id.nav_my_providers:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), MyProviderListActivity.class);
                } else {
                    showDialog();
                }
                break;
            case R.id.nav_request_history:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), HistoryActivity.class);
                } else {
                    Toast.makeText(getApplicationContext(), "You have to log in", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.nav_provider_history:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), ProviderHistoryActivity.class);
                } else {
                    showDialog();
                }
                break;
            case R.id.nav_req_chat_history:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), ChatRequestorHistoryListActivity.class);
                } else {
                    showDialog();
                }
                break;
            case R.id.nav_chat_history:
                if (userToken != null) {
                    newIntent = new Intent(getApplicationContext(), ChatHistoryListActivity.class);
                } else {
                    showDialog();
                }
                break;
            case R.id.nav_settings:

                newIntent = new Intent(getApplicationContext(), SettingsActivity.class);

                break;
            case R.id.nav_logout:

                mDrawerLayout.closeDrawers();
                if (userToken != null) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(AppUtils.cleanLangStr(this, logoutData.getLg10_are_you_sure_yo(), R.string.txt_logout_confirm))
                            .setCancelable(false)
                            .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //PreferenceStorage.clearPref();
                                    dialog.dismiss();
                                    userLogout();
                                }
                            })
                            .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    showDialog();

                }
                break;
        }
        if (newIntent != null) {
            mDrawerLayout.closeDrawers();
            AppUtils.appStartIntent(this, newIntent);
        }
        return false;
    }

    public void userLogout() {
        if (AppUtils.isNetworkAvailable(this)) {

            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Call<LogoutData> classificationCall = apiService.postUserLogout(AppConstants.deviceType, PreferenceStorage.getKey(AppConstants.refreshedToken), PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                classificationCall.enqueue(new Callback<LogoutData>() {
                    @Override
                    public void onResponse(Call<LogoutData> call, Response<LogoutData> response) {
                        ProgressDlg.dismissProgressDialog();
                        if (response.isSuccessful()) {
                            if (response != null && response.body() != null
                                    && response.body().getResponse() != null
                                    && response.body().getResponse().getResponseCode().equalsIgnoreCase("1")) {
                                PreferenceStorage.removeKey(AppConstants.USER_TOKEN);
                                PreferenceStorage.removeKey(AppConstants.USER_NAME);
                                PreferenceStorage.removeKey(AppConstants.USER_SUBS_TYPE);
                                PreferenceStorage.removeKey(AppConstants.USER_EMAIL);
                                PreferenceStorage.removeKey(AppConstants.USER_PROFILE_IMG);
                                PreferenceStorage.removeKey(AppConstants.USER_PHONE);
                                PreferenceStorage.removeKey(AppConstants.MY_ADDRESS);
                                PreferenceStorage.removeKey(AppConstants.MY_LATITUDE);
                                PreferenceStorage.removeKey(AppConstants.MY_LONGITUDE);
                                //PreferenceStorage.clearPref();

                                Intent a = new Intent(getApplicationContext(), MyLoginActivity.class);
                                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                AppUtils.appStartIntent(getApplicationContext(), a);
                                if (currentActivity != null)
                                    currentActivity.finish();
                            } else {
                                //AppUtils.showToast(getCurrentActivity(), AppUtils.cleanLangStr(getCurrentActivity(), commonData.getLg7_no_data_were_fo(), R.string.txt_logout_fail));//TODO:
                            }
                        } else {
                            //AppUtils.showToast(getCurrentActivity(), AppUtils.cleanLangStr(getCurrentActivity(), commonData.getLg7_no_data_were_fo(), R.string.txt_logout_fail));//TODO:
                        }
                    }

                    @Override
                    public void onFailure(Call<LogoutData> call, Throwable t) {
                        ProgressDlg.dismissProgressDialog();
                        //AppUtils.showToast(getCurrentActivity(), AppUtils.cleanLangStr(getCurrentActivity(), commonData.getLg7_no_data_were_fo(), R.string.txt_logout_fail));//TODO:

                    }
                });
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        if (mLocation != null) {
            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG_BASE", "Base Resumed");
        getAppTheme();
        applyThemeColor();
        getLocaleData();
        if (!checkPlayServices()) {
            Toast.makeText(this, AppUtils.cleanLangStr(this, commonData.getLg7_please_install_(), R.string.err_txt_install_play_ser), Toast.LENGTH_SHORT).show();
        }
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
        }

        updateUserInfoDetails();
        setNavigationText("", "", "", "");
        new getChatHistoryCount().execute();
    }

    private void applyThemeColor() {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(appColor));
        navigation_view.getHeaderView(0).setBackgroundColor(appColor);
        for (int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i)
                    .getIcon()
                    .setColorFilter(appColor, PorterDuff.Mode.SRC_ATOP);
        }

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    public String getLatitude() {
        String latitude = Latitude;
        return latitude;
    }

    public String getLongitude() {
        String longitude = Longitude;
        return longitude;
    }

    public String getLocation() {
        String location = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try {
            if (mLocation != null) {
                addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1);
                if (addresses != null && addresses.size() > 0) {
                    location = addresses.get(0).getAddressLine(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mLocation != null) {
            Latitude = String.valueOf(mLocation.getLatitude());
            Longitude = String.valueOf(mLocation.getLongitude());
            if (PreferenceStorage.getKey(AppConstants.MY_LATITUDE) == null
                    && PreferenceStorage.getKey(AppConstants.MY_LONGITUDE) == null) {
                PreferenceStorage.setKey(AppConstants.MY_LATITUDE, Latitude);
                PreferenceStorage.setKey(AppConstants.MY_LONGITUDE, Longitude);
                PreferenceStorage.setKey(AppConstants.MY_ADDRESS, getLocation());
            }
        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_enable_permissi(), R.string.err_txt_enable_permission), Toast.LENGTH_LONG).show();

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);


    }


    public ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            mGoogleApiClient.disconnect();
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();
        }
        try {
            String navDataStr = PreferenceStorage.getKey(CommonLangModel.navigation);
            navData = new Gson().fromJson(navDataStr, LanguageModel.Navigation.class);
        } catch (Exception e) {
            navData = new LanguageModel().new Navigation();
        }

        try {
            String logoutDataStr = PreferenceStorage.getKey(CommonLangModel.logout);
            logoutData = new Gson().fromJson(logoutDataStr, LanguageModel.Logout.class);
        } catch (Exception e) {
            logoutData = new LanguageModel().new Logout();
        }
        try {
            String signUpDataStr = PreferenceStorage.getKey(CommonLangModel.sign_up);
            signUpData = new Gson().fromJson(signUpDataStr, LanguageModel.Sign_up.class);
        } catch (Exception e) {
            signUpData = new LanguageModel().new Sign_up();
        }
        try {
            String profileDataStr = PreferenceStorage.getKey(CommonLangModel.profile);
            profileData = new Gson().fromJson(profileDataStr, LanguageModel.Profile.class);
        } catch (Exception e) {
            profileData = new LanguageModel().new Profile();
        }


    }

    public int getAppTheme() {
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }
        return appColor;
    }


    public void getChatCount() {
        if (AppUtils.isNetworkAvailable(this)) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            apiService.getChatCount(PreferenceStorage.getKey(AppConstants.USER_TOKEN)).enqueue(new Callback<GETChatCount>() {
                @Override
                public void onResponse(Call<GETChatCount> call, Response<GETChatCount> response) {
                    setNavigationText(response.body().getData().getProvider_chat_count(), response.body().getData().getRequester_chat_count(), response.body().getData().getRequester_count(), response.body().getData().getProvider_count());
                }

                @Override
                public void onFailure(Call<GETChatCount> call, Throwable t) {

                }
            });
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, "", R.string.txt_enable_internet));
        }
    }


    private class getChatHistoryCount extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            getChatCount();
            return null;
        }
    }

//    private void showCustomLoginDialog() {
//        SweetAlertDialog sweetAlertDialog;
//        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
//        sweetAlertDialog.setTitleText(getResources().getString(R.string.txt_login));
//        sweetAlertDialog.setContentText(getResources().getString(R.string.log_in_first));
//        sweetAlertDialog.setConfirmText(getResources().getString(R.string.txt_login));
//        sweetAlertDialog.setCancelText(getResources().getString(R.string.cancel));
//        sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//            @Override
//            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                sweetAlertDialog.dismiss();
//            }
//        });
//        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//            @Override
//            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                Intent intent= new Intent(getApplicationContext(),MyLoginActivity.class);
//                startActivity(intent);
//            }
//        });
//        sweetAlertDialog.show();
//    }

    void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.log_in_first))
                .setCancelable(false)
                .setPositiveButton(AppUtils.cleanLangStr(this, commonData.getLg7_yes(), R.string.txt_login), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //PreferenceStorage.clearPref();
                        Intent intent = new Intent(getApplicationContext(), MyLoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(AppUtils.cleanLangStr(this, commonData.getLg7_no(), R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

}
