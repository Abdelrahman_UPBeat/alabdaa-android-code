package in.alibdaa.upbeat.digital.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HistoryListData extends BaseResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("next_page")
        @Expose
        private Integer nextPage;
        @SerializedName("current_page")
        @Expose
        private String currentPage;
        @SerializedName("total_pages")
        @Expose
        private Integer totalPages;
        @SerializedName("request_list")
        @Expose
        private ArrayList<RequestList> requestList = null;

        public Integer getNextPage() {
            return nextPage;
        }

        public void setNextPage(Integer nextPage) {
            this.nextPage = nextPage;
        }

        public String getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(String currentPage) {
            this.currentPage = currentPage;
        }

        public Integer getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        public ArrayList<RequestList> getRequestList() {
            return requestList;
        }

        public void setRequestList(ArrayList<RequestList> requestList) {
            this.requestList = requestList;
        }

    }

    public static class RequestList implements Parcelable {

        @SerializedName("r_id")
        @Expose
        private String rId;
        @SerializedName("requester_id")
        @Expose
        private String requesterId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("currency_code")
        @Expose
        private String currencyCode;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("request_date")
        @Expose
        private String requestDate;
        @SerializedName("request_time")
        @Expose
        private String requestTime;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("contact_number")
        @Expose
        private String contactNumber;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("acceptor_id")
        @Expose
        private String acceptorId;
        @SerializedName("requester_name")
        @Expose
        private String requesterName;
        @SerializedName("requester_email")
        @Expose
        private String requesterEmail;
        @SerializedName("request_image")
        @Expose
        private String requestImage;
        @SerializedName("requester_mobile")
        @Expose
        private String requesterMobile;
        @SerializedName("acceptor_name")
        @Expose
        private String acceptorName;
        @SerializedName("acceptor_email")
        @Expose
        private String acceptorEmail;
        @SerializedName("acceptor_image")
        @Expose
        private String acceptorImage;
        @SerializedName("acceptor_mobile")
        @Expose
        private String acceptorMobile;
        @SerializedName("status")
        @Expose
        private String status;

        public RequestList() {
            rId = null;
            requesterId = null;
            title = null;
            description = null;
            currencyCode = null;
            location = null;
            requestDate = null;
            requestTime = null;
            amount = null;
            contactNumber = null;
            latitude = null;
            longitude = null;
            acceptorId = null;
            requesterName = null;
            requesterEmail = null;
            requestImage = null;
            requesterMobile = null;
            acceptorName = null;
            acceptorEmail = null;
            acceptorImage = null;
            acceptorMobile = null;
            status = null;
        }

        protected RequestList(Parcel in) {
            rId = in.readString();
            requesterId = in.readString();
            title = in.readString();
            description = in.readString();
            currencyCode = in.readString();
            location = in.readString();
            requestDate = in.readString();
            requestTime = in.readString();
            amount = in.readString();
            contactNumber = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            acceptorId = in.readString();
            requesterName = in.readString();
            requesterEmail = in.readString();
            requestImage = in.readString();
            requesterMobile = in.readString();
            acceptorName = in.readString();
            acceptorEmail = in.readString();
            acceptorImage = in.readString();
            acceptorMobile = in.readString();
            status = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(rId);
            dest.writeString(requesterId);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeString(currencyCode);
            dest.writeString(location);
            dest.writeString(requestDate);
            dest.writeString(requestTime);
            dest.writeString(amount);
            dest.writeString(contactNumber);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(acceptorId);
            dest.writeString(requesterName);
            dest.writeString(requesterEmail);
            dest.writeString(requestImage);
            dest.writeString(requesterMobile);
            dest.writeString(acceptorName);
            dest.writeString(acceptorEmail);
            dest.writeString(acceptorImage);
            dest.writeString(acceptorMobile);
            dest.writeString(status);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RequestList> CREATOR = new Creator<RequestList>() {
            @Override
            public RequestList createFromParcel(Parcel in) {
                return new RequestList(in);
            }

            @Override
            public RequestList[] newArray(int size) {
                return new RequestList[size];
            }
        };

        public String getRId() {
            return rId;
        }

        public void setRId(String rId) {
            this.rId = rId;
        }

        public String getRequesterId() {
            return requesterId;
        }

        public void setRequesterId(String requesterId) {
            this.requesterId = requesterId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getRequestDate() {
            return requestDate;
        }

        public void setRequestDate(String requestDate) {
            this.requestDate = requestDate;
        }

        public String getRequestTime() {
            return requestTime;
        }

        public void setRequestTime(String requestTime) {
            this.requestTime = requestTime;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getAcceptorId() {
            return acceptorId;
        }

        public void setAcceptorId(String acceptorId) {
            this.acceptorId = acceptorId;
        }

        public String getRequesterName() {
            return requesterName;
        }

        public void setRequesterName(String requesterName) {
            this.requesterName = requesterName;
        }

        public String getRequesterEmail() {
            return requesterEmail;
        }

        public void setRequesterEmail(String requesterEmail) {
            this.requesterEmail = requesterEmail;
        }

        public String getRequestImage() {
            return requestImage;
        }

        public void setRequestImage(String requestImage) {
            this.requestImage = requestImage;
        }

        public String getRequesterMobile() {
            return requesterMobile;
        }

        public void setRequesterMobile(String requesterMobile) {
            this.requesterMobile = requesterMobile;
        }

        public String getAcceptorName() {
            return acceptorName;
        }

        public void setAcceptorName(String acceptorName) {
            this.acceptorName = acceptorName;
        }

        public String getAcceptorEmail() {
            return acceptorEmail;
        }

        public void setAcceptorEmail(String acceptorEmail) {
            this.acceptorEmail = acceptorEmail;
        }

        public String getAcceptorImage() {
            return acceptorImage;
        }

        public void setAcceptorImage(String acceptorImage) {
            this.acceptorImage = acceptorImage;
        }

        public String getAcceptorMobile() {
            return acceptorMobile;
        }

        public void setAcceptorMobile(String acceptorMobile) {
            this.acceptorMobile = acceptorMobile;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }
}
