package in.alibdaa.upbeat.digital.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.alibdaa.upbeat.digital.CreateProviderActivity;
import in.alibdaa.upbeat.digital.MainActivity;
import in.alibdaa.upbeat.digital.MyLoginActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.SubscriptionActivity;
import in.alibdaa.upbeat.digital.adapters.CategoryListAdapter;
import in.alibdaa.upbeat.digital.datamodel.CategoryList;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CategoryFragment extends Fragment implements RetrofitHandler.RetrofitResHandler {

    MainActivity mainActivity;
    Context mContext;
    LanguageModel.Common_used_texts commonUsedTexts = new LanguageModel().new Common_used_texts();
    CategoryListAdapter categoryListAdapter;
    GridLayoutManager gridLayoutManager;
    @BindView(R.id.rv_categorylist)
    RecyclerView rvCategorylist;
    Unbinder unbinder;

    @BindView(R.id.fab_provider)
    FloatingActionButton fabProvider;

    public void CategoryFragment(MainActivity mainActivity, Context mContext) {
        this.mainActivity = mainActivity;
        this.mContext = mContext;
        commonUsedTexts = mainActivity.commonData;


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_category, container, false);
        unbinder = ButterKnife.bind(this, mView);
        gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        rvCategorylist.setLayoutManager(gridLayoutManager);

        try {
            fabProvider.setBackgroundTintList(ColorStateList.valueOf(mainActivity.appColor));
        } catch (Exception e) {
        }

        if (AppUtils.isNetworkAvailable(getActivity())) {

//            ProgressDlg.showProgressDialog(getActivity(), null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Call<CategoryList> classificationCall = apiService.getCategories(PreferenceStorage.getKey(AppConstants.USER_TOKEN));
                RetrofitHandler.executeRetrofit(getActivity(), classificationCall, AppConstants.CATEGORIES, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(getApplicationContext(), getString(R.string.txt_enable_internet));
        }


        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {
        CategoryList response = (CategoryList) myRes;
        if (response.getData() != null && response.getData().getCategory_list() != null) {
            categoryListAdapter = new CategoryListAdapter(getActivity(),response.getData().getCategory_list());
            rvCategorylist.setAdapter(categoryListAdapter);
        }
    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseType) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseType) {

    }


    @OnClick(R.id.fab_provider)
    public void onViewClicked() {

        String userToken = PreferenceStorage.getKey(AppConstants.USER_TOKEN);

        if(userToken!=null) {
            if (PreferenceStorage.getIntKey(AppConstants.USER_SUBS_TYPE) > 0) {
                Intent in = new Intent(getActivity(), CreateProviderActivity.class);
                AppUtils.appStartIntent(getActivity(), in);
            } else {
                Intent myIntent = new Intent(getActivity(), SubscriptionActivity.class);
                myIntent.putExtra("FromPage", AppConstants.PAGE_CREATE_PROVIDER);
                AppUtils.appStartIntent(getActivity(), myIntent);
            }
        }else{

            showDialog();
        }
    }

    void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getResources().getString(R.string.log_in_first))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.txt_login), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //PreferenceStorage.clearPref();
                        Intent intent = new Intent(getContext(), MyLoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.txt_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
