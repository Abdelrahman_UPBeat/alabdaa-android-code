package in.alibdaa.upbeat.digital;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.LocaleUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class ForgotPasswordActivity extends AppCompatActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.tv_txt_email)
    TextView tvTxtEmail;
    @BindView(R.id.tiet_email)
    TextInputEditText tietEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    public LanguageModel.Common_used_texts commonData = new LanguageModel().new Common_used_texts();
    public LanguageModel.Sign_up signUpData = new LanguageModel().new Sign_up();
    public LanguageModel.Login loginData = new LanguageModel().new Login();
    public int appColor = 0;
    @BindView(R.id.tb_toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);


        tietEmail.addTextChangedListener(new RegisterTextWatcher(tietEmail));
    }

    private class RegisterTextWatcher implements TextWatcher {
        private View view;

        private RegisterTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (view.getId() == R.id.tiet_email) {
                if (!tietEmail.getText().toString().isEmpty()) {
                    validateEmail();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private boolean validateEmail() {
        if (tietEmail.getText().toString().isEmpty()) {
            tietEmail.setError(AppUtils.cleanLangStr(this, commonData.getLg7_enter_your_emai(), R.string.err_email));
            tietEmail.requestFocus();
            return false;
        } else if (!AppUtils.isValidEmail(tietEmail.getText().toString())) {
            tietEmail.setError(AppUtils.cleanLangStr(this, signUpData.getLg1_enter_valid_ema(), R.string.err_valid_email));
            tietEmail.requestFocus();
            return false;
        } else {
            tietEmail.setError(null);
        }
        return true;
    }


    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {

        if (myRes instanceof EmptyData) {
            EmptyData emptyData = (EmptyData) myRes;
            Toast.makeText(this, emptyData.getResponseHeader().getResponseMessage(), Toast.LENGTH_SHORT).show();
            ForgotPasswordActivity.this.finish();
        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes != null && myRes instanceof EmptyData) {
            EmptyData emptyData = (EmptyData) myRes;
            Toast.makeText(this, emptyData.getResponseHeader().getResponseMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        if (myRes != null && myRes instanceof EmptyData) {
            EmptyData emptyData = (EmptyData) myRes;
            Toast.makeText(this, emptyData.getResponseHeader().getResponseMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        updatePassword();
    }

    private void updatePassword() {
        if (!validateEmail()) {
            return;
        }

        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClientNoHeader().create(ApiInterface.class);
            try {
                Call<EmptyData> classificationCall = apiService.postForgotPassword(tietEmail.getText().toString(), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
                RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.FORGOT_PASSWORD, this, false);
            } catch (Exception e) {
                ProgressDlg.dismissProgressDialog();
                e.printStackTrace();
            }

        } else {
            AppUtils.showToast(ForgotPasswordActivity.this, AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceStorage.getKey(AppConstants.MY_LANGUAGE).equalsIgnoreCase("ar")) {
            LocaleUtils.setLocale(new Locale("ar"));
        }
        try {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getLocaleData();
            getAppTheme();
            mToolbar.setBackgroundColor(appColor);
            mToolbar.setTitle(AppUtils.cleanLangStr(this, loginData.getLg2_forgot_password(), R.string.txt_forgot_pwd));
            btnSubmit.setBackgroundColor(appColor);
            tvTxtEmail.setText(AppUtils.cleanLangStr(this, signUpData.getLg1_email(), R.string.hint_email));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            String signUpDataStr = PreferenceStorage.getKey(CommonLangModel.sign_up);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
            signUpData = new Gson().fromJson(commonDataStr, LanguageModel.Sign_up.class);
            loginData = new Gson().fromJson(commonDataStr, LanguageModel.Login.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();
        }
    }

    public int getAppTheme() {
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }
        return appColor;
    }

}
