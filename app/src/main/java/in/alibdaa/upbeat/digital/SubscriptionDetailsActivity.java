package in.alibdaa.upbeat.digital;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionImageUploadModel;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import in.alibdaa.upbeat.digital.viewwidgets.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by Hari on 16-05-2018.
 */

public class SubscriptionDetailsActivity extends AppCompatActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.tb_toolbar)
    Toolbar tbToolbar;
    @BindView(R.id.iv_pro_img)
    CircleImageView ivProImg;
    @BindView(R.id.iv_card_img)
    ImageView ivCardImg;
    @BindView(R.id.bt_continue)
    Button btContinue;
    @BindView(R.id.tv_upload_your_picture)
    TextView tvUploadYourPicture;
    @BindView(R.id.tv_your_picture)
    TextView tvYourPicture;
    @BindView(R.id.tv_card_upload)
    TextView tvCardUpload;
    @BindView(R.id.tv_card_id)
    TextView tvCardId;
    @BindView(R.id.tv_take_upload)
    TextView tvTakeUpload;
    @BindView(R.id.tv_upload_id)
    TextView tvUploadId;
    private BottomSheetDialog attachChooser;
    private static final int RC_LOAD_IMG_CAMERA = 101;
    private static final int RC_LOAD_IMG_BROWSER = 102;
    MultipartBody.Part icCardImg;
    MultipartBody.Part icProImg;
    private String type = "";
    List<MultipartBody.Part> imagePartList = new ArrayList<>();
    public LanguageModel.Common_used_texts common_used_texts = new LanguageModel().new Common_used_texts();
    public LanguageModel.Sign_up sign_up_text = new LanguageModel().new Sign_up();
    public LanguageModel.Subscription subscription_used_texts = new LanguageModel().new Subscription();
    public int appColor = 0;

    String fromPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_details);
        ButterKnife.bind(this);
        fromPage = getIntent().getStringExtra("FromPage");
    }

    @OnClick({R.id.iv_pro_img, R.id.iv_card_img})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_pro_img:
                type = "0";
                selectImage();
                break;
            case R.id.iv_card_img:
                type = "1";
                selectImage();
                break;
        }
    }

    private void selectImage() {
        attachChooser = new BottomSheetDialog(SubscriptionDetailsActivity.this);
        attachChooser.setContentView((SubscriptionDetailsActivity.this).getLayoutInflater().inflate(R.layout.popup_add_attach_options,
                new LinearLayout(SubscriptionDetailsActivity.this)));
        attachChooser.show();
        //TODO: lang
        LinearLayout btnStartCamera = (LinearLayout) attachChooser.findViewById(R.id.btn_from_camera);
        LinearLayout btnStartFileBrowser = (LinearLayout) attachChooser.findViewById(R.id.btn_from_local);
        btnStartCamera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attachChooser.dismiss();
                if (AppUtils.checkPermission(SubscriptionDetailsActivity.this))
                    cameraIntent();
            }
        });
        btnStartFileBrowser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attachChooser.dismiss();
                if (AppUtils.checkPermission(SubscriptionDetailsActivity.this))
                    galleryIntent();
            }
        });
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RC_LOAD_IMG_CAMERA);
    }


    public void postSubscriptionDetails(boolean isLoading) {
        if (AppUtils.isNetworkAvailable(SubscriptionDetailsActivity.this)) {
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SubscriptionImageUploadModel> callPostSubscriptionDetails = apiInterface.postSubscriptionDetails(PreferenceStorage.getKey(AppConstants.USER_TOKEN), imagePartList, PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(SubscriptionDetailsActivity.this, callPostSubscriptionDetails, AppConstants.SUBSCRIPTIONIMAGELIST, this, isLoading);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, common_used_texts.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(intent, RC_LOAD_IMG_BROWSER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_LOAD_IMG_BROWSER) {
            onSelectFromGalleryResult(data);
        } else if (requestCode == RC_LOAD_IMG_CAMERA) {
            onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        // Bitmap thumbnail = null;
        Bitmap scaled = null;
        try {
            if (data != null && data.getData() != null) {
                String fileP = AppUtils.resolveFileUri(SubscriptionDetailsActivity.this, data.getData());
                Bitmap d = new BitmapDrawable(getResources(), new File(fileP).getAbsolutePath()).getBitmap();
                int nh = (int) (160 * (512.0 / 160));
                scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
            }
            if (scaled != null) {
                //scaled = thumbnail;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                scaled.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                Uri selectedImage = data.getData();
                String encodeImage = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());
                if (type.equalsIgnoreCase("0")) {
                    icProImg = MultipartBody.Part.createFormData("profile_img", "proImage.jpg", requestFile);
                    imagePartList.add(icProImg);
                    ivProImg.setImageBitmap(scaled);
                } else if (type.equalsIgnoreCase("1")) {
                    icCardImg = MultipartBody.Part.createFormData("ic_card_image", "cardImage.jpg", requestFile);
                    imagePartList.add(icCardImg);
                    ivCardImg.setImageBitmap(scaled);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            if (data.getExtras().get("data") != null) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                if (thumbnail != null) {
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    String encodeImage = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                }
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), bytes.toByteArray());
                if (type.equalsIgnoreCase("0")) {
                    icProImg = MultipartBody.Part.createFormData("profile_img", "proImage.jpg", requestFile);
                    imagePartList.add(icProImg);
                    ivProImg.setImageBitmap(thumbnail);
                } else if (type.equalsIgnoreCase("1")) {
                    icCardImg = MultipartBody.Part.createFormData("ic_card_image", "cardImage.jpg", requestFile);
                    imagePartList.add(icCardImg);
                    ivCardImg.setImageBitmap(thumbnail);
                }
            }
        } catch (Exception e) {

        }


    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseType) {

        if (myRes instanceof SubscriptionImageUploadModel) {
            SubscriptionImageUploadModel emptyData = (SubscriptionImageUploadModel) myRes;
            AppUtils.showToast(SubscriptionDetailsActivity.this, emptyData.getResponseHeader().getResponseMessage());
            try {
                PreferenceStorage.setKey(AppConstants.USER_PROFILE_IMG, emptyData.getData().getProfileImg());
                Intent callMainActivity = null;
                switch (fromPage) {
                    case AppConstants.PAGE_LOGIN:
                        callMainActivity = new Intent(SubscriptionDetailsActivity.this, MainActivity.class);
                        break;
                    case AppConstants.PAGE_CREATE_PROVIDER:
                        callMainActivity = new Intent(SubscriptionDetailsActivity.this, CreateProviderActivity.class);
                        break;
                    case AppConstants.PAGE_CHAT:
                        callMainActivity = new Intent(SubscriptionDetailsActivity.this, MainActivity.class);
                        //TODO: chat page
                    /*callMainActivity.putExtra(AppConstants.chatFrom, itemsData.get(position).getProviderId());
                    callMainActivity.putExtra(AppConstants.chatUsername, itemsData.get(position).getUsername());
                    callMainActivity.putExtra(AppConstants.chatImg, itemsData.get(position).getProfileImg());*/
                        break;
                    case AppConstants.PAGE_MY_PROFILE:
                        callMainActivity = new Intent(SubscriptionDetailsActivity.this, MyProfileActivity.class);
                        break;
                }

                AppUtils.appStartIntent(SubscriptionDetailsActivity.this, callMainActivity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            SubscriptionDetailsActivity.this.finish();
        }


    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        Intent callMainActivity = new Intent(SubscriptionDetailsActivity.this, MainActivity.class);
        AppUtils.appStartIntent(SubscriptionDetailsActivity.this, callMainActivity);
        SubscriptionDetailsActivity.this.finish();

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
    }

    @OnClick(R.id.bt_continue)
    public void onViewClicked() {
        if (icCardImg != null) {
            postSubscriptionDetails(false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, sign_up_text.getLg1_upload_ic_card(), R.string.err_card_img));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getLocaleData();
        setAppTheme();
        setSupportActionBar(tbToolbar);
        tbToolbar.setTitle(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_payment_process(), R.string.hint_subscription));
        tbToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tbToolbar.setBackgroundColor(appColor);
        tvCardId.setText(AppUtils.cleanLangStr(this, "", R.string.take_picture));
        tvCardUpload.setText(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_takeupload(), R.string.take_upload));
        tvTakeUpload.setText(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_takeupload(), R.string.take_upload));
        tvUploadId.setText(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_upload_your_id(), R.string.upload_your_ID));
        tvUploadYourPicture.setText(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_upload_your_pic(), R.string.upload_your_picture));
        tvYourPicture.setText(AppUtils.cleanLangStr(this, "", R.string.id_card_take_picture));
        btContinue.setText(AppUtils.cleanLangStr(this, subscription_used_texts.getLg9_continue(), R.string.txt_continue));

        btContinue.setBackgroundColor(appColor);
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            String signUpdataStr = PreferenceStorage.getKey(CommonLangModel.sign_up);
            common_used_texts = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
            sign_up_text = new Gson().fromJson(signUpdataStr, LanguageModel.Sign_up.class);
            String subDataStr = PreferenceStorage.getKey(CommonLangModel.subscription);
            subscription_used_texts = new Gson().fromJson(subDataStr, LanguageModel.Subscription.class);
        } catch (Exception e) {
            subscription_used_texts = new LanguageModel().new Subscription();
            common_used_texts = new LanguageModel().new Common_used_texts();
            sign_up_text = new LanguageModel().new Sign_up();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setAppTheme() {
        appColor = 0;
        try {
            String themeColor = PreferenceStorage.getKey(AppConstants.APP_THEME);
            appColor = Color.parseColor(themeColor);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(appColor);
            }
        } catch (Exception e) {
            appColor = getResources().getColor(R.color.colorPrimary);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
