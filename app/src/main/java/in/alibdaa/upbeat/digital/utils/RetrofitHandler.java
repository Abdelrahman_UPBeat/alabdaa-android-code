package in.alibdaa.upbeat.digital.utils;

import android.content.Context;

import org.apache.http.HttpException;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.datamodel.CategoryList;
import in.alibdaa.upbeat.digital.datamodel.ChatDetailListData;
import in.alibdaa.upbeat.digital.datamodel.ChatHistoryListData;
import in.alibdaa.upbeat.digital.datamodel.ChatSendMessageModel;
import in.alibdaa.upbeat.digital.datamodel.ColorSettingModel;
import in.alibdaa.upbeat.digital.datamodel.EmptyData;
import in.alibdaa.upbeat.digital.datamodel.HistoryListData;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.LoginData;
import in.alibdaa.upbeat.digital.datamodel.ProfileData;
import in.alibdaa.upbeat.digital.datamodel.ProfileUpdateModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderAvailableTimings;
import in.alibdaa.upbeat.digital.datamodel.ProviderHistoryModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.datamodel.RequestListData;
import in.alibdaa.upbeat.digital.datamodel.ResponseHeader;
import in.alibdaa.upbeat.digital.datamodel.StripeDetailsModel;
import in.alibdaa.upbeat.digital.datamodel.SubCategoryList;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionData;
import in.alibdaa.upbeat.digital.datamodel.SubscriptionSuccessModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitHandler {

    private static ResponseHeader mBaseResponse = null;

    public static <T> void executeRetrofit(final Context mContext, Call<T> call, final String responeModel, final RetrofitResHandler retrofitResHandler, final boolean isLoadMore) {

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                ProgressDlg.dismissProgressDialog();
                switch (responeModel) {
                    case AppConstants.LOGIN_DATA:
                        try {
                            LoginData myRes = (LoginData) response.body();
                            mBaseResponse = myRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.SUBSCRIPTION_DATA:
                        try {
                            SubscriptionData subsRes = (SubscriptionData) response.body();
                            mBaseResponse = subsRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.REQUESTLIST_DATA:
                        try {
                            RequestListData reqRes = (RequestListData) response.body();
                            mBaseResponse = reqRes.getResponseHeader();
                        } catch (Exception e) {

                        }
                        break;
                    case AppConstants.PROVIDERLIST_DATA:
                        try {
                            ProviderListData proRes = (ProviderListData) response.body();
                            mBaseResponse = proRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.PROFILE_DATA:
                        try {
                            ProfileData profRes = (ProfileData) response.body();
                            mBaseResponse = profRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.PROFILE_UPDATE_DATA:
                        try {
                            ProfileUpdateModel profRes = (ProfileUpdateModel) response.body();
                            mBaseResponse = profRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.HISTORY_DATA:
                        try {
                            HistoryListData histRes = (HistoryListData) response.body();
                            mBaseResponse = histRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.LANGUAGE_DATA:
                        try {
                            LanguageModel langRes = (LanguageModel) response.body();
                            mBaseResponse = langRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.COLORSETTING_DATA:
                        try {
                            ColorSettingModel colorRes = (ColorSettingModel) response.body();
                            mBaseResponse = colorRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.CHAT_HISTORYLIST_DATA:
                        try {
                            ChatHistoryListData chathistRes = (ChatHistoryListData) response.body();
                            mBaseResponse = chathistRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.CHAT_DETAILLIST_DATA:
                        try {
                            ChatDetailListData chathistRes = (ChatDetailListData) response.body();
                            mBaseResponse = chathistRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;

                    case AppConstants.CHAT_SENDDETAILLIST_DATA:
                        try {
                            ChatSendMessageModel chathistRes = (ChatSendMessageModel) response.body();
                            mBaseResponse = chathistRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.SUBSCRIPTIONSUCCESS:
                        try {
                            SubscriptionSuccessModel subscriptionSuccessModel = (SubscriptionSuccessModel) response.body();
                            mBaseResponse = subscriptionSuccessModel.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.STRIPEDETAILS:
                        try {
                            StripeDetailsModel stripeDetailsModel = (StripeDetailsModel) response.body();
                            mBaseResponse = stripeDetailsModel.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;

                    case AppConstants.CATEGORIES:
                        try {
                            CategoryList categoryListModel = (CategoryList) response.body();
                            mBaseResponse = categoryListModel.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.SUBCATEGORIES:
                        try {
                            SubCategoryList subCategoryList = (SubCategoryList) response.body();
                            mBaseResponse = subCategoryList.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;

                    case AppConstants.PROVIDERAVAIL:
                        try {
                            ProviderAvailableTimings subCategoryList = (ProviderAvailableTimings) response.body();
                            mBaseResponse = subCategoryList.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                    case AppConstants.MYPROVIDERHISTORY:
                        try {
                            ProviderHistoryModel subCategoryList = (ProviderHistoryModel) response.body();
                            mBaseResponse = subCategoryList.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;

                    case AppConstants.MYBOOKINGS:
                        try {
                            ProviderHistoryModel subCategoryList = (ProviderHistoryModel) response.body();
                            mBaseResponse = subCategoryList.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
//                    case AppConstants.CHATCOUNT:
//                        try {
//                            GETChatCount chatCount = (GETChatCount) response.body();
//                            mBaseResponse = chatCount.getResponseHeader();
//                        } catch (Exception e) {
//                        }
//                        break;

                    case AppConstants.SIGNUP_DATA:
                    case AppConstants.REQUEST_ACCEPT_DATA:
                    case AppConstants.CREATE_REQUEST_DATA:
                    case AppConstants.UPDATE_REQUEST_DATA:
                    case AppConstants.DELETE_REQUEST_DATA:
                    case AppConstants.CREATE_PROVIDER_DATA:
                    case AppConstants.UPDATE_PROVIDER_DATA:
                    case AppConstants.DELETE_PROVIDER_DATA:
                    case AppConstants.REQUEST_COMPLETE_DATA:
                    case AppConstants.CHANGEPASSWORD:
                    case AppConstants.FORGOT_PASSWORD:
                    case AppConstants.SUBS_SUCCESS_DATA:
                    case AppConstants.VIEWS:

                        try {
                            EmptyData reqAccRes = (EmptyData) response.body();
                            mBaseResponse = reqAccRes.getResponseHeader();
                        } catch (Exception e) {
                        }
                        break;
                }

                if (response.isSuccessful()) {
                    if (mBaseResponse != null && mBaseResponse.getResponseCode() != null
                            && mBaseResponse.getResponseCode().equalsIgnoreCase("1")) {
                        retrofitResHandler.onSuccess(response.body(), isLoadMore, responeModel);
                    } else {
                        AppUtils.showToast(mContext, mBaseResponse.getResponseMessage());
                        retrofitResHandler.onResponseFailure(response.body(), isLoadMore, responeModel);
                    }
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                ProgressDlg.dismissProgressDialog();
                if (t instanceof UnknownHostException || t instanceof HttpException || t instanceof ConnectException || t instanceof SocketException || t instanceof SocketTimeoutException)
                    AppUtils.showToast(mContext, mContext.getString(R.string.txt_server_error));
                retrofitResHandler.onRequestFailure(null, isLoadMore, responeModel);
            }
        });
    }

    public interface RetrofitResHandler<T> {

        void onSuccess(T myRes, boolean isLoadMore, String responseType);

        //Always check myRes for null. OnFailure the myRes will be null.
        void onResponseFailure(T myRes, boolean isLoadMore, String responseType);

        void onRequestFailure(T myRes, boolean isLoadMore, String responseType);
    }
}
