package in.alibdaa.upbeat.digital.interfaces;

/**
 * Created by Hari on 14-05-2018.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
