package in.alibdaa.upbeat.digital;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import in.alibdaa.upbeat.digital.adapters.ChatRequestorHistoryAdapter;
import in.alibdaa.upbeat.digital.datamodel.ChatHistoryListData;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * Created by Hari on 12-05-2018.
 */

public class ChatRequestorHistoryListActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler {


    @BindView(R.id.rv_message_chat_list)
    RecyclerView rvMessageChatList;
    @BindView(R.id.tv_no_chat_available)
    TextView tvNoChatAvailable;
    int currentPageNo = 1, nextPageNo = -1;
    ChatHistoryListData chatHistoryListData;
    ChatRequestorHistoryAdapter mAdapter;
    LinearLayoutManager mLayoutManager;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    //private OnLoadMoreListener mOnLoadMoreListener;
    public LanguageModel.Common_used_texts common_used_texts = new LanguageModel().new Common_used_texts();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentActivity(this);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        mLayoutManager = new LinearLayoutManager(this);
        rvMessageChatList.setLayoutManager(mLayoutManager);

        if (rvMessageChatList.getAdapter() == null) {
            mAdapter = new ChatRequestorHistoryAdapter(this, new ArrayList<ChatHistoryListData.ChatList>());
            rvMessageChatList.setAdapter(mAdapter);
        }


        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");

                isLoading = true;
                mAdapter.itemsData.add(null);
                //mUsers.add(null);
                mAdapter.notifyItemInserted(mAdapter.itemsData.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("haint", "Load More 2");
                        getChatHistoryList(true);
                    }
                }, 1000);

            }
        });




        rvMessageChatList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && (nextPageNo > currentPageNo) && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    isLoading = true;
                    if (mAdapter.mOnLoadMoreListener != null) {
                        mAdapter.mOnLoadMoreListener.onLoadMore();
                    }
                }
            }
        });

    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.subscription);
            common_used_texts = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            common_used_texts = new LanguageModel().new Common_used_texts();
        }
    }

    public void getChatHistoryList(boolean isLoadMore) {
        if (AppUtils.isNetworkAvailable(this)) {
            if (!isLoadMore) {
                ProgressDlg.clearDialog();
                ProgressDlg.showProgressDialog(this, null, null);
                nextPageNo = 1;
            }
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatHistoryListData> chatHistoryListDataCall = apiService.postChatRequestorHistoryList(nextPageNo, PreferenceStorage.getKey(AppConstants.USER_TOKEN), PreferenceStorage.getKey(AppConstants.MY_LANGUAGE));
            RetrofitHandler.executeRetrofit(this, chatHistoryListDataCall, AppConstants.CHAT_HISTORYLIST_DATA, this, isLoadMore);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }


    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        if (isLoadMore) {
            //Remove loading item
            mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
            mAdapter.notifyItemRemoved(mAdapter.itemsData.size());
        }
        if (myRes instanceof ChatHistoryListData) {
            chatHistoryListData = (ChatHistoryListData) myRes;
            isLoading = false;
            if (chatHistoryListData != null && chatHistoryListData.getData() != null && chatHistoryListData.getData().getChatList().size() > 0) {
                currentPageNo = Integer.parseInt(chatHistoryListData.getData().getCurrentPage());
                nextPageNo = chatHistoryListData.getData().getNextPage();

                if (!isLoadMore || currentPageNo == 1) {
                    mAdapter.itemsData = new ArrayList<>();
                    rvMessageChatList.setVisibility(View.VISIBLE);
                    tvNoChatAvailable.setVisibility(View.GONE);
                }
                //Load data
                mAdapter.updateRecyclerView(this, chatHistoryListData.getData().getChatList());

                //
            } else if (isLoadMore && mAdapter.itemsData.size() > 0) {
                rvMessageChatList.setVisibility(View.VISIBLE);
                tvNoChatAvailable.setVisibility(View.GONE);
            } else {
                rvMessageChatList.setVisibility(View.GONE);
                tvNoChatAvailable.setVisibility(View.VISIBLE);
                tvNoChatAvailable.setText(AppUtils.cleanLangStr(ChatRequestorHistoryListActivity.this, commonData.getLg7_no_details_were(), R.string.txt_no_details));
            }
        }

    }

    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {
        //Remove loading item
        failureHandling(isLoadMore, responseModel);
    }

    private void failureHandling(boolean isLoadMore, String responseModel) {
        isLoading = false;
        switch (responseModel) {
            case AppConstants.CHAT_HISTORYLIST_DATA:
                if (isLoadMore && mAdapter.itemsData != null && mAdapter.itemsData.size() > 1) {
                    mAdapter.itemsData.remove(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyItemRemoved(mAdapter.itemsData.size() - 1);
                    mAdapter.notifyDataSetChanged();
                } else {
                    rvMessageChatList.setVisibility(View.GONE);
                    tvNoChatAvailable.setVisibility(View.VISIBLE);
                    tvNoChatAvailable.setText(AppUtils.cleanLangStr(ChatRequestorHistoryListActivity.this, commonData.getLg7_no_details_were(), R.string.txt_no_details));
                }
                break;
        }
    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {
        failureHandling(isLoadMore, responseModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_chat_history(), R.string.txt_chat_reqhistory));
        getChatHistoryList(false);
    }

}
