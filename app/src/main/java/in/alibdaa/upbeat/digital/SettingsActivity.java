package in.alibdaa.upbeat.digital;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.alibdaa.upbeat.digital.datamodel.CommonLangModel;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.interfaces.OnSetMyLocation;
import in.alibdaa.upbeat.digital.network.ApiClient;
import in.alibdaa.upbeat.digital.network.ApiInterface;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.utils.AppUtils;
import in.alibdaa.upbeat.digital.utils.LocaleUtils;
import in.alibdaa.upbeat.digital.utils.PreferenceStorage;
import in.alibdaa.upbeat.digital.utils.ProgressDlg;
import in.alibdaa.upbeat.digital.utils.RetrofitHandler;
import retrofit2.Call;

public class SettingsActivity extends BaseNavigationActivity implements RetrofitHandler.RetrofitResHandler, OnSetMyLocation {

    @BindView(R.id.tv_txt_change_loc)
    TextView tvTxtChangeLoc;
    @BindView(R.id.et_change_loc)
    EditText etChangeLoc;
    @BindView(R.id.tv_txt_change_lang)
    TextView tvTxtChangeLang;
    @BindView(R.id.et_change_lang)
    EditText etChangeLang;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    String latitude, longitude, address, language;

    public static Context mContext;

    String[] items;

    LanguageAdapter adapter;

    LanguageModel.Common_used_texts commonData;
    @BindView(R.id.sp_change_lang)
    Spinner spChangeLang;
    @BindView(R.id.tv_contact)
    TextView tvContact;
    @BindView(R.id.tv_terms)
    TextView tvTerms;
    @BindView(R.id.tv_shareapp)
    TextView tvShareapp;
    @BindView(R.id.tv_rateus)
    TextView tvRateus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAG_SETTINGS", "Settings create");
        setCurrentActivity(this);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        mContext = this;
        getLocaleData();
        items = new String[3];
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_settings(), R.string.txt_settings));
        items[0] = AppUtils.cleanLangStr(this, "", R.string.txt_english);
        items[1] = AppUtils.cleanLangStr(this, "", R.string.txt_malay);
        items[2] = AppUtils.cleanLangStr(this, "", R.string.txt_arabic);

        language = PreferenceStorage.getKey(AppConstants.MY_LANGUAGE);
        if (language != null && !language.isEmpty() && language.equalsIgnoreCase("ar")) {
            etChangeLang.setText(AppUtils.cleanLangStr(this, commonData.getLg7_arabic(), R.string.txt_arabic));
            etChangeLang.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arabic_48, 0, 0, 0);
        } else if (language != null && !language.isEmpty() && language.equalsIgnoreCase("ma")) {
            etChangeLang.setText(AppUtils.cleanLangStr(this, commonData.getLg7_malay(), R.string.txt_malay));
            etChangeLang.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lang_malay, 0, 0, 0);
        } else {
            etChangeLang.setText(AppUtils.cleanLangStr(this, commonData.getLg7_english(), R.string.txt_english));
            etChangeLang.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lang_english, 0, 0, 0);
        }

        latitude = PreferenceStorage.getKey(AppConstants.MY_LATITUDE);
        longitude = PreferenceStorage.getKey(AppConstants.MY_LONGITUDE);
        address = PreferenceStorage.getKey(AppConstants.MY_ADDRESS);
        if (latitude != null && !latitude.isEmpty() && longitude != null && !longitude.isEmpty()) {
            etChangeLoc.setText(PreferenceStorage.getKey(AppConstants.MY_ADDRESS));
        } else {
            etChangeLoc.setText(getLocation());
        }

//        spChangeLang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String lang = "en";
//                switch (position) {
//                    case 0:
//                        lang = "en";
//                        break;
//                    case 1:
//                        lang = "ar";
//                        break;
//                }
//                language = lang;
//                getLocaleData(lang);
//            }
//        });

        spChangeLang.setAdapter(new LanguageAdapter(SettingsActivity.this, R.layout.list_item_language, items));

        spChangeLang.setTag(R.string.tag_check);
        try {
            if (language.equalsIgnoreCase("en")) {
                spChangeLang.setSelection(0, false);
            } else if (language.equalsIgnoreCase("ma")) {
                spChangeLang.setSelection(1, false);
            } else
                spChangeLang.setSelection(2, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        spChangeLang.setTag(null);

        spChangeLang.setPrompt(AppUtils.cleanLangStr(this, commonData.getLg7_choose_language(), R.string.txt_choose_lang));

        spChangeLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spChangeLang.getTag() == null) {
                    String lang = "en";
                    switch (position) {
                        case 0:
                            lang = "en";
                            break;
                        case 1:
                            lang = "ma";
                            break;
                        case 2:
                            lang = "ar";
                            break;
                    }
                    language = lang;
                    getLocaleData(lang);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvTxtChangeLang.setText(AppUtils.cleanLangStr(this, commonData.getLg7_change_language(), R.string.txt_change_lang));
        tvTxtChangeLoc.setText(AppUtils.cleanLangStr(this, commonData.getLg7_change_location(), R.string.txt_change_loc));
        tvContact.setText(AppUtils.cleanLangStr(this, commonData.getLg7_contact_us(), R.string.contact_us));
        tvRateus.setText(AppUtils.cleanLangStr(this, commonData.getLg7_rate_us(), R.string.rate_us));
        tvShareapp.setText(AppUtils.cleanLangStr(this, commonData.getLg7_share_app(), R.string.share_app));
        tvTerms.setText(AppUtils.cleanLangStr(this, commonData.getLg7_terms_and_condi(), R.string.terms_and_conditions));
        setToolBarTitle(AppUtils.cleanLangStr(this, navData.getLg3_settings(), R.string.txt_settings));
        Log.d("TAG_SETTINGS", "Settings resumed");

    }

    @Override
    public void onLocationSet(String latitude, String longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        PreferenceStorage.setKey(AppConstants.MY_LATITUDE, latitude);
        PreferenceStorage.setKey(AppConstants.MY_LONGITUDE, longitude);
        PreferenceStorage.setKey(AppConstants.MY_ADDRESS, address);
        etChangeLoc.setText(address);
    }

    @Override
    public void onSuccess(Object myRes, boolean isLoadMore, String responseModel) {
        switch (responseModel) {
            case AppConstants.LANGUAGE_DATA:
                setLocale(language);
                PreferenceStorage.setKey(AppConstants.LANGUAGE_SET, true);
                PreferenceStorage.setKey(AppConstants.MY_LANGUAGE, language);
                AppUtils.setLangInPref((LanguageModel) myRes);
                SettingsActivity.this.recreate();
                break;
        }
    }


    @Override
    public void onResponseFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onRequestFailure(Object myRes, boolean isLoadMore, String responseModel) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent a = new Intent(getApplicationContext(), MainActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        AppUtils.appStartIntent(getApplicationContext(), a);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.et_change_loc, R.id.et_change_lang, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_change_loc:
                Intent callMapActivity = new Intent(SettingsActivity.this, MapActivity.class);
                callMapActivity.putExtra("From", AppConstants.PAGE_SETTINGS);
                callMapActivity.putExtra("Latitude", latitude);
                callMapActivity.putExtra("Longitude", longitude);
                callMapActivity.putExtra("Address", address);
                AppUtils.appStartIntent(SettingsActivity.this, callMapActivity);
                break;
            case R.id.et_change_lang:
                break;
            case R.id.btn_submit:
                break;
        }
    }

    /*private synchronized void checkForLocale() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setCancelable(true);
        TextView title = new TextView(this);
        title.setText(AppUtils.cleanLangStr(this, commonData.getLg7_choose_language(), R.string.txt_choose_lang));//TODO: Lang
        title.setBackgroundColor(Color.WHITE);
        title.setPadding(15, 15, 15, 15);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(20);

        //adapter = new LanguageAdapter(SettingsActivity.this, )
        builder.setCustomTitle(title);
        builder.setAdapter(new LanguageAdapter(SettingsActivity.this, R.layout.list_item_language),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int item) {
                        String lang = "en";
                        switch (item) {
                            case 0:
                                lang = "en";
                                break;
                            case 1:
                                lang = "ma";
                                break;
                        }
                        language = lang;
                        dialog.dismiss();
                        getLocaleData(lang);
                    }
                });
        builder.show();
    }*/


    public synchronized void getLocaleData(String langCode) {
        if (AppUtils.isNetworkAvailable(this)) {
            ProgressDlg.clearDialog();
            ProgressDlg.showProgressDialog(this, null, null);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<LanguageModel> classificationCall = apiService.getLanguageData(langCode);
            RetrofitHandler.executeRetrofit(this, classificationCall, AppConstants.LANGUAGE_DATA, this, false);
        } else {
            AppUtils.showToast(getApplicationContext(), AppUtils.cleanLangStr(this, commonData.getLg7_please_enable_i(), R.string.txt_enable_internet));
        }
    }

    private void getLocaleData() {
        try {
            String commonDataStr = PreferenceStorage.getKey(CommonLangModel.common_used_texts);
            commonData = new Gson().fromJson(commonDataStr, LanguageModel.Common_used_texts.class);
        } catch (Exception e) {
            commonData = new LanguageModel().new Common_used_texts();
        }
    }

    public void onRateUsClicked(View view) {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void onShareAppClicked(View view) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "Share Via"));
        } catch (Exception e) {
            e.printStackTrace();
            //e.toString();
        }
    }

    public void onContactUsClicked(View view) {
//        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
//                "mailto", "aslughayer11@gmail.com", null));
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
//        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
//        startActivity(Intent.createChooser(emailIntent, "Contact Us"));
        Intent i = new Intent(this, WebViewActivity.class);
        i.putExtra(AppConstants.TbTitle, "Contact us");
        i.putExtra(AppConstants.URL, AppConstants.ContactUsURL);
        startActivity(i);
    }

    public void onTermsandConditionsClicked(View view) {

        Intent i = new Intent(this, WebViewActivity.class);
        i.putExtra(AppConstants.TbTitle, "Terms and Conditions");
        i.putExtra(AppConstants.URL, AppConstants.TermsConditionsURL);
        startActivity(i);

//        Intent i = new Intent(Intent.ACTION_VIEW,
//                Uri.parse(AppConstants.TermsConditionsURL));
//        startActivity(i);
    }


    private class LanguageAdapter extends ArrayAdapter<String> {

        String[] items = new String[2];

        public LanguageAdapter(@NonNull Context context, int resource, String[] items) {
            super(context, resource, items);
            this.items = items;
            /*items[0] = AppUtils.cleanLangStr(context, commonData.getLg7_english(), R.string.txt_english);
            items[1] = AppUtils.cleanLangStr(context, commonData.getLg7_malay(), R.string.txt_malay);*/
        }

        ViewHolder holder;

        class ViewHolder {
            ImageView icon;
            TextView title;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_language, null);
                holder = new ViewHolder();
                holder.icon = (ImageView) convertView.findViewById(R.id.iv_lang_icon);
                holder.title = (TextView) convertView.findViewById(R.id.tv_lang_txt);
                convertView.setTag(holder);
            } else {
                // view already defined, retrieve view holder
                holder = (ViewHolder) convertView.getTag();
            }

            holder.title.setText(items[position]);
            if (position == 0)
                holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_lang_english));
            else if (position == 1)
                holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_lang_malay));
            else {
                holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_arabic_48));
            }

            return convertView;
        }
    }


    public void setLocale(String localeName) {
//        myLocale = new Locale(localeName);
        PreferenceStorage.setKey("locale", localeName);
//        SessionHandler.getInstance().save(Splashscreen.this, "locale", localeName);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = new Configuration();
//        Locale.setDefault(myLocale);
//        conf.locale = myLocale;
//        conf.setLayoutDirection(new Locale(localeName));
//        getBaseContext().getResources().updateConfiguration(conf, null);
//        onConfigurationChanged(conf);
        PreferenceStorage.setKey("localechanged", "true");
//        SessionHandler.getInstance().save(Splashscreen.this, "localechanged", "true");
        AppConstants.localeName = localeName;
        LocaleUtils.setLocale(new Locale(localeName));
        LocaleUtils.updateConfigActivity(this, getBaseContext().getResources().getConfiguration());
        Intent callMainAct = new Intent(this, MainActivity.class);
        callMainAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callMainAct);
        finish();
//        aviLoadingView.setVisibility(View.VISIBLE);
//        loadData();
    }

}
