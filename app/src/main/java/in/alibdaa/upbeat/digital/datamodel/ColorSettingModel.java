package in.alibdaa.upbeat.digital.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColorSettingModel extends BaseResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("morning")
        @Expose
        private String morning;
        @SerializedName("afternoon")
        @Expose
        private String afternoon;
        @SerializedName("evening")
        @Expose
        private String evening;
        @SerializedName("night")
        @Expose
        private String night;

        public String getMorning() {
            return morning;
        }

        public void setMorning(String morning) {
            this.morning = morning;
        }

        public String getAfternoon() {
            return afternoon;
        }

        public void setAfternoon(String afternoon) {
            this.afternoon = afternoon;
        }

        public String getEvening() {
            return evening;
        }

        public void setEvening(String evening) {
            this.evening = evening;
        }

        public String getNight() {
            return night;
        }

        public void setNight(String night) {
            this.night = night;
        }
    }
}
