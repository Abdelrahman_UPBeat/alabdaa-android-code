package in.alibdaa.upbeat.digital.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.alibdaa.upbeat.digital.MainActivity;
import in.alibdaa.upbeat.digital.MyProviderListActivity;
import in.alibdaa.upbeat.digital.ProvideActivity;
import in.alibdaa.upbeat.digital.R;
import in.alibdaa.upbeat.digital.SubCategoryListActivity;
import in.alibdaa.upbeat.digital.datamodel.CategoryList;
import in.alibdaa.upbeat.digital.datamodel.LanguageModel;
import in.alibdaa.upbeat.digital.datamodel.ProviderListData;
import in.alibdaa.upbeat.digital.interfaces.OnLoadMoreListener;
import in.alibdaa.upbeat.digital.utils.AppConstants;
import in.alibdaa.upbeat.digital.viewwidgets.ViewBinderHelper;

public class CategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mActivity;
    Context mContext;
    public ArrayList<ProviderListData.ProviderList> itemsData = new ArrayList<>();
    int viewType;
    LanguageModel.Request_and_provider_list langReqProvData = new LanguageModel().new Request_and_provider_list();
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    public OnLoadMoreListener mOnLoadMoreListener;
    List<CategoryList.Category_list> category_list;


    private int SELF = 1, LOADING = 2;

    public CategoryListAdapter(Context mContext, List<CategoryList.Category_list> category_list) {
        this.mContext = mContext;
        this.category_list = category_list;
    }

    public CategoryListAdapter(Activity mActivity, Context mContext, ArrayList<ProviderListData.ProviderList> itemsData, int viewType) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.itemsData = itemsData;
        this.viewType = viewType;
        if (mActivity instanceof MainActivity) {
            langReqProvData = ((MainActivity) mActivity).langReqProvData;
        } else if (mActivity instanceof MyProviderListActivity) {
            langReqProvData = ((MyProviderListActivity) mActivity).requestAndProviderList;
        }
        // uncomment if you want to open only one row at a time
        binderHelper.setOpenOnlyOne(true);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
       /* // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_provider, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;*/
        View itemView;
        itemView = LayoutInflater.from(mContext).inflate(R.layout.adapter_categories_list, parent, false);
        return new CategoryViewHolder(itemView);


//        if (viewType == LOADING) {
//            itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
//            return new LoadingViewHolder(itemView);
//        } else {
//            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_provider, parent, false);
//            return new CategoryListAdapter.CategoryViewHolder(itemView);
//        }

    }

//    @Override
//    public int getItemViewType(int position) {
//        if (itemsData.get(position) == null) {
//            return LOADING;
//        } else {
//            return SELF;
//        }
//    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        CategoryViewHolder categoryViewHolder = (CategoryViewHolder) viewHolder;


        categoryViewHolder.tvCatName.setText(category_list.get(position).getCategory_name());

        Picasso.with(mContext)
                .load(AppConstants.BASE_URL + category_list.get(position).getCategory_image())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(categoryViewHolder.ivCatImg);


        categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (category_list.get(position).getIs_subcategory().equalsIgnoreCase("1")) {
                    Intent callSubCategoryList = new Intent(mContext, SubCategoryListActivity.class);
                    callSubCategoryList.putExtra(AppConstants.CatID, category_list.get(position).getCatrgory_id());
                    callSubCategoryList.putExtra(AppConstants.SUBCATNAME, category_list.get(position).getCategory_name());
                    mContext.startActivity(callSubCategoryList);
                } else {
                    Intent callSubCategoryList = new Intent(mContext, ProvideActivity.class);
                    callSubCategoryList.putExtra(AppConstants.CATNAME, category_list.get(position).getCategory_name());
                    callSubCategoryList.putExtra(AppConstants.CatID, category_list.get(position).getCatrgory_id());
                    callSubCategoryList.putExtra(AppConstants.SubCatID, "");
                    mContext.startActivity(callSubCategoryList);
                }


            }
        });

    }

    public void updateRecyclerView(Context mContext, ArrayList<ProviderListData.ProviderList> itemsData) {
        this.mContext = mContext;
        this.itemsData.addAll(itemsData);
        notifyDataSetChanged();
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return category_list.size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_cat_img)
        ImageView ivCatImg;
        @BindView(R.id.tv_cat_name)
        TextView tvCatName;

        public CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }
    }
}
